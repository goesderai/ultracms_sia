ultracms.siaMainAccountEditor = {

	_initComp: function(){
		$('#btnSimpan').click(function(){
			var validate = ultracms.siaMainAccountEditor._validateForm();
			if(validate.isValid){
			    siaJS_confirm_form_submission('mainAccountForm');
			}else{
				siaJS_alert(validate.errMsgs);
			}
		});

		$('#parent').select2();
	},

	_validateForm: function(){
		var retval = {isValid: true, errMsgs: ""};
		/* Check empty fields */
		var emptyFields = new Array();
		if(ultracms.utils.isEmpty($('#txtKodeAkun').val())){
			emptyFields.push(ultracms.utils.getLang('sia_main_account.code'));
		}
		if(ultracms.utils.isEmpty($('#txtName').val())){
			emptyFields.push(ultracms.utils.getLang('sia_main_account.name'));
		}
		if(emptyFields.length > 0){
			var errHeading = ultracms.language.alerts.empty_fields;
			retval.isValid = false;
			retval.errMsgs = ultracms.utils.createErrorMessage(errHeading, emptyFields, '<br>');
			return retval;
		}
		return retval;
	},

	init: function(){
		this._initComp();
	}

};
