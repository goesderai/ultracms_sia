/*
 * English language dictionary
 */
ultracms.language = {
  questions: {
    confirm_save: 'Are you sure you want to save this data?',
    confirm_close: 'Are you sure you want to close this page?',
    confirm_delete: 'Are you sure you want to delete this data?',
    confirm_action: 'Are you sure you want to perform that action?'
  },
  alerts: {
    empty_fields: 'Cannot save current data, these fields are empty!',
    mandatory_fields: 'These fields cannot be empty!',
    invalid_fields: 'Cannot save current data, these fields contains invalid value!'
  },
  sia_change_pass: {
    password: 'Password',
    new_password: 'New Password',
    confirm_password: 'Confirm New Password',
    wrong_password_confirmation: 'Wrong password confirmation!'
  },
  sia_users: {
    employee: 'Employee',
    username: 'Username',
    password: 'Password',
    confirm_password: 'Confirm Password',
    roles: 'Role(s)'
  },
  sia_students: {
    nis: 'NIS',
    name: 'Name',
    birth_place: 'Birth Place',
    birth_date: 'Birth Date',
    address: 'Address',
    grade: 'Grade',
    educational_level: 'Educational Level',
    father_name: "Father's Name",
    mother_name: "Mother's Name",
    accepted_date: 'Accepted Date',
    weight: 'Weight (kg)',
    height: 'Height (cm)'
  },
  sia_employees: {
    name: 'Name',
    birth_place: 'Birth Place',
    birth_date: 'Birth Date',
    address: 'Address',
    phone: 'Phone Number',
    id_number: 'ID Number',
    nett_salary: 'Nett. Salary',
    benefit: 'Benefit',
    others: 'Others'
  },
  sia_main_account: {
    code: 'Account Code',
    name: 'Account Name'
  },
  sia_posting_profile: {
    trx_type: 'Transaction Type',
    account_debit: 'Debit Account',
    account_credit: 'Credit Account'
  },
  sia_student_grade_mutation: {
    process_date: 'Process Date',
    grade: 'Grade',
    dest_grade: 'Destination Grade',
    level: 'Level',
    dest_level: 'Destination Level',
    nis: 'NIS',
    name: 'Name',
    student_list: 'Student List'
  },
  sia_edu_grade: {
    edit_level_prompt: 'Edit level name to:',
    new_level_prompt: 'New level name:',
    grade_name: 'Grade Name'
  },
  sia_trx_pembayaran: {
    trx: 'Transaction',
    trx_type: 'Type Transaction',
    trx_sub_type: 'Transaction Sub Type',
    trx_amount: 'Transaction Amount',
    trx_date: 'Transaction Date',
    remark: 'Remark'
  }
};
