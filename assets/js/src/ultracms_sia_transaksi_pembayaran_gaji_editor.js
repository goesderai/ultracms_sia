ultracms.siaTransaksiPembayaranGajiEditor = {
	initComp: function(){
	var datePickerOptsFrom = {
			locale : 'id',
			format : 'DD-MMM-YYYY',
			ignoreReadonly : true
		};
		if($("#txtState").val() == 'ADD'){
			$("#txtGapok").val(0);
			$("#txtHarian").val(0);
			$("#txtTambahan").val(0);
			$("#txtTotalGaji").val(0);
			$("#txtJmlKasbon").val(0);
			$("#txtTotalBayarKasbon").val(0);
			$("#txtTotalGaji").val(0);
			$("#txtSisaGaji").val(0);
		}
		
		$('#trxDatePicker').datetimepicker(datePickerOptsFrom);
		$('#trxDatePicker').on('dp.change', function(e) {
			$('#trx_date').val(e.date.format('YYYY-MM-DD').toString());
		});
		$('#cmbEmployee').change(
				function() {
					
					var url = ultracms.config.baseUrl + "/index.php/sia_trx_gaji/get_employee_data/"+$('#cmbEmployee').val();
					$.get(url, function(resp){
					  $('#txtJabatan').val(resp.jabatan);
					  $('#txtGapok').val(resp.gaji);
					  $('#txtJmlKasbon').val(resp.kasbon);
					  $('#txtSisaGaji').val(resp.gaji);
					  if(resp.kasbon > 0){
						  $('#txtTotalBayarKasbon').removeAttr("readonly");
					  }else{
						  $('#txtTotalBayarKasbon').attr("readonly", "readonly");
					  }
					});
				}
		);
		$('#txtHarian').blur(
				function(){
					ultracms.siaTransaksiPembayaranGajiEditor.recalculate();
				}
			);
		$('#txtTambahan').blur(
				function(){
					ultracms.siaTransaksiPembayaranGajiEditor.recalculate();
				}
			);
		$('#txtTotalBayarKasbon').blur(
				function(){
					ultracms.siaTransaksiPembayaranGajiEditor.compareKasbon();
				}
			);
		if($('#txtJmlKasbon').val() == 0){
			$('#txtTotalBayarKasbon').attr("readonly", "readonly");
		}
		/* Form */
		$('#pembayaranGajiForm').submit(function(){
			var validate = ultracms.siaTransaksiPembayaranGajiEditor.validateForm();
			if(validate.isValid){
				if($('#txtSisaGaji').val() < 0 ){
					alert("Gaji tidak cukup untuk membayar Kasbon!");
				}else{
					return confirm(ultracms.language.questions.confirm_save);
				}
				
			}else{
				siaJS_alert(validate.errMsgs);
				return false;
			}
		});
	},
	validateForm: function(){
		var retval = {isValid: true, errMsgs: ""};
		var emptyFields = new Array();
		if($('#cmbEmployee').val() == 0 ){
			emptyFields.push('Pilih salah satu pegawai!');			
		}
		if($('#trx_date').val() == '' ){
			emptyFields.push('Tanggal Transaksi Tidak boleh kosong!');			
		}
		if($('#cmbTipeTransaksi').val() == 0){
			emptyFields.push('Tipe Transaksi Harus diisi!');	
		}
		if(emptyFields.length > 0){
			var errHeading = ultracms.language.alerts.empty_fields;
			retval.isValid = false;
			retval.errMsgs = ultracms.utils.createErrorMessage(errHeading, emptyFields);
			return retval;
		}
		return retval;
	},
	init: function(){
		this.initComp();
	},
	recalculate: function(){
		var gapok = parseFloat($("#txtGapok").val());
		if (isNaN (gapok))
			gapok=0.0;
		
		var harian = parseFloat($("#txtHarian").val());
		if (isNaN (harian))
			harian=0.0;
		
		var tambahan = parseFloat($("#txtTambahan").val());
		if (isNaN (tambahan))
			tambahan=0.0;
		
		var gatot = gapok + harian + tambahan;
		
		var kasbon = parseFloat($("#txtJmlKasbon").val());
		if (isNaN (kasbon))
			kasbon=0.0;
		
		var byrKasbon = parseFloat($("#txtTotalBayarKasbon").val());
		if (isNaN (byrKasbon))
			byrKasbon=0.0;
		
		var sisaGaji =gatot - byrKasbon;
		
		$("#txtTotalGaji").val(gatot);
		$("#txtSisaGaji").val(sisaGaji);
	},
	compareKasbon: function(){
		var kasbon = parseFloat($("#txtJmlKasbon").val());
		if (isNaN (kasbon))
			kasbon=0.0;
		
		var byrKasbon = parseFloat($("#txtTotalBayarKasbon").val());
		if (isNaN (byrKasbon))
			byrKasbon=0.0;
		if(byrKasbon > kasbon){
			alert("Bayar kasbon lebih besar dari kasbon");
			$("#txtTotalBayarKasbon").val(0);
		}else{
			ultracms.siaTransaksiPembayaranGajiEditor.recalculate();
		}
		
	}
	
};