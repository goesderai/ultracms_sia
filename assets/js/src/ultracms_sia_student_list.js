ultracms.siaStudentList = {
	
	_setCmbGradeOnChangeListener: function(){
		$('#cmbGrade').change(function(){
			var selectedGrade = $(this).val();
			var url = ultracms.config.baseUrl + '/index.php/sia_ajax/classroom_select_options/' + selectedGrade;
			$('#cmbClassroom').html('<option>Loading...</option>').load(url);
		});
	},
	
	_setBtnResetOnClickListener: function(){
		$('#btnReset').click(function(){
			var clsrmData = JSON.parse($('#cmbClassroomInitialValue').val());
			$('#txtNis').val('');
			$('#txtNama').val('');
			$('#txtParentName').val('');
			$('#cmbGrade').val('0');
			$('#cmbStatus').val('0');
			$('#cmbClassroom').html(`<option value="${clsrmData.value}" disabled="disabled" selected="selected">${clsrmData.label}</option>`);
		});
	},
	
	_initComp: function(){
		this._setCmbGradeOnChangeListener();
		this._setBtnResetOnClickListener();
	},
	
	init: function(){
		this._initComp();
	}
	
};
