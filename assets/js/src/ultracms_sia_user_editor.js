ultracms.siaUserEditor = {
	
	initComp: function(){
		/* select2 */
		$('#cmbEmployee').select2();
		
		/* Form */
		$('#btnSimpan').click(function(){
			var validate = ultracms.siaUserEditor.validateForm();
			if(validate.isValid){
			    siaJS_confirm_form_submission('userForm');
			}else{
				siaJS_alert(validate.errMsgs);
			}
		});
	},
	
	validateForm: function(){
		var editorState = $('#editorState').val();
		var retval = {isValid: true, errMsgs: ""};
		var passNotEmpty = true;
		var confirmPassNotEmpty = true;
		
		/* Check empty fields */
		var emptyFields = new Array();
		if(ultracms.utils.isEmpty($('#cmbEmployee').val())){
			emptyFields.push(ultracms.utils.getLang('sia_users.employee'));
		}
		if(ultracms.utils.isEmpty($('#txtUsername').val())){
			emptyFields.push(ultracms.utils.getLang('sia_users.username'));
		}
		if(editorState == 'insert' && ultracms.utils.isEmpty($('#txtPassword').val())){
			emptyFields.push(ultracms.utils.getLang('sia_users.password'));
			passNotEmpty = false;
		}
		if(editorState == 'insert' && ultracms.utils.isEmpty($('#txtConfirmPassword').val())){
			emptyFields.push(ultracms.utils.getLang('sia_users.confirm_password'));
			confirmPassNotEmpty = false;
		}
		if(ultracms.utils.isEmpty($('#selectRole').val())){
			emptyFields.push(ultracms.utils.getLang('sia_users.roles'));
		}
		if(emptyFields.length > 0){
			var errHeading = ultracms.language.alerts.empty_fields;
			retval.isValid = false;
			retval.errMsgs = ultracms.utils.createErrorMessage(errHeading, emptyFields, '<br>');
			return retval;
		}
		
		/* Check invalid fields */
		var invalidFields = new Array();
		if(passNotEmpty && confirmPassNotEmpty && ($('#txtPassword').val() != $('#txtConfirmPassword').val())){
			invalidFields.push(ultracms.utils.getLang('sia_users.confirm_password'));
		}
		if(invalidFields.length > 0){
			var errHeading = ultracms.language.alerts.invalid_fields;
			retval.isValid = false;
			retval.errMsgs = ultracms.utils.createErrorMessage(errHeading, invalidFields, '<br>');
			return retval;
		}
		
		return retval;
	},
	
	init: function(){
		this.initComp();
	}
	
};