ultracms.siaStudentGradeMutationList = {
	initComp: function(){
		/* Date picker */
		var datePickerDateFormat = ultracms.config.datePickerDateFormat;
		var datePickerValueFormat = ultracms.config.datePickerValueFormat;
		var datePickerOpts = {locale: 'id', format: datePickerDateFormat, ignoreReadonly: true};
		$('#mutation_date_picker').datetimepicker(datePickerOpts).on('dp.change', function(e){
			$('#mutation_date').val(e.date.format(datePickerValueFormat).toString());
		});
		
		/* Grade combo */
		$('#cmbGradeId').change(function(){
			var selectedGrade = $(this).val();
			var url = ultracms.config.baseUrl + '/index.php/sia_ajax/classroom_select_options/' + selectedGrade;
			$('#cmbDestClassroomId').html('<option>Loading...</option>').load(url);
		});
		
		/* Reset button */
		$('#btnReset').click(function(){
			$('#mutationDate').val('');
			$('#mutation_date').val('');
			$('#cmbGradeId').val(0);
			$('#cmbDestClassroomId').html('<option value="0">-- Pilih jenjang terlebih dahulu --</option>');
		});
	},
	
	init: function(){
		this.initComp();
	}
};