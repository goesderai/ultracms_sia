ultracms.siaRptKasTahunan = {
  _initComp: function () {
    $("#reportForm").submit(function () {
      const validate = ultracms.siaRptKasTahunan._validateForm();
      if (validate.isValid) {
        siaJS_rpt_disable_submit_button()
        return true;
      } else {
        siaJS_alert(validate.errMsgs);
        return false;
      }
    });
  },

  _validateForm: function () {
    let retval = {isValid: true, errMsgs: ""};

    /* Check empty fields */
    let emptyFields = new Array();
    if (ultracms.utils.isEmpty($('#txtFinancialYear').val())) {
      emptyFields.push('Tahun');
    }
    if (emptyFields.length > 0) {
      retval.isValid = false;
      retval.errMsgs = ultracms.utils.createErrorMessage(ultracms.language.alerts.mandatory_fields, emptyFields);
    }

    return retval;
  },

  init: function () {
    this._initComp();
  }

};
