ultracms.SingleResultStudentLookupDialog = {
	_serviceUrl: (ultracms.config.baseUrl + '/index.php/sia_ajax'),
	_chooseButtonCallback: null,
	_additionalParamsGetter: null,
	
	_setEventListeners: function(){
		$('#singleResultStudentLookup_btnSearch').click(function(){
			ultracms.SingleResultStudentLookupDialog._loadSearchResult(1);
		});
		
		$('#singleResultStudentLookup_btnReset').click(function(){
			ultracms.SingleResultStudentLookupDialog._resetSearchParams();
		});
		
		$('#singleResultStudentLookup_btnPilih').click(function(){
			if(ultracms.SingleResultStudentLookupDialog._chooseButtonCallback != null){
				var selectedItem = ultracms.SingleResultStudentLookupDialog._getSelectedItem();
				if(!ultracms.utils.isEmpty(selectedItem)){
					ultracms.SingleResultStudentLookupDialog._chooseButtonCallback(selectedItem);
					$('#singleResultStudentLookupDialog').modal('hide');
				}else{
					alert('PERHATIAN!\n' +
						'Tidak ada data yang terpilih!');
				}
			}
		});
	},
	
	_getSelectedItem: function(){
		var checkedRadio = $('#singleResultStudentLookup_resultContainer input:checked');
		if(checkedRadio.length > 0){
			var item = JSON.parse($(checkedRadio[0]).parent().parent().attr('data-value'));
			return item;
		}
		return null;
	},
	
	_createParams: function(page){
		if(ultracms.utils.isEmpty(page)){
			page = 1;
		}
		var params = new Array();
		params.push({key: "p", value: page});
		params.push({key: "pz", value: 5});
		params.push({key: "nis", value: $('#singleResultStudentLookup_txtNis').val()});
		params.push({key: "nama", value: $('#singleResultStudentLookup_txtNama').val()});
		if(this._additionalParamsGetter != null){
			var extraParams = this._additionalParamsGetter();
			for(i = 0; i < extraParams.length; i++){
				var p = extraParams[i];
				params.push({key: p.key, value: p.value});
			}
		}
		
		return params;
	},
	
	_loadSearchResult: function(page){
		if(ultracms.utils.isEmpty(page)){
			page = 1;
		}
		var params = ultracms.SingleResultStudentLookupDialog._createParams(page);
		var requestUrl = ultracms.utils.buildUrl(this._serviceUrl + '/student_lookup_dlg_list/1', params);
		$('#singleResultStudentLookup_resultContainer').load(requestUrl, function(resp, status, xhr){
			if(status == 'success'){
				var jsonResp = JSON.parse(resp);
				$('#singleResultStudentLookup_txtPage').val(jsonResp.page);
				$('#singleResultStudentLookup_txtPages').val(jsonResp.pages);
				$(this).html(jsonResp.html);
				ultracms.SingleResultStudentLookupDialog._configurePagination();
			}else{
				$(this).html('Request failed, there was an error: ' + xhr.status + " " + xhr.statusText);
			}
		});
	},
	
	_configurePagination: function(){
		var currPage = parseInt($('#singleResultStudentLookup_txtPage').val());
		var pages = parseInt($('#singleResultStudentLookup_txtPages').val());
		if(ultracms.utils.isEmpty(pages)){
			$('#singleResultStudentLookup_paginationContainer').hide();
		}else{
			if(currPage > 1){
				$('#singleResultStudentLookup_aNavFirst').attr('onclick', 'singleResultStudentLookup_gotoPage(1)');
				$('#singleResultStudentLookup_aNavPrev').attr('onclick', 'singleResultStudentLookup_gotoPage(' + (currPage - 1) + ')');
				$('#singleResultStudentLookup_liNavFirst').removeClass('disabled');
				$('#singleResultStudentLookup_liNavPrev').removeClass('disabled');
			}else{
				$('#singleResultStudentLookup_aNavFirst').removeAttr('onclick');
				$('#singleResultStudentLookup_aNavPrev').removeAttr('onclick');
				$('#singleResultStudentLookup_liNavFirst').addClass('disabled');
				$('#singleResultStudentLookup_liNavPrev').addClass('disabled');
			}
			if(currPage < pages){
				$('#singleResultStudentLookup_aNavLast').attr('onclick', 'singleResultStudentLookup_gotoPage(' + pages + ')');
				$('#singleResultStudentLookup_aNavNext').attr('onclick', 'singleResultStudentLookup_gotoPage(' + (currPage + 1) + ')');
				$('#singleResultStudentLookup_liNavLast').removeClass('disabled');
				$('#singleResultStudentLookup_liNavNext').removeClass('disabled');
			}else{
				$('#singleResultStudentLookup_aNavLast').removeAttr('onclick');
				$('#singleResultStudentLookup_aNavNext').removeAttr('onclick');
				$('#singleResultStudentLookup_liNavLast').addClass('disabled');
				$('#singleResultStudentLookup_liNavNext').addClass('disabled');
			}
			$('#singleResultStudentLookup_navPageInfo').html('Page ' + currPage + ' of ' + pages);
			$('#singleResultStudentLookup_paginationContainer').show();
		}
	},
	
	_resetSearchResult: function(){
		$('#singleResultStudentLookup_resultContainer').html('');
		$('#singleResultStudentLookup_paginationContainer').hide();
	},
	
	_resetSearchParams: function(){
		$('#singleResultStudentLookup_txtNis').val('');
		$('#singleResultStudentLookup_txtNama').val('');
		$('#singleResultStudentLookup_txtPage').val('');
		$('#singleResultStudentLookup_txtPages').val('');
	},
	
	gotoPage: function(page){
		this._loadSearchResult(page);
	},
	
	resetDialog: function(){
		this._resetSearchParams();
		this._resetSearchResult();
	},
	
	init: function(additionalParamsGetter, chooseButtonCallback){
		if(typeof additionalParamsGetter === 'function'){
			this._additionalParamsGetter = additionalParamsGetter;
		}
		if(typeof chooseButtonCallback === 'function'){
			this._chooseButtonCallback = chooseButtonCallback;
		}
		this._resetSearchParams();
		this._resetSearchResult();
		this._setEventListeners();
	}
};

/*
 * Helper functions
 * prefix: singleResultStudentLookup_
 */
function singleResultStudentLookup_gotoPage(page){
	ultracms.SingleResultStudentLookupDialog.gotoPage(page);
}