ultracms.siaTransaksiPembayaranEditor = {
  initComp: function () {
    try {
      //date pickers
      const datePickerOptsFrom = {
        locale:         'id',
        format:         ultracms.config.datePickerDateFormat,
        ignoreReadonly: true
      };
      $('#trxDatePicker').datetimepicker(datePickerOptsFrom);
      $('#trxDatePicker').on('dp.change', function (e) {
        $('#trx_date').val(e.date.format(ultracms.config.datePickerValueFormat).toString());
      });

      //inputs
      $('#txtAmount').number(true, 2);
      $('#cmbTipeTransaksi').change(function () {
        const url = siaJS_site_url('/sia_transaksi_pembayaran/get_sub_trx_type/' + $(this).val() + '/' + 0);
        $('#cmbJenSubType').empty();
        $('#cmbJenisTransaksi').empty().load(url);
      });
      $('#cmbJenisTransaksi').change(function () {
        const url = siaJS_site_url('/sia_transaksi_pembayaran/get_sub_trx_type/' + $(this).val() + '/' + 1);
        $('#cmbJenSubType').empty().load(url);
      });

      //buttons
      $('#btnSimpan').click(function () {
        const validate = ultracms.siaTransaksiPembayaranEditor.validateForm();
        if (validate.isValid) {
          siaJS_confirm_form_submission('pembayaranForm');
        } else {
          siaJS_alert(validate.errMsgs);
        }
      });
    } catch (e) {
      console.error(e);
    }
  },

  validateForm: function () {
    let retval = {isValid: true, errMsgs: ""};
    const editorState = $('#editorState').val();

    /* Check empty fields */
    let emptyFields = new Array();
    if (editorState == 'insert') {
      if (ultracms.utils.isEmpty($('#cmbTipeTransaksi').val())) {
        emptyFields.push(ultracms.utils.getLang('sia_trx_pembayaran.trx'));
      }
      if (ultracms.utils.isEmpty($('#cmbJenisTransaksi').val())) {
        emptyFields.push(ultracms.utils.getLang('sia_trx_pembayaran.trx_type'));
      }
      if (ultracms.utils.isEmpty($('#cmbJenSubType').val())) {
        emptyFields.push(ultracms.utils.getLang('sia_trx_pembayaran.trx_sub_type'));
      }
      if (ultracms.utils.isEmpty($('#txtAmount').val())) {
        emptyFields.push(ultracms.utils.getLang('sia_trx_pembayaran.trx_amount'));
      }
      if (ultracms.utils.isEmpty($('#trx_date').val())) {
        emptyFields.push(ultracms.utils.getLang('sia_trx_pembayaran.trx_date'));
      }
      if (ultracms.utils.isEmpty($('#txtRemark').val())) {
        emptyFields.push(ultracms.utils.getLang('sia_trx_pembayaran.remark'));
      }
    } else if (editorState == 'update' || editorState == 'koreksi') {
      if (ultracms.utils.isEmpty($('#txtRemark').val())) {
        emptyFields.push(ultracms.utils.getLang('sia_trx_pembayaran.remark'));
      }
    }
    if (emptyFields.length > 0) {
      retval.isValid = false;
      retval.errMsgs = ultracms.utils.createErrorMessage(ultracms.utils.getLang('alerts.empty_fields'), emptyFields, '<br>');
    }

    return retval;
  },

  init: function () {
    this.initComp();
  }

};
