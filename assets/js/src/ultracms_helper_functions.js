/* 
 * helper functions
 * prefix: siaJS_
 */
function siaJS_confirm(msgKey, resumeUrl) {
  const focusOnButton = (arguments[2] !== undefined) ? arguments[2] : 0;
  ultracms.dialogs.showConfirmationDialog(msgKey,
    function () {
      ultracms.dialogs.destroyDialog(ultracms.dialogs.selectors.yesNoDialog.dialog);
      window.location = resumeUrl;
    },
    function () {
      ultracms.dialogs.destroyDialog(ultracms.dialogs.selectors.yesNoDialog.dialog);
    },
    focusOnButton
  );
}

function siaJS_confirm_form_submission(formId) {
  const focusOnButton = (arguments[2] !== undefined) ? arguments[2] : 0;
  ultracms.dialogs.showFormSubmissionDialog(
    function () {
      $(`#${formId}`).submit();
    },
    focusOnButton
  );
}

function siaJS_alert(message) {
  ultracms.dialogs.showAlertDialog(message, function () {
    ultracms.dialogs.destroyDialog(ultracms.dialogs.selectors.alertDialog.dialog);
  });
}

function siaJS_site_url(path) {
  if (ultracms.utils.isEmpty(path)) {
    return ultracms.config.baseUrl;
  } else {
    return ultracms.config.baseUrl + '/index.php' + path.trim();
  }
}

function siaJS_rpt_disable_submit_button(form) {
  try {
    $(form).find('button[type="submit"]')
      .attr('disabled', true)
      .html('Refresh page to generate another report.');
  } catch (e) {
    console.error(e);
  }
}
