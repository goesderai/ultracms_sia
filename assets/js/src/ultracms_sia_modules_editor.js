ultracms.siaModulesEditor = {
	_initComp : function() {
		$('#cmbParent').select2();
		$('#btnSimpan').click(function() {
			var validate = ultracms.siaModulesEditor._validateForm();
			if (validate.isValid) {
			    siaJS_confirm_form_submission('modulesForm');
			} else {
				siaJS_alert(validate.errMsgs);
			}
		});
	},

	_validateForm : function() {
		var retval = {isValid : true, errMsgs : ""};
		
		/* Check empty fields */
		var caption = $('#txtCaption').val();
		var icon = $('#txtIcon').val();
		var treeLevel = $('#cmbTreeLevel').val();
		var emptyFields = new Array();
		if(ultracms.utils.isEmpty($('#txtCaption').val())){
			emptyFields.push('Caption');
		}
		if(ultracms.utils.isEmpty($('#txtIcon').val())){
			emptyFields.push('Icon Class');
		}
		if(!(treeLevel === '0' || treeLevel === '1')){
			emptyFields.push('Tree Level');
		}
		
		if (emptyFields.length > 0) {
			var errHeading = ultracms.language.alerts.empty_fields;
			retval.isValid = false;
			retval.errMsgs = ultracms.utils.createErrorMessage(errHeading, emptyFields, '<br>');
			return retval;
		}
		return retval;
	},

	init : function() {
		this._initComp();
	}

};