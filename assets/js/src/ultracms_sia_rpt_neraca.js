ultracms.siaRptNeraca = {
	_initComp: function(){
		$("#reportForm").submit(function(){
			const validate = ultracms.siaRptNeraca._validateForm();
			if(validate.isValid){
			  siaJS_rpt_disable_submit_button(this);
				return true;
			}else{
				siaJS_alert(validate.errMsgs);
				return false;
			}
		});
		const datePickerOpts = {
				locale : 'id',
				format : ultracms.config.datePickerDateFormat,
				ignoreReadonly : true
			};
		$('#trxDatePickerFrom').datetimepicker(datePickerOpts);
		$('#trxDatePickerFrom').on('dp.change', function(e) {
			$('#dateFrom').val(e.date.format(ultracms.config.datePickerValueFormat).toString());
		});
		$('#trxDatePickerTo').datetimepicker(datePickerOpts);
		$('#trxDatePickerTo').on('dp.change', function(e) {
			$('#dateTo').val(e.date.format(ultracms.config.datePickerValueFormat).toString());
		});
	},
	
	_validateForm: function(){
		let retval = {isValid: true, errMsgs: ""};
		
		//validation here

		return retval;
	},
	
	init: function(){
		this._initComp();
	}
	
};
