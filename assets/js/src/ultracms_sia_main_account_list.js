ultracms.siaMainAccountList = {
	
	_initComp: function(){
		$('#account_code, #parent').select2();
		
		$('#btnReset').click(function(){
			$('#account_code, #parent').val('0').trigger('change');
			$('#txtNama').val('');
		});
	},
	
	init: function(){
		this._initComp();
	}
	
};