ultracms.siaTransactionTypeList = {
	
	_initComp: function(){
		$('#cmbTipeTransaksi, #cmbDocType').select2();
		$('#btnReset').click(function(){
			$('#cmbTipeTransaksi, #cmbDocType').val(0).trigger('change');
			$('#cmbStatus').val('0');
			$('#cmbStatusDC').val('0');
			$('#txtNama').val('');
		});
	},
	
	init: function(){
		this._initComp();
	}
	
};