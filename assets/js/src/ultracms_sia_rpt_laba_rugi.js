ultracms.siaRptLabaRugi = {
  _initComp: function () {
    $("#reportForm").submit(function () {
      const validate = ultracms.siaRptLabaRugi._validateForm();
      if (!validate.isValid) {
        siaJS_alert(validate.errMsgs);
        return false;
      } else {
        siaJS_rpt_disable_submit_button(this);
      }
    });
    const datePickerOpts = {
      locale: 'id',
      format: ultracms.config.datePickerDateFormat,
      ignoreReadonly: true
    };
    $('#trxDatePickerFrom').datetimepicker(datePickerOpts);
    $('#trxDatePickerFrom').on('dp.change', function (e) {
      $('#dateFrom').val(e.date.format(ultracms.config.datePickerValueFormat).toString());
    });
    $('#trxDatePickerTo').datetimepicker(datePickerOpts);
    $('#trxDatePickerTo').on('dp.change', function (e) {
      $('#dateTo').val(e.date.format(ultracms.config.datePickerValueFormat).toString());
    });
  },

  _validateForm: function () {
    let retval = {isValid: true, errMsgs: ""};

    // Check empty fields
    let emptyFields = [];
    if (ultracms.utils.isEmpty($('#dateFrom').val())) {
      emptyFields.push('Tanggal Dari');
    }
    if (ultracms.utils.isEmpty($('#dateTo').val())) {
      emptyFields.push('Tanggal Sampai');
    }
    if (emptyFields.length > 0) {
      const errHeading = ultracms.language.alerts.mandatory_fields;
      retval.isValid = false;
      retval.errMsgs = ultracms.utils.createErrorMessage(errHeading, emptyFields, '<br>');

    }
    return retval;
  },

  init: function () {
    this._initComp();
  }

};
