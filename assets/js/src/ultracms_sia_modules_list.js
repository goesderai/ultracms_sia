ultracms.siaModulesList = {
	_initComps: function(){
		$('#cmbParent').select2();
		$('#btnReset').click(function(){
			$('#cmbParent').val(0).trigger('change');
			$('#txtClassName').val('');
			$('#txtCaption').val('');
		});
	},
	
	init: function(){
		this._initComps();
	}
};