ultracms.siaTransaksiPendapatanEditor = {
  initComp: function () {
    //date pickers
    const datePickerOptsFrom = {
      locale: 'id',
      format: ultracms.config.datePickerDateFormat,
      ignoreReadonly: true
    };
    $('#trxDatePicker').datetimepicker(datePickerOptsFrom);
    $('#trxDatePicker').on('dp.change', function (e) {
      $('#trx_date').val(e.date.format(ultracms.config.datePickerValueFormat).toString());
    });


    //buttons
    $('#btnSimpan').click(function () {
      const validate = ultracms.siaTransaksiPendapatanEditor.validateForm();
      if (validate.isValid) {
        siaJS_confirm_form_submission('pendapatanForm');
      } else {
        siaJS_alert(validate.errMsgs);
      }
    });

    //inputs
    $('#cmbTipeTransaksi').change(function () {
      const url = siaJS_site_url('/sia_transaksi_pendapatan/get_sub_trx_type/' + $(this).val() + '/' + 1);
      $('#cmbJenSubType').val(null);
      $('#cmbJenSubType').load(url);
    });
    $('#trx_typ').val('D');
    $('#txtAmount').number(true, 2);
  },

  validateForm: function () {
    let retval = {isValid: true, errMsgs: ""};

    /* Check empty fields */
    let emptyFields = [];
    const editorState = $('#editorState').val();
    if (editorState === 'ADD') {
      if (ultracms.utils.isEmpty($('#cmbTipeTransaksi').val())) {
        emptyFields.push('Tipe Transaksi');
      }
      if (ultracms.utils.isEmpty($('#cmbJenSubType').val())) {
        emptyFields.push('Sub Jenis Transaksi');
      }
    }
    if (emptyFields.length > 0) {
      const errHeading = ultracms.language.alerts.empty_fields;
      retval.isValid = false;
      retval.errMsgs = ultracms.utils.createErrorMessage(errHeading, emptyFields, '<br>');
      return retval;
    }
    return retval;
  },

  init: function () {
    this.initComp();
  }

};
