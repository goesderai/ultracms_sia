ultracms.siaPostingProfileList = {
	
	_initComp: function(){
		$('#account_debet, #account_credit, #cmbTipeTransaksi').select2();
		$('#btnReset').click(function(){
			$('#account_debet, #account_credit, #cmbTipeTransaksi').val(0).trigger('change');
		});
	},
	
	init: function(){
		this._initComp();
	}
	
};