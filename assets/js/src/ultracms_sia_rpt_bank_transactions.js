ultracms.siaRptBankTransactions = {
  _initComp: function () {
    $("#reportForm").submit(function () {
      const validate = ultracms.siaRptBankTransactions._validateForm();
      if (validate.isValid) {
        siaJS_rpt_disable_submit_button(this);
        return true;
      } else {
    	  siaJS_alert(validate.errMsgs);
        return false;
      }
    });
    const datePickerOpts = {
      locale: 'id',
      format: ultracms.config.datePickerDateFormat,
      ignoreReadonly: true
    };
    $('#trxDatePickerFrom').datetimepicker(datePickerOpts);
    $('#trxDatePickerFrom').on('dp.change', function (e) {
      $('#transaction_date_from').val(
        e.date.format(ultracms.config.datePickerValueFormat).toString());
    });
    $('#trxDatePickerTo').datetimepicker(datePickerOpts);
    $('#trxDatePickerTo').on('dp.change', function (e) {
      $('#transaction_date_to').val(
        e.date.format(ultracms.config.datePickerValueFormat).toString());
    });
  },

  _validateForm: function () {
    let retval = {isValid: true, errMsgs: ""};
    //validation here
    return retval;
  },

  init: function () {
    this._initComp();
  }

};
