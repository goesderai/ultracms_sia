ultracms.siaLogin = {
	_initComp: function(){
		/* Form */
		$('#loginForm').submit(function(){
			var validate = ultracms.siaLogin.validateForm();
			if(validate.isValid){
				return confirm(ultracms.language.questions.confirm_save);
			}else{
				alert(validate.errMsgs);
				return false;
			}
		});
	},
	
	validateForm: function(){
		var retval = {isValid: true, errMsgs: ""};
		
		/* Check empty fields */
		var emptyFields = new Array();
		if(ultracms.utils.isEmpty($('#txtUsername').val())){
			emptyFields.push('Username');
		}
		if(ultracms.utils.isEmpty($('#txtPassword').val())){
			emptyFields.push('Password');
		}
		
		if(emptyFields.length > 0){
			var errHeading = ultracms.language.alerts.mandatory_fields;
			retval.isValid = false;
			retval.errMsgs = ultracms.utils.createErrorMessage(errHeading, emptyFields);
			return retval;
		}
	},
	
	init: function(){
		this._initComp();
	}
};