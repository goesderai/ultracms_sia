ultracms.siaSalaryEditor = {
	
	initComp: function(){
		/* Date Pickers */
		/*var datePickerDateFormat = ultracms.config.datePickerDateFormat;
		var datePickerValueFormat = ultracms.config.datePickerValueFormat;
		var datePickerOpts = {locale: 'id', format: datePickerDateFormat, ignoreReadonly: true};
		$('#birth_date_picker').datetimepicker(datePickerOpts);
		$('#birth_date_picker').on('dp.change', function(e){
			$('#birth_date').val(e.date.format(datePickerValueFormat).toString());
		});*/
		
		/* Form */
		$('#salaryForm').submit(function(){
			var validate = ultracms.siaSalaryEditor.validateForm();
			if(validate.isValid){
				return confirm(ultracms.language.questions.confirm_save);
			}else{
				siaJS_alert(validate.errMsgs);
				return false;
			}
		});
	},
	
	validateForm: function(){
		var retval = {isValid: true, errMsgs: ""};
		
		/* Check empty fields */
		var emptyFields = new Array();
		/*if(ultracms.utils.isEmpty($('#txtNama').val())){
			emptyFields.push('Nama');
		}
		if(ultracms.utils.isEmpty($('#txtBirthPlace').val())){
			emptyFields.push('Tempat Lahir');
		}
		if($('#birth_date_picker').data('DateTimePicker').date() == null){
			emptyFields.push('Tanggal Lahir');
		}
		if(ultracms.utils.isEmpty($('#txtIdNumber').val())){
			emptyFields.push('No. Identitas');
		}
		if(ultracms.utils.isEmpty($('#txtAddress').val())){
			emptyFields.push('Alamat');
		}
		if(ultracms.utils.isEmpty($('#txtPhoneNumber').val())){
			emptyFields.push('No. Telephone');
		}*/
		if(emptyFields.length > 0){
			var errHeading = ultracms.language.alerts.empty_fields;
			retval.isValid = false;
			retval.errMsgs = ultracms.utils.createErrorMessage(errHeading, emptyFields);
			return retval;
		}
		
		return retval;
	},
	
	init: function(){
		this.initComp();
	}
	
};