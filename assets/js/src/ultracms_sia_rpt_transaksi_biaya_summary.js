ultracms.siaRptBiayaSum = {
	_initComp : function() {

		$("#reportForm").submit(function() {
			const
			validate = ultracms.siaRptBiayaSum._validateForm();
			if (validate.isValid) {
				siaJS_rpt_disable_submit_button(this);
				return true;
			} else {
				siaJS_alert(validate.errMsgs);
				return false;
			}
		});

		$('#btnReset').click(function() {
			$('#txtTrxDateFrom').val('');
			$('#txtTrxDateTo').val('');
			$('#transaction_date_from').val('');
			$('#transaction_date_to').val('');
			document.getElementById('cmbMonth').disabled = false;
			document.getElementById('txtFinancialYear').disabled = false;
			document.getElementById('txtTrxDateFrom').disabled = false;
			document.getElementById('txtTrxDateTo').disabled = false;
		});

		var datePickerOptsFrom = {
			locale : 'id',
			format : 'DD-MMM-YYYY',
			ignoreReadonly : true
		};
		$('#trxDatePickerFrom').datetimepicker(datePickerOptsFrom);
		$('#trxDatePickerFrom')
				.on(
						'dp.change',
						function(e) {
							$('#transaction_date_from').val(
									e.date.format('YYYY-MM-DD').toString());
							document.getElementById('cmbMonth').disabled = true;
							document.getElementById('txtFinancialYear').disabled = true;

						});

		var datePickerOptsTo = {
			locale : 'id',
			format : 'DD-MMM-YYYY',
			ignoreReadonly : true
		};
		$('#trxDatePickerTo').datetimepicker(datePickerOptsTo);
		$('#trxDatePickerTo')
				.on(
						'dp.change',
						function(e) {
							$('#transaction_date_to').val(
									e.date.format('YYYY-MM-DD').toString());
							document.getElementById('cmbMonth').disabled = true;
							document.getElementById('txtFinancialYear').disabled = true;
						});
		$('#cmbMonth').change(function() {
			document.getElementById('txtTrxDateFrom').disabled = true;
			$('#transaction_date_from').val('');
			document.getElementById('txtTrxDateTo').disabled = true;
			$('#transaction_date_to').val('');
		});

		$('#txtFinancialYear').blur(function() {
			document.getElementById('txtTrxDateFrom').disabled = true;
			$('#transaction_date_from').val('');
			document.getElementById('txtTrxDateTo').disabled = true;
			$('#transaction_date_to').val('');
		});

	},
	_validateForm : function() {
		let
		retval = {
			isValid : true,
			errMsgs : ""
		};

		let
		emptyFields = [];
		if ($('#cmbTipeTransaksi').val() == 0) {
			emptyFields.push('Transaksi');
		}
		if (ultracms.utils.isEmpty($('#txtFinancialYear').val())) {
			if (!ultracms.utils.isEmpty($('#cmbMonth').val())) {
				emptyFields.push('Tahun');
			}

		}

		if (emptyFields.length > 0) {
			const
			errHeading = ultracms.language.alerts.empty_fields;
			retval.isValid = false;
			retval.errMsgs = ultracms.utils.createErrorMessage(errHeading,
					emptyFields, '<br>');
			return retval;
		}

	},

	init : function() {
		this._initComp();
	}

};