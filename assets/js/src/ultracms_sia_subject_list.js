ultracms.siaSubjectList = {
	editRow : function(editUrl, subjectJson) {
		var subject = JSON.parse(subjectJson);
		$('#formEditorSubject').attr('action', editUrl);
		$('#subjectName').val(subject.name);
		$('#subjectEditorForm').modal('show')
			.on('shown.bs.modal', function(){
				$('#subjectName').focus();
			});
	},	
	
	initComp : function() {
		$('#btnTambah').click(function(){
			var insertUrl = ultracms.config.baseUrl+'/index.php/sia_subjects/insert';
			$('#formEditorSubject').attr('action', insertUrl);
			$('#subjectName').val('');
			$('#subjectEditorForm').modal('show')
				.on('shown.bs.modal', function(){
					$('#subjectName').focus();
				});
		});
		
		$('#formEditorSubject').submit(function(){
			if(ultracms.utils.isEmpty($('#subjectName').val())){
				alert('Nama mata pelajaran tidak boleh kosong!');
				return false;
			}else{
				return confirm(ultracms.language.questions.confirm_save);
			}
		});
	},

	init : function() {
		this.initComp();
	}	
};