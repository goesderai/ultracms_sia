ultracms.siaRptKas = {
  _initComp: function () {
    $("#reportForm").submit(function () {
      const validate = ultracms.siaRptKas._validateForm();
      if (validate.isValid) {
        siaJS_rpt_disable_submit_button(this);
        return true;
      } else {
        siaJS_alert(validate.errMsgs);
        return false;
      }
    });
    const datePickerOpts = {
      locale: 'id',
      format: ultracms.config.datePickerDateFormat,
      ignoreReadonly: true
    };
    $('#trxDatePickerFrom').datetimepicker(datePickerOpts);
    $('#trxDatePickerFrom').on('dp.change', function (e) {
      $('#transaction_date').val(e.date.format(ultracms.config.datePickerValueFormat).toString());
    });
  },

  _validateForm: function () {
    let retval = {isValid: true, errMsgs: ""};

    /* Check empty fields */
    let emptyFields = new Array();
    if (ultracms.utils.isEmpty($('#txtTrxDateFrom').val())) {
      emptyFields.push('Tanggal');
    }
    if (emptyFields.length > 0) {
      retval.isValid = false;
      retval.errMsgs = ultracms.utils.createErrorMessage(ultracms.language.alerts.mandatory_fields, emptyFields, '<br>');
      return retval;
    }

    return retval;
  },

  init: function () {
    this._initComp();
  }

};
