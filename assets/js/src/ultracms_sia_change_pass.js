ultracms.siaChangePass = {
	
	initComp: function(){
		$('#btnSimpan').click(function(){
			var validate = ultracms.siaChangePass.validateForm();
			if(validate.isValid){
			    siaJS_confirm_form_submission('changePassForm');
			}else{
				siaJS_alert(validate.errMsgs);
			}
		});
	},
	
	validateForm: function(){
		var retval = {isValid: true, errMsgs: ""};
		
		/* Check empty fields */
		var emptyFields = new Array();
		if(ultracms.utils.isEmpty($('#txtPass').val())){
			emptyFields.push(ultracms.utils.getLang('sia_change_pass.password'));
		}
		if(ultracms.utils.isEmpty($('#txtNewPass').val())){
			emptyFields.push(ultracms.utils.getLang('sia_change_pass.new_password'));
		}
		if(ultracms.utils.isEmpty($('#txtConfirmNewPass').val())){
			emptyFields.push(ultracms.utils.getLang('sia_change_pass.confirm_password'));
		}
		if(emptyFields.length > 0){
			var errHeading = ultracms.language.alerts.empty_fields;
			retval.isValid = false;
			retval.errMsgs = ultracms.utils.createErrorMessage(errHeading, emptyFields, '<br>');
			return retval;
		}
		
		/* Check confirmed password */
		var isNewPassEmpty = ultracms.utils.isEmpty($('#txtNewPass').val());
		var isConfirmPassEmpty = ultracms.utils.isEmpty($('#txtConfirmNewPass').val());
		if((!isNewPassEmpty && !isConfirmPassEmpty) && ($('#txtNewPass').val() != $('#txtConfirmNewPass').val())){
			retval.isValid = false;
			retval.errMsgs = ultracms.utils.getLang('sia_change_pass.wrong_password_confirmation');
			return retval;
		}
		
		return retval;
	},
	
	init: function(){
		this.initComp();
	}
	
};