ultracms.siaTransaksiSppEditor = {
  initComp: function () {
    const datePickerOptsFrom = {
      locale: 'id',
      format: 'DD-MMM-YYYY',
      ignoreReadonly: true
    };

    $('#trxDatePicker')
      .datetimepicker(datePickerOptsFrom)
      .on('dp.change', function (e) {
        $('#trx_date').val(e.date.format('YYYY-MM-DD').toString());
      });

    $('#btnPilihBulan').click(function () {
        if (parseInt($('#txtIdSiswa').val()) === 0 || parseInt($('#cmbYear').val()) === 0) {
          siaJS_alert('Pilih siswa dan tahun terlebih dahulu');
        } else {
          let url = ultracms.config.baseUrl + '/index.php/sia_transaksi_spp/get_month/' + $('#txtIdSiswa').val() + '/' + $('#cmbYear').val();
          $('#chkMonth').load(url);
        }
    });

    $('#cmbYear').change(function () {
        $('#chkMonth').empty();
    });

    $('#chkMonth').click(function () {
        ultracms.siaTransaksiSppEditor.getCheckBoxLength();
    });

    $('#txtTotal').number(true, 0);

    $('#txtAmount').number(true, 0).blur(function () {
        ultracms.siaTransaksiSppEditor.getCheckBoxLength();
    });

    $('#txtDiskon').number(true, 0).blur(function () {
        ultracms.siaTransaksiSppEditor.getCheckBoxLength();
    });

    $('#btnSimpan').click(function () {
      var validate = ultracms.siaTransaksiSppEditor.validateForm();
      if (validate.isValid) {
        siaJS_confirm_form_submission('sppForm');
      } else {
        siaJS_alert(validate.errMsgs);
      }
    });

    /*
     * init dialog dan tombol untuk menampilkannya
     */
    ultracms.SingleResultStudentLookupDialog.init(null, function (item) {
      $('#txtSiswa').val(item.nis + ' - ' + item.name);
      $('#txtIdSiswa').val(item.id);
      $('#txtKelas').val(item.classroom_name);
    });
    $('#btnBrowseSiswa').click(function () {
      ultracms.SingleResultStudentLookupDialog.resetDialog();
      $('#singleResultStudentLookupDialog').modal({backdrop: 'static'});
      $('#chkMonth').empty();
    });
  },

  validateForm: function () {
    let retval = {isValid: true, errMsgs: ''};
    /* Check empty fields */
    let emptyFields = [];
    const student = $('#txtIdSiswa').val();
    const year = $('#cmbYear').val();
    const date = $('#txtTrxDate').val();
    const amount = $('#txtAmount').val();
    if (student === 0) {
      emptyFields.push('Nama Siswa Harus Diisi!');
    }
    if (date === '') {
      emptyFields.push('Tanggal Transaksi Harus Diisi!');
    }
    if (year === 0) {
      emptyFields.push('Tahun Pembayaran Harus Diisi!');
    }
    if (amount === '') {
      emptyFields.push('Jumlah Bayar per Bulan Harus Diisi!');
    }
    if (emptyFields.length > 0) {
      var errHeading = ultracms.language.alerts.empty_fields;
      retval.isValid = false;
      retval.errMsgs = ultracms.utils.createErrorMessage(errHeading, emptyFields, '<br>');
      return retval;
    }
    return retval;
  },

  init: function () {
    this.initComp();
  },

  getCheckBoxLength: function () {
    const validate = ultracms.siaTransaksiSppEditor.validateForm();
    if (validate.isValid) {
      const url = ultracms.config.baseUrl + '/index.php/sia_transaksi_spp/get_financial_month_terbayar/' + $('#txtIdSiswa').val() + '/' + $('#cmbYear').val();
      $.get(url, function (resp) {
        let count = 0;
        let form = document.forms.sppForm;
        let bulanArray = form.elements['chkBulanDibayar[]'];
        let financialMonthArray = resp.split(',');
        for (let i = 0; i < bulanArray.length; i++) {
          if (bulanArray[i].checked) {
            let isExistFinancialMonth = false;
            for (let j = 0; j < financialMonthArray.length; j++) {
              if (bulanArray[i].value === financialMonthArray[j]) {
                isExistFinancialMonth = true;
                break;
              }
            }
            if (!isExistFinancialMonth) {
              count++;
            }
          }
        }
        let amount = $('#txtAmount').val();
        let diskon = $('#txtDiskon').val();
        let totalAmount = (amount - diskon) * count;
        $('#txtTotal').val(totalAmount);
      });
    } else {
      siaJS_alert(validate.errMsgs);
    }
  },

  getTotalAmount: function () {
    const amount = $('#txtAmount').val();
    const diskon = $('#txtDiskon').val();
    const month = ultracms.siaTransaksiSppEditor.getCheckBoxLength();
    let totalAmount = (amount - diskon) * month;
    return totalAmount;
  },

  setTotalBayar: function () {
    const totalBayar = ultracms.siaTransaksiSppEditor.getTotalAmount();
    $('#txtTotal').val(totalBayar);
  }

};
