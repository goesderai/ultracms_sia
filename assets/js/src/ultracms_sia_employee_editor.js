ultracms.siaEmployeeEditor = {

	initComp: function(){
		/* Date Pickers */
		var datePickerDateFormat = ultracms.config.datePickerDateFormat;
		var datePickerValueFormat = ultracms.config.datePickerValueFormat;
		var datePickerOpts = {locale: 'id', format: datePickerDateFormat, ignoreReadonly: true};
		$('#birth_date_picker').datetimepicker(datePickerOpts);
		$('#birth_date_picker').on('dp.change', function(e){
			$('#birth_date').val(e.date.format(datePickerValueFormat).toString());
		});

		/* Number boxes */
		ultracms.utils.setAsNumberInputBoxes("txtGapok,txtTunjangan,txtLainnya");

		/* Form */
		$('#btnSimpan').click(function(){
			var validate = ultracms.siaEmployeeEditor.validateForm();
			if(validate.isValid){
				siaJS_confirm_form_submission('employeeForm');
			}else{
				siaJS_alert(validate.errMsgs);
				return false;
			}
		});
	},

	validateForm: function(){
		var isCashier = $("#isCashier").val();
		var retval = {isValid: true, errMsgs: ""};

		/* Check empty fields */
		var emptyFields = new Array();
		if(ultracms.utils.isEmpty($('#txtNama').val())){
			emptyFields.push(ultracms.utils.getLang('sia_employees.name'));
		}
		if(ultracms.utils.isEmpty($('#txtBirthPlace').val())){
			emptyFields.push(ultracms.utils.getLang('sia_employees.birth_place'));
		}
		if($('#birth_date_picker').data('DateTimePicker').date() == null){
			emptyFields.push(ultracms.utils.getLang('sia_employees.birth_date'));
		}
		if(ultracms.utils.isEmpty($('#txtIdNumber').val())){
			emptyFields.push(ultracms.utils.getLang('sia_employees.id_number'));
		}
		if(ultracms.utils.isEmpty($('#txtAddress').val())){
			emptyFields.push(ultracms.utils.getLang('sia_employees.address'));
		}
		if(ultracms.utils.isEmpty($('#txtPhoneNumber').val())){
			emptyFields.push(ultracms.utils.getLang('sia_employees.phone'));
		}
		if(isCashier == 1){
			if(ultracms.utils.isEmpty($('#txtGapok').val())){
				emptyFields.push(ultracms.utils.getLang('sia_employees.nett_salary'));
			}
			if(ultracms.utils.isEmpty($('#txtTunjangan').val())){
				emptyFields.push(ultracms.utils.getLang('sia_employees.benefit'));
			}
			if(ultracms.utils.isEmpty($('#txtLainnya').val())){
				emptyFields.push(ultracms.utils.getLang('sia_employees.others'));
			}
		}
		if(emptyFields.length > 0){
			var errHeading = ultracms.language.alerts.empty_fields;
			retval.isValid = false;
			retval.errMsgs = ultracms.utils.createErrorMessage(errHeading, emptyFields, '<br>');
			return retval;
		}

		return retval;
	},

	init: function(){
		this.initComp();
	}

};
