ultracms.siaStudentEditor = {

  setCmbGradeOnChangeListener: function () {
    $('#cmbGrade').change(function () {
      var selectedGrade = $(this).val();
      var url = ultracms.config.baseUrl + '/index.php/sia_ajax/classroom_select_options/' + selectedGrade;
      $('#cmbClassroom').html('<option>Loading...</option>').load(url);
    });
  },

  setFormOnSubmitListener: function () {
    $('#btnSimpan').click(function () {
      var validate = ultracms.siaStudentEditor.validateForm();
      if (validate.isValid) {
        siaJS_confirm_form_submission('studentForm');
      } else {
        siaJS_alert(validate.errMsgs);
      }
    });
  },

  initDatePickers: function () {
    var datePickerDateFormat = ultracms.config.datePickerDateFormat;
    var datePickerValueFormat = ultracms.config.datePickerValueFormat;
    var datePickerOpts = {locale: 'id', format: datePickerDateFormat, ignoreReadonly: true};
    $('#birth_date_picker').datetimepicker(datePickerOpts).on('dp.change', function (e) {
      $('#birth_date').val(e.date.format(datePickerValueFormat).toString());
    });
    $('#accepted_date_picker').datetimepicker(datePickerOpts).on('dp.change', function (e) {
      $('#accepted_date').val(e.date.format(datePickerValueFormat).toString());
    });
    $('#transfer_date_picker').datetimepicker(datePickerOpts).on('dp.change', function (e) {
      $('#transfer_date').val(e.date.format(datePickerValueFormat).toString());
    });
  },

  initComp: function () {
    /* Grade combobox */
    this.setCmbGradeOnChangeListener();

    /* Date Pickers */
    this.initDatePickers();

    /* Form */
    this.setFormOnSubmitListener();
  },

  validateForm: function () {
    var retval = {isValid: true, errMsgs: ""};

    /* Check empty fields */
    var emptyFields = new Array();
    if (ultracms.utils.isEmpty($('#txtNis').val())) {
      emptyFields.push(ultracms.utils.getLang('sia_students.nis'));
    }
    if (ultracms.utils.isEmpty($('#txtNama').val())) {
      emptyFields.push(ultracms.utils.getLang('sia_students.name'));
    }
    if (ultracms.utils.isEmpty($('#txtBirthPlace').val())) {
      emptyFields.push(ultracms.utils.getLang('sia_students.birth_place'));
    }
    if ($('#birth_date_picker').data('DateTimePicker').date() == null) {
      emptyFields.push(ultracms.utils.getLang('sia_students.birth_date'));
    }
    if (ultracms.utils.isEmpty($('#txtAddress').val())) {
      emptyFields.push(ultracms.utils.getLang('sia_students.address'));
    }
    if (ultracms.utils.isEmpty($('#cmbGrade').val())) {
      emptyFields.push(ultracms.utils.getLang('sia_students.grade'));
    }
    if (ultracms.utils.isEmpty($('#cmbClassroom').val())) {
      emptyFields.push(ultracms.utils.getLang('sia_students.educational_level'));
    }
    if (ultracms.utils.isEmpty($('#txtFatherName').val())) {
      emptyFields.push(ultracms.utils.getLang('sia_students.father_name'));
    }
    if (ultracms.utils.isEmpty($('#txtMotherName').val())) {
      emptyFields.push(ultracms.utils.getLang('sia_students.mother_name'));
    }
    if ($('#accepted_date_picker').data('DateTimePicker').date() == null) {
      emptyFields.push(ultracms.utils.getLang('sia_students.accepted_date'));
    }
    if (ultracms.utils.isEmpty($('#cmbTahunAjaran').val())) {
      emptyFields.push('Masuk Tahun Ajaran');
    }
    if (emptyFields.length > 0) {
      var errHeading = ultracms.language.alerts.empty_fields;
      retval.isValid = false;
      retval.errMsgs = ultracms.utils.createErrorMessage(errHeading, emptyFields, '<br>');
      return retval;
    }

    /* Check invalid fields */
    var invalidFields = new Array();
    if (!ultracms.utils.isEmpty($('#txtWeight').val()) && !ultracms.utils.isNumeric($('#txtWeight').val(), false)) {
      invalidFields.push(ultracms.utils.getLang('sia_students.weight'));
    }
    if (!ultracms.utils.isEmpty($('#txtHeight').val()) && !ultracms.utils.isNumeric($('#txtHeight').val(), false)) {
      invalidFields.push(ultracms.utils.getLang('sia_students.height'));
    }
    if (invalidFields.length > 0) {
      var errHeading = ultracms.language.alerts.invalid_fields;
      retval.isValid = false;
      retval.errMsgs = ultracms.utils.createErrorMessage(errHeading, invalidFields, '<br>');
      return retval;
    }

    return retval;
  },

  init: function () {
    this.initComp();
  }

};
