ultracms.siaTransaksiSppList = {

	initComp : function() {
		$('#btnReset').click(function() {
			$('#txtNis, #txtNama, #txtKeterangan, #txtTrxDateFrom, #transaction_date_from, #txtTrxDateTo, #transaction_date_to').val('');
		});

		const datePickerOpts = {
			locale : 'id',
			format : 'DD-MMM-YYYY',
			ignoreReadonly : true
		};
		$('#trxDatePickerFrom')
      .datetimepicker(datePickerOpts)
      .on('dp.change', function(e) {
        $('#transaction_date_from').val(e.date.format('YYYY-MM-DD').toString());
      });

    $('#trxDatePickerTo')
      .datetimepicker(datePickerOpts)
      .on('dp.change', function(e) {
        $('#transaction_date_to').val(e.date.format('YYYY-MM-DD').toString());
      });
	},

	init : function() {
		this.initComp();
	}

};
