ultracms.siaRptMainAccountTrx = {
	_initComp : function() {
		$("#reportForm")
				.submit(
						function() {
							var validate = ultracms.siaRptMainAccountTrx._validateForm();
							if (!validate.isValid) {
								siaJS_alert(validate.errMsgs);
								return false;
							} else {
								return true;
							}
						});

		$('#btnReset').click(function() {
			$('#txtTrxDateFrom').val('');
			$('#txtTrxDateTo').val('');
			$('#transaction_date_from').val('');
			$('#transaction_date_to').val('');
		});

		var datePickerOptsFrom = {
			locale : 'id',
			format : 'DD-MMM-YYYY',
			ignoreReadonly : true
		};
		$('#trxDatePickerFrom').datetimepicker(datePickerOptsFrom);
		$('#trxDatePickerFrom').on(
				'dp.change',
				function(e) {
					$('#transaction_date_from').val(
							e.date.format('YYYY-MM-DD').toString());
				});

		var datePickerOptsTo = {
			locale : 'id',
			format : 'DD-MMM-YYYY',
			ignoreReadonly : true
		};
		$('#trxDatePickerTo').datetimepicker(datePickerOptsTo);
		$('#trxDatePickerTo').on(
				'dp.change',
				function(e) {
					$('#transaction_date_to').val(
							e.date.format('YYYY-MM-DD').toString());
				});

	},
	_validateForm : function() {
		var retval = {isValid: true, errMsgs: ""};
		
		/* Check empty fields */
		var emptyFields = new Array();
		if(ultracms.utils.isEmpty($('#transaction_date_from').val())){
			emptyFields.push('Tanggal Transaksi Awal');
		}
		
		if(ultracms.utils.isEmpty($('#transaction_date_to').val())){
			emptyFields.push('Tanggal Transaksi Akhir');
		}
		if(emptyFields.length > 0){
			var errHeading = ultracms.language.alerts_mandatory_fields;
			retval.isValid = false;
			retval.errMsgs = ultracms.utils.createErrorMessage(errHeading, emptyFields);
			
		}
		return retval;
	},

	init : function() {
		this._initComp();
	}

};