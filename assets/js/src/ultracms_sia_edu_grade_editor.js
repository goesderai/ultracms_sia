ultracms.siaEduGradeEditor = {

  loadClassroomData: function (gradeId) {
    const url = siaJS_site_url('/sia_edu_grade/classrooms/' + gradeId);
    $('#classroomsContainer').load(url, function () {
      $('td.actionColumn button.btn-danger').each(function (i) {
        $(this).click(function () {
          if (confirm(ultracms.language.questions.confirm_delete)) {
            const btnId = $(this).attr('id');
            const btnIdParts = btnId.split('-');
            const classroomId = btnIdParts[1];
            ultracms.siaEduGradeEditor.deleteClassroom(classroomId);
          }
        });
      });
      $('a.classroomEditLink').each(function (i) {
        $(this).click(function () {
          const linkId = $(this).attr('id');
          const linkIdParts = linkId.split('-');
          const classroomId = linkIdParts[1];
          const newClassroomName = prompt(ultracms.utils.getLang('sia_edu_grade.edit_level_prompt'));
          if (!ultracms.utils.isEmpty(newClassroomName)) {
            ultracms.siaEduGradeEditor.updateClassroom(classroomId, newClassroomName);
          }
        });
      });
    });
  },

  deleteClassroom: function (classroomId) {
    const url = siaJS_site_url('/sia_edu_grade/delete_classroom/' + classroomId);
    $.get(url, function (resp) {
      alert(resp.message);
      ultracms.siaEduGradeEditor.loadClassroomData($('#gradeId').val());
    }, 'json');
  },

  updateClassroom: function (classroomId, newClassroomName) {
    const url = siaJS_site_url('/sia_edu_grade/update_classroom/'
      + classroomId + '/'
      + encodeURI(newClassroomName));
    $.get(url, function (resp) {
      alert(resp.message);
      ultracms.siaEduGradeEditor.loadClassroomData($('#gradeId').val());
    }, 'json');
  },

  setBtnAddClassroomOnClickListener: function () {
    $('#btnAddClassroom').click(function () {
      const gradeId = $('#gradeId').val();
      const newClassroomName = prompt(ultracms.utils.getLang('sia_edu_grade.new_level_prompt'));
      if (!ultracms.utils.isEmpty(gradeId) && !ultracms.utils.isEmpty(newClassroomName)) {
        const url = siaJS_site_url('/sia_edu_grade/add_classroom/'
          + gradeId + '/'
          + encodeURI(newClassroomName));
        $.get(url, function (resp) {
          alert(resp.message);
          ultracms.siaEduGradeEditor.loadClassroomData(gradeId);
        }, 'json');
      }
    });
  },

  setFormOnSubmitListener: function () {
    $('#btnSimpan').click(function () {
      const validate = ultracms.siaEduGradeEditor.validateForm();
      if (validate.isValid) {
        siaJS_confirm_form_submission('formEduGrade');
      } else {
        siaJS_alert(validate.errMsgs);
      }
    });
  },

  validateForm: function () {
    let retval = {isValid: true, errMsgs: ""};

    /* Check empty fields */
    let emptyFields = new Array();
    if (ultracms.utils.isEmpty($('#txtNamaTingkat').val())) {
      emptyFields.push(ultracms.utils.getLang('sia_edu_grade.grade_name'));
    }
    if (emptyFields.length > 0) {
      const errHeading = ultracms.language.alerts.empty_fields;
      retval.isValid = false;
      retval.errMsgs = ultracms.utils.createErrorMessage(errHeading, emptyFields, '<br>');
      return retval;
    }

    return retval;
  },

  initComp: function () {
    this.loadClassroomData($('#gradeId').val());
    this.setBtnAddClassroomOnClickListener();
    this.setFormOnSubmitListener();
  },

  init: function () {
    this.initComp();
  }

};
