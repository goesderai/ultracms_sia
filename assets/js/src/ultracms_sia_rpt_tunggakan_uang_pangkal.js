ultracms.siaRptTransaksiTunggakanUangPangkal = {

  _initComp: function () {
    /*ultracms.utils.setAsNumberInputBoxes("txtFinancialYear");*/
    $("#reportForm").submit(function () {
      const validate = ultracms.siaRptTransaksiTunggakanUangPangkal._validateForm();
      if (!validate.isValid) {
        siaJS_alert(validate.errMsgs);
        return false;
      } else {
        siaJS_rpt_disable_submit_button(this);
        return true;
      }
    });

    $('#cmbGrade').change(function () {
      const selectedGrade = $(this).val();
      const url = ultracms.config.baseUrl + '/index.php/sia_ajax/classroom_select_options/' + selectedGrade;
      $('#cmbClassroom').html('<option>Loading...</option>').load(url);
    });
  },

  _validateForm: function () {
    let retval = {isValid: true, errMsgs: ""};

    /* Check empty fields */
    let emptyFields = [];
    if (emptyFields.length > 0) {
      retval.isValid = false;
      retval.errMsgs = ultracms.utils.createErrorMessage(ultracms.language.alerts.mandatory_fields, emptyFields);
    }

    return retval;
  },

  init: function () {
    this._initComp();
  }

};
