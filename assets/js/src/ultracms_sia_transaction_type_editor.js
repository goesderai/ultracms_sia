ultracms.siaTransactionTypeEditor = {
		
	_initComp: function(){
		$('#cmbTipeTransaksi, #cmbDocType').select2();
		$('#btnSimpan').click(function(){
			var validate = ultracms.siaTransactionTypeEditor._validateForm();
			if(validate.isValid){
			    siaJS_confirm_form_submission('transactionTypeForm');
			}else{
				siaJS_alert(validate.errMsgs);
			}
		});
	},
	
	_validateForm: function(){
		var retval = {isValid: true, errMsgs: ""};
		
		/* Check empty fields */
		var emptyFields = new Array();
		if(ultracms.utils.isEmpty($('#txtTipeTransaksi').val())){
			emptyFields.push('Kode Tipe Transaksi');
		}
		if(ultracms.utils.isEmpty($('#txtNamaTipeTransaksi').val())){
			emptyFields.push('Nama Tipe Transaksi');
		}
		if(ultracms.utils.isEmpty($('#cmbDocType').val())){
			emptyFields.push('Dokument Type');
		}
		
		if(emptyFields.length > 0){
			var errHeading = ultracms.language.alerts.empty_fields;
			retval.isValid = false;
			retval.errMsgs = ultracms.utils.createErrorMessage(errHeading, emptyFields, '<br>');
			return retval;
		}
		return retval;
	},
	
	init: function(){
		this._initComp();
	}
	
};