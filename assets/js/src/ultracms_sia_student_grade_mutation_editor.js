ultracms.siaStudentGradeMutationEditor = {
  initComp: function () {
    /* Date picker */
    const datePickerDateFormat = ultracms.config.datePickerDateFormat;
    const datePickerValueFormat = ultracms.config.datePickerValueFormat;
    const datePickerOpts = {locale: 'id', format: datePickerDateFormat, ignoreReadonly: true};
    $('#mutation_date_picker').datetimepicker(datePickerOpts).on('dp.change', function (e) {
      $('#mutation_date').val(e.date.format(datePickerValueFormat).toString());
    });

    /* Combo boxes */
    $('#cmbDestGradeId').change(function () {
      const selectedGrade = $(this).val();
      const url = ultracms.config.baseUrl + '/index.php/sia_ajax/classroom_select_options/' + selectedGrade;
      $('#cmbDestClassroomId').html('<option>Loading...</option>').load(url);
    });
    $('#cmbDestClassroomId').change(function () {
      if (ultracms.utils.isEmpty($(this).val())) {
        $('#btnAddItem').attr('disabled', 'disabled').attr('title', 'Pilih jenjang & tingkatan tujuan terlebih dahulu');
      } else {
        $('#btnAddItem').removeAttr('disabled').attr('title', 'Tambah siswa');
      }
    });

    /* btnSimpan */
    $('#btnSimpan').click(function () {
      const validate = ultracms.siaStudentGradeMutationEditor.validateForm();
      if (validate.isValid) {
        siaJS_confirm_form_submission('mutationForm');
      } else {
        siaJS_alert(validate.errMsgs);
      }
    });

    this._initMultipleResultStudentLookupDialog();

    this._initBtnAddItem();
  },

  _initMultipleResultStudentLookupDialog: function () {
    ultracms.MultipleResultStudentLookupDialog.init(
      function () {
        let retval = new Array();

        /* dest classroom id */
        retval.push({key: 'destClassroomId', value: $('#cmbDestClassroomId').val()});

        /* exclude ids */
        const hiddens = $('#tblStudentsBody input[type="hidden"]');
        let ids = new Array();
        for (let i = 0; i < hiddens.length; i++) {
          ids.push($(hiddens[i]).val());
        }
        const idCsv = (ids.length > 0) ? ids.join() : '';
        retval.push({key: 'excludeIds', value: idCsv});

        return retval;
      },
      function (selectedItems) {
        if (selectedItems.length > 0) {
          let rowNum = $('#tblStudentsBody').children('tr').length;
          const template = '<tr><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td>' +
            '<td class="text-center"><button type="button" onclick="stdGrdMtnEditor_removeRow(event)" class="btn btn-danger btn-xs" title="Hapus">' +
            '<i class="fa fa-times"></i></button><input type="hidden" name="studentId[]" value="{6}" /></td></tr>';
          let j = 0;
          while (j < selectedItems.length) {
            rowNum++;
            const item = selectedItems[j];
            const newRow = ultracms.utils.formatString(template, rowNum, item.nis, item.name, item.grade_name,
              item.classroom_name, item.id);
            $('#tblStudentsBody').append(newRow);
            j++;
          }
        }
      }
    );
  },

  _initBtnAddItem: function () {
    $('#btnAddItem').click(function () {
      ultracms.MultipleResultStudentLookupDialog.resetDialog();
      $('#multipleResultStudentLookupDialog').modal({backdrop: 'static'});
    });
  },

  _reArrangeRowNum: function () {
    var trs = $('#tblStudentsBody').children('tr');
    for (i = 0; i < trs.length; i++) {
      $(trs[i]).children(':first').html(i + 1);
    }
  },

  validateForm: function () {
    let retval = {isValid: true, errMsgs: ""};

    /* Check empty fields */
    let emptyFields = new Array();
    if (ultracms.utils.isEmpty($('#mutation_date').val())) {
      emptyFields.push(ultracms.utils.getLang('sia_student_grade_mutation.process_date'));
    }
    if (ultracms.utils.isEmpty($('#cmbDestGradeId').val())) {
      emptyFields.push(ultracms.utils.getLang('sia_student_grade_mutation.dest_grade'));
    }
    if (ultracms.utils.isEmpty($('#cmbDestClassroomId').val())) {
      emptyFields.push(ultracms.utils.getLang('sia_student_grade_mutation.dest_level'));
    }
    if (emptyFields.length > 0) {
      const errHeading = ultracms.language.alerts.empty_fields;
      retval.isValid = false;
      retval.errMsgs = ultracms.utils.createErrorMessage(errHeading, emptyFields, '<br>');
      return retval;
    }

    return retval;
  },

  removeRow: function (event) {
    const el = event.target;
    const elNode = el.nodeName.trim();
    if (elNode === 'I') {
      $(el).parent().parent().parent().remove();
    } else if (elNode === 'BUTTON') {
      $(el).parent().parent().remove();
    }
    this._reArrangeRowNum();
  },

  init: function () {
    this.initComp();
  }
};

/*
 * Helper functions
 * prefix: stdGrdMtnEditor_
 */
function stdGrdMtnEditor_removeRow(event) {
  if (confirm(ultracms.language.questions.confirm_delete)) {
    ultracms.siaStudentGradeMutationEditor.removeRow(event);
  }
}
