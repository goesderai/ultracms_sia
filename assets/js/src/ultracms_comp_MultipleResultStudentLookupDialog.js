ultracms.MultipleResultStudentLookupDialog = {
  _serviceUrl: (ultracms.config.baseUrl + '/index.php/sia_ajax'),
  _chooseButtonCallback: null,
  _additionalParamsGetter: null,

  _setEventListeners: function () {
    $('#studentLookupCmbGrade').change(function () {
      const selectedGrade = $(this).val();
      const url = ultracms.MultipleResultStudentLookupDialog._serviceUrl + '/classroom_select_options/' + selectedGrade;
      $('#studentLookupCmbClassroom').html('<option>Loading...</option>').load(url);
    });

    $('#btnSearch').click(function () {
      ultracms.MultipleResultStudentLookupDialog._loadSearchResult(1);
    });

    $('#btnReset').click(function () {
      ultracms.MultipleResultStudentLookupDialog._resetSearchParams();
    });

    $('#studentLookupMasterCb').change(function () {
      const isChecked = $(this).is(":checked");
      ultracms.utils.setCheckboxesCheckedState("studentLookupSlaveCb", isChecked);
    });

    $('#studentLookupBtnPilih').click(function () {
      if (ultracms.MultipleResultStudentLookupDialog._chooseButtonCallback != null) {
        const selectedItems = ultracms.MultipleResultStudentLookupDialog._getSelectedItems();
        ultracms.MultipleResultStudentLookupDialog._chooseButtonCallback(selectedItems);
        $('#multipleResultStudentLookupDialog').modal('hide');
      }
    });
  },

  _getSelectedItems: function () {
    let retval = new Array();
    const checkedCbs = $('#resultContainer input:checked');
    if (checkedCbs.length > 0) {
      for (let i = 0; i < checkedCbs.length; i++) {
        const item = JSON.parse($(checkedCbs[i]).parent().parent().attr('data-value'));
        retval.push(item);
      }
    }
    return retval;
  },

  _createParams: function (page) {
    if (ultracms.utils.isEmpty(page)) {
      page = 1;
    }
    let params = new Array();
    params.push({key: "p", value: page});
    params.push({key: "pz", value: 5});
    params.push({key: "nis", value: $('#txtNis').val()});
    params.push({key: "nama", value: $('#txtNama').val()});
    params.push({key: "srcClassroomId", value: $('#studentLookupCmbClassroom').val()});
    if (this._additionalParamsGetter != null) {
      const extraParams = this._additionalParamsGetter();
      for (let i = 0; i < extraParams.length; i++) {
        const p = extraParams[i];
        params.push({key: p.key, value: p.value});
      }
    }

    return params;
  },

  _loadSearchResult: function (page) {
    if (ultracms.utils.isEmpty(page)) {
      page = 1;
    }
    const params = ultracms.MultipleResultStudentLookupDialog._createParams(page);
    const requestUrl = ultracms.utils.buildUrl(this._serviceUrl + '/student_lookup_dlg_list', params);
    $('#resultContainer').load(requestUrl, function (resp, status, xhr) {
      if (status == 'success') {
        const jsonResp = JSON.parse(resp);
        $('#txtPage').val(jsonResp.page);
        $('#txtPages').val(jsonResp.pages);
        $(this).html(jsonResp.html);
        ultracms.MultipleResultStudentLookupDialog._configurePagination();
      } else {
        $(this).html('Request failed, there was an error: ' + xhr.status + " " + xhr.statusText);
      }
    });
  },

  _configurePagination: function () {
    const currPage = parseInt($('#txtPage').val());
    const pages = parseInt($('#txtPages').val());
    if (ultracms.utils.isEmpty(pages)) {
      $('#paginationContainer').hide();
    } else {
      if (currPage > 1) {
        $('#aNavFirst').attr('onclick', 'multipleResultStudentLookup_gotoPage(1)');
        $('#aNavPrev').attr('onclick', 'multipleResultStudentLookup_gotoPage(' + (currPage - 1) + ')');
        $('#liNavFirst').removeClass('disabled');
        $('#liNavPrev').removeClass('disabled');
      } else {
        $('#aNavFirst').removeAttr('onclick');
        $('#aNavPrev').removeAttr('onclick');
        $('#liNavFirst').addClass('disabled');
        $('#liNavPrev').addClass('disabled');
      }
      if (currPage < pages) {
        $('#aNavLast').attr('onclick', 'multipleResultStudentLookup_gotoPage(' + pages + ')');
        $('#aNavNext').attr('onclick', 'multipleResultStudentLookup_gotoPage(' + (currPage + 1) + ')');
        $('#liNavLast').removeClass('disabled');
        $('#liNavNext').removeClass('disabled');
      } else {
        $('#aNavLast').removeAttr('onclick');
        $('#aNavNext').removeAttr('onclick');
        $('#liNavLast').addClass('disabled');
        $('#liNavNext').addClass('disabled');
      }
      $('#navPageInfo').html('Page ' + currPage + ' of ' + pages);
      $('#paginationContainer').show();
    }
  },

  _resetSearchResult: function () {
    $('#studentLookupMasterCb').prop('checked', false);
    $('#resultContainer').html('');
    $('#paginationContainer').hide();
  },

  _resetSearchParams: function () {
    $('#txtNis').val('');
    $('#txtNama').val('');
    $('#txtPage').val('');
    $('#txtPages').val('');
    $('#studentLookupCmbGrade').val('0');
    $('#studentLookupCmbClassroom').html('<option value="0" disabled="disabled" selected="selected">-- Pilih jenjang terlebih dahulu --</option>');
  },

  gotoPage: function (page) {
    this._loadSearchResult(page);
  },

  resetDialog: function () {
    this._resetSearchParams();
    this._resetSearchResult();
  },

  init: function (additionalParamsGetter, chooseButtonCallback) {
    if (typeof additionalParamsGetter === 'function') {
      this._additionalParamsGetter = additionalParamsGetter;
    }
    if (typeof chooseButtonCallback === 'function') {
      this._chooseButtonCallback = chooseButtonCallback;
    }
    this._resetSearchParams();
    this._resetSearchResult();
    this._setEventListeners();
  }
};

/*
 * Helper functions
 * prefix: studentLookup_
 */
function multipleResultStudentLookup_gotoPage(page) {
  ultracms.MultipleResultStudentLookupDialog.gotoPage(page);
}
