/* BEGIN Loading indicator script */
function siaJS_remove_loading_indicator() {
  $('#loadingDiv').fadeOut(500, () => {
    $('#loadingDiv').remove();
  });
}

$(window).on('load', () => {
  $('body').append('<div id="loadingDiv"><div class="loader">Loading...</div></div>');
  setTimeout(siaJS_remove_loading_indicator, 500);
});
/* END Loading indicator script */

//------------------------------ ULTRA CMS BASE OBJECT ------------------------------//
const ultracms = ultracms || {};

/* config */
ultracms.config = {
  baseUrl:               "/ultracms_sia",
  datePickerDateFormat:  "DD-MMM-YYYY",
  datePickerValueFormat: "YYYY-MM-DD",
  modalDialog:           {backdrop: 'static', keyboard: false}
};

/* utils */
ultracms.utils = {

  isEmpty: function (val) {
    return (val === null || val === undefined || parseInt(val) === 0 || val === "" || val.length === 0);
  },

  isNumeric: function (val, allowNegative) {
    if (allowNegative === true) {
      return !isNaN(val);
    } else {
      return /^[0-9.]+$/.test(val);
    }
  },

  isNumberOnly: function (val) {
    return /^[0-9]+$/.test(val);
  },

  formatString: function (string) {
    if (this.isEmpty(string)) {
      return null;
    }
    const args = arguments;
    if (args.length > 1) {
      let str = string.trim();
      for (let i = 1; i < args.length; i++) {
        const regexp = new RegExp('\\{' + i + '\\}', 'gi');
        str = str.replace(regexp, args[i]);
      }
      return str;
    } else {
      return string;
    }
  },

  createErrorMessage: function (messageHeading, messages) {
    if (this.isEmpty(messageHeading) || this.isEmpty(messages)) {
      return "";
    } else {
      const lineBreak = (arguments[2] !== undefined) ? arguments[2] : '\n';
      let msg = `<strong>${messageHeading}</strong>`;
      for (let i = 0; i < messages.length; i++) {
        let j = i + 1;
        msg += `${lineBreak}${j}. ${messages[i]}`;
      }
      return msg;
    }
  },

  setAsNumberInputBoxes: function (inputBoxes) {
    if (!this.isEmpty(inputBoxes)) {
      const inputBoxesIds = inputBoxes.trim().split(",");
      for (let i = 0; i < inputBoxesIds.length; i++) {
        $("#" + inputBoxesIds[i]).keyup(function (e) {
          if (!ultracms.utils.isNumeric($(this).val(), false)) {
            $(this).val("");
          }
        });
      }
    }
  },

  /**
   * Build url for GET request
   * @param url The base url
   * @param params Array of json objects, example: [{key: "action", value: "do something"}, ...]<br>
   * note: the key value of the object must be a String
   * @return string
   */
  buildUrl: function (url, params) {
    if (this.isEmpty(url) || this.isEmpty(params)) {
      return "";
    } else {
      let paramKey;
      let paramValue = null;
      const firstParam = params[0];
      paramKey = firstParam.key.trim();
      if (typeof firstParam.value === "string") {
        paramValue = firstParam.value.trim();
      } else {
        paramValue = firstParam.value.toString();
      }
      let retval = url + "?" + paramKey + "=" + encodeURI(paramValue);
      if (params.length > 1) {
        for (let i = 1; i < params.length; i++) {
          const param = params[i];
          paramKey = param.key.trim();
          if (typeof param.value === "string") {
            paramValue = param.value.trim();
          } else {
            paramValue = param.value.toString();
          }
          retval += "&" + paramKey + "=" + encodeURI(paramValue);
        }
      }
      return retval;
    }
  },

  setCheckboxesCheckedState: function (checkboxesClass, isChecked) {
    if (this.isEmpty(checkboxesClass)) {

    } else {
      if (typeof isChecked !== "boolean") {
        isChecked = false;
      }
      const slaveCbs = $("." + checkboxesClass);
      if (slaveCbs.length > 0) {
        for (let i = 0; i < slaveCbs.length; i++) {
          $(slaveCbs[i]).prop("checked", isChecked);
        }
      }
    }
  },

  getCheckboxesValue: function (checkboxesClass) {
    if (this.isEmpty(checkboxesClass)) {
      return null;
    } else {
      const retval = [];
      const cbs = $("." + checkboxesClass);
      if (cbs.length > 0) {
        for (let i = 0; i < cbs.length; i++) {
          retval.push($(cbs[i]).val());
        }
      }
      return retval;
    }
  },

  getCheckboxesValueAsString: function (checkboxesClass, separator) {
    if (this.isEmpty(checkboxesClass) || this.isEmpty(separator)) {
      return null;
    } else {
      const vals = this.getCheckboxesValue(checkboxesClass);
      return vals.join(separator);
    }
  },

  getLang: function (langKey) {
    if (this.isEmpty(langKey)) {
      return '';
    } else {
      return eval(`ultracms.language.${langKey.toLowerCase().trim()}`);
    }
  }

};

/* dialogs */
ultracms.dialogs = {

  selectors: {
    yesNoDialog: {
      dialog:    '#yesNoDialog',
      message:   '#yesNoDialogMessage',
      yesButton: '#yesNoDialogBtnYes',
      noButton:  '#yesNoDialogBtnNo'
    },
    alertDialog: {
      dialog:   '#alertDialog',
      message:  '#alertDialogMessage',
      okButton: '#alertDialogBtnOk'
    }
  },

  destroyDialog: function (dialog) {
    $(dialog).modal('hide').on('hidden.bs.modal', function () {
      $(this).data('bs.modal', null);
    });
  },

  showConfirmationDialog: function (msgKey, btnYesCallback, btnNoCallback, focusOnButton) {
    $(ultracms.dialogs.selectors.yesNoDialog.message).text(ultracms.utils.getLang(msgKey));
    $(ultracms.dialogs.selectors.yesNoDialog.dialog)
      .modal(ultracms.config.modalDialog)
      .on('shown.bs.modal', function (event) {
        $(this).find(ultracms.dialogs.selectors.yesNoDialog.yesButton).click(btnYesCallback);
        $(this).find(ultracms.dialogs.selectors.yesNoDialog.noButton).click(btnNoCallback);
        $(this).find('.btn.btn-warning')[focusOnButton].focus();
      });
  },

  showFormSubmissionDialog: function (onConfirmedCallback, focusOnButton) {
    $(ultracms.dialogs.selectors.yesNoDialog.message).text(ultracms.utils.getLang('questions.confirm_save'));
    $(ultracms.dialogs.selectors.yesNoDialog.dialog)
      .modal(ultracms.config.modalDialog)
      .on('shown.bs.modal', function (event) {
        $(this).find(ultracms.dialogs.selectors.yesNoDialog.yesButton).click(onConfirmedCallback);
        $(this).find(ultracms.dialogs.selectors.yesNoDialog.noButton).click(function () {
          ultracms.dialogs.destroyDialog(ultracms.dialogs.selectors.yesNoDialog.dialog);
        });
        $(this).find('.btn.btn-warning')[focusOnButton].focus();
      });
  },

  showAlertDialog: function (message, btnOkCallback) {
    $(ultracms.dialogs.selectors.alertDialog.message).html(message);
    $(ultracms.dialogs.selectors.alertDialog.dialog)
      .modal(ultracms.config.modalDialog)
      .on('shown.bs.modal', function (event) {
        $(this).find(ultracms.dialogs.selectors.alertDialog.okButton)
               .click(btnOkCallback)
               .focus();
      });
  }

};
