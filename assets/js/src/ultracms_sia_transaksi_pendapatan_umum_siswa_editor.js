ultracms.siaTransaksiPendapatanUmumSiswaEditor = {
  emptyFields:   [],
  invalidFields: [],
  initComp:      function () {
    // date pickers
    const datePickerOptsFrom = {
      locale:         'id',
      format:         ultracms.config.datePickerDateFormat,
      ignoreReadonly: true
    };
    $('#trxDatePicker').datetimepicker(datePickerOptsFrom);
    $('#trxDatePicker').on('dp.change', function (e) {
      $('#trx_date').val(e.date.format(ultracms.config.datePickerValueFormat).toString());
    });

    /* buttons */
    $('#btnSimpan').click(function () {
      const validate = ultracms.siaTransaksiPendapatanUmumSiswaEditor.validateForm();
      if (validate.isValid) {
        siaJS_confirm_form_submission('pendapatanUmumSiswaForm');
      } else {
        siaJS_alert(validate.errMsgs);
      }
    });

    // inputs
    $('#trx_typ').val('D');
    $('#txtJmlTrx, #txtDiskon, #txtTotalBayar, #txtBayar, #txtSisaBayar').number(true, 2);
    $('#txtJmlTrx').blur(
      function () {
        ultracms.siaTransaksiPendapatanUmumSiswaEditor.recalculateSisaHutang(true);
      }
    );
    $('#txtDiskon').blur(
      function () {
        ultracms.siaTransaksiPendapatanUmumSiswaEditor.recalculateTotalBayar();
        if ($('#txtBayar').val() > 0) {
          ultracms.siaTransaksiPendapatanUmumSiswaEditor.recalculateSisaBayar();
        }
      }
    );
    $('#txtBayar').blur(
      function () {
        ultracms.siaTransaksiPendapatanUmumSiswaEditor.recalculateSisaBayar();
      }
    );
    $('#cmbTipeTransaksi').change(function () {
      ultracms.siaTransaksiPendapatanUmumSiswaEditor.resetForm();
      const url = siaJS_site_url('/sia_transaksi_pendapatan_umum_siswa/get_sub_trx_type/' + $(this).val() + '/' + 1);
      $('#cmbJenSubType').val(null);
      document.getElementById('cmbTahunAjaran').disabled = true;
      $('#cmbTahunAjaran').val('');
      $('#cmbJenSubType').load(url);
    });
    $('#cmbJenSubType').change(function () {
      ultracms.siaTransaksiPendapatanUmumSiswaEditor.resetForm();

      const subType = parseInt($('#cmbJenSubType').val());
      if (subType === 145 || subType === 353 || subType === 150 || subType === 358) {
        document.getElementById('cmbTahunAjaran').disabled = false;
      } else {
        document.getElementById('cmbTahunAjaran').disabled = true;
        $('#cmbTahunAjaran').val('');
      }

      /*if (subType === 352 || subType === 353 || subType === 358) {
    	  document.getElementById('txtDiskon').disabled = true;
    	  document.getElementById('txtBayar').disabled = true;
      }else{
    	  document.getElementById('txtDiskon').disabled = false;
    	  document.getElementById('txtBayar').disabled = false;
      }*/
    });

    // student lookup dialog
    ultracms.SingleResultStudentLookupDialog.init(null, function (item) {
      $('#txtSiswa').val(item.nis + ' - ' + item.name);
      $('#txtIdSiswa').val(item.id);
      ultracms.siaTransaksiPendapatanUmumSiswaEditor.recalculateSisaHutang(false);
      /*const subType = parseInt($('#cmbJenSubType').val());
      if (subType === 352 || subType === 353 || subType === 358) {
    	  
           * cari sisa hutang 
           
    	  const url = siaJS_site_url('/sia_transaksi_pendapatan_umum_siswa/getSisaHutangByStudentId/' + item.id + '/' + subType);
    	  $.get(url, function (res){
    		  if(res>0){
    			  $('#txtJmlTrx').val(res);
    			  $('#txtRemark').val("Sisa Hutang : " + res);
    		  }
		  });
      }*/
    });
    $('#btnBrowseSiswa').click(function () {
      ultracms.SingleResultStudentLookupDialog.resetDialog();
      $('#singleResultStudentLookupDialog').modal({backdrop: 'static'});
    });
  },

  validateForm: function () {
    ultracms.siaTransaksiPendapatanUmumSiswaEditor.emptyFields = [];
    ultracms.siaTransaksiPendapatanUmumSiswaEditor.invalidFields = [];
    let retval = {isValid: true, errMsgs: ""};

    /* Check empty fields */
    const transactionType = parseInt($('#cmbJenSubType').val());
    const tahunAjaran = $('#cmbTahunAjaran').val();
    const idSiswa = $('#txtIdSiswa').val();
    if ((transactionType === 145 || transactionType === 353 || transactionType === 150) && tahunAjaran === '') {
      siaTransaksiPendapatanUmumSiswaEditor_addEmptyFields('Tahun Ajaran Harus diisi!');
    }
    if (ultracms.utils.isEmpty(idSiswa)) {
      siaTransaksiPendapatanUmumSiswaEditor_addEmptyFields('Siswa Harus diisi!');
    }

    if (transactionType === 352 || transactionType === 353 || transactionType === 358) {
      //cari sisa hutang
      const url = siaJS_site_url('/sia_transaksi_pendapatan_umum_siswa/getSisaHutangByStudentId/' + idSiswa + '/' + transactionType);
      $.get(url, function (res) {
        const nRes = parseFloat(res);
        $('#sisa_hutang').val(nRes);
      });

      //validasi sisa hutang
      const nTrx        = parseFloat($('#txtJmlTrx').val()),
            nSisaHutang = parseFloat($('#sisa_hutang').val());
      if (nTrx > nSisaHutang) {
        siaTransaksiPendapatanUmumSiswaEditor_addInvalidFields('Jumlah Transaksi Tidak boleh melebihi sisa Hutang siswa!');
      }
    }

    //Empty fields error
    if (ultracms.siaTransaksiPendapatanUmumSiswaEditor.emptyFields.length > 0) {
      const errHeading = ultracms.language.alerts.empty_fields;
      retval.isValid = false;
      retval.errMsgs = ultracms.utils.createErrorMessage(errHeading,
                                                         ultracms.siaTransaksiPendapatanUmumSiswaEditor.emptyFields,
                                                         '<br>');
      return retval;
    }

    //Invalid fields error
    if (ultracms.siaTransaksiPendapatanUmumSiswaEditor.invalidFields.length > 0) {
      const errHeading = ultracms.language.alerts.invalid_fields;
      retval.isValid = false;
      retval.errMsgs = ultracms.utils.createErrorMessage(errHeading,
                                                         ultracms.siaTransaksiPendapatanUmumSiswaEditor.invalidFields,
                                                         '<br>');
      return retval;
    }

    return retval;
  },

  init: function () {
    this.initComp();
  },

  recalculateSisaHutang: function ($remarkOnly) {
    const studentId = $('#txtIdSiswa').val();
    const subType = parseInt($('#cmbJenSubType').val());
    if (subType === 352 || subType === 353 || subType === 358) {
      /*
         * cari sisa hutang
         */
      const url = siaJS_site_url('/sia_transaksi_pendapatan_umum_siswa/getSisaHutangByStudentId/' + studentId + '/' + subType);
      $.get(url, function (res) {
        if (res > 0) {
          if (!$remarkOnly) {
            $('#txtJmlTrx').val(parseFloat(res));
            $('#txtBayar').val(parseFloat(res));
            $('#txtTotalBayar').val(parseFloat(res));
          } else {
            $('#txtBayar').val($('#txtJmlTrx').val());
            $('#txtTotalBayar').val($('#txtJmlTrx').val());
          }
          $('#txtRemark').val('Sisa hutang: Rp.' + $.number(parseFloat(res) - parseFloat($('#txtJmlTrx').val()), 2));
        }
      });
    } else {
      ultracms.siaTransaksiPendapatanUmumSiswaEditor.recalculateTotalBayar();
      if ($('#txtBayar').val() > 0) {
        ultracms.siaTransaksiPendapatanUmumSiswaEditor.recalculateSisaBayar();
      }
    }
  },

  recalculateTotalBayar: function () {
    let trx = 0;
    let dis = 0;
    if (!ultracms.utils.isEmpty($('#txtJmlTrx').val())) {
      trx = $('#txtJmlTrx').val();
    }
    if (!ultracms.utils.isEmpty($('#txtDiskon').val())) {
      dis = $('#txtDiskon').val();
    }
    const jumlahTrx = parseFloat(trx);
    const diskon = parseFloat(dis);
    const totalBayar = jumlahTrx - diskon;
    $('#txtTotalBayar').val(totalBayar);

  },

  recalculateSisaBayar: function () {
    let byr = 0;
    if (!ultracms.utils.isEmpty($('#txtBayar').val())) {
      byr = $('#txtBayar').val();
    }
    const totalBayar = parseFloat($('#txtTotalBayar').val());
    const dibayar = parseFloat(byr);
    const sisaBayar = totalBayar - dibayar;
    $('#txtSisaBayar').val(sisaBayar);
  },

  resetForm: function () {
    $('#txtRemark').val("");
    $('#txtDiskon').val("0");
    $('#txtBayar').val("0");
    $('#txtJmlTrx').val("0");
    $('#txtTotalBayar').val("0");
    $('#txtSisaBayar').val("0");
    $('#txtSiswa').val("");
    $('#txtIdSiswa').val("");
    const trxType = parseInt($('#cmbTipeTransaksi').val());
    if (trxType === 350) {
      $('#txtBayar').attr('readonly', 'readonly');
      $('#txtDiskon').attr('readonly', 'readonly');
    } else {
      $('#txtBayar').removeAttr('readonly');
      $('#txtDiskon').removeAttr('readonly');
    }
  }

};

//Helper functions
//prefix: siaTransaksiPendapatanUmumSiswaEditor_
function siaTransaksiPendapatanUmumSiswaEditor_addEmptyFields(item) {
  ultracms.siaTransaksiPendapatanUmumSiswaEditor.emptyFields.push(item);
}

function siaTransaksiPendapatanUmumSiswaEditor_addInvalidFields(item) {
  ultracms.siaTransaksiPendapatanUmumSiswaEditor.invalidFields.push(item);
}
