ultracms.siaRptStudent = {
	
	initComp: function(){
		$('#cmbReportType').change(function(e){
			var rptType = $(this).val();
			if(rptType == '01'){
				ultracms.siaRptStudent._hideFilter();
			}
			if(rptType == '02'){
				ultracms.siaRptStudent._showFilter02();
			}
		});
	},
	
	_hideFilter: function(){
		$('#rptStudentFilter').html('').hide();
	},
	
	_showFilter02: function(){
		var rptType = $('#cmbReportType').val();
		var url = ultracms.config.baseUrl + '/index.php/sia_rpt_students/reports/' + rptType;
		$('#rptStudentFilter').load(url, function(response, status, xhr){
			if(status == "error"){
				var errMsg = "Sorry but there was an error: " + xhr.status + " " + xhr.statusText;
				$('#rptStudentFilter').html(errMsg).show();
			}
			if(status == "success" && response.length > 0){
				var datePickerDateFormat = ultracms.config.datePickerDateFormat;
				var datePickerValueFormat = ultracms.config.datePickerValueFormat;
				var datePickerOpts = {locale: 'id', format: datePickerDateFormat, ignoreReadonly: true};
				$('#birthDatePickerFrom').datetimepicker(datePickerOpts).on('dp.change', function(e){
					$('#birth_date_from').val(e.date.format(datePickerValueFormat).toString());
				});
				$('#birthDatePickerTo').datetimepicker(datePickerOpts).on('dp.change', function(e){
					$('#birth_date_to').val(e.date.format(datePickerValueFormat).toString());
				});
				$('#cmbGrade').change(function(){
					var selectedGrade = $(this).val();
					var url = ultracms.config.baseUrl + '/index.php/sia_ajax/classroom_select_options/' + selectedGrade;
					$('#cmbClassroom').html('<option>Loading...</option>').load(url);
				});
				$('#rptStudentFilter').show();
			}else{
				ultracms.siaRptStudent.hideFilter();
			}
		});
	},
	
	init: function(){
		this.initComp();
	}
	
};