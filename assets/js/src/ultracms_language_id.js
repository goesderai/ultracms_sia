/*
 * Indonesian language dictionary
 */
ultracms.language = {
  questions: {
    confirm_save: 'Apakah anda yakin ingin menyimpan data ini?',
    confirm_close: 'Apakah anda yakin ingin menutup halaman ini?',
    confirm_delete: 'Apakah anda yakin ingin menghapus data ini?',
    confirm_action: 'Apakah anda yakin ingin melakukan aksi tersebut?'
  },
  alerts: {
    empty_fields: 'Tidak bisa menyimpan data, field-field berikut masih kosong!',
    mandatory_fields: 'Field-field berikut tidak boleh kosong!',
    invalid_fields: 'Tidak bisa menyimpan data, field-field berikut masih salah!'
  },
  sia_change_pass: {
    password: 'Kata Sandi',
    new_password: 'Kata Sandi Baru',
    confirm_password: 'Konfirmasi Kata Sandi Baru',
    wrong_password_confirmation: 'Konfirmasi password baru salah!'
  },
  sia_users: {
    employee: 'Pegawai',
    username: 'Nama Pengguna',
    password: 'Kata Sandi',
    confirm_password: 'Konfirmasi Kata Sandi',
    roles: 'Peran'
  },
  sia_students: {
    nis: 'NIS',
    name: 'Nama',
    birth_place: 'Tempat Lahir',
    birth_date: 'Tanggal Lahir',
    address: 'Alamat',
    grade: 'Tingkatan',
    educational_level: 'Jenjang',
    father_name: "Nama Ayah Kandung",
    mother_name: "Nama Ibu Kandung",
    accepted_date: 'Tanggal Diterima',
    weight: 'Berat Badan (kg)',
    height: 'Tinggi Badan (cm)'
  },
  sia_employees: {
    name: 'Nama',
    birth_place: 'Tempat Lahir',
    birth_date: 'Tanggal Lahir',
    address: 'Alamat',
    phone: 'No. Telephone',
    id_number: 'No. Identitas',
    nett_salary: 'Gaji Pokok',
    benefit: 'Tunjangan',
    others: 'Lain-Lain'
  },
  sia_main_account: {
    code: 'Kode Akun',
    name: 'Nama Akun'
  },
  sia_posting_profile: {
    trx_type: 'Tipe Transaksi',
    account_debit: 'Akun Debit',
    account_credit: 'Akun Kredit'
  },
  sia_student_grade_mutation: {
    process_date: 'Tanggal Proses',
    grade: 'Jenjang',
    dest_grade: 'Jenjang Tujuan',
    level: 'Tingkatan',
    dest_level: 'Tingkatan Tujuan',
    nis: 'NIS',
    name: 'Nama',
    student_list: 'Daftar Siswa'
  },
  sia_edu_grade: {
    edit_level_prompt: 'Edit nama tingkatan menjadi:',
    new_level_prompt: 'Nama tingkatan baru:',
    grade_name: 'Nama Jenjang'
  },
  sia_trx_pembayaran: {
    trx: 'Transaksi',
    trx_type: 'Jenis Transaksi',
    trx_sub_type: 'Sub Jenis Transaksi',
    trx_amount: 'Jumlah Transaksi',
    trx_date: 'Tanggal Transaksi',
    remark: 'Keterangan'
  }
};
