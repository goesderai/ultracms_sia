ultracms.siaPostingProfileEditor = {
	_initComp: function(){
		/* select2 */
		$('#cmbTipeTransaksi, #account_debet, #account_credit').select2();
		
		/* btnSimpan */
		$('#btnSimpan').click(function(){
			const validate = ultracms.siaPostingProfileEditor._validateForm();
			if(validate.isValid){
				siaJS_confirm_form_submission('postingProfileForm');
			}else{
        siaJS_alert(validate.errMsgs);
				return false;
			}
		});
	},
	
	_validateForm: function(){
		let retval = {isValid: true, errMsgs: ""};

		/* Check empty fields */
		let emptyFields = [];
		if(ultracms.utils.isEmpty($('#account_debet').val())){
			emptyFields.push(ultracms.utils.getLang('sia_posting_profile.account_debit'));
		}
		if(ultracms.utils.isEmpty($('#account_credit').val())){
			emptyFields.push(ultracms.utils.getLang('sia_posting_profile.account_credit'));
		}
		if($('#hidenState').val() === 'ADD'){
		  const tipeTransaksi = $('#cmbTipeTransaksi').val();
			if(ultracms.utils.isEmpty(tipeTransaksi) || tipeTransaksi === '0'){
				emptyFields.push(ultracms.utils.getLang('sia_posting_profile.trx_type'));
			}
		}

		if(emptyFields.length > 0){
			const errHeading = ultracms.language.alerts.empty_fields;
			retval.isValid = false;
			retval.errMsgs = ultracms.utils.createErrorMessage(errHeading, emptyFields, '<br>');
			return retval;
		}
		return retval;
	},
	
	init: function(){
		this._initComp();
	}
	
};
