ultracms.siaTransaksiPendapatanList = {
	initComp : function() {
		$('#btnReset').click(function() {
			$('#txtTrxDateFrom').val('');
			$('#txtTrxDateTo').val('');
			$('#transaction_date_from').val('');
			$('#transaction_date_to').val('');
			/*$('#cmbTipeTransaksi').prop('selectedIndex',0);*/
			$('#txtNomor').val('');
			$('#txtNama').val('');
			$('#txtKeterangan').val('');
		});

		var datePickerOptsFrom = {
			locale : 'id',
			format : 'DD-MMM-YYYY',
			ignoreReadonly : true
		};
		$('#trxDatePickerFrom').datetimepicker(datePickerOptsFrom);
		$('#trxDatePickerFrom').on('dp.change', function(e) {
			$('#transaction_date_from').val(e.date.format('YYYY-MM-DD').toString());
		});
		
		var datePickerOptsTo = {
				locale : 'id',
				format : 'DD-MMM-YYYY',
				ignoreReadonly : true
			};
			$('#trxDatePickerTo').datetimepicker(datePickerOptsTo);
			$('#trxDatePickerTo').on('dp.change', function(e) {
				$('#transaction_date_to').val(e.date.format('YYYY-MM-DD').toString());
			});
		
	},
	validateForm : function() {
		/*if(($('#trxDatePickerFrom').val() == null && $('#trxDatePickerFrom').val() == '')){
			if(($('#trxDatePickerTo').val() != null && $('#trxDatePickerTo').val() == '')){
				alert('Tanggal Transaksi Awal Harus diisi');
				$('#trxDatePickerFrom').val('');
				$('#trxDatePickerTo').val('');
			}
		}*/
		
		
		
	},

	init : function() {
		this.initComp();
	}
	
	

};