<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
class SiaAdminUiHelper {

	/**
	 * Render SIA Admin page heading
	 * @param string $pageTitle Page title
	 * @param string $iconClass Current page icon class
	 * @return string Page heading html
	 */
	public function renderPageHeading($pageTitle, $iconClass){
		if(empty($pageTitle) || empty($iconClass)){
			return '';
		}else{
			return	'<div class="row">
						<div class="col-lg-12 col-md-12 col-xs-12">
							<h1 class="page-header margin-top-none"><small>'.$pageTitle.'</small></h1>
							<ol class="breadcrumb">
								<li><i class="fa fa-dashboard"></i> <a href="'.site_url('sia_dashboard').'">Dashboard</a></li>
								<li class="active"><i class="fa '.$iconClass.'"></i> '.$pageTitle.'</li>
							</ol>
						</div>
					</div>';
		}
	}



	public function visibleCorrectionButton($transactionNumber, $transactionId){
		if(empty($transactionNumber)){
			return false;
		}else{
			$CI =& get_instance();
			$CI->load->model(array("DAO_Transaksi", "DAO_Transaksi_Pendapatan"));
			$trx = $CI->DAO_Transaksi->getRowByCriteria("gen_trx_debet_credit", array("transaction_number"=>$transactionNumber, "ref_transaction_id"=>null));
			$trx2 = $CI->DAO_Transaksi->getRowByCriteria("gen_trx_debet_credit", array("ref_transaction_number"=>$transactionNumber, "ref_transaction_id"=>null));
			$retval = true;

			if(!is_null($trx)){
				if(!is_null($trx['ref_transaction_number'])){
					$retval = false;
				}
			}
			if(!is_null($trx2)){
				$retval = false;
			}
			
			if(empty($transactionId)){
				return $retval;
			}else{
				$trxp = $CI->DAO_Transaksi_Pendapatan->getRowByCriteria("gen_trx_pendapatan", array("correction_trx_id"=>$transactionId));
				if(!is_null($trxp)){
					$retval = false;
				}
			}
			return $retval;
		}
	}
}