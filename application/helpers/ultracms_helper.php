<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'SiaAdminUiHelper.php';

/**
 * UltraCMS Helper
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */

// BEGIN Session related functions ------------------------------------------------------------------------------------------

if (!function_exists("userHasRoles")) {

  /**
   * Checks if current logged user has certain role(s)
   * @param array $roles Role ids to check
   * @return boolean
   */
  function userHasRoles($roles)
  {
    if (is_null($roles) || empty($roles) || !is_array($roles)) {
      return false;
    } else {
      $assignedRoles = $_SESSION[SESSKEY_USERDATA]['assigned_roles'];
      $hasRole       = false;
      foreach ($roles as $role) {
        if (in_array($role, $assignedRoles)) {
          $hasRole = true;
          break;
        }
      }
      return $hasRole;
    }
  }

}

// END Session related functions --------------------------------------------------------------------------------------------

// BEGIN UI related functions -----------------------------------------------------------------------------------------------

if (!function_exists('scriptTag')) {

  /**
   * Create script tag
   * @param string $src Script source
   * @param string $type Script type
   * @return string Script tag
   */
  function scriptTag($src, $type = 'text/javascript')
  {
    $CI        =& get_instance();
    $appConfig = $CI->config->item('ultracms');

    if (!is_null($src) && !empty($src)) {
      $srcUrl = base_url($src) . "?v=" . $appConfig["appVersion"];
      return sprintf('<script src="%s" type="%s"></script>', $srcUrl, $type);
    } else {
      return '';
    }
  }

}

if (!function_exists('linkTag')) {
  function linkTag($href, $isRelPath = true)
  {
    $CI        =& get_instance();
    $appConfig = $CI->config->item('ultracms');

    if (!is_null($href) && !empty($href)) {
      $hrefUrl = $isRelPath ? base_url($href) : $href;
      $hrefUrl .= "?v=" . $appConfig["appVersion"];
      return sprintf('<link rel="stylesheet" type="text/css" href="%s"/>', $hrefUrl);
    } else {
      return '';
    }
  }
}

if (!function_exists('generateHeadTags')) {

  /**
   * Generate standard head tags
   * @param string $title Title tag
   * @param array  $meta Additional meta tags
   * @param array  $css Additional css files
   * @param array  $js Additional js files
   * @return string HTML Head tags
   */
  function generateHeadTags($title, $meta = [], $css = [], $js = [])
  {
    $CI         =& get_instance();
    $appConfig  = $CI->config->item('ultracms');
    $appLocale  = $appConfig['appLanguage'];
    $appName    = $appConfig['appName'];
    $appVersion = $appConfig['appVersion'];

    // Set locale to session's locale (if any)
    if (isset($_SESSION[SESSKEY_USERDATA]) && isset($_SESSION[SESSKEY_USERDATA]['locale'])) {
      $appLocale = $_SESSION[SESSKEY_USERDATA]['locale'];
    }

    // Standard meta tags
    $standardMeta = [
      '<meta charset="utf-8">',
      '<meta http-equiv="X-UA-Compatible" content="IE=edge">',
      '<meta name="viewport" content="width=device-width, initial-scale=1">',
      '<meta name="robots" content="noindex, nofollow">',
      sprintf('<title>%s | %s</title>', $title, $appName)
    ];

    // Standard css files
    $standardCss   = [];
    $standardCss[] = link_tag('assets/themes/sb-admin/css/bootstrap.min.css');
    $standardCss[] = link_tag('assets/themes/sb-admin/css/sb-admin.min.css?v=' . $appVersion);
    $standardCss[] = link_tag('assets/themes/sb-admin/font-awesome/css/font-awesome.min.css');
    $standardCss[] = link_tag('assets/themes/sb-admin/css/sia-admin.min.css?v=' . $appVersion);

    // Standard js files
    $languageJs   = sprintf("ultracms_language_%s.min.js", $appLocale);
    $standardJs   = [];
    $standardJs[] = '<!--[if lt IE 9]>' . PHP_EOL .
                    '<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>' . PHP_EOL .
                    '<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>' . PHP_EOL .
                    '<![endif]-->';
    $standardJs[] = scriptTag('assets/themes/sb-admin/js/jquery-2.2.4.min.js');
    $standardJs[] = scriptTag('assets/themes/sb-admin/js/bootstrap.min.js');
    $standardJs[] = scriptTag('assets/js/ultracms_base.min.js');
    $standardJs[] = scriptTag('assets/js/ultracms_helper_functions.min.js');
    $standardJs[] = scriptTag('assets/js/' . $languageJs);

    // Adding additional tags and files
    if (!is_null($meta) && is_array($meta)) {
      foreach ($meta as $additionalMeta) {
        $standardMeta[] = $additionalMeta;
      }
    }
    if (!is_null($css) && is_array($css)) {
      foreach ($css as $additionalCss) {
        $standardCss[] = link_tag($additionalCss);
      }
    }
    if (!is_null($js) && is_array($js)) {
      foreach ($js as $additionalJs) {
        $standardJs[] = scriptTag($additionalJs);
      }
    }

    // To string
    $strMeta = implode(PHP_EOL, $standardMeta);
    $strCss  = implode(PHP_EOL, $standardCss);
    $strJs   = implode(PHP_EOL, $standardJs);

    return $strMeta . PHP_EOL . $strCss . PHP_EOL . $strJs;
  }

}

if (!function_exists('siteUrl')) {

  /**
   * Site url with parameters
   * @param string $url Base url
   * @param array  $params The parameters
   */
  function siteUrl($url, $params = [])
  {
    if (is_null($url) || empty($url) || is_null($params) || !is_array($params)) {
      return "";
    } else {
      $retval = site_url($url);
      if (count($params) > 0) {
        $retval .= '?' . http_build_query($params, '', '&amp;');
      }
      return $retval;
    }
  }

}

if (!function_exists('generatePagingLinks')) {

  /**
   * Paging links generator
   * @param string  $baseUrl Base url for current paging
   * @param array   $urlParams Current url params
   * @param integer $recordCount Record count
   * @param integer $pageSize The page size
   * @param integer $currentPage Current page
   */
  function generatePagingLinks($baseUrl, $urlParams, $recordCount, $pageSize, $currentPage = 1)
  {
    if (is_null($baseUrl) || empty($baseUrl)) {
      return "";
    }
    if (is_null($urlParams) || empty($urlParams) || !is_array($urlParams)) {
      return "";
    }
    if (is_null($recordCount) || empty($recordCount) || !is_numeric($recordCount)) {
      return "";
    }
    if (is_null($pageSize) || empty($pageSize) || !is_numeric($pageSize)) {
      $pageSize = 10;
    }

    // Loading labels
    $CI     =& get_instance();
    $locale = isset($_SESSION[SESSKEY_USERDATA]['locale']) ? $_SESSION[SESSKEY_USERDATA]['locale'] : $CI->config->item('language');
    $CI->lang->load('sia_common_labels', $locale);
    $labels              = [];
    $labels['pagenav']   = $CI->lang->line('label_pagenav');
    $labels['pageInfo']  = $CI->lang->line('label_page_info');
    $labels['page']      = $CI->lang->line('label_page');
    $labels['of']        = $CI->lang->line('label_of');
    $labels['firstPage'] = $CI->lang->line('label_first_page');
    $labels['prevPage']  = $CI->lang->line('label_prev_page');
    $labels['nextPage']  = $CI->lang->line('label_next_page');
    $labels['lastPage']  = $CI->lang->line('label_last_page');
    $icons               = [];
    $icons['firstPage']  = '<i class="fa fa-fast-backward"></i>';
    $icons['prevPage']   = '<i class="fa fa-backward"></i>';
    $icons['nextPage']   = '<i class="fa fa-forward"></i>';
    $icons['lastPage']   = '<i class="fa fa-fast-forward"></i>';

    // Generating links...
    $pages = ceil($recordCount / $pageSize);
    if ($pages > 1) {
      $pagingLinks = sprintf('<nav aria-label="%s"><ul class="pagination pagination-sm">', $labels['pagenav']);
      // first & prev links
      if ($currentPage > 1) {
        $urlParams[QSPARAM_PAGE] = 1;
        $urlFirstPage            = siteUrl($baseUrl, $urlParams);
        $pagingLinks             .= sprintf('<li><a href="%s" aria-label="%s"><span aria-hidden="true">%s</span></a></li>', $urlFirstPage, $labels['firstPage'], $icons['firstPage']);

        $urlParams[QSPARAM_PAGE] = $currentPage - 1;
        $urlPrevPage             = siteUrl($baseUrl, $urlParams);
        $pagingLinks             .= sprintf('<li><a href="%s" aria-label="%s"><span aria-hidden="true">%s</span></a></li>', $urlPrevPage, $labels['prevPage'], $icons['prevPage']);
      } else {
        $pagingLinks .= sprintf('<li class="disabled"><a href="%s" aria-label="%s"><span aria-hidden="true">%s</span></a></li>', '#', $labels['firstPage'], $icons['firstPage']);
        $pagingLinks .= sprintf('<li class="disabled"><a href="%s" aria-label="%s"><span aria-hidden="true">%s</span></a></li>', '#', $labels['prevPage'], $icons['prevPage']);
      }
      // current page & total pages info
      $pagingLinks .= sprintf('<li class="disabled"><span>%s %d %s %d<span class="sr-only">%s</span></span></li>', $labels['page'], $currentPage, $labels['of'], $pages, $labels['pageInfo']);
      // next & last links
      if ($currentPage < $pages) {
        $urlParams[QSPARAM_PAGE] = $currentPage + 1;
        $urlNextPage             = siteUrl($baseUrl, $urlParams);
        $pagingLinks             .= sprintf('<li><a href="%s" aria-label="%s"><span aria-hidden="true">%s</span></a></li>', $urlNextPage, $labels['nextPage'], $icons['nextPage']);

        $urlParams[QSPARAM_PAGE] = $pages;
        $urlLastPage             = siteUrl($baseUrl, $urlParams);
        $pagingLinks             .= sprintf('<li><a href="%s" aria-label="%s"><span aria-hidden="true">%s</span></a></li>', $urlLastPage, $labels['lastPage'], $icons['lastPage']);
      } else {
        $pagingLinks .= sprintf('<li class="disabled"><a href="%s" aria-label="%s"><span aria-hidden="true">%s</span></a></li>', '#', $labels['firstPage'], $icons['nextPage']);
        $pagingLinks .= sprintf('<li class="disabled"><a href="%s" aria-label="%s"><span aria-hidden="true">%s</span></a></li>', '#', $labels['lastPage'], $icons['lastPage']);
      }
      $pagingLinks .= '</ul></nav>';
      return $pagingLinks;
    } else {
      return "";
    }
  }

}

if (!function_exists('formatDateForDisplay')) {

  /**
   * Format date for displaying it on form, list, ect
   * @param mixed $date The date
   * @return string The formatted date
   */
  function formatDateForDisplay($date)
  {
    $CI        =& get_instance();
    $appConfig = $CI->config->item('ultracms');

    if (is_null($date) || empty($date)) {
      return "";
    } else {
      $format = $appConfig['dateFormatForDisplay'];
      return date($format, strtotime($date));
    }
  }

}

if (!function_exists('formatDateForReport')) {

  /**
   * Format date for displaying it on form, list, ect
   * @param mixed $date The date
   * @return string The formatted date
   */
  function formatDateForReport($date)
  {
    $CI        =& get_instance();
    $appConfig = $CI->config->item('ultracms');

    if (is_null($date) || empty($date)) {
      return "";
    } else {
      $format = $appConfig['dateFormatForReport'];
      return date($format, strtotime($date));
    }
  }

}

if (!function_exists('formatMonthForDisplay')) {

  function formatMonthForDisplay($month)
  {
    $bulan = [
      '1'  => 'Januari',
      '2'  => 'Februari',
      '3'  => 'Maret',
      '4'  => 'April',
      '5'  => 'Mei',
      '6'  => 'Juni',
      '7'  => 'Juli',
      '8'  => 'Agustus',
      '9'  => 'September',
      '10' => 'Oktober',
      '11' => 'November',
      '12' => 'Desember',
    ];
    return $bulan[$month];
  }

}


if (!function_exists('createErrorMessages')) {

  /**
   * Create error message alert html
   * @param string $heading The error heading
   * @param array  $errorMessages The error messages
   * @return string Error message alert html
   */
  function createErrorMessages($heading, $errorMessages)
  {
    if (is_null($heading) || empty($heading) || is_null($errorMessages) || empty($errorMessages) || !is_array($errorMessages)) {
      return "";
    } else {
      $errList = ol($errorMessages);
      $errHtml = sprintf('<div class="alert alert-danger" role="alert"><b>%s</b><br>%s</div>', $heading, $errList);
      return $errHtml;
    }
  }

}

if (!function_exists('renderPageHeading')) {
  /**
   * Render SIA Admin page heading
   * @param string $pageTitle Page title
   * @param string $iconClass Current page icon class
   */
  function renderPageHeading($pageTitle, $iconClass)
  {
    $renderer = new SiaAdminUiHelper();
    return $renderer->renderPageHeading($pageTitle, $iconClass);
  }
}

if (!function_exists('printEmpty')) {
  /**
   * Print a value if not empty, otherwise print default string
   * @param Mixed  $value Value to print
   * @param string $default Default is "-"
   */
  function printEmpty($value, $default = "-")
  {
    echo !empty($value) ? $value : $default;
  }
}

if (!function_exists('visibleCorrectionButton')) {
  function visibleCorrectionButton($transactionNumber, $transactionId)
  {
    $trx = new SiaAdminUiHelper();
    return $trx->visibleCorrectionButton($transactionNumber, $transactionId);
  }
}

/**
 * Get lov label by its value
 * @param mixed $value The lov value
 * @param array $lov An array in key => value notation
 * @return string Lov label
 */
if (!function_exists('getLovLabel')) {
  function getLovLabel($value, $lov)
  {
    if (empty($value) || !is_array($lov) || !array_key_exists($value, $lov)) {
      return '';
    }
    return $lov[$value];
  }
}

if (!function_exists('rptPrintCompanyInfo')) {
  function rptPrintCompanyInfo($address, $phone, $fax, $email, $fb, $ig)
  {
    $companyInfoTemplate = '%s | Telp. %s | Fax %s<br>' .
                           '<i class="fa fa-envelope"></i> %s | <i class="fa fa-facebook-square"></i> %s | <i class="fa fa-instagram"></i> %s';
    echo sprintf($companyInfoTemplate, $address, $phone, $fax, $email, $fb, $ig);
  }
}

// END UI related functions -------------------------------------------------------------------------------------------------

// BEGIN Combobox options generator -----------------------------------------------------------------------------------------

if (!function_exists('transactionYearComboOption')) {
  /**
   * Menampilkan Tahun 2018 s/d Tahun sekarang + 1 untuk combobox transaksi
   */
  function transactionYearComboOption()
  {
    $currentYear      = (int)date('Y');
    $comboYearArray   = [];
    $comboYearArray[] = "-- Pilih Tahun Pembayaran --";
    for ($i = 2018; $i <= ($currentYear + 1); $i++) {
      $comboYearArray[$i] = $i;
    }
    return $comboYearArray;
  }
}

if (!function_exists('monthComboOptions')) {
  function monthComboOptions($showEmptyOption = true)
  {
    $monthArray = [];

    if ($showEmptyOption) $monthArray[0] = "-- Pilih Bulan --";
    $monthArray[1]  = "Januari";
    $monthArray[2]  = "Februari";
    $monthArray[3]  = "Maret";
    $monthArray[4]  = "April";
    $monthArray[5]  = "Mei";
    $monthArray[6]  = "Juni";
    $monthArray[7]  = "Juli";
    $monthArray[8]  = "Agustus";
    $monthArray[9]  = "September";
    $monthArray[10] = "Oktober";
    $monthArray[11] = "Nopember";
    $monthArray[12] = "Desember";

    return $monthArray;
  }
}

if (!function_exists('emptyComboOption')) {
  function emptyComboOption($label, $padding = "--")
  {
    return sprintf("%s %s %s", $padding, $label, $padding);
  }
}

// END Combobox options generator -------------------------------------------------------------------------------------------


// BEGIN Formater & Parser --------------------------------------------------------------------------------------------------

if (!function_exists('sanitizeNumber')) {
  function sanitizeNumber($str)
  {
    if (is_null($str) || empty($str)) {
      return "0";
    }
    return preg_replace("/[^0-9\.]/", "", $str);
  }
}

if (!function_exists('parseFloat')) {
  function parseFloat($str)
  {
    $num = sanitizeNumber($str);
    return (float)$num;
  }
}

if (!function_exists('parseInt')) {
  function parseInt($str)
  {
    $num = sanitizeNumber($str);
    return (int)$num;
  }
}

// END Formater & Parser ----------------------------------------------------------------------------------------------------

