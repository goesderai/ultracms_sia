<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*
|--------------------------------------------------------------------------
| ULTRA CMS - SIA Constants
|--------------------------------------------------------------------------
*/

/*
 * Query String Parameters
 * prefix: QSPARAM_
 */
defined('QSPARAM_PAGE')      OR define('QSPARAM_PAGE', 'p');
defined('QSPARAM_PAGE_SIZE') OR define('QSPARAM_PAGE_SIZE', 'pz');
defined('QSPARAM_REC_ID')    OR define('QSPARAM_REC_ID', 'id');
defined('QSPARAM_TRX_TYP')   OR define('QSPARAM_TRX_TYP', 'typ');

/*
 * Query filter keys
 * prefix: QRYFILTER_
 */
defined('QRYFILTER_FIELD')    OR define('QRYFILTER_FIELD', 'field');
defined('QRYFILTER_OPERATOR') OR define('QRYFILTER_OPERATOR', 'operator');
defined('QRYFILTER_VALUE')    OR define('QRYFILTER_VALUE', 'value');

/*
 * Query filter operator keys
 * prefix: QRYFILTER_OPERATOR_
 * contoh penggunaan operator OR:
 * $filter = array(
 *   QRYFILTER_FIELD => array('field_1', 'field_2'),
 *   QRYFILTER_OPERATOR => array(
 *     QRYFILTER_OPERATOR_OR,   // Operator utama
 *     QRYFILTER_OPERATOR_LIKE, // Operator untuk field_1
 *     QRYFILTER_OPERATOR_LIKE  // Operator untuk field_2
 *   ),
 *   QRYFILTER_VALUE => array(
 *     $value_1, // Value untuk field_1
 *     $value_2  // Value untuk field_1
 *   )
 * );
 */
defined('QRYFILTER_OPERATOR_EQUAL')                    OR define('QRYFILTER_OPERATOR_EQUAL', '=');
defined('QRYFILTER_OPERATOR_NOT_EQUAL')                OR define('QRYFILTER_OPERATOR_NOT_EQUAL', '!=');
defined('QRYFILTER_OPERATOR_IN')                       OR define('QRYFILTER_OPERATOR_IN', 'in');
defined('QRYFILTER_OPERATOR_NOT_IN')                   OR define('QRYFILTER_OPERATOR_NOT_IN', 'not in');
defined('QRYFILTER_OPERATOR_LIKE')                     OR define('QRYFILTER_OPERATOR_LIKE', 'like');
defined('QRYFILTER_OPERATOR_ISNULL')                   OR define('QRYFILTER_OPERATOR_ISNULL', 'is null');
defined('QRYFILTER_OPERATOR_BETWEEN')                  OR define('QRYFILTER_OPERATOR_BETWEEN', 'between');
defined('QRYFILTER_OPERATOR_OR')                       OR define('QRYFILTER_OPERATOR_OR', 'or');
defined('QRYFILTER_OPERATOR_GREATER_THAN')             OR define('QRYFILTER_OPERATOR_GREATER_THAN', '>');
defined('QRYFILTER_OPERATOR_SMALLER')                  OR define('QRYFILTER_OPERATOR_SMALLER', '<');
defined('QRYFILTER_OPERATOR_MORE_THAN_OR_EQUAL_TO')    OR define('QRYFILTER_OPERATOR_MORE_THAN_OR_EQUAL_TO', '>=');
defined('QRYFILTER_OPERATOR_IS_LESS_THAN_OR_EQUAL_TO') OR define('QRYFILTER_OPERATOR_IS_LESS_THAN_OR_EQUAL_TO', '<=');


/*
 * Alert types keys
 * prefix: ALERTTYPE_
 */
defined('ALERTTYPE_SUCCESS')  OR define('ALERTTYPE_SUCCESS', 'alert-success');
defined('ALERTTYPE_INFO')     OR define('ALERTTYPE_INFO', 'alert-info');
defined('ALERTTYPE_WARNING')  OR define('ALERTTYPE_WARNING', 'alert-warning');
defined('ALERTTYPE_DANGER')   OR define('ALERTTYPE_DANGER', 'alert-danger');

/*
 * View data keys
 * prefix: VDATAKEY_
 */
defined('VDATAKEY_APP_NAME')                OR define('VDATAKEY_APP_NAME', 'appName');
defined('VDATAKEY_APP_VERSION')             OR define('VDATAKEY_APP_VERSION', 'appVersion');
defined('VDATAKEY_APP_LANGUAGE')            OR define('VDATAKEY_APP_LANGUAGE', 'appLanguage');
defined('VDATAKEY_PAGE_TITLE')              OR define('VDATAKEY_PAGE_TITLE', 'pageTitle');
defined('VDATAKEY_PAGE_ICON')               OR define('VDATAKEY_PAGE_ICON', 'pageIcon');
defined('VDATAKEY_PAGE_CSS')                OR define('VDATAKEY_PAGE_CSS', 'pageCss');
defined('VDATAKEY_PAGE_JS')                 OR define('VDATAKEY_PAGE_JS', 'pageJs');
defined('VDATAKEY_PAGE_JS_INITFUNC')        OR define('VDATAKEY_PAGE_JS_INITFUNC', 'pageJsInitFunc');
defined('VDATAKEY_RECORDSET')               OR define('VDATAKEY_RECORDSET', 'recordset');
defined('VDATAKEY_RECORDCOUNT')             OR define('VDATAKEY_RECORDCOUNT', 'recordcount');
defined('VDATAKEY_ROWNUM_START')            OR define('VDATAKEY_ROWNUM_START', 'rownumStart');
defined('VDATAKEY_POST_DATA')               OR define('VDATAKEY_POST_DATA', 'postData');
defined('VDATAKEY_FORM_ACTION_URL')         OR define('VDATAKEY_FORM_ACTION_URL', 'formActionUrl');
defined('VDATAKEY_ALERT_TYPE')              OR define('VDATAKEY_ALERT_TYPE', 'alertType');
defined('VDATAKEY_ALERT_MESSAGE')           OR define('VDATAKEY_ALERT_MESSAGE', 'alertMessage');
defined('VDATAKEY_ALERT_PROCEED_URL')       OR define('VDATAKEY_ALERT_PROCEED_URL', 'alertProceedUrl');
defined('VDATAKEY_LIST_PAGE')               OR define('VDATAKEY_LIST_PAGE', 'listPage');
defined('VDATAKEY_LIST_PAGE_SIZE')          OR define('VDATAKEY_LIST_PAGE_SIZE', 'listPageSize');
defined('VDATAKEY_EDITOR_STATE')            OR define('VDATAKEY_EDITOR_STATE', 'editorState');
defined('VDATAKEY_ERROR_MESSAGES')          OR define('VDATAKEY_ERROR_MESSAGES', 'errorMessages');
defined('VDATAKEY_SIDEBAR_MENU_GENERATOR')  OR define('VDATAKEY_SIDEBAR_MENU_GENERATOR', 'sidebarMenuGen');
defined('VDATAKEY_INVOICE_URL')             OR define('VDATAKEY_INVOICE_URL', 'invoiceUrl');
defined('VDATAKEY_INVOICE_DATA')            OR define('VDATAKEY_INVOICE_DATA', 'invoiceData');

/*
 * Session data keys
 * prefix: SESSKEY_
 */
defined('SESSKEY_USERDATA') OR define('SESSKEY_USERDATA', 'userData');

/*
 * Editor states
 */
defined('EDITOR_STATE_INSERT') OR define('EDITOR_STATE_INSERT', 'insert');
defined('EDITOR_STATE_UPDATE') OR define('EDITOR_STATE_UPDATE', 'update');

/*
 * Role IDs
 * prefix: ROLEID_
 */
defined('ROLEID_ROOT')       OR define('ROLEID_ROOT', 1);
defined('ROLEID_ADMIN')      OR define('ROLEID_ADMIN', 2);
defined('ROLEID_DATA_ENTRY') OR define('ROLEID_DATA_ENTRY', 3);
defined('ROLEID_CASHIER')    OR define('ROLEID_CASHIER', 4);

/*
 * Report generator fields/properties
 * prefix: RPTGEN_
 */
defined('RPTGEN_MYSQL_RESOURCE')          or define('RPTGEN_MYSQL_RESOURCE', 'mysql_resource');
defined('RPTGEN_HEADER')                  or define('RPTGEN_HEADER', 'header');
defined('RPTGEN_FOOTER')                  or define('RPTGEN_FOOTER', 'footer');
defined('RPTGEN_FIELDS_WIDTH')            or define('RPTGEN_FIELDS_WIDTH', 'fieldsWidth');
defined('RPTGEN_FIELDS')                  or define('RPTGEN_FIELDS', 'fields');
defined('RPTGEN_DATE_FIELDS')             or define('RPTGEN_DATE_FIELDS', 'dateFields');
defined('RPTGEN_NUMBER_FIELDS')           or define('RPTGEN_NUMBER_FIELDS', 'numberFields');
defined('RPTGEN_NUMBER_FIELDS_SUMMARIES') or define('RPTGEN_NUMBER_FIELDS_SUMMARIES', 'numberFieldsSummaries');
defined('RPTGEN_ROW_NUMBER')              or define('RPTGEN_ROW_NUMBER', 'row_number');
defined('RPTGEN_SHOW_NUMBER_COLUMN')      or define('RPTGEN_SHOW_NUMBER_COLUMN', 'show_number_column');
