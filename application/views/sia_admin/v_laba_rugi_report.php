<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $pageTitle . ' | ' . $appName; ?></title>
  <link
    href="<?php echo base_url('assets/themes/sb-admin/css/bootstrap.min.css'); ?>"
    rel="stylesheet" type="text/css"/>
  <link
    href="<?php echo base_url('assets/themes/sb-admin/font-awesome/css/font-awesome.min.css'); ?>"
    rel="stylesheet" type="text/css"/>
  <link
    href="<?php echo base_url('assets/themes/sb-admin/css/sia-report.min.css'); ?>"
    rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="container"><!-- BEGIN report -->
  <div class="report-section report-header">
    <h3><?php echo $COMPANY_NAME; ?></h3>
    <p><?php echo sprintf("%s | %s | %s<br>%s", $COMPANY_ADDRESS, $COMPANY_PHONE, $COMPANY_FAX, $COMPANY_EMAIL); ?></p>
  </div>
  <div class="row">
    <div align="center">
      <h3><label>Laporan Laba-Rugi</label></h3>
      <h5><label><?php echo $ket . formatDateForReport($tanggalDari) ?>
          s/d <?php echo formatDateForReport($tanggalSampai) ?></label></h5>
    </div>
  </div>

  <div class="report-section report-body">
    <div class="row">
      <div style="float: right">
        <h5><label></label></h5>
      </div>
    </div>
    <div class="row">
      <table width="100%">
        <tr>
          <td width="49%" valign="top">
            <table class="table table-bordered table-hover table-striped">
              <tbody>
              <tr>
                <td align="left" colspan="3"><b>PENDAPATAN</b></td>
              </tr>
              <?php
              $sumPendapatan = 0;
              $sqlPendapatan = $DAO_Transaksi->getPendapatan($filters);
              $recordsetPendapatan = $DAO_Transaksi->db->query($sqlPendapatan);

              $i = 1;
              foreach ($recordsetPendapatan->result() as $row):
                if ($row->saldo <> 0) {

                  ?>
                  <tr>
                    <td style="text-align: right;"><?php echo $i ?></td>
                    <td align="left"><?php echo isset($row->nama) ? $row->nama : '-' ?></td>
                    <td
                      align="right"><?php echo isset($row->saldo) ? number_format((float)$row->saldo, 2, '.', ',') : 0 ?></td>
                  </tr>
                  <?php ;
                }
                $sumPendapatan = $sumPendapatan + $row->saldo;
                $i++;
              endforeach;
              ?>
              <tr>
                <td align="left" colspan="2"><b>Jumlah Pendapatan</b></td>
                <td align="right"><b><?php echo number_format((float)$sumPendapatan, 2, '.', ',') ?></b></td>
              </tr>
              <tr>
                <td colspan="3">&nbsp;</td>
              </tr>
              <tr>
                <td align="left" colspan="3"><b>BIAYA / BEBAN</b></td>
              </tr>
              <?php
              $sumBeban = 0;
              $sqlBeban = $DAO_Transaksi->getBeban($filters);
              $recordsetBeban = $DAO_Transaksi->db->query($sqlBeban);
              $i = 1;
              foreach ($recordsetBeban->result() as $row):
                if ($row->saldo <> 0) { ?>
                  <tr>
                    <td style="text-align: right;"><?php echo $i ?></td>
                    <td align="left"><?php echo isset($row->nama) ? $row->nama : '-' ?></td>
                    <td
                      align="right"><?php echo isset($row->saldo) ? number_format((float)$row->saldo, 2, '.', ',') : 0 ?></td>
                  </tr>
                  <?php ;
                }
                $sumBeban = $sumBeban + $row->saldo;
                $i++;
              endforeach;
              ?>
              <tr>
                <td align="left" colspan="2"><b>Jumlah Biaya/Beban (2)</b></td>
                <td align="right"><b><?php echo number_format((float)$sumBeban, 2, '.', ',') ?></b></td>
              </tr>

              <tr>
                <td colspan="3">&nbsp;</td>
              </tr>
              <tr>
                <td align="left" colspan="2"><b>Laba Rugi (1) - (2)</b></td>
                <td align="right"><b><?php echo number_format((float)$sumPendapatan - $sumBeban, 2, '.', ',') ?></b>
                </td>
              </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </table>
    </div>

  </div>
  <div class="report-section">
    <button type="button" class="btn btn-primary btn-sm hidden-print"
            onclick="javascript:window.print();"><i class="fa fa-print"></i> Print
    </button>
  </div>
  <!-- END report --></div>
</body>
</html>
