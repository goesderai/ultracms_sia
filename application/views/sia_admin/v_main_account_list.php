<!-- Page Content -->
<?php echo form_open('sia_main_account/index', 'method="get"'); ?>
<!-- Search Filter -->
<div class="row">
  <div class="col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtAkun">Nomor Akun</label>
      <?php
      $mainAccount = $DAO_Main_Account->getAllAsArray();
      $selectedMainAccount = isset($postData['account_code']) ? $postData['account_code'] : '';
      $cmbMainAccountAttr = array('id' => 'account_code', 'class' => 'form-control input-sm');
      echo form_dropdown('account_code', $mainAccount, $selectedMainAccount, $cmbMainAccountAttr);
      ?>
    </div>
  </div>
  <div class="col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtNama">Nama Akun</label>
      <input id="txtNama" name="name" type="text" class="form-control input-sm"
             value="<?php echo isset($postData['name']) ? $postData['name'] : ''; ?>"/>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtParent">Parent</label>
      <?php
      $parent = $DAO_Main_Account->getAllAsArray();
      $selectedparent = isset($postData['parent']) ? $postData['parent'] : '';
      $cmbparentAttr = array('id' => 'parent', 'class' => 'form-control input-sm');
      echo form_dropdown('parent', $parent, $selectedparent, $cmbparentAttr);
      ?>
    </div>
  </div>
</div><!-- /.row -->
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel-upper-buttons">
      <button id="btnCari" type="submit" class="btn btn-primary btn-sm">
        <i class="fa fa-search"></i> <?php echo lang('label_search'); ?>
      </button>
      <button id="btnReset" type="button" class="btn btn-primary btn-sm">
        <i class="fa fa-undo"></i> <?php echo lang('label_reset'); ?>
      </button>
      <a href="<?php echo site_url('sia_main_account/insert'); ?>" class="btn btn-primary btn-sm"
         title="<?php echo lang('label_add_new_hint'); ?>">
        <i class="fa fa-plus"></i> <?php echo lang('label_add_new'); ?>
      </a>
    </div>
  </div>
</div><!-- /.row -->
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <?php
    if (isset($errorMessages) && !empty($errorMessages)) {
      $errHeading = lang('message_error_data_deletion');
      echo createErrorMessages($errHeading, $errorMessages);
    }
    ?>
  </div>
</div><!-- /.row -->

<!-- Search Result -->
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="table-responsive">
      <table class="table table-bordered table-hover table-striped">
        <thead>
        <tr>
          <th width="5%" class="text-center"><?php echo lang('label_rownum'); ?></th>
          <th width="15%" class="text-center"><?php echo lang('main_account_code'); ?></th>
          <th width="25%" class="text-center"><?php echo lang('main_account_name'); ?></th>
          <th width="30%" class="text-center"><?php echo lang('main_account_parent'); ?></th>
          <th width="10%" class="text-center"><?php echo lang('label_action'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $baseUrlParams = array(QSPARAM_PAGE => $listPage, QSPARAM_PAGE_SIZE => $listPageSize) + $_GET;
        if (isset($recordset)):
          $i = $rownumStart + 1;
          foreach ($recordset as $row):
            $urlParams = $baseUrlParams + array(QSPARAM_REC_ID => $row->id);
            $editUrl = siteUrl('sia_main_account/update', $urlParams);
            $deleteUrl = siteUrl('sia_main_account/delete', $urlParams);
            ?>
            <tr>
              <td><?php echo $i; ?></td>
              <td><a href="<?php echo $editUrl; ?>" title="<?php echo lang('label_edit'); ?>">
                  <?php echo $row->account_code; ?></a></td>
              <td><?php echo $row->name; ?></td>
              <td><?php $mainAccountParent = $DAO_Main_Account->getMainAccountName($row->parent);
                echo $mainAccountParent; ?></td>
              <td class="text-center">
                <a href="javascript:void(0);"
                   onclick="siaJS_confirm('questions.confirm_delete', '<?php echo $deleteUrl; ?>');"
                   title="<?php echo lang('label_delete'); ?>" class="btn btn-danger btn-xs">
                  <i class="fa fa-times"></i>
                </a>
              </td>
            </tr>
            <?php
            unset($urlParams, $editUrl);
            $i++;
          endforeach;
        endif;
        ?>
        </tbody>
      </table>

      <?php if (isset($recordcount)):
        $pagingLinks = generatePagingLinks('sia_main_account/index', $baseUrlParams, $recordcount, $listPageSize, $listPage);
        ?>
        <div class="pull-right"><?php echo $pagingLinks; ?></div>
      <?php endif; ?>
    </div>
  </div>
</div><!-- /.row -->
<?php echo form_close(); ?>
