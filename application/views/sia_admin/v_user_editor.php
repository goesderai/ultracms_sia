<!-- BEGIN Form -->
<?php echo form_open($formActionUrl, 'id="userForm"'); ?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <?php
    if (isset($errorMessages) && !empty($errorMessages)) {
      $errHeading = lang('message_error_mandatory_fields');
      echo createErrorMessages($errHeading, $errorMessages);
    }
    ?>
    <?php include_once 'includes/user_main_form.php'; ?>
  </div>
</div><!-- /.row -->
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel-upper-buttons">
      <button id="btnSimpan" type="button" class="btn btn-primary btn-sm" title="Simpan perubahan">
        <i class="fa fa-save"></i> <?php echo lang('label_save'); ?>
      </button>
      <a href="javascript:void(0);" class="btn btn-primary btn-sm"
         onclick="siaJS_confirm('questions.confirm_close', '<?php echo site_url('sia_users/index'); ?>')">
        <i class="fa fa-minus-circle"></i> <?php echo lang('label_close'); ?>
      </a>
    </div>
  </div>
</div><!-- /.row -->
<input type="hidden" name="id" value="<?php echo isset($postData['id']) ? $postData['id'] : ''; ?>"/>
<input id="editorState" type="hidden" value="<?php echo $editorState; ?>"/>
<!-- END Form -->
