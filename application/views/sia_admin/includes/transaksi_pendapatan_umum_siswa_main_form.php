<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtNoTransaksi">Nomor Transaksi</label>
      <input id="txtNoTransaksi" name="transaction_number" type="text" class="form-control input-sm" readonly="readonly"
             value="<?php echo isset($postData['transaction_number']) ? $postData['transaction_number'] : ''; ?>"/>
    </div>
  </div>

  <div class="col-lg-3 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtTrxDate">Tanggal Transaksi</label>
      <div id="trxDatePicker" class="input-group date">
        <input id="txtTrxDate" name="transaction_date" type="text" class="form-control input-sm"
               readonly="readonly" <?php if ($editorState == 'EDIT' || $editorState == 'KOREKSI') {
          echo 'disabled="disabled"';
        } ?>
               value="<?php echo isset($postData['transaction_date']) ? formatDateForDisplay($postData['transaction_date']) : formatDateForDisplay(date('Y-m-d')); ?>"/>
        <span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
      </div>
      <input id="trx_date" name="trx_date" type="hidden"
             value="<?php echo isset($postData['transaction_date']) ? $postData['transaction_date'] : date('Y-m-d'); ?>"/>
    </div>
  </div>

  <div class="col-lg-3 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtCreatedBy">Nama Pegawai</label>
      <input id="txtCreatedByName" name="created_by_name" type="text" class="form-control input-sm" readonly="readonly"
             value="<?php
             $user = isset($postData['created_by']) ? $DAO_Security->getUserById($postData['created_by']) : $DAO_Security->getUserById($userData['id']);
             echo $user['username']; ?>"/>
      <input type="hidden" id="txtCreatedBy" name="created_by" type="text" class="form-control input-sm"
             readonly="readonly"
             value="<?php echo isset($postData['created_by']) ? $postData['created_by'] : $userData['id']; ?>"/>
    </div>
  </div>
</div><!-- /.row -->
<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <input id="trx_typ" name="trx_typ" type="hidden"/>
      <label for="cmbTipeTransaksi">Tipe Transaksi</label>
      <?php
      if ($editorState == 'EDIT' || $editorState == 'KOREKSI') {
        $value = isset($postData['transaction_type']) ? $postData['transaction_type'] : '';
        $transactionType = $DAO_TipeTransaksi->getRow($value);
        $parentJenisTransaksi = $DAO_TipeTransaksi->getRow($transactionType['parent_type']);
        echo '<input id="txtTipeTransaksi" type="text" class="form-control input-sm" readonly="true"
						value="' . $parentJenisTransaksi['nama'] . '" />';
        echo '<input id="txthTipeTransaksi" name="transaction_type" type="hidden" class="form-control input-sm"
						value="' . $value . '" />';
      } else {
        $tipeTransaksi = $DAO_TipeTransaksi->getPendapatanTransaction('D', 2, null, 0, array(emptyComboOption(lang('label_all'))));
        $selectedtipeTransaksi = isset($postData['transaction_type']) ? $postData['transaction_type'] : '';
        $cmbTipeTransaksiAttr = array('id' => 'cmbTipeTransaksi', 'class' => 'form-control input-sm');
        echo form_dropdown('transaction_type', $tipeTransaksi, $selectedtipeTransaksi, $cmbTipeTransaksiAttr);
      }

      ?>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-xs-12">
    <div class="form-group">
      <input id="trx_typ2" name="trx_typ2" type="hidden"/>
      <label for="cmbTipeTransaksi2">Sub Jenis Transaksi <span class="sign sign-mandatory">*</span></label>
      <?php
      if ($editorState == 'EDIT' || $editorState == 'KOREKSI') {
        $value = isset($postData['transaction_type']) ? $postData['transaction_type'] : '';
        $transactionType = $DAO_TipeTransaksi->getRow($value);
        if ($transactionType['is_sub_type'] == 1) {
          echo '<input id="txtSubTipeTransaksi" type="text" class="form-control input-sm" readonly="true"
							value="' . $transactionType['nama'] . '" />';
          echo '<input id="txthSubTipeTransaksi" name="sub_transaction_type" type="hidden" class="form-control input-sm"
							value="' . $value . '" />';
        } else {
          echo '<input id="txtSubTipeTransaksi" type="text" class="form-control input-sm" readonly="true"
							value="" />';
          echo '<input id="txthSubTipeTransaksi" name="sub_transaction_type" type="hidden" class="form-control input-sm"
							value="" />';
        }
      } else {
        $cmbTipeTransaksiAttr = array('id' => 'cmbJenSubType', 'class' => 'form-control input-sm');
        echo form_dropdown('sub_transaction_type', null, null, $cmbTipeTransaksiAttr);
      }
      /*}*/
      ?>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="cmbStayedAt">Tahun Ajaran</label>
      <?php
      $tahunAjaranOptions = array('' => '-- Pilih --') + $DAO_Lov->getLovs('TAHUN_AJARAN');
      $selectedTahunAjaran = isset($postData['tahun_ajaran']) ? $postData['tahun_ajaran'] : '';
      $cmbTahunAjaranAttr = array('id' => 'cmbTahunAjaran', 'class' => 'form-control input-sm', 'disabled' => 'true');
      echo form_dropdown('tahun_ajaran', $tahunAjaranOptions, $selectedTahunAjaran, $cmbTahunAjaranAttr);
      ?>
    </div>
  </div>
</div><!-- /.row -->
<div class="row">
  <div class="col-lg-3 col-md-6 col-xs-12">
    <div class="form-group">
      <?php
      $studentId = isset($postData['student_id']) ? $postData['student_id'] : '';
      $student = $DAO_Siswa->getRow($studentId);
      ?>
      <label for="txtSiswa">Nama Siswa <span class="sign sign-mandatory">*</span></label>
      <div class="input-group">
        <input type="text" id="txtSiswa" class="form-control input-sm" readonly="readonly"
               value="<?php echo !empty($studentId) ? $student['nis'] . ' - ' . $student['name'] : ''; ?>"/>
        <input type="hidden" id="txtIdSiswa" name="student_id"
               value="<?php echo $studentId; ?>"/>
        <span class="input-group-btn">
                    <button type="button" id="btnBrowseSiswa" class="btn btn-primary btn-sm"
                      <?php echo $editorState == 'EDIT' || $editorState == 'KOREKSI' ? 'disabled="disabled"' : '' ?>>
                      <i class="fa fa-search"></i>
                    </button>
                  </span>
      </div>
    </div>
  </div>

  <div class="col-lg-3 col-md-4 col-xs-12">
    <div class="form-group">
      <label for="txtAmount">Jumlah Transaksi</label>
      <input id="txtJmlTrx" name="jumlah_transaksi" type="text" class="form-control input-sm" style="text-align:right;"
        <?php if ($editorState == 'EDIT' || $editorState == 'KOREKSI') {
          echo 'readonly="true"';
        } ?>
             value="<?php echo isset($postData['jumlah_transaksi']) ? number_format((float)$postData['jumlah_transaksi'], 2, '.', ',') : 0; ?>"/>
    </div>
  </div>
  <div class="col-lg-3 col-md-2 col-xs-12">
    <div class="form-group">
      <label for="txtDiskon">Diskon (Rp.)</label>
      <input id="txtDiskon" type="text" name="diskon" class="form-control input-sm" style="text-align:right;"
        <?php if ($editorState == 'EDIT' || $editorState == 'KOREKSI') {
          echo 'readonly="true"';
        } ?>
             value="<?php echo isset($postData['diskon']) ? number_format((float)$postData['diskon'], 2, '.', ',') : 0; ?>">
    </div>

  </div>
  <div class="col-lg-3 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtTotalBayar">Total Bayar</label>
      <input id="txtTotalBayar" name="total_bayar" type="text" class="form-control input-sm" style="text-align:right;"
             readonly="readonly"
             value="<?php echo isset($postData['total_bayar']) ? number_format((float)$postData['total_bayar'], 2, '.', ',') : 0; ?>"/>
    </div>
  </div>

</div><!-- /.row -->
<div class="row">


</div><!-- /.row -->
<div class="row">
  <div class="col-lg-3 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtBayar">Bayar</label>
      <input id="txtBayar" name="dibayar" type="text" class="form-control input-sm" style="text-align:right;"
        <?php if ($editorState == 'EDIT' || $editorState == 'KOREKSI') {
          echo 'readonly="true"';
        } ?>
             value="<?php echo isset($postData['dibayar']) ? number_format((float)$postData['dibayar'], 2, '.', ',') : 0; ?>"/>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtSisaBayar">Sisa Bayar</label>
      <input id="txtSisaBayar" name="sisa_bayar" type="text" class="form-control input-sm" style="text-align:right;"
             readonly="readonly"
             value="<?php echo isset($postData['sisa_bayar']) ? number_format((float)$postData['sisa_bayar'], 2, '.', ',') : 0; ?>"/>
    </div>
  </div>
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtRemark">Keterangan</label>
      <textarea id="txtRemark" name="remark" type="text"
                class="form-control input-sm"><?php echo isset($postData['remark']) ? $postData['remark'] : ''; ?></textarea>
    </div>
  </div>
  <input type="hidden" id="sisa_hutang" value="0"/>
</div>
<?php include_once 'popups/single_result_student_lookup_dialog.php'; ?>

