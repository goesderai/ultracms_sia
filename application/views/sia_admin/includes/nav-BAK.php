<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="#"><?php echo $appName;?></a>
	</div>
	<!-- Top Menu Items -->
	<ul class="nav navbar-right top-nav">
		<!--
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
			<ul class="dropdown-menu message-dropdown">
				<li class="message-preview">
					<a href="#">
						<div class="media">
							<span class="pull-left">
								<img class="media-object" src="http://placehold.it/50x50" alt="">
							</span>
							<div class="media-body">
								<h5 class="media-heading">
									<strong>John Smith</strong>
								</h5>
								<p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
								<p>Lorem ipsum dolor sit amet, consectetur...</p>
							</div>
						</div>
					</a>
				</li>
				<li class="message-preview">
					<a href="#">
						<div class="media">
							<span class="pull-left">
								<img class="media-object" src="http://placehold.it/50x50" alt="">
							</span>
							<div class="media-body">
								<h5 class="media-heading">
									<strong>John Smith</strong>
								</h5>
								<p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
								<p>Lorem ipsum dolor sit amet, consectetur...</p>
							</div>
						</div>
					</a>
				</li>
				<li class="message-preview">
					<a href="#">
						<div class="media">
							<span class="pull-left">
								<img class="media-object" src="http://placehold.it/50x50" alt="">
							</span>
							<div class="media-body">
								<h5 class="media-heading">
									<strong>John Smith</strong>
								</h5>
								<p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
								<p>Lorem ipsum dolor sit amet, consectetur...</p>
							</div>
						</div>
					</a>
				</li>
				<li class="message-footer">
					<a href="#">Read All New Messages</a>
				</li>
			</ul>
		</li>
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
			<ul class="dropdown-menu alert-dropdown">
				<li>
					<a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
				</li>
				<li>
					<a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
				</li>
				<li>
					<a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
				</li>
				<li>
					<a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
				</li>
				<li>
					<a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
				</li>
				<li>
					<a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
				</li>
				<li class="divider"></li>
				<li>
					<a href="#">View All</a>
				</li>
			</ul>
		</li>
		-->
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				<i class="fa fa-user"></i> <?php echo $_SESSION[SESSKEY_USERDATA]['name'];?> <b class="caret"></b>
			</a>
			<ul class="dropdown-menu user-menu">
				<!--
				<li>
					<a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
				</li>
				<li>
					<a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
				</li>
				-->
				<li>
					<a href="<?php echo site_url('sia_change_password');?>"><i class="fa fa-fw fa-gear"></i> Change Password</a>
				</li>
				<li class="divider"></li>
				<li>
					<a href="<?php echo site_url('sia_login/logout');?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
				</li>
			</ul>
		</li>
	</ul>
	<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
	<div class="collapse navbar-collapse navbar-ex1-collapse">
		<ul class="nav navbar-nav side-nav">
			<li>
				<a href="index.html"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
			</li>
			<li>
				<a href="javascript:;" data-toggle="collapse" data-target="#admin"><i class="fa fa-fw fa-folder"></i> System Settings <i class="fa fa-fw fa-caret-down"></i></a>
				<ul id="admin" class="collapse">
					<li>
						<a href="<?php echo site_url('sia_posting_profile');?>"><i class="fa fa-fw fa-link"></i> Posting Profile</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="javascript:;" data-toggle="collapse" data-target="#master"><i class="fa fa-fw fa-folder"></i> Master Data <i class="fa fa-fw fa-caret-down"></i></a>
				<ul id="master" class="collapse">
					<li>
						<a href="<?php echo site_url('sia_students');?>"><i class="fa fa-fw fa-graduation-cap"></i> Siswa</a>
					</li>
					<li>
						<a href="<?php echo site_url('sia_employees');?>"><i class="fa fa-fw fa-users"></i> Karyawan</a>
					</li>
					<li>
						<a href="<?php echo site_url('sia_main_account');?>"><i class="fa fa-fw fa-sitemap"></i> Kode Akun</a>
					</li>
					<li>
						<a href="<?php echo site_url('sia_transaction_type');?>"><i class="fa fa-fw fa-sitemap"></i> Tipe Transaksi</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="javascript:;" data-toggle="collapse" data-target="#transaksi"><i class="fa fa-fw fa-folder"></i> Transaksi <i class="fa fa-fw fa-caret-down"></i></a>
				<ul id="transaksi" class="collapse">
					<li>
						<a href="<?php echo site_url('sia_transaksi_spp');?>"><i class="fa fa-fw fa-usd"></i> Pembayaran SPP</a>
					</li>
					<li>
						<a href="<?php echo site_url('sia_transaksi_pendapatan');?>"><i class="fa fa-fw fa-usd"></i> Transaksi Pendapatan</a>
					</li>
					<li>
						<a href="<?php echo site_url('sia_transaksi_pembayaran');?>"><i class="fa fa-fw fa-usd"></i> Transaksi Pembayaran</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="javascript:;" data-toggle="collapse" data-target="#report"><i class="fa fa-fw fa-folder"></i> Report <i class="fa fa-fw fa-caret-down"></i></a>
				<ul id="report" class="collapse">
					<li>
						<a href="<?php echo site_url('sia_rpt_students');?>"><i class="fa fa-fw fa-file"></i> Siswa</a>
					</li>
					<li>
						<a href="<?php echo site_url('sia_rpt_spp');?>"><i class="fa fa-fw fa-file"></i> Pembayaran SPP</a>
					</li>
					<li>
						<a href="<?php echo site_url('sia_rpt_pendapatan');?>"><i class="fa fa-fw fa-file"></i> Transaksi Pendapatan</a>
					</li>
					<li>
						<a href="<?php echo site_url('sia_rpt_pembayaran');?>"><i class="fa fa-fw fa-file"></i> Transaksi Pembayaran</a>
					</li>
				</ul>
			</li>
		</ul>
	</div>
	<!-- /.navbar-collapse -->
</nav>