<div class="container-fluid">
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtEnteredAs"><?php echo lang('student_entered_school_as'); ?></label>
				<input id="txtEnteredAs" name="entered_school_as" type="text" class="form-control input-sm" maxlength="25"
					value="<?php echo isset($postData['entered_school_as'])? $postData['entered_school_as'] : '';?>" />
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtStudentOrigin"><?php echo lang('student_origin'); ?></label>
				<input id="txtStudentOrigin" name="student_origin" type="text" class="form-control input-sm" maxlength="25"
					value="<?php echo isset($postData['student_origin'])? $postData['student_origin'] : '';?>" />
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtPlayGroupName"><?php echo lang('student_play_group_name'); ?></label>
				<input id="txtPlayGroupName" name="play_group_name" type="text" class="form-control input-sm" maxlength="25"
					value="<?php echo isset($postData['play_group_name'])? $postData['play_group_name'] : '';?>" />
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtPlayGroupLocation"><?php echo lang('student_play_group_location'); ?></label>
				<input id="txtPlayGroupLocation" name="play_group_location" type="text" class="form-control input-sm" maxlength="25"
					value="<?php echo isset($postData['play_group_location'])? $postData['play_group_location'] : '';?>" />
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtNomorSttb"><?php echo lang('student_tahun_dan_nomor_sttb'); ?></label>
				<input id="txtNomorSttb" name="tahun_dan_nomor_sttb" type="text" class="form-control input-sm" maxlength="25"
					value="<?php echo isset($postData['tahun_dan_nomor_sttb'])? $postData['tahun_dan_nomor_sttb'] : '';?>" />
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtYearsOfLearning"><?php echo lang('student_years_of_learning'); ?></label>
				<input id="txtYearsOfLearning" name="years_of_learning" type="text" class="form-control input-sm" maxlength="2"
					value="<?php echo isset($postData['years_of_learning'])? $postData['years_of_learning'] : '';?>" />
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtNamaSekolahAsal"><?php echo lang('student_origin_school_name'); ?></label>
				<input id="txtNamaSekolahAsal" name="origin_school_name" type="text" class="form-control input-sm" maxlength="25"
					value="<?php echo isset($postData['origin_school_name'])? $postData['origin_school_name'] : '';?>" />
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtTanggalPindah"><?php echo lang('student_transfer_date'); ?></label>
				<div id="transfer_date_picker" class="input-group date">
					<input id="txtTanggalPindah" name="txtTanggalPindah" type="text" class="form-control input-sm" 
						value="<?php echo isset($postData['transfer_date'])? formatDateForDisplay($postData['transfer_date']) : '';?>" readonly="readonly" />
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
				</div>
				<input id="transfer_date" name="transfer_date" type="hidden"  
						value="<?php echo isset($postData['transfer_date'])? $postData['transfer_date'] : '';?>" />
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtPindahDariTingkat"><?php echo lang('student_transfer_from_grade'); ?></label>
				<input id="txtPindahDariTingkat" name="transfer_from_grade" type="text" class="form-control input-sm" maxlength="15"
					value="<?php echo isset($postData['transfer_from_grade'])? $postData['transfer_from_grade'] : '';?>" />
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtAcceptedDate"><?php echo lang('student_accepted_date'); ?> <span class="sign sign-mandatory">*</span></label>
				<div id="accepted_date_picker" class="input-group date">
					<input id="txtAcceptedDate" name="txtAcceptedDate" type="text" class="form-control input-sm" 
						value="<?php echo isset($postData['accepted_date'])? formatDateForDisplay($postData['accepted_date']) : '';?>" readonly="readonly" />
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
				</div>
				<input id="accepted_date" name="accepted_date" type="hidden"  
						value="<?php echo isset($postData['accepted_date'])? $postData['accepted_date'] : '';?>" />
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-lg-3 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtAcceptedGrade"><?php echo lang('student_accepted_grade'); ?></label>
				<input id="txtAcceptedGrade" name="accepted_grade" type="text" class="form-control input-sm" 
					value="<?php echo isset($postData['accepted_grade'])? $postData['accepted_grade'] : '';?>" />
			</div>
		</div>
		<div class="col-md-3 col-xs-12">
		<div class="form-group">
			<label for="txtFinancialYear">Masuk Tahun Ajaran <span class="sign sign-mandatory">*</span></label>
			<?php
					$tahunAjaranOptions = array(lang('student_choose')) + $DAO_Lov->getLovs('TAHUN_AJARAN');
					$selectedTahunAjaran = isset($postData['accepted_year'])? $postData['accepted_year'] : '';
					$cmbTahunAjaranAttr = array('id'=>'cmbTahunAjaran', 'class'=>'form-control input-sm');
					echo form_dropdown('accepted_year', $tahunAjaranOptions, $selectedTahunAjaran, $cmbTahunAjaranAttr);
				?>
		</div>
	</div>
		<div class="col-lg-6 col-md-6 col-xs-12">&nbsp;</div>
	</div><!-- /.row -->
</div>