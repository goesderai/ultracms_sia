<div class="container-fluid">
	<div class="row">
		<div class="col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtNis"><?php echo lang('student_nis'); ?> <span class="sign sign-mandatory">*</span></label>
				<input id="txtNis" name="nis" type="text" class="form-control input-sm" maxlength="25"
					value="<?php echo isset($postData['nis'])? $postData['nis'] : '';?>" />
			</div>
		</div>
		<div class="col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtNama"><?php echo lang('student_name'); ?> <span class="sign sign-mandatory">*</span></label>
				<input id="txtNama" name="name" type="text" class="form-control input-sm"
					value="<?php echo isset($postData['name'])? $postData['name'] : '';?>" />
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtNickname"><?php echo lang('student_nickname'); ?></label>
				<input id="txtNickname" name="nickname" type="text" class="form-control input-sm"
					value="<?php echo isset($postData['nickname'])? $postData['nickname'] : '';?>" />
			</div>
		</div>
		<div class="col-md-6 col-xs-12">
			<div class="form-group">
				<label for="cmbGender"><?php echo lang('student_gender'); ?></label>
				<?php
					$genderOptions = $DAO_Lov->getLovs('GENDER');
					$selectedGender = isset($postData['gender'])? $postData['gender'] : '';
					$cmbGenderAttr = array('id'=>'cmbGender', 'class'=>'form-control input-sm');
					echo form_dropdown('gender', $genderOptions, $selectedGender, $cmbGenderAttr);
				?>
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtBirthPlace"><?php echo lang('student_place_of_birth'); ?> <span class="sign sign-mandatory">*</span></label>
				<input id="txtBirthPlace" name="birth_place" type="text" class="form-control input-sm"
					value="<?php echo isset($postData['birth_place'])? $postData['birth_place'] : '';?>" />
			</div>
		</div>
		<div class="col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtBirthDate"><?php echo lang('student_date_of_birth'); ?> <span class="sign sign-mandatory">*</span></label>
				<div id="birth_date_picker" class="input-group date">
					<input id="txtBirthDate" name="txtBirthDate" type="text" class="form-control input-sm"
						value="<?php echo isset($postData['birth_date'])? formatDateForDisplay($postData['birth_date']) : '';?>" readonly="readonly" />
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
				</div>
				<input id="birth_date" name="birth_date" type="hidden"
					value="<?php echo isset($postData['birth_date'])? $postData['birth_date'] : '';?>" />
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtWeight"><?php echo lang('student_weight'); ?></label>
				<input id="txtWeight" name="weight" type="number" class="form-control input-sm"
					value="<?php echo isset($postData['weight'])? $postData['weight'] : '';?>" />
			</div>
		</div>
		<div class="col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtHeight"><?php echo lang('student_height'); ?></label>
				<input id="txtHeight" name="height" type="number" class="form-control input-sm"
					value="<?php echo isset($postData['height'])? $postData['height'] : '';?>" />
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtBloodType"><?php echo lang('student_blood_type'); ?></label>
				<input id="txtBloodType" name="blood_type" type="text" class="form-control input-sm"
					value="<?php echo isset($postData['blood_type'])? $postData['blood_type'] : '';?>" />
			</div>
		</div>
		<div class="col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtIllnessHistory"><?php echo lang('student_illness_history'); ?></label>
				<input id="txtIllnessHistory" name="illness_history" type="text" class="form-control input-sm"
					value="<?php echo isset($postData['illness_history'])? $postData['illness_history'] : '';?>" />
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtNthChild"><?php echo lang('student_nth_child'); ?></label>
				<input id="txtNthChild" name="nth_child" type="number" class="form-control input-sm"
					value="<?php echo isset($postData['nth_child'])? $postData['nth_child'] : '';?>" />
			</div>
		</div>
		<div class="col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtNSiblings"><?php echo lang('student_n_siblings'); ?></label>
				<input id="txtNSiblings" name="n_siblings" type="number" class="form-control input-sm"
					value="<?php echo isset($postData['n_siblings'])? $postData['n_siblings'] : '';?>" />
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtNStepSiblings"><?php echo lang('student_n_step_siblings'); ?></label>
				<input id="txtNStepSiblings" name="n_step_siblings" type="number" class="form-control input-sm"
					value="<?php echo isset($postData['n_step_siblings'])? $postData['n_step_siblings'] : '';?>" />
			</div>
		</div>
		<div class="col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtNFosterSiblings"><?php echo lang('student_n_foster_siblings'); ?></label>
				<input id="txtNFosterSiblings" name="n_foster_siblings" type="number" class="form-control input-sm"
					value="<?php echo isset($postData['n_foster_siblings'])? $postData['n_foster_siblings'] : '';?>" />
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-md-6 col-xs-12">
			<div class="form-group">
				<label for="cmbReligion"><?php echo lang('student_religion'); ?></label>
				<?php
					$religions = $DAO_Agama->getSelectOptions();
					$selectedReligion = isset($postData['religion_id'])? $postData['religion_id'] : '';
					$cmbReligionAttr = array('id'=>'cmbReligion', 'class'=>'form-control input-sm');
					echo form_dropdown('religion_id', $religions, $selectedReligion, $cmbReligionAttr);
				?>
			</div>
		</div>
		<div class="col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtNationality"><?php echo lang('student_citizenship'); ?></label>
				<input id="txtNationality" name="nationality" type="text" class="form-control input-sm"
					value="<?php echo isset($postData['nationality'])? $postData['nationality'] : '';?>" />
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtAddress"><?php echo lang('student_address'); ?> <span class="sign sign-mandatory">*</span></label>
				<input id="txtAddress" name="address" type="text" class="form-control input-sm"
					value="<?php echo isset($postData['address'])? $postData['address'] : '';?>" />
			</div>
		</div>
		<div class="col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtPhoneNumber"><?php echo lang('student_phone'); ?></label>
				<input id="txtPhoneNumber" name="phone_number" type="text" class="form-control input-sm"
					value="<?php echo isset($postData['phone_number'])? $postData['phone_number'] : '';?>" />
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtDailyLanguage"><?php echo lang('student_daily_language'); ?></label>
				<input id="txtDailyLanguage" name="daily_language" type="text" class="form-control input-sm"
					value="<?php echo isset($postData['daily_language'])? $postData['daily_language'] : '';?>" />
			</div>
		</div>
		<div class="col-md-6 col-xs-12">
			<div class="form-group">
				<label for="cmbStayedAt"><?php echo lang('student_stayed_at'); ?></label>
				<?php
					$stayOptions = array('-' => lang('student_choose')) + $DAO_Lov->getLovs('STAYED_AT');
					$selectedStay = isset($postData['stayed_at'])? $postData['stayed_at'] : '';
					$cmbStayAttr = array('id'=>'cmbStayedAt', 'class'=>'form-control input-sm');
					echo form_dropdown('stayed_at', $stayOptions, $selectedStay, $cmbStayAttr);
				?>
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-md-6 col-xs-12">
			<div class="form-group">
				<label for="cmbGrade"><?php echo lang('student_educational_level'); ?> <span class="sign sign-mandatory">*</span></label>
				<?php
        $newGradeIds = [GRADE_PAUD_BARU_ID, GRADE_SD_BARU_ID];
        $cmbGradeAttr = array('id'=>'cmbGrade', 'class'=>'form-control input-sm');
        $gradeOptions = $DAO_Edu->getGradeSelectOptions();
        if ($editorState == 'update') {
          $cmbGradeAttr['disabled'] = 'disabled';
          $selectedGrade = isset($postData['grade_id']) ? $postData['grade_id'] : GRADE_PAUD_BARU_ID;
          echo form_dropdown('', $gradeOptions, $selectedGrade, $cmbGradeAttr);
          echo sprintf('<input type="hidden" name="grade_id" value="%s" />', $selectedGrade);
        } else if ($editorState == 'insert') {
          $newGradeOptions = [];
          foreach ($gradeOptions as $key => $val) {
            if (in_array($key, $newGradeIds)) {
              $newGradeOptions[$key] = $val;
            }
          }
          echo form_dropdown('grade_id', $newGradeOptions, GRADE_PAUD_BARU_ID, $cmbGradeAttr);
        }
				?>
			</div>
		</div>
		<div class="col-md-6 col-xs-12">
			<div class="form-group">
				<label for="cmbClassroom"><?php echo lang('student_grade'); ?> <span class="sign sign-mandatory">*</span></label>
				<?php
				$cmbClassroomAttr = array('id'=>'cmbClassroom', 'class'=>'form-control input-sm');
				if($editorState == 'update'){
					$classroomOptions = $DAO_Edu->getClassroomSelectOptions($selectedGrade);
					$selectedClassroom = isset($postData['classroom_id'])? $postData['classroom_id'] : '';
          $cmbClassroomAttr['disabled'] = 'disabled';
          echo form_dropdown('', $classroomOptions, $selectedClassroom, $cmbClassroomAttr);
          echo sprintf('<input type="hidden" name="classroom_id" value="%s" />', $selectedClassroom);
				}else if($editorState == 'insert'){
					$classroomOptions = $DAO_Edu->getClassroomSelectOptions(GRADE_PAUD_BARU_ID);
					echo form_dropdown('classroom_id', $classroomOptions, 0, $cmbClassroomAttr);
				}
				?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-xs-12">
			<div class="form-group">
				<label for="cmbStatus"><?php echo lang('student_status'); ?></label>
				<?php
				$cmbStatusAttr = array('id'=>'cmbStatus', 'class'=>'form-control input-sm');
				$cmbStatusOptions = $DAO_Lov->getLovs('STATUS_SISWA', 'sequence');
				$selectedStatus = isset($postData['status'])? $postData['status'] : STATUS_CALON;
				/*if($editorState == 'update'){
					$cmbStatusAttr['disabled'] = 'disabled';
				}*/
				echo form_dropdown('status', $cmbStatusOptions, $selectedStatus, $cmbStatusAttr);
				?>
			</div>
		</div>
	</div>
</div><!-- /.container-fluid -->
