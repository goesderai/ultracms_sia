<div class="row">
  <div class="col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtTipeTransaksi"><?php echo lang('trxtype_code'); ?> <span
          class="sign sign-mandatory"> *</span></label>
      <input id="txtTipeTransaksi" name="transaction_type" type="text" class="form-control input-sm"
             value="<?php echo isset($postData['transaction_type']) ? $postData['transaction_type'] : ''; ?>"/>
    </div>
  </div>
  <div class="col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtNamaTipeTransaksi"><?php echo lang('trxtype_name'); ?> <span
          class="sign sign-mandatory"> *</span></label>
      <input id="txtNamaTipeTransaksi" name="nama" type="text" class="form-control input-sm"
             value="<?php echo isset($postData['nama']) ? $postData['nama'] : ''; ?>"/>
    </div>
  </div>
</div><!-- /.row -->

<div class="row">
  <div class="col-md-6 col-xs-12">
    <div class="form-group">
      <input id="trx_typ" name="trx_typ" type="hidden"/>
      <label for="cmbTipeTransaksi"><?php echo lang('trxtype_parent'); ?></label>
      <?php
      $tipeTransaksi = $DAO_TipeTransaksi->getAllTrxParentTypeAsArray(array(emptyComboOption(lang('label_all'))));
      $selectedtipeTransaksi = isset($postData['parent_type']) ? $postData['parent_type'] : '';
      $cmbTipeTransaksiAttr = array('id' => 'cmbTipeTransaksi', 'class' => 'form-control input-sm');
      echo form_dropdown('parent_type', $tipeTransaksi, $selectedtipeTransaksi, $cmbTipeTransaksiAttr);
      ?>
    </div>
  </div>
  <div class="col-md-6 col-xs-12">
    <div class="form-group">
      <label for="cmbDocType"><?php echo lang('trxtype_doc_type'); ?></label>
      <?php
      $docTypes = $DAO_Document_Type->getSelectOptions();
      $selectedDocType = isset($postData['document_type_id']) ? $postData['document_type_id'] : '';
      $cmbDocTypeAttr = array('id' => 'cmbDocType', 'class' => 'form-control input-sm');
      echo form_dropdown('document_type_id', $docTypes, $selectedDocType, $cmbDocTypeAttr);
      ?>
    </div>
  </div>
</div><!-- /.row -->

<div class="row">
  <div class="col-md-3 col-xs-12">
    <div class="form-group">
      <?php
      $isActive = isset($postData['active']) ? $postData['active'] : 'N';
      $isActiveChecked = ($isActive == 'Y');
      ?>
      <div class="pretty p-icon p-curve p-smooth">
        <input type="checkbox" name="active" value="Y" <?php echo $isActiveChecked ? 'checked="checked"' : ''; ?>/>
        <div class="state p-success">
          <i class="icon fa fa-check"></i>
          <label><?php echo lang('trxtype_active'); ?></label>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-3 col-xs-12">
    <div class="form-group">
      <?php
      $isSubType = isset($postData['is_sub_type']) ? $postData['is_sub_type'] : 0;
      $isSubTypeChecked = ($isSubType == 1);
      ?>
      <div class="pretty p-icon p-curve p-smooth">
        <input type="checkbox" name="is_sub_type"
               value="1" <?php echo $isSubTypeChecked ? 'checked="checked"' : ''; ?>/>
        <div class="state p-success">
          <i class="icon fa fa-check"></i>
          <label><?php echo lang('trxtype_sub_type'); ?></label>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-6 col-xs-12">
    <div class="form-group">
      <?php
      $transactionFlow = isset($postData['transaction_flow']) ? $postData['transaction_flow'] : 'D';
      if ($editorState == 'EDIT') {
        echo sprintf('<input type="hidden" name="transaction_flow" value="%s" />', $transactionFlow);
      }
      ?>
      <div class="pretty p-default p-round p-smooth">
        <input type="radio" value="D" name="transaction_flow"
          <?php echo ($transactionFlow == 'D') ? 'checked="checked"' : ''; ?>
          <?php echo ($editorState == 'EDIT') ? 'disabled="disabled"' : ''; ?>/>
        <div class="state p-success-o"><label><?php echo lang('trxtype_debit'); ?></label></div>
      </div>
      <div class="pretty p-default p-round p-smooth">
        <input type="radio" value="C" name="transaction_flow"
          <?php echo ($transactionFlow == 'C') ? 'checked="checked"' : ''; ?>
          <?php echo ($editorState == 'EDIT') ? 'disabled="disabled"' : ''; ?>/>
        <div class="state p-success-o"><label><?php echo lang('trxtype_credit'); ?></label></div>
      </div>
    </div>
  </div>
</div><!-- /.row -->

