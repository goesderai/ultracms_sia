<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtNoTransaksi">Nomor Transaksi</label>
      <input id="txtNoTransaksi" name="transaction_number" type="text" class="form-control input-sm" readonly="readonly"
             value="<?php echo isset($postData['transaction_number']) ? $postData['transaction_number'] : ''; ?>"/>
      <input id="editorState" type="hidden" value="<?php echo $editorState;?>" />
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtTrxDate">Tanggal Transaksi</label>
      <div id="trxDatePicker" class="input-group date">
        <input id="txtTrxDate" name="transaction_date" type="text" class="form-control input-sm"
               readonly="readonly" <?php if ($editorState == 'EDIT' || $editorState == 'KOREKSI') {
          echo 'disabled="disabled"';
        } ?>
               value="<?php echo isset($postData['transaction_date']) ? formatDateForDisplay($postData['transaction_date']) : formatDateForDisplay(date('Y-m-d')); ?>"/>
        <span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
      </div>
      <input id="trx_date" name="trx_date" type="hidden"
             value="<?php echo isset($postData['transaction_date']) ? $postData['transaction_date'] : date('Y-m-d');; ?>"/>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtCreatedBy">Nama Pegawai</label>
      <input id="txtCreatedByName" name="created_by_name" type="text" class="form-control input-sm" readonly="readonly"
             value="<?php
             $user = isset($postData['created_by']) ? $DAO_Security->getUserById($postData['created_by']) : $DAO_Security->getUserById($userData['id']);
             echo $user['username']; ?>"/>
      <input type="hidden" id="txtCreatedBy" name="created_by" type="text" class="form-control input-sm"
             readonly="readonly"
             value="<?php echo isset($postData['created_by']) ? $postData['created_by'] : $userData['id']; ?>"/>
    </div>
  </div>
</div><!-- /.row -->

<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <input id="trx_typ" name="trx_typ" type="hidden"/>
      <label for="cmbTipeTransaksi">Tipe Transaksi <span class="sign sign-mandatory">*</span></label>
      <?php
      if ($editorState == 'EDIT' || $editorState == 'KOREKSI') {
        $value = isset($postData['transaction_type']) ? $postData['transaction_type'] : '';
        $transactionType = $DAO_TipeTransaksi->getRow($value);
        $parentJenisTransaksi = $DAO_TipeTransaksi->getRow($transactionType['parent_type']);
        echo '<input id="txtTipeTransaksi" type="text" class="form-control input-sm" readonly="true"
						value="' . $parentJenisTransaksi['nama'] . '" />';
        echo '<input id="txthTipeTransaksi" name="transaction_type" type="hidden" class="form-control input-sm"
						value="' . $value . '" />';
      } else {
        $tipeTransaksi = $DAO_TipeTransaksi->getPendapatanTransaction('D', 3, null, 0, array(emptyComboOption(lang('label_all'))));
        $selectedtipeTransaksi = isset($postData['transaction_type']) ? $postData['transaction_type'] : '';
        $cmbTipeTransaksiAttr = array('id' => 'cmbTipeTransaksi', 'class' => 'form-control input-sm');
        echo form_dropdown('transaction_type', $tipeTransaksi, $selectedtipeTransaksi, $cmbTipeTransaksiAttr);
      }

      ?>
    </div>
  </div>

  <div class="col-lg-3 col-md-6 col-xs-12">
    <div class="form-group">
      <input id="trx_typ2" name="trx_typ2" type="hidden"/>
      <label for="cmbTipeTransaksi2">Sub Jenis Transaksi <span class="sign sign-mandatory">*</span></label>
      <?php
      if ($editorState == 'EDIT' || $editorState == 'KOREKSI') {
        $value = isset($postData['transaction_type']) ? $postData['transaction_type'] : '';
        $transactionType = $DAO_TipeTransaksi->getRow($value);
        if ($transactionType['is_sub_type'] == 1) {
          echo '<input id="txtSubTipeTransaksi" type="text" class="form-control input-sm" readonly="true"
							value="' . $transactionType['nama'] . '" />';
          echo '<input id="txthSubTipeTransaksi" name="sub_transaction_type" type="hidden" class="form-control input-sm"
							value="' . $value . '" />';
        } else {
          echo '<input id="txtSubTipeTransaksi" type="text" class="form-control input-sm" readonly="true"
							value="" />';
          echo '<input id="txthSubTipeTransaksi" name="sub_transaction_type" type="hidden" class="form-control input-sm"
							value="" />';
        }
      } else {
        $cmbTipeTransaksiAttr = array('id' => 'cmbJenSubType', 'class' => 'form-control input-sm');
        echo form_dropdown('sub_transaction_type', null, null, $cmbTipeTransaksiAttr);
      }
      /*}*/
      ?>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-xs-12">
    <label for="cmbPegawai">Pegawai</label>
    <?php
    if ($editorState == 'EDIT') {
      $value = isset($postData['employee_id']) ? $postData['employee_id'] : null;
      if ($value != null) {
        $employee = $DAO_Employee->getRow($value);
        echo '<input id="txtEmployee" type="text" class="form-control input-sm" readonly="true"
							value="' . $employee['name'] . '-' . $employee['id_number'] . '" />';
        echo '<input id="txthEmployee" name="employee_id" type="hidden" class="form-control input-sm"
							value="' . $value . '" />';
      } else {
        echo '<input id="txtEmployee" type="text" class="form-control input-sm" readonly="true"
							value="-- Pilih Pegawai --" />';
        echo '<input id="txthEmployee" name="employee_id" type="hidden" class="form-control input-sm"
							value="' . $value . '" />';
      }

    } else {
      $employee = array('-- Pilih --') + $DAO_Employee->getSelectOptionsForUserMapping();
      $selectedEmployee = isset($postData['employee_id']) ? $postData['employee_id'] : '';
      $cmbEmployeeAttr = array('id' => 'cmbEmployee', 'class' => 'form-control input-sm');
      echo form_dropdown('employee_id', $employee, $selectedEmployee, $cmbEmployeeAttr);
    }
    ?>

  </div>
</div><!-- /.row -->

<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtAmount">Jumlah Transaksi</label>
      <input id="txtAmount" name="debet_kredit" type="text" class="form-control input-sm" style="text-align:right;"
        <?php if ($editorState == 'EDIT' || $editorState == 'KOREKSI') {
          echo 'readonly="true"';
        } ?>
             value="<?php
             if ($editorState == 'ADD') {

             } else {
               if ($postData['amount_credit'] != 0) {
                 echo number_format((float)$postData['amount_credit'], 2, '.', ',');
               } else {
                 echo number_format((float)-$postData['amount_debet'], 2, '.', ',');
               }
             } ?>"/>
    </div>
  </div>
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtRemark">Keterangan</label>
      <textarea id="txtRemark" name="remark" type="text"
                class="form-control input-sm"><?php echo isset($postData['remark']) ? $postData['remark'] : ''; ?></textarea>
    </div>
  </div>
</div><!-- /.row -->
