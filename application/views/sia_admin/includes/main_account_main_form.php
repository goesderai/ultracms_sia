<div class="row">
  <div class="col-md-6 col-xs-12">
    <div class="form-group">
      <label for="parent"><?php echo lang('main_account_parent'); ?></label>
      <?php
      $mainAccount = $DAO_Main_Account->getAllAsArray();
      $selectedMainAccount = isset($postData['parent']) ? $postData['parent'] : '';
      $cmbMainAccountAttr = array('id' => 'parent', 'class' => 'form-control input-sm');
      echo form_dropdown('parent', $mainAccount, $selectedMainAccount, $cmbMainAccountAttr);
      ?>
    </div>
  </div>
  <div class="col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtKodeAkun"><?php echo lang('main_account_code'); ?> <span
          class="sign sign-mandatory">*</span></label>
      <input id="txtKodeAkun" name="account_code" type="text" class="form-control input-sm"
             value="<?php echo isset($postData['account_code']) ? $postData['account_code'] : ''; ?>"/>
    </div>
  </div>
</div><!-- /.row -->
<div class="row">
  <div class="col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtName"><?php echo lang('main_account_name'); ?> <span class="sign sign-mandatory">*</span></label>
      <input id="txtName" name="name" type="text" class="form-control input-sm"
             value="<?php echo isset($postData['name']) ? $postData['name'] : ''; ?>"/>
    </div>
  </div>
</div><!-- /.row -->

