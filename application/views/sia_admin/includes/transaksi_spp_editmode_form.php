<div class="container-fluid">
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<input id="trx_typ" name="trx_typ" type="hidden"/>
				<label for="cmbTipeTransaksi">Tipe Transaksi</label>
				<?php
					if ($editorState == 'EDIT'){
						$value = isset($postData['transaction_type'])? $postData['transaction_type'] : '';
						$transactionType = $DAO_TipeTransaksi->getRow($value);
						echo '<input id="txtTipeTransaksi" type="text" class="form-control input-sm" readonly="true"
						value="'.$transactionType['transaction_type'].'" />';
						echo '<input id="txthTipeTransaksi" name="transaction_type" type="hidden" class="form-control input-sm"
						value="'.$value.'" />';
					}else{
						$tipeTransaksi = $DAO_TipeTransaksi->getAllAsArray('D', 0, array(emptyComboOption(lang('label_all'))));
						$selectedtipeTransaksi = isset($postData['transaction_type'])? $postData['transaction_type'] : '';
						$cmbTipeTransaksiAttr = array('id'=>'cmbTipeTransaksi', 'class'=>'form-control input-sm');
						echo form_dropdown('transaction_type', $tipeTransaksi, $selectedtipeTransaksi, $cmbTipeTransaksiAttr);
					}
					
				?>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12">
			<label for="cmbStudent">NIS</label>
				<?php
					if ($editorState == 'EDIT'){
						$value = isset($postData['student_id'])? $postData['student_id'] : '';
						$student = $DAO_Siswa->getRow($value);
						echo '<input id="txtStudent" type="text" class="form-control input-sm" readonly="true"
						value="'.$student['nis'].'-'.$student['name'].'" />';
						echo '<input id="txthStudent" name="student_id" type="hidden" class="form-control input-sm"
						value="'.$value.'" />';
					}else{
						$students = $DAO_Siswa->getSelectOptions(array('-- Pilih Siswa --'));
						$selectedStudent = isset($postData['student_id'])? $postData['student_id'] : '';
						$cmbStudentAttr = array('id'=>'cmbStudent', 'class'=>'form-control input-sm' );
						echo form_dropdown('student_id', $students, $selectedStudent, $cmbStudentAttr);
					}
				?>
			
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtAmount">Jumlah Transaksi</label>
				<input id="txtAmount" name="debet_kredit" type="text" class="form-control input-sm" style="text-align:right;"
				<?php if ($editorState == 'EDIT'){echo 'readonly="true"';} ?>
					value="<?php echo isset($postData['amount_credit'])? $postData['amount_credit'] : '';?>" />
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtTrxDate">Tanggal Transaksi</label>
				<div id="trxDatePicker" class="input-group date">
					<input id="txtTrxDate" name="transaction_date" type="text" class="form-control input-sm" readonly="true" <?php if ($editorState == 'EDIT'){echo 'disabled="disabled"';} ?>
						value="<?php echo isset($postData['transaction_date'])? formatDateForDisplay($postData['transaction_date']) : '';?>" />
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
				</div>
				<input id="trx_date" name="trx_date" type="hidden" 
					value="<?php echo isset($postData['transaction_date'])? $postData['transaction_date'] : '';?>" />
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtRemark">Keterangan</label>
				<input id="txtRemark" name="remark" type="text" class="form-control input-sm"
					value="<?php echo isset($postData['remark'])? $postData['remark'] : '';?>" />
			</div>
		</div>
	</div><!-- /.row -->
	
</div><!-- /.container-fluid -->
