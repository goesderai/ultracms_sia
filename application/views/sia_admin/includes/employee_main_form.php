<div class="container-fluid">
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtNama"><?php echo lang('employee_name'); ?> <span class="sign sign-mandatory">*</span></label>
				<input id="txtNama" name="name" type="text" class="form-control input-sm"
					value="<?php echo isset($postData['name'])? $postData['name'] : '';?>" />
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtNickname"><?php echo lang('employee_nickname'); ?></label>
				<input id="txtNickname" name="nickname" type="text" class="form-control input-sm"
					value="<?php echo isset($postData['nickname'])? $postData['nickname'] : '';?>" />
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="cmbGender"><?php echo lang('employee_gender'); ?></label>
				<?php
					$genderOptions = $DAO_Lov->getLovs('GENDER');
					$selectedGender = isset($postData['gender'])? $postData['gender'] : '';
					$cmbGenderAttr = array('id'=>'cmbGender', 'class'=>'form-control input-sm');
					echo form_dropdown('gender', $genderOptions, $selectedGender, $cmbGenderAttr);
				?>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtBirthPlace"><?php echo lang('employee_birth_place'); ?> <span class="sign sign-mandatory">*</span></label>
				<input id="txtBirthPlace" name="birth_place" type="text" class="form-control input-sm"
					value="<?php echo isset($postData['birth_place'])? $postData['birth_place'] : '';?>" />
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtBirthDate"><?php echo lang('employee_birth_date'); ?> <span class="sign sign-mandatory">*</span></label>
				<div id="birth_date_picker" class="input-group date">
					<input id="txtBirthDate" name="txtBirthDate" type="text" class="form-control input-sm"
						value="<?php echo isset($postData['birth_date'])? formatDateForDisplay($postData['birth_date']) : '';?>" readonly="readonly" />
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
				</div>
				<input id="birth_date" name="birth_date" type="hidden"
					value="<?php echo isset($postData['birth_date'])? $postData['birth_date'] : '';?>" />
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtIdNumber"><?php echo lang('employee_identity_number'); ?> <span class="sign sign-mandatory">*</span></label>
				<input id="txtIdNumber" name="id_number" type="text" class="form-control input-sm"
					value="<?php echo isset($postData['id_number'])? $postData['id_number'] : '';?>" />
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtAddress"><?php echo lang('employee_address'); ?> <span class="sign sign-mandatory">*</span></label>
				<input id="txtAddress" name="address" type="text" class="form-control input-sm"
					value="<?php echo isset($postData['address'])? $postData['address'] : '';?>" />
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtPhoneNumber"><?php echo lang('employee_phone'); ?> <span class="sign sign-mandatory">*</span></label>
				<input id="txtPhoneNumber" name="phone_number" type="text" class="form-control input-sm"
					value="<?php echo isset($postData['phone_number'])? $postData['phone_number'] : '';?>" />
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="cmbJabatan"><?php echo lang('employee_position'); ?> <span class="sign sign-mandatory">*</span></label>
				<?php
					$jabatanOptions = $DAO_Lov->getLovs('JABATAN');
					$selectedJabatan = isset($postData['jabatan'])? $postData['jabatan'] : '';
					$cmbJabatanAttr = array('id'=>'cmbJabatan', 'class'=>'form-control input-sm');
					echo form_dropdown('jabatan', $jabatanOptions, $selectedJabatan, $cmbJabatanAttr);
				?>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12">&nbsp;</div>
	</div><!-- /.row -->
</div><!-- /.container-fluid -->
