<div class="row">
  <div class="col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtNis">Nama Siswa</label>
      <?php
      $students = $DAO_Siswa->getSelectOptions(array('-- All --'));
      $selectedStudent = isset($postData['student_id']) ? $postData['student_id'] : '';
      $cmbStudentAttr = array('id' => 'cmbStudent', 'class' => 'form-control input-sm');
      echo form_dropdown('student_id', $students, $selectedStudent, $cmbStudentAttr);
      ?>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6 col-xs-12">
    <label>Tanggal Bayar</label>
    <div class="form-horizontal">
      <div class="form-group">
        <div class="col-md-5 col-xs-12">
          <div id="trxDatePickerFrom" class="input-group date">
            <input id="txtTrxDateFrom" name="txtTrxDateFrom" type="text" class="form-control input-sm"
                   value="<?php echo isset($postData['transaction_date_from']) ? formatDateForDisplay($postData['transaction_date_from']) : ''; ?>"
                   readonly="readonly"/>
            <span class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</span>
          </div>
          <input id="transaction_date_from" name="transaction_date_from" type="hidden"
                 value="<?php echo isset($postData['transaction_date_from']) ? $postData['transaction_date_from'] : ''; ?>"/>
        </div>
        <div class="col-md-2 col-xs-12 text-center">
          <label>s/d</label>
        </div>
        <div class="col-md-5 col-xs-12">
          <div id="trxDatePickerTo" class="input-group date">
            <input id="txtTrxDateTo" name="txtTrxDateTo" type="text" class="form-control input-sm"
                   value="<?php echo isset($postData['transaction_date_to']) ? formatDateForDisplay($postData['transaction_date_to']) : ''; ?>"
                   readonly="readonly"/>
            <span class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</span>
          </div>
          <input id="transaction_date_to" name="transaction_date_to" type="hidden"
                 value="<?php echo isset($postData['transaction_date_to']) ? $postData['transaction_date_to'] : ''; ?>"/>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-3 col-xs-12">
    <div class="form-group">
      <label for="txtFinancialMonth">Bulan</label>
      <?php
      $month = monthComboOptions();
      $selectedMonth = isset($postData['financial_month']) ? $postData['financial_month'] : '';
      $cmbMonthAttr = array('id' => 'cmbMonth', 'class' => 'form-control input-sm');
      echo form_dropdown('financial_month', $month, $selectedMonth, $cmbMonthAttr);
      ?>
    </div>
  </div>
  <div class="col-md-3 col-xs-12">
    <div class="form-group">
      <label for="txtFinancialYear">Tahun</label>
      <input id="txtFinancialYear" name="financial_year" type="number" class="form-control input-sm"
             value="<?php echo isset($postData['financial_year']) ? $postData['financial_year'] : ''; ?>"/>
    </div>
  </div>
</div>
