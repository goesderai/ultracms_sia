<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <input id="trx_typ" name="trx_typ" type="hidden"/>
      <label for="cmbTipeTransaksi">Tipe Transaksi</label>
      <?php
      $tipeTransaksi = $DAO_TipeTransaksi->getAllAsArray('C', 4, array(emptyComboOption(lang('label_all'))));
      $selectedtipeTransaksi = isset($postData['trx_type']) ? $postData['trx_type'] : '';
      $cmbTipeTransaksiAttr = array('id' => 'cmbTipeTransaksi', 'class' => 'form-control input-sm');
      echo form_dropdown('trx_type', $tipeTransaksi, $selectedtipeTransaksi, $cmbTipeTransaksiAttr);
      ?>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <label>Tanggal Transaksi</label>
    <div class="form-horizontal">
      <div class="form-group">
        <div class="col-lg-5 col-md-5 col-xs-12">
          <div id="trxDatePickerFrom" class="input-group date">
            <input id="txtTrxDateFrom" name="txtTrxDateFrom" type="text" class="form-control input-sm"
                   value="<?php echo isset($postData['transaction_date_from']) ? formatDateForDisplay($postData['transaction_date_from']) : ''; ?>"
                   readonly="readonly"/>
            <span class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</span>
          </div>
          <input id="transaction_date_from" name="transaction_date_from" type="hidden"
                 value="<?php echo isset($postData['transaction_date_from']) ? $postData['transaction_date_from'] : ''; ?>"/>
        </div>
        <div class="col-lg-2 col-md-2 col-xs-12 text-center">
          <label>s/d</label>
        </div>
        <div class="col-lg-5 col-md-5 col-xs-12">
          <div id="trxDatePickerTo" class="input-group date">
            <input id="txtTrxDateTo" name="txtTrxDateTo" type="text" class="form-control input-sm"
                   value="<?php echo isset($postData['transaction_date_to']) ? formatDateForDisplay($postData['transaction_date_to']) : ''; ?>"
                   readonly="readonly"/>
            <span class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</span>
          </div>
          <input id="transaction_date_to" name="transaction_date_to" type="hidden"
                 value="<?php echo isset($postData['transaction_date_to']) ? $postData['transaction_date_to'] : ''; ?>"/>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-6 col-md-6 col-xs-12">&nbsp;</div>
</div>
