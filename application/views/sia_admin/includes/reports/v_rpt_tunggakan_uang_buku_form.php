<div class="row">
  <div class="col-md-3 col-xs-12">
    <div class="form-group">
      <label for="cmbGrade">Jenjang</label>
      <?php
      $grades = array("0" => "--- Pilih ---");
      $grades += $DAO_Edu->getGradeSelectOptions();
      $selectedGrade = '3';
      $cmbGradeAttr = array('id' => 'cmbGrade', 'class' => 'form-control input-sm', 'disabled' => 'disabled');
      echo form_dropdown('grade', $grades, $selectedGrade, $cmbGradeAttr);
      ?>
    </div>
  </div>
  <div class="col-md-3 col-xs-12">
    <div class="form-group">
      <label for="cmbClassroom">Tingkatan</label>
      <select id="cmbClassroom" name="classroom" class="form-control input-sm">
        <option value="0" disabled="disabled" selected="selected">--- Pilih jenjang terlebih dahulu ---</option>
      </select>
    </div>
  </div>
</div><!-- /row -->
<div class="row">
  <div class="col-md-3 col-xs-12">
    <div class="form-group">
      <label for="txtFinancialYear">Tahun Ajaran <span class="sign sign-mandatory">*</span></label>
      <?php
      $tahunAjaranOptions = array('' => '-- Pilih --') + $DAO_Lov->getLovs('TAHUN_AJARAN');
      $selectedTahunAjaran = isset($postData['financial_year']) ? $postData['financial_year'] : '';
      $cmbTahunAjaranAttr = array('id' => 'cmbTahunAjaran', 'class' => 'form-control input-sm');
      echo form_dropdown('financial_year', $tahunAjaranOptions, $selectedTahunAjaran, $cmbTahunAjaranAttr);
      ?>
    </div>
  </div>
</div><!-- /.row -->
