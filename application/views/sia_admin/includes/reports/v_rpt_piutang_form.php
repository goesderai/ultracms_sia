<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <input id="trx_typ" name="trx_typ" type="hidden"/>
      <label for="cmbTipeTransaksi">Tipe Transaksi <span class="sign sign-mandatory">*</span></label>
      <?php
      $tipeTransaksi = $DAO_TipeTransaksi->getTransactionTypePiutang(array(emptyComboOption(lang('label_all'))));
      $selectedtipeTransaksi = isset($postData['trx_type']) ? $postData['trx_type'] : '';
      $cmbTipeTransaksiAttr = array('id' => 'cmbTipeTransaksi', 'class' => 'form-control input-sm');
      echo form_dropdown('trx_type', $tipeTransaksi, $selectedtipeTransaksi, $cmbTipeTransaksiAttr);
      ?>
    </div>
  </div>
  
</div>
<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtFinancialYear">Tahun</label>
      <input id="txtFinancialYear" name="financial_year" type="number"
             min="1970" max="<?php echo date('Y'); ?>" class="form-control input-sm"
             value="<?php echo isset($postData['financial_year']) ? $postData['financial_year'] : date('Y'); ?>"/>
    </div>
  </div>
</div>
