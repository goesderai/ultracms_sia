<div class="row">
	<div class="col-md-6 col-xs-12">
		<label>Tanggal Transaksi</label>
		<div class="form-horizontal">
			<div class="form-group">
				<div class="col-md-5 col-xs-12">
					<div id="trxDatePickerFrom" class="input-group date">
						<input id="txtTrxDateFrom" name="txtTrxDateFrom" type="text" class="form-control input-sm" readonly="readonly" />
						<span class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</span>
					</div>
					<input id="dateFrom" name="date_from" type="hidden"/>	
				</div>
        <div class="col-md-2 col-xs-12 text-center">
          <label>s/d</label>
        </div>
        <div class="col-md-5 col-xs-12">
          <div id="trxDatePickerTo" class="input-group date">
            <input id="txtTrxDateTo" name="txtTrxDateFrom" type="text" class="form-control input-sm" readonly="readonly" />
            <span class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</span>
          </div>
          <input id="dateTo" name="date_to" type="hidden"/>
        </div>
			</div>
		</div>
	</div>
</div>
