<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtNama">Nama</label>
      <input id="txtNama" name="nama" type="text" class="form-control input-sm"
             value="<?php echo isset($postData['nama']) ? $postData['nama'] : ''; ?>"/>
    </div>
  </div>
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtIdNumber">Nomor Identitas</label>
      <input id="txtIdNumber" name="id_number" type="text" class="form-control input-sm"
             value="<?php echo isset($postData['id_number']) ? $postData['id_number'] : ''; ?>"/>
    </div>
  </div>
</div><!-- /.row -->
<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="cmbGender">Jenis Kelamin</label>
      <?php
      $genderOptions = array("-- Pilih --") + $DAO_Lov->getLovs('GENDER');
      $cmbGenderAttr = array('id' => 'cmbGender', 'class' => 'form-control input-sm');
      echo form_dropdown('gender', $genderOptions, null, $cmbGenderAttr);
      ?>
    </div>
  </div>

  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtAlamat">Alamat</label>
      <input id="txtAlamat" name="alamat" type="text" class="form-control input-sm"
             value="<?php echo isset($postData['alamat']) ? $postData['alamat'] : ''; ?>"/>
    </div>
  </div>
</div><!-- /.row -->
