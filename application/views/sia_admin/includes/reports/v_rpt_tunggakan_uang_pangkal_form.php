<div class="row">
  <div class="col-md-3 col-xs-12">
    <div class="form-group">
      <label for="cmbGrade">Jenjang</label>
      <?php
      $grades = array("0" => "--- Pilih ---");
      $grades += $DAO_Edu->getGradeSelectOptions();
      $selectedGrade = '0';
      $cmbGradeAttr = array('id' => 'cmbGrade', 'class' => 'form-control input-sm');
      echo form_dropdown('grade', $grades, $selectedGrade, $cmbGradeAttr);
      ?>
    </div>
  </div>
  <div class="col-md-3 col-xs-12">
    <div class="form-group">
      <label for="cmbClassroom">Tingkatan</label>
      <select id="cmbClassroom" name="classroom" class="form-control input-sm">
        <option value="0" disabled="disabled" selected="selected">--- Pilih jenjang terlebih dahulu ---</option>
      </select>
    </div>
  </div>
</div><!-- /row -->
