<div class="row">
  <div class="col-lg-6 col-md-10 col-xs-12">
    <label>Tanggal Transaksi</label>
    <div class="form-horizontal">
      <div class="form-group">
        <div class="col-lg-6 col-md-10 col-xs-12">
          <div id="trxDatePickerFrom" class="input-group date">
            <input id="txtTrxDateFrom" name="txtTrxDateFrom" type="text" class="form-control input-sm"
                   value="<?php echo isset($postData['transaction_date']) ? formatDateForDisplay($postData['transaction_date']) : ''; ?>"
                   readonly="readonly"/>
            <span class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</span>
          </div>
          <input id="transaction_date" name="transaction_date" type="hidden"
                 value="<?php echo isset($postData['transaction_date']) ? $postData['transaction_date'] : ''; ?>"/>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-6 col-md-6 col-xs-12">&nbsp;</div>
</div>
