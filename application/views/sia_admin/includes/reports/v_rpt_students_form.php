<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtNama">Jenis Laporan</label>
      <?php
      $cmbReportTypeAttr = array('id' => 'cmbReportType', 'class' => 'form-control input-sm');
      echo form_dropdown('reportType', $reportTypes, null, $cmbReportTypeAttr);
      ?>
    </div>
  </div>
</div>
<div class="row" id="rptStudentFilter" style="display: none"></div>
