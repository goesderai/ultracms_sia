<div class="row">
  <div class="col-md-4 col-xs-12">
    <div class="form-group">
      <label for="txtFinancialYear">Tahun</label>
      <input id="txtFinancialYear" name="financial_year" type="number"
             min="1970" max="<?php echo date('Y'); ?>" class="form-control input-sm"
             value="<?php echo isset($postData['financial_year']) ? $postData['financial_year'] : date('Y'); ?>"/>
    </div>
  </div>
</div><!-- /.row -->
