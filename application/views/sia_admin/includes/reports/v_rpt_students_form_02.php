<div class="col-lg-12 col-md-12 col-xs-12">
	<div class="container-fluid">
		<h3>Parameter Laporan</h3>
		<div class="row">
			<div class="col-md-6 col-xs-12">
				<label>Tanggal Lahir</label>
				<div class="form-horizontal">
					<div class="form-group">
						<div class="col-lg-5 col-md-5 col-xs-12">
							<div id="birthDatePickerFrom" class="input-group date">
								<input id="txtBirthDateFrom" name="txtBirthDateFrom" type="text" class="form-control input-sm" readonly="readonly" />
								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
							<input id="birth_date_from" name="birth_date_from" type="hidden" />
						</div>
						<div class="col-lg-2 col-md-2 col-xs-12 text-center">
							<label>s/d</label>
						</div>
						<div class="col-lg-5 col-md-5 col-xs-12">
							<div id="birthDatePickerTo" class="input-group date">
								<input id="txtBirthDateTo" name="txtBirthDateTo" type="text" class="form-control input-sm" readonly="readonly" />
								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
							<input id="birth_date_to" name="birth_date_to" type="hidden" />
						</div>
					</div>
				</div>
			</div>
		</div><!-- /.row -->

		<div class="row">
			<div class="col-md-3 col-xs-12">
				<div class="form-group">
					<label for="cmbGender">Jenis Kelamin</label>
					<?php
						$genderOptions = array("-- Pilih --") + $DAO_Lov->getLovs('GENDER');
						$cmbGenderAttr = array('id'=>'cmbGender', 'class'=>'form-control input-sm');
						echo form_dropdown('gender', $genderOptions, null, $cmbGenderAttr);
					?>
				</div>
			</div>
			<div class="col-md-3 col-xs-12">
				<div class="form-group">
					<label for="cmbReligion">Agama</label>
					<?php
						$religions = $DAO_Agama->getSelectOptions(array("-- Pilih --"));
						$cmbReligionAttr = array('id'=>'cmbReligion', 'class'=>'form-control input-sm');
						echo form_dropdown('religion_id', $religions, null, $cmbReligionAttr);
					?>
				</div>
			</div>
		</div><!-- /.row -->

		<div class="row">
			<div class="col-md-3 col-xs-12">
				<div class="form-group">
					<label for="cmbGrade">Jenjang</label>
					<?php
    					$grades = array("0" => "--- Pilih ---");
    					$grades += $DAO_Edu->getGradeSelectOptions();
    					$selectedGrade = '0';
    					$cmbGradeAttr = array('id'=>'cmbGrade', 'class'=>'form-control input-sm');
    					echo form_dropdown('grade', $grades, $selectedGrade, $cmbGradeAttr);
					?>
				</div>
			</div>
			<div class="col-md-3 col-xs-12">
				<div class="form-group">
					<label for="cmbClassroom">Tingkatan</label>
					<select id="cmbClassroom" name="classroom" class="form-control input-sm">
						<option value="0" disabled="disabled" selected="selected">--- Pilih jenjang terlebih dahulu ---</option>
					</select>
				</div>
			</div>
		</div><!-- /row -->
    <div class="row">
      <div class="col-md-3 col-xs-12">
        <div class="form-group">
          <label for="cmbStatus"><?php echo lang('student_status'); ?></label>
          <?php
          $cmbStatusAttr = array('id'=>'cmbStatus', 'class'=>'form-control input-sm');
          $cmbStatusOptions = $DAO_Lov->getLovs('STATUS_SISWA', 'sequence');
          echo form_dropdown('status', $cmbStatusOptions, 2, $cmbStatusAttr);
          ?>
        </div>
      </div>
    </div>
	</div>
</div>
