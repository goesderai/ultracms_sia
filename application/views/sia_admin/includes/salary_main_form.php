<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtGapok"><?php echo lang('employee_nett_salary'); ?> <span
          class="sign sign-mandatory">*</span></label>
      <input id="txtGapok" name="gaji_pokok" type="text" class="form-control input-sm" maxlength="18"
             value="<?php echo isset($postData['gaji_pokok']) ? $postData['gaji_pokok'] : '0'; ?>"/>
    </div>
  </div>
</div><!-- /.row -->

<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtTunjangan"><?php echo lang('employee_benefit'); ?> <span
          class="sign sign-mandatory">*</span></label>
      <input id="txtTunjangan" name="tunjangan" type="text" class="form-control input-sm" maxlength="18"
             value="<?php echo isset($postData['tunjangan']) ? $postData['tunjangan'] : '0'; ?>"/>
    </div>
  </div>
</div><!-- /.row -->

<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtLainnya"><?php echo lang('employee_others'); ?> <span class="sign sign-mandatory">*</span></label>
      <input id="txtLainnya" name="lain_lain" type="text" class="form-control input-sm" maxlength="18"
             value="<?php echo isset($postData['lain_lain']) ? $postData['lain_lain'] : '0'; ?>"/>
    </div>
  </div>
</div><!-- /.row -->
