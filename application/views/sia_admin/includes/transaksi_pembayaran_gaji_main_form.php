<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <input id="trx_typ" name="trx_typ" type="hidden"/>
      <label for="cmbTipeTransaksi">Tipe Transaksi</label>
      <?php
      if ($editorState == 'EDIT') {
        $value = isset($postData['transaction_type']) ? $postData['transaction_type'] : '';
        $transactionType = $DAO_TipeTransaksi->getRow($value);
        echo '<input id="txtTipeTransaksi" type="text" class="form-control input-sm" readonly="true"
						value="' . $transactionType['nama'] . '" />';
        echo '<input id="txthTipeTransaksi" name="transaction_type" type="hidden" class="form-control input-sm"
						value="' . $value . '" />';
      } else {
        $tipeTransaksi = $DAO_TipeTransaksi->getPembayaranGajiTransaction('C', 5, array(emptyComboOption(lang('label_all'))));
        $selectedtipeTransaksi = isset($postData['transaction_type']) ? $postData['transaction_type'] : '';
        $cmbTipeTransaksiAttr = array('id' => 'cmbTipeTransaksi', 'class' => 'form-control input-sm');
        echo form_dropdown('transaction_type', $tipeTransaksi, $selectedtipeTransaksi, $cmbTipeTransaksiAttr);
      }

      ?>
    </div>
  </div>

  <div class="col-lg-3 col-md-6 col-xs-12">
    <input id="txtState" type="hidden" value="<?php echo $editorState ?>">
    <label for="cmbPegawai">Pegawai</label>
    <?php
    if ($editorState == 'EDIT') {
      $value = isset($postData['employee_id']) ? $postData['employee_id'] : null;
      if ($value != null) {
        $employee = $DAO_Employee->getRow($value);
        echo '<input id="txtEmployee" type="text" class="form-control input-sm" readonly="true"
							value="' . $employee['name'] . '-' . $employee['id_number'] . '" />';
        echo '<input id="txthEmployee" name="employee_id" type="hidden" class="form-control input-sm"
							value="' . $value . '" />';
      } else {
        echo '<input id="txtEmployee" type="text" class="form-control input-sm" readonly="true"
							value="-- Pilih Pegawai --" />';
        echo '<input id="txthEmployee" name="employee_id" type="hidden" class="form-control input-sm"
							value="' . $value . '" />';
      }

    } else {
      $employee = array('-- Pilih --') + $DAO_Employee->getSelectOptionsForUserMapping();
      $selectedEmployee = isset($postData['employee_id']) ? $postData['employee_id'] : '';
      $cmbEmployeeAttr = array('id' => 'cmbEmployee', 'class' => 'form-control input-sm');
      echo form_dropdown('employee_id', $employee, $selectedEmployee, $cmbEmployeeAttr);
    }
    ?>

  </div>
  <div class="col-lg-3 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtJabatan">Jabatan<span class="sign sign-mandatory">*</span></label>
      <input id="txtJabatan" name="jabatan" type="text" class="form-control input-sm" readonly="readonly" value=""/>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtGapok">Gaji Pokok <span class="sign sign-mandatory">*</span></label>
      <input id="txtGapok" name="gaji_pokok" type="text" class="form-control input-sm" readonly="readonly"
             value="<?php echo isset($postData['gaji_pokok']) ? $postData['gaji_pokok'] : ''; ?>"/>
    </div>
  </div>

  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtHarian">Harian<span class="sign sign-mandatory">*</span></label>
      <input id="txtHarian" name="harian" type="text" class="form-control input-sm"
        <?php if ($editorState == 'EDIT') {
          echo 'readonly="readonly"';
        } ?>
             value="<?php echo isset($postData['harian']) ? $postData['harian'] : ''; ?>" onkeyup="validAngka(this)"/>
    </div>
  </div>

</div><!-- /.row -->

<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtTambahan">Tambahan</label>
      <input id="txtTambahan" name="tambahan" type="text"
             class="form-control input-sm" <?php if ($editorState == 'EDIT') {
        echo 'readonly="readonly"';
      } ?>
             value="<?php echo isset($postData['tambahan']) ? $postData['tambahan'] : ''; ?>"
             onkeyup="validAngka(this)"/>
    </div>
  </div>

  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtTotalGaji">Total Gaji</label>
      <input id="txtTotalGaji" name="total_gaji" type="text" class="form-control input-sm" readonly="readonly"
             value="<?php echo isset($postData['total_gaji']) ? $postData['total_gaji'] : ''; ?>"/>
    </div>
  </div>

</div><!-- /.row -->

<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtJmlKasbon">Jumlah Kasbon</label>
      <input id="txtJmlKasbon" name="total_kasbon" type="text" class="form-control input-sm" readonly="readonly"
             value="<?php echo isset($postData['total_kasbon']) ? $postData['total_kasbon'] : ''; ?>"/>
    </div>
  </div>

  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtTotalBayarKasbon">Total Bayar Kasbon</label>
      <input id="txtTotalBayarKasbon" name="bayar_kasbon" type="text" class="form-control input-sm"
        <?php if ($editorState == 'EDIT') {
          echo 'readonly="readonly"';
        } ?>
             value="<?php echo isset($postData['bayar_kasbon']) ? $postData['bayar_kasbon'] : ''; ?>"
             onkeyup="validAngka(this)"/>
    </div>
  </div>

</div><!-- /.row -->

<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="lblSisaGaji">Sisa Gaji</label>
      <input id="txtSisaGaji" name="total_gaji_dibayar" type="text" class="form-control input-sm" readonly="readonly"
             value="<?php echo isset($postData['total_gaji_dibayar']) ? $postData['total_gaji_dibayar'] : ''; ?>"/>
    </div>
  </div>

  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtTrxDate">Tanggal Transaksi <span class="sign sign-mandatory">*</span></label>
      <div id="trxDatePicker" class="input-group date">
        <input id="txtTrxDate" name="transaction_date" type="text" class="form-control input-sm"
               readonly="readonly" <?php if ($editorState == 'EDIT') {
          echo 'disabled="disabled"';
        } ?>
               value="<?php echo isset($postData['transaction_date']) ? formatDateForDisplay($postData['transaction_date']) : date('YYYY-MM-DD'); ?>"/>
        <span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
      </div>
      <input id="trx_date" name="trx_date" type="hidden"
             value="<?php echo isset($postData['transaction_date']) ? $postData['transaction_date'] : date('Y-m-d'); ?>"/>
    </div>
  </div>

</div><!-- /.row -->

<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtRemark">Keterangan <span class="sign sign-mandatory">*</span></label>
      <input id="txtRemark" name="remark" type="text" class="form-control input-sm"
             value="<?php echo isset($postData['remark']) ? $postData['remark'] : ''; ?>"/>
    </div>
  </div>
</div><!-- /.row -->
