<div class="row">
  <div class="col-md-4 col-xs-12">
    <div class="form-group">
      <label for="mutationDate">Tanggal Proses Kenaikan Jenjang <span class="sign sign-mandatory">*</span></label>
      <div id="mutation_date_picker" class="input-group date">
        <input id="mutationDate" type="text" class="form-control input-sm" readonly="readonly"
               value="<?php echo isset($postData['mutation_date']) ? formatDateForDisplay($postData['mutation_date']) : formatDateForDisplay(date('Y-m-d')); ?>"/>
        <span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
      </div>
      <input id="mutation_date" name="mutation_date" type="hidden"
             value="<?php echo isset($postData['mutation_date']) ? $postData['mutation_date'] : date('Y-m-d'); ?>"/>
    </div>
  </div>
  <div class="col-md-4 col-xs-12">
    <div class="form-group">
      <label for="cmbDestGradeId">Jenjang Tujuan <span class="sign sign-mandatory">*</span></label>
      <?php
      $gradeOptions = array('-- Pilih --') + $DAO_Edu->getGradeSelectOptions();
      $selectedGrade = isset($postData['dest_grade_id']) ? $postData['dest_grade_id'] : '';
      $cmbGradeAttr = array('id' => 'cmbDestGradeId', 'class' => 'form-control input-sm');
      echo form_dropdown('dest_grade_id', $gradeOptions, $selectedGrade, $cmbGradeAttr);
      ?>
    </div>
  </div>
  <div class="col-md-4 col-xs-12">
    <div class="form-group">
      <label for="cmbDestClassroomId">Tingkatan Tujuan <span class="sign sign-mandatory">*</span></label>
      <?php
      $cmbClassroomAttr = array('id' => 'cmbDestClassroomId', 'class' => 'form-control input-sm');
      if (!empty($selectedGrade)) {
        $classroomOptions = $DAO_Edu->getClassroomSelectOptions($selectedGrade);
        $selectedClassroom = isset($postData['dest_classroom_id']) ? $postData['dest_classroom_id'] : '';
        echo form_dropdown('dest_classroom_id', $classroomOptions, $selectedClassroom, $cmbClassroomAttr);
      } else {
        $classroomOptions = array('-- Pilih jenjang tujuan terlebih dahulu --');
        echo form_dropdown('dest_classroom_id', $classroomOptions, 0, $cmbClassroomAttr);
      }
      ?>
    </div>
  </div>
</div><!-- /.row -->

<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="form-group">
      <label for="tblStudents">Daftar Siswa</label>
      <table id="tblStudents" class="table table-bordered table-hover table-striped">
        <thead>
        <tr>
          <th width="5%" class="text-center">No</th>
          <th width="20%" class="text-center">NIS</th>
          <th width="25%" class="text-center">Nama</th>
          <th width="20%" class="text-center">Jenjang</th>
          <th width="20%" class="text-center">Tingkatan</th>
          <th width="10%" class="text-center">Aksi</th>
        </tr>
        </thead>
        <tbody id="tblStudentsBody"></tbody>
      </table>
    </div>
  </div>
</div><!-- /.row -->

<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="form-group">
      <?php $isDisabled = ($editorState == 'insert'); ?>
      <button id="btnAddItem" type="button"
              class="btn btn-success btn-sm" <?php echo $isDisabled ? 'disabled="disabled"' : ''; ?>
              title="<?php echo $isDisabled ? 'Pilih jenjang &amp; tingkatan tujuan terlebih dahulu' : 'Tambah siswa'; ?>">
        <i class="fa fa-plus"></i> <?php echo lang('label_add_new'); ?>
      </button>
    </div>
  </div>
</div><!-- /.row -->

<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="form-group">
      <button id="btnSimpan" type="button" class="btn btn-primary btn-sm"
              title="<?php echo lang('label_save_hint'); ?>">
        <i class="fa fa-save"></i> <?php echo lang('label_save'); ?>
      </button>
      <a href="javascript:void(0)" class="btn btn-primary btn-sm" title="<?php echo lang('label_close_hint'); ?>"
         onclick="siaJS_confirm('questions.confirm_close', '<?php echo site_url('sia_student_grade_mutation/index'); ?>')">
        <i class="fa fa-minus-circle"></i> <?php echo lang('label_close'); ?>
      </a>
    </div>
  </div>
</div><!-- /.row -->

<input type="hidden" id="id" name="id" value="<?php echo isset($postData['id']) ? $postData['id'] : ''; ?>"/>

<?php include_once 'popups/multiple_result_student_lookup_dialog.php'; ?>
