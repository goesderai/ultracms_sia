<div class="row">
  <div class="col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtNoTransaksi">Nomor Transaksi</label>
      <input id="txtNoTransaksi" name="transaction_number" type="text" class="form-control input-sm"
             readonly="readonly"
             value="<?php echo isset($postData['transaction_number']) ? $postData['transaction_number'] : ''; ?>"/>
    </div>
  </div>
  <div class="col-md-3 col-xs-12">
    <div class="form-group">
      <label for="txtTrxDate">Tanggal Transaksi <span class="sign sign-mandatory">*</span></label>
      <div id="trxDatePicker" class="input-group date">
        <input id="txtTrxDate" name="transaction_date" type="text" class="form-control input-sm"
               readonly="readonly" <?php if ($editorState == 'update' || $editorState == 'koreksi') {
          echo 'disabled="disabled"';
        } ?>
               value="<?php echo isset($postData['transaction_date']) ? formatDateForDisplay($postData['transaction_date']) : formatDateForDisplay(date('Y-m-d')); ?>"/>
        <span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
      </div>
      <input id="trx_date" name="trx_date" type="hidden"
             value="<?php echo isset($postData['transaction_date']) ? $postData['transaction_date'] : date('Y-m-d'); ?>"/>
    </div>
  </div>
  <div class="col-md-3 col-xs-12">
    <div class="form-group">
      <label for="txtCreatedBy">Nama Pegawai</label>
      <input id="txtCreatedByName" name="created_by_name" type="text" class="form-control input-sm"
             readonly="readonly"
             value="<?php
             $user = isset($postData['created_by']) ? $DAO_Security->getUserById($postData['created_by']) : $DAO_Security->getUserById($userData['id']);
             echo $user['username']; ?>"/>
      <input type="hidden" id="txtCreatedBy" name="created_by" type="text" class="form-control input-sm"
             readonly="readonly"
             value="<?php echo isset($postData['created_by']) ? $postData['created_by'] : $userData['id']; ?>"/>
    </div>
  </div>
</div><!-- /.row -->

<div class="row">
  <div class="col-md-6 col-xs-12">
    <div class="form-group">
      <label for="cmbTipeTransaksi">Transaksi <span class="sign sign-mandatory">*</span></label>
      <?php
      if ($editorState == 'update' || $editorState == 'koreksi') {
        $value = isset($postData['transaction_type']) ? $postData['transaction_type'] : '';
        $transactionType = $DAO_TipeTransaksi->getRow($value);
        if ($transactionType['is_sub_type'] == 0) {
          echo '<input id="txtTipeTransaksi" type="text" class="form-control input-sm" readonly="true"
							value="' . $transactionType['nama'] . '" />';
          echo '<input id="txthTipeTransaksi" name="transaction_type" type="hidden" class="form-control input-sm"
							value="' . $value . '" />';
        } else {
          $parentJenisTransaksi = $DAO_TipeTransaksi->getRow($transactionType['parent_type']);
          $parentTransactionType = $DAO_TipeTransaksi->getRow($parentJenisTransaksi['parent_type']);
          echo '<input id="txtTipeTransaksi" type="text" class="form-control input-sm" readonly="true"
							value="' . $parentTransactionType['nama'] . '" />';
          echo '<input id="txthTipeTransaksi" name="transaction_type" type="hidden" class="form-control input-sm"
							value="' . $parentTransactionType['id'] . '" />';
        }
      } else {
        $tipeTransaksi = $DAO_TipeTransaksi->getPembayaranTransaction('C', 4, NULL, 0, array(emptyComboOption(lang('label_all'))));
        $selectedtipeTransaksi = isset($postData['transaction_type']) ? $postData['transaction_type'] : '';
        $cmbTipeTransaksiAttr = array('id' => 'cmbTipeTransaksi', 'class' => 'form-control input-sm');
        echo form_dropdown('transaction_type', $tipeTransaksi, $selectedtipeTransaksi, $cmbTipeTransaksiAttr);
      }
      /*}*/
      ?>
    </div>
  </div>
  <div class="col-md-6 col-xs-12">
    <div class="form-group">
      <input id="trx_typ1" name="trx_typ1" type="hidden"/>
      <label for="cmbJenisTransaksi">Jenis Transaksi <span class="sign sign-mandatory">*</span></label>
      <?php
      if ($editorState == 'update' || $editorState == 'koreksi') {
        $value = isset($postData['transaction_type']) ? $postData['transaction_type'] : '';
        $transactionType = $DAO_TipeTransaksi->getRow($value);
        if ($transactionType['is_sub_type'] == 0) {
          echo '<input id="txtJenisTransaksi" type="text" class="form-control input-sm" readonly="true"
							value="' . $transactionType['nama'] . '" />';
          echo '<input id="txthJenisTransaksi" name="jenis_transaksi" type="hidden" class="form-control input-sm"
							value="' . $value . '" />';
        } else {
          $parentTransactionType = $DAO_TipeTransaksi->getRow($transactionType['parent_type']);
          echo '<input id="txtJenisTransaksi" type="text" class="form-control input-sm" readonly="true"
							value="' . $parentTransactionType['nama'] . '" />';
          echo '<input id="txthJenisTransaksi" name="jenis_transaksi" type="hidden" class="form-control input-sm"
							value="' . $parentTransactionType['id'] . '" />';
        }
      } else {
        $cmbJenisTransaksiAttr = array('id' => 'cmbJenisTransaksi', 'class' => 'form-control input-sm');
        echo form_dropdown('jenis_transaction', null, null, $cmbJenisTransaksiAttr);
      }
      /*}*/
      ?>
    </div>
  </div>
</div><!-- /.row -->

<div class="row">
  <div class="col-md-6 col-xs-12">
    <div class="form-group">
      <input id="trx_typ2" name="trx_typ2" type="hidden"/>
      <label for="cmbTipeTransaksi2">Sub Jenis Transaksi <span class="sign sign-mandatory">*</span></label>
      <?php
      if ($editorState == 'update' || $editorState == 'koreksi') {
        $value = isset($postData['transaction_type']) ? $postData['transaction_type'] : '';
        $transactionType = $DAO_TipeTransaksi->getRow($value);
        if ($transactionType['is_sub_type'] == 1) {
          echo '<input id="txtSubTipeTransaksi" type="text" class="form-control input-sm" readonly="true"
							value="' . $transactionType['nama'] . '" />';
          echo '<input id="txthSubTipeTransaksi" name="sub_transaction_type" type="hidden" class="form-control input-sm"
							value="' . $value . '" />';
        } else {
          echo '<input id="txtSubTipeTransaksi" type="text" class="form-control input-sm" readonly="true"
							value="" />';
          echo '<input id="txthSubTipeTransaksi" name="sub_transaction_type" type="hidden" class="form-control input-sm"
							value="" />';
        }
      } else {
        $cmbTipeTransaksiAttr = array('id' => 'cmbJenSubType', 'class' => 'form-control input-sm');
        echo form_dropdown('sub_transaction_type', null, null, $cmbTipeTransaksiAttr);
      }
      /*}*/
      ?>
    </div>
  </div>
  <div class="col-md-6 col-xs-12">
    <label for="cmbPegawai">Pegawai</label>
    <?php
    if ($editorState == 'update' || $editorState == 'koreksi') {
      $value = isset($postData['employee_id']) ? $postData['employee_id'] : null;
      if ($value != null) {
        $employee = $DAO_Employee->getRow($value);
        echo '<input id="txtEmployee" type="text" class="form-control input-sm" readonly="true"
							value="' . $employee['name'] . '-' . $employee['id_number'] . '" />';
        echo '<input id="txthEmployee" name="employee_id" type="hidden" class="form-control input-sm"
							value="' . $value . '" />';
      } else {
        echo '<input id="txtEmployee" type="text" class="form-control input-sm" readonly="true"
							value="-- Pilih Pegawai --" />';
        echo '<input id="txthEmployee" name="employee_id" type="hidden" class="form-control input-sm"
							value="' . $value . '" />';
      }

    } else {
      $employee = array('-- Pilih --') + $DAO_Employee->getSelectOptionsForUserMapping();
      $selectedEmployee = isset($postData['employee_id']) ? $postData['employee_id'] : '';
      $cmbEmployeeAttr = array('id' => 'cmbEmployee', 'class' => 'form-control input-sm');
      echo form_dropdown('employee_id', $employee, $selectedEmployee, $cmbEmployeeAttr);
    }
    ?>
  </div>
</div><!-- /.row -->

<div class="row">
  <div class="col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtAmount">Jumlah Transaksi <span class="sign sign-mandatory">*</span></label>
      <input id="txtAmount" name="debet_kredit" type="text" class="form-control input-sm" style="text-align:right;"
        <?php if ($editorState == 'update' || $editorState == 'koreksi') {
          echo 'readonly="true"';
        } ?>
             value="<?php echo isset($postData['amount_debet']) ? number_format((float)$postData['amount_debet'], 2, '.', ',') : ''; ?>"/>
    </div>
  </div>
  <div class="col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtRemark">Keterangan <span class="sign sign-mandatory">*</span></label>
      <textarea id="txtRemark" name="remark" type="text"
                class="form-control input-sm"><?php echo isset($postData['remark']) ? $postData['remark'] : ''; ?></textarea>
    </div>
  </div>
</div><!-- /.row -->

<input type="hidden" id="editorState" value="<?php echo $editorState; ?>"/>
