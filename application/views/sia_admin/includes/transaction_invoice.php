<div class="container">
  <!--Header-->
  <div class="row invoice-header">
    <!--<div class="col-md-8 col-xs-12">
      <img alt="Logo" src="<?php /*echo $isCopy ? base_url('assets/img/Logo_bw.png') : base_url('assets/img/Logo.png'); */ ?>" class="img-responsive"/>
      <p><?php /*echo $invoiceData->getCompanyAddress(); */ ?> | <?php /*echo $invoiceData->getCompanyPhone(); */ ?></p>
    </div>-->
    <div class="col-md-6 col-print-6">
      <h3><?php echo $COMPANY_NAME; ?></h3>
      <p><?php echo $COMPANY_ADDRESS; ?><br/>
        <i class="fa fa-phone-square"></i> <?php echo $COMPANY_PHONE; ?>
      </p>
    </div>
    <div class="col-md-6 col-print-6 text-right">
      <h3><?php echo $isCopy ? 'INVOICE (Copy)' : 'INVOICE'; ?></h3>
      <p><i class="fa fa-envelope"></i> <?php echo $invoiceData->getCompanyEmail(); ?><br/>
        <i class="fa fa-facebook-square"></i> <?php echo $COMPANY_FACEBOOK; ?> | <i class="fa fa-instagram"></i> <?php echo $COMPANY_INSTAGRAM; ?></p>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-print-6">
      <p>Telah Terima dari:<br/>
        Nama <?php echo $invoiceData->getTarget() == 'S' ? 'Siswa' : 'Karyawan'; ?>: <?php echo printEmpty($invoiceData->getTargetName()); ?><br/>
        <?php echo $invoiceData->getTarget() == 'S' ? 'NIS' : 'Nomor Identitas'; ?>: <?php echo printEmpty($invoiceData->getTargetIdNumber()); ?>
      </p>
    </div>
    <div class="col-md-6 col-print-6 text-right">
      <p>Nomor Invoice: <?php echo $invoiceData->getNumber(); ?><br/>
        Tanggal: <?php echo $invoiceData->getDate(); ?><br/>
        Sub: <?php echo printEmpty($invoiceData->getSub()); ?><br/>
        TRX: <?php echo printEmpty($invoiceData->getTrx()); ?>
      </p>
    </div>
  </div>

  <!--Details-->
  <div class="row">
    <div class="col-md-12 col-print-12">
      <table class="invoice-details">
        <thead>
        <tr>
          <th>No.</th>
          <th>No. Transaksi</th>
          <th>Jenis Transaksi</th>
          <th>Bayar</th>
          <th>Sisa</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $i          = 0;
        $totalBayar = 0;
        $totalSisa  = 0;
        foreach ($invoiceData->getDetails() as $row):
          $i++;
          $totalBayar = $totalBayar + $row->getTrxAmount();
          $totalSisa  = $totalSisa + $row->getTrxBalance();
          ?>
          <tr>
            <td rowspan="2" valign="top"><?php echo $i; ?></td>
            <td><?php echo $row->getTrxNumber(); ?></td>
            <td><?php echo $row->getTrxType(); ?></td>
            <td rowspan="2" valign="top" class="text-right"><?php echo number_format((float)$row->getTrxAmount(), 2, '.', ','); ?></td>
            <td rowspan="2" valign="top" class="text-right"><?php echo number_format((float)$row->getTrxBalance(), 2, '.', ','); ?></td>
          </tr>
          <tr>
            <td colspan="2">Ket: <?php echo $row->getRemark(); ?></td>
          </tr>
        <?php
        endforeach;
        ?>
        <tr>
          <td colspan="3" class="text-center">Total</td>
          <td class="text-right"><?php echo number_format((float)$totalBayar, 2, '.', ','); ?></td>
          <td class="text-right"><?php echo number_format((float)$totalSisa, 2, '.', ','); ?></td>
        </tr>
        </tbody>
      </table>
    </div>
  </div>

  <!--TTD-->
  <div class="row">
    <div class="col-md-3 col-print-3">
      <p>&nbsp;</p>
      <p class="text-center">
        Penyetor
        <br/><br/><br/><br/>
        (<?php echo str_repeat('&nbsp;', 35); ?>)
      </p>
    </div>
    <div class="col-md-3 col-print-3"></div>
    <div class="col-md-3 col-print-3"></div>
    <div class="col-md-3 col-print-3">
      <p>&nbsp;</p>
      <p class="text-center">
        Menerima
        <br/><br/><br/><br/>
        (<?php echo $_SESSION[SESSKEY_USERDATA]['name']; ?>)
      </p>
    </div>
  </div>
</div>
