<div class="modal fade" id="multipleResultStudentLookupDialog" tabindex="-1" role="dialog" aria-labelledby="studentLookupTitle">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="studentLookupTitle">Pencarian Siswa</h4>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<!-- search params -->
					<div class="row">
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<label for="studentLookupCmbGrade">Jenjang <span class="sign sign-mandatory">*</span></label>
								<?php 
                					$gradeOptions = array('-- Pilih --') + $DAO_Edu->getGradeSelectOptions();
                					$selectedGrade = '';
                					$cmbGradeAttr = array('id'=>'studentLookupCmbGrade', 'class'=>'form-control input-sm');
                					echo form_dropdown('src_grade_id', $gradeOptions, $selectedGrade, $cmbGradeAttr);
                				?>
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<label for="studentLookupCmbClassroom">Tingkatan <span class="sign sign-mandatory">*</span></label>
								<select id="studentLookupCmbClassroom" class="form-control input-sm">
									<option value="0" disabled="disabled" selected="selected">-- Pilih jenjang terlebih dahulu --</option>
								</select>
							</div>
						</div>
					</div><!-- /.row -->
					<div class="row">
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<label for="txtNis">NIS</label>
								<input id="txtNis" type="text" class="form-control input-sm" />
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<label for="txtNama">Nama</label>
								<input id="txtNama" type="text" class="form-control input-sm" />
							</div>
						</div>
					</div><!-- /.row -->
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<button id="btnSearch" type="button" class="btn btn-primary btn-sm">
									<i class="fa fa-search"></i> Cari
								</button>
								<button id="btnReset" type="button" class="btn btn-primary btn-sm">
									<i class="fa fa-undo"></i> Reset
								</button>
							</div>
						</div>
					</div><!-- /.row -->
					
					<!-- search result -->
					<div class="row">
						<div  class="col-md-12 col-xs-12">
							<div class="table-responsive">
                                <!-- result grid -->
    							<table class="table table-bordered table-hover table-striped">
    								<thead>
    									<tr>
    										<th width="3%" class="text-center">
    											<input type="checkbox" id="studentLookupMasterCb" />
    										</th>
    										<th width="5%" class="text-center">No</th>
    										<th width="22%" class="text-center">NIS</th>
    										<th width="30%" class="text-center">Nama</th>
    										<th width="20%" class="text-center">Jenjang</th>
    										<th width="20%" class="text-center">Tingkatan</th>
    									</tr>
    								</thead>
    								<tbody id="resultContainer"></tbody>
    							</table>
                                
                                <!-- result paging -->
    							<div id="paginationContainer" class="pull-right">
    								<nav aria-label="Page navigation">
    									<ul class="pagination pagination-sm">
    										<li id="liNavFirst"><a id="aNavFirst" href="javascript:void(0);" aria-label="First Page"><span aria-hidden="true"><i class="fa fa-fast-backward"></i></span></a></li>
    										<li id="liNavPrev"><a id="aNavPrev" href="javascript:void(0);" aria-label="Previous Page"><span aria-hidden="true"><i class="fa fa-backward"></i></span></a></li>
    										<li class="disabled"><span id="navPageInfo">Page ? of ?<span class="sr-only">Paging Info</span></span></li>
    										<li id="liNavNext"><a id="aNavNext" href="javascript:void(0);" aria-label="Next Page"><span aria-hidden="true"><i class="fa fa-forward"></i></span></a></li>
    										<li id="liNavLast"><a id="aNavLast" href="javascript:void(0);" aria-label="Last Page"><span aria-hidden="true"><i class="fa fa-fast-forward"></i></span></a></li>
    									</ul>
    								</nav>
    							</div>
    							<input type="hidden" id="txtPage" value="" />
    							<input type="hidden" id="txtPages" value="" />
							</div>
						</div>
					</div><!-- /.row -->
				</div>
			</div>
			<div class="modal-footer">
				<button id="studentLookupBtnPilih" type="button" class="btn btn-primary btn-sm">
					<i class="fa fa-check"></i> Pilih
				</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">
					<i class="fa fa-minus-circle"></i> Tutup
				</button>
			</div>
		</div>
	</div>
</div>