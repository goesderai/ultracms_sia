<div class="modal fade" id="singleResultStudentLookupDialog" tabindex="-1" role="dialog" aria-labelledby="singleResultStudentLookup_Title">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="singleResultStudentLookup_Title">Pencarian Siswa</h4>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<!-- search params -->
					<div class="row">
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<label for="singleResultStudentLookup_txtNis">NIS</label>
								<input id="singleResultStudentLookup_txtNis" type="text" class="form-control input-sm" />
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<label for="singleResultStudentLookup_txtNama">Nama</label>
								<input id="singleResultStudentLookup_txtNama" type="text" class="form-control input-sm" />
							</div>
						</div>
					</div><!-- /.row -->
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<button id="singleResultStudentLookup_btnSearch" type="button" class="btn btn-primary btn-sm">
									<i class="fa fa-search"></i> Cari
								</button>
								<button id="singleResultStudentLookup_btnReset" type="button" class="btn btn-primary btn-sm">
									<i class="fa fa-undo"></i> Reset
								</button>
							</div>
						</div>
					</div><!-- /.row -->
					
					<!-- search result -->
					<div class="row">
						<div  class="col-md-12 col-xs-12">
							<div class="table-responsive">
                                <!-- result grid -->
    							<table class="table table-bordered table-hover table-striped">
    								<thead>
    									<tr>
    								      <th width="3%" class="text-center">&nbsp;</th>
    								      <th width="5%" class="text-center">No</th>
    								      <th width="22%" class="text-center">NIS</th>
    								      <th width="30%" class="text-center">Nama</th>
    								      <th width="20%" class="text-center">Jenjang</th>
    								      <th width="20%" class="text-center">Tingkatan</th>
    									</tr>
    								</thead>
    								<tbody id="singleResultStudentLookup_resultContainer"></tbody>
    							</table>
                                
                                <!-- result paging -->
    							<div id="singleResultStudentLookup_paginationContainer" class="pull-right">
    								<nav aria-label="Page navigation">
    									<ul class="pagination pagination-sm">
    										<li id="singleResultStudentLookup_liNavFirst">
                                              <a id="singleResultStudentLookup_aNavFirst" href="javascript:void(0);" 
                                                aria-label="First Page"><span aria-hidden="true">
                                                <i class="fa fa-fast-backward"></i></span>
                                              </a>
                                            </li>
    										<li id="singleResultStudentLookup_liNavPrev">
                                              <a id="singleResultStudentLookup_aNavPrev" href="javascript:void(0);" 
                                                aria-label="Previous Page"><span aria-hidden="true">
                                                <i class="fa fa-backward"></i></span>
                                              </a>
                                            </li>
    										<li class="disabled">
                                              <span id="singleResultStudentLookup_navPageInfo">Page ? of ?
                                              <span class="sr-only">Paging Info</span></span>
                                            </li>
    										<li id="singleResultStudentLookup_liNavNext">
                                              <a id="singleResultStudentLookup_aNavNext" href="javascript:void(0);" 
                                                aria-label="Next Page"><span aria-hidden="true">
                                                <i class="fa fa-forward"></i></span>
                                              </a>
                                            </li>
    										<li id="singleResultStudentLookup_liNavLast">
                                              <a id="singleResultStudentLookup_aNavLast" href="javascript:void(0);" 
                                                aria-label="Last Page"><span aria-hidden="true">
                                                <i class="fa fa-fast-forward"></i></span>
                                              </a>
                                            </li>
    									</ul>
    								</nav>
    							</div>
    							<input type="hidden" id="singleResultStudentLookup_txtPage" value="" />
    							<input type="hidden" id="singleResultStudentLookup_txtPages" value="" />
							</div>
						</div>
					</div><!-- /.row -->
				</div>
			</div>
			<div class="modal-footer">
				<button id="singleResultStudentLookup_btnPilih" type="button" class="btn btn-primary btn-sm">
					<i class="fa fa-check"></i> Pilih
				</button>
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">
					<i class="fa fa-minus-circle"></i> Tutup
				</button>
			</div>
		</div>
	</div>
</div>