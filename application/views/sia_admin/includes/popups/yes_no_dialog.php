<div class="modal fade" id="yesNoDialog" tabindex="-1" role="dialog" aria-labelledby="yesNoDialogTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="yesNoDialogTitle"><?php echo lang("label_confirmation");?></h4>
      </div>
      <div class="modal-body">
        <p>
          <i style="float: left; margin: 0 10px 10px 0; font-size: 35px; color: green;" class="fa fa-question-circle"></i>
          <span id="yesNoDialogMessage"></span>
        </p>
      </div>
      <div class="modal-footer">
        <button id="yesNoDialogBtnYes" type="button" class="btn btn-warning"><?php echo lang("label_yes");?></button>
        <button id="yesNoDialogBtnNo" type="button" class="btn btn-warning"><?php echo lang("label_no");?></button>
      </div>
    </div>
  </div>
</div>