<div class="modal fade" id="alertDialog" tabindex="-1" role="dialog" aria-labelledby="alertDialogTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="alertDialogTitle"><?php echo lang("label_attention");?></h4>
      </div>
      <div class="modal-body">
        <table style="width: 100%">
          <tr>
            <td valign="top">
              <i style="font-size: 35px; color: #ff9900;" class="fa fa-exclamation-triangle"></i>
            </td>
            <td valign="top" id="alertDialogMessage"></td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button id="alertDialogBtnOk" type="button" class="btn btn-warning"><?php echo lang("label_ok");?></button>
      </div>
    </div>
  </div>
</div>