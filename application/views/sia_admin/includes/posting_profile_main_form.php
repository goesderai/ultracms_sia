<input id="hidenState" type="hidden" value="<?php echo $editorState ?>"/>

<div class="row">
  <div class="col-md-6 col-xs-12">
    <div class="form-group">
      <input id="trx_typ" name="trx_typ" type="hidden"/>
      <label for="cmbTipeTransaksi"><?php echo lang('pstngprfl_trx_type'); ?> <span class="sign sign-mandatory">*</span></label>
      <?php
      if ($editorState == 'EDIT') {
        $value = isset($postData['trx_type']) ? $postData['trx_type'] : '';
        $transactionType = $DAO_TipeTransaksi->getRow($value);
        echo '<input id="txtTipeTransaksi" type="text" class="form-control input-sm" readonly="true"
						value="' . $transactionType['transaction_type'] . ' ' . $transactionType['nama'] . '" />';
        echo '<input id="txthTipeTransaksi" name="trx_type" type="hidden" class="form-control input-sm"
						value="' . $value . '" />';
      } else {
        $tipeTransaksi = $DAO_TipeTransaksi->getAllTrxTypeAsArray(array(emptyComboOption(lang('label_all'))));
        $selectedtipeTransaksi = isset($postData['trx_type']) ? $postData['trx_type'] : '';
        $cmbTipeTransaksiAttr = array('id' => 'cmbTipeTransaksi', 'class' => 'form-control input-sm');
        echo form_dropdown('trx_type', $tipeTransaksi, $selectedtipeTransaksi, $cmbTipeTransaksiAttr);
      }
      ?>
    </div>
  </div>
</div><!-- /.row -->

<div class="row">
  <div class="col-md-6 col-xs-12">
    <div class="form-group">
      <label for="cmbTransaction"><?php echo lang('pstngprfl_account_debit'); ?> <span
          class="sign sign-mandatory">*</span></label>
      <?php
      $mainAccount = $DAO_Main_Account->getAllAsArray();
      $selectedMainAccount = isset($postData['account_debet']) ? $postData['account_debet'] : '';
      $cmbMainAccountAttr = array('id' => 'account_debet', 'class' => 'form-control input-sm');
      echo form_dropdown('account_debet', $mainAccount, $selectedMainAccount, $cmbMainAccountAttr);
      ?>
    </div>
  </div>
</div><!-- /.row -->

<div class="row">
  <div class="col-md-6 col-xs-12">
    <div class="form-group">
      <label for="cmbBalance"><?php echo lang('pstngprfl_account_credit'); ?> <span class="sign sign-mandatory">*</span></label>
      <?php
      $mainAccount = $DAO_Main_Account->getAllAsArray();
      $selectedMainAccount = isset($postData['account_credit']) ? $postData['account_credit'] : '';
      $cmbMainAccountAttr = array('id' => 'account_credit', 'class' => 'form-control input-sm');
      echo form_dropdown('account_credit', $mainAccount, $selectedMainAccount, $cmbMainAccountAttr);
      ?>
    </div>
  </div>
</div><!-- /.row -->
