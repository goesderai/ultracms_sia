<?php
$ocupations = $DAO_Lov->getLovs('PEKERJAAN');
$ocupationOptions = array('-' => lang('student_choose')) + $ocupations;
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtFatherName"><?php echo lang('student_father_name'); ?> <span class="sign sign-mandatory">*</span></label>
				<input id="txtFatherName" name="father_name" type="text" class="form-control input-sm"
					value="<?php echo isset($postData['father_name'])? $postData['father_name'] : '';?>" />
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtMotherName"><?php echo lang('student_mother_name'); ?> <span class="sign sign-mandatory">*</span></label>
				<input id="txtMotherName" name="mother_name" type="text" class="form-control input-sm"
					value="<?php echo isset($postData['mother_name'])? $postData['mother_name'] : '';?>" />
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtFatherLastEdu"><?php echo lang('student_father_last_education'); ?></label>
				<input id="txtFatherLastEdu" name="father_last_education" type="text" class="form-control input-sm"
					value="<?php echo isset($postData['father_last_education'])? $postData['father_last_education'] : '';?>" />
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtMotherLastEdu"><?php echo lang('student_mother_last_education'); ?></label>
				<input id="txtMotherLastEdu" name="mother_last_education" type="text" class="form-control input-sm"
					value="<?php echo isset($postData['mother_last_education'])? $postData['mother_last_education'] : '';?>" />
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="cmbParentOcupation"><?php echo lang('student_parent_ocupation'); ?></label>
				<?php
					$selectedParentOcupation = isset($postData['parent_ocupation'])? $postData['parent_ocupation'] : '';
					$cmbParentOcupationAttr = array('id'=>'cmbParentOcupation', 'class'=>'form-control input-sm');
					echo form_dropdown('parent_ocupation', $ocupationOptions, $selectedParentOcupation, $cmbParentOcupationAttr);
				?>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtGuardianName"><?php echo lang('student_guardian_name'); ?></label>
				<input id="txtGuardianName" name="guardian_name" type="text" class="form-control input-sm"
					value="<?php echo isset($postData['guardian_name'])? $postData['guardian_name'] : '';?>" />
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtGuardianLastEdu"><?php echo lang('student_guardian_last_education'); ?></label>
				<input id="txtGuardianLastEdu" name="guardian_last_education" type="text" class="form-control input-sm"
					value="<?php echo isset($postData['guardian_last_education'])? $postData['guardian_last_education'] : '';?>" />
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtGuardianRelationship"><?php echo lang('student_guardian_relationship'); ?></label>
				<input id="txtGuardianRelationship" name="guardian_relationship" type="text" class="form-control input-sm"
					value="<?php echo isset($postData['guardian_relationship'])? $postData['guardian_relationship'] : '';?>" />
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="cmbGuardianOcupation"><?php echo lang('student_guardian_ocupation'); ?></label>
				<?php
					$selectedGuardianOcupation = isset($postData['guardian_ocupation'])? $postData['guardian_ocupation'] : '';
					$cmbGuardianOcupationAttr = array('id'=>'cmbGuardianOcupation', 'class'=>'form-control input-sm');
					echo form_dropdown('guardian_ocupation', $ocupationOptions, $selectedGuardianOcupation, $cmbGuardianOcupationAttr);
				?>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12">&nbsp;</div>
	</div><!-- /.row -->
</div><!-- /.container-fluid -->
