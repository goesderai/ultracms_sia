<div class="container-fluid">
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="cmbEmployee"><?php echo lang('user_employee'); ?> <span class="sign sign-mandatory">*</span></label>
				<?php
					$employeeOptions = array('-- Pilih --') + $DAO_Employee->getSelectOptionsForUserMapping();
					$selectedEmployee = isset($postData['employee_id'])? $postData['employee_id'] : '';
					$cmbEmployeeAttr = array('id'=>'cmbEmployee', 'class'=>'form-control input-sm', 'style'=>'width: 100%');
					echo form_dropdown('employee_id', $employeeOptions, $selectedEmployee, $cmbEmployeeAttr);
				?>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtUsername"><?php echo lang('user_username'); ?> <span class="sign sign-mandatory">*</span></label>
				<input id="txtUsername" name="username" type="text" class="form-control input-sm"
					value="<?php echo isset($postData['username'])? $postData['username'] : '';?>" />
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtPassword"><?php echo lang('user_password'); ?>&nbsp;
                  <?php if($editorState != 'update'): ?><span class="sign sign-mandatory">*</span><?php endif;?></label>
				<input id="txtPassword" name="password" type="password" class="form-control input-sm" value="" />
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="txtConfirmPassword"><?php echo lang('user_confirm_password'); ?>&nbsp;
                  <?php if($editorState != 'update'): ?><span class="sign sign-mandatory">*</span><?php endif;?></label>
				<input id="txtConfirmPassword" name="confirmPassword" type="password" class="form-control input-sm" value="" />
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="selectRole"><?php echo lang('user_roles'); ?> <span class="sign sign-mandatory">*</span></label>
				<?php
					$roleOptions = $DAO_Security->getRoleSelectOptions();
					$selectedRoles = isset($postData['user_roles'])? $postData['user_roles'] : array();
					$selectRoleAttr = array('id' => 'selectRole', 'class' => 'form-control input-sm',
					    'style' => 'height: 150px; font-size: 14px');
					echo form_multiselect('role[]', $roleOptions, $selectedRoles, $selectRoleAttr);
				?>
				<p><i class="fa fa-info-circle"></i> <?php echo lang('user_role_hint'); ?></p>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12">
    		<?php
	    		$checked = true;
	    		if(isset($postData['active'])){
	    			$checked = ($postData['active'] == 'Y');
	    		}
    		?>
    		<div class="pretty p-icon p-curve p-smooth">
                <input type="checkbox" name="active" value="Y" <?php echo $checked? 'checked="checked"' : ''; ?>/>
                <div class="state p-success">
                    <i class="icon fa fa-check"></i>
                    <label><?php echo lang('user_active'); ?></label>
                </div>
            </div>

		</div>
	</div>
</div><!-- /.container-fluid -->