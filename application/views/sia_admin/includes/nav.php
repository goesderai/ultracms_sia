<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="#"><?php echo $appName;?></a>
	</div>
	
	<!-- Top Menu Items -->
	<ul class="nav navbar-right top-nav">
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				<i class="fa fa-user"></i> <?php echo $_SESSION[SESSKEY_USERDATA]['name'];?> <b class="caret"></b>
			</a>
			<ul class="dropdown-menu user-menu">
				<li>
					<a href="<?php echo site_url('sia_change_password');?>"><i class="fa fa-fw fa-gear"></i> <?php echo lang('label_change_password'); ?></a>
				</li>
				<li class="divider"></li>
				<li>
					<a href="<?php echo site_url('sia_login/logout');?>"><i class="fa fa-fw fa-power-off"></i> <?php echo lang('label_logout'); ?></a>
				</li>
			</ul>
		</li>
	</ul>
	
	<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
	<div class="collapse navbar-collapse navbar-ex1-collapse">
		<?php echo $sidebarMenuGen->generateTree($_SESSION[SESSKEY_USERDATA]['granted_modules']);?>
	</div>
	
</nav>