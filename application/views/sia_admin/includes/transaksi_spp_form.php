<div class="row">
  <div class="col-lg-4 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtNoTransaksi"><?php echo lang('trxspp_transaction_number'); ?></label>
      <input id="txtNoTransaksi" name="transaction_number" type="text" class="form-control input-sm" readonly="readonly"
             value="<?php echo isset($postData['transaction_number']) ? $postData['transaction_number'] : ''; ?>"/>
    </div>
  </div>
  <div class="col-lg-4 col-md-6 col-xs-12">
    <div class="form-group">
      <?php
      $studentId = isset($postData['student_id']) ? $postData['student_id'] : '';
      $student = $DAO_Siswa->getRow($studentId);
      ?>
      <label for="txtSiswa"><?php echo lang('trxspp_student_name'); ?> <span
          class="sign sign-mandatory">*</span></label>
      <div class="input-group">
        <input type="text" id="txtSiswa" class="form-control input-sm" readonly="readonly"
               value="<?php echo !empty($studentId) ? $student['nis'] . ' - ' . $student['name'] : ''; ?>"/>
        <input type="hidden" id="txtIdSiswa" name="student_id"
               value="<?php echo $studentId; ?>"/>
        <span class="input-group-btn">
            <button type="button" id="btnBrowseSiswa" <?php if ($editorState == 'EDIT') echo 'disabled="disabled"'; ?>
                    class="btn btn-primary btn-sm"><i class="fa fa-search"></i></button>
          </span>
      </div>
    </div>
  </div>
  <div class="col-lg-4 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtKelas"><?php echo lang('trxspp_student_grade_class'); ?></label>
      <input id="txtKelas" type="text" name="kelas" class="form-control input-sm" readonly="readonly"
             value="<?php echo isset($postData['student_id']) ? $DAO_Edu->getGradeAndClassroomLabel($postData['student_id']) : ''; ?>"/>
    </div>
  </div>
</div><!-- /.row -->

<div class="row">
  <div class="col-lg-3 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtTrxDate"><?php echo lang('trxspp_transaction_date'); ?></label>
      <div id="trxDatePicker" class="input-group date">
        <input id="txtTrxDate" name="transaction_date" type="text" class="form-control input-sm"
               readonly="readonly" <?php if ($editorState == 'EDIT') {
          echo 'disabled="disabled"';
        } ?>
               value="<?php echo isset($postData['transaction_date']) ? formatDateForDisplay($postData['transaction_date']) : formatDateForDisplay(date('Y-m-d')); ?>"/>
        <span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
      </div>
      <input id="trx_date" name="transaction_date" type="hidden"
             value="<?php echo isset($postData['transaction_date']) ? $postData['transaction_date'] : date('Y-m-d'); ?>"/>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="cmbYear"><?php echo lang('trxspp_year'); ?> <span class="sign sign-mandatory">*</span></label>
      <?php
      if ($editorState == 'EDIT') {
        $studentId = isset($postData['financial_year']) ? $postData['financial_year'] : '';
        echo '<input id="txtYear" type="text" class="form-control input-sm" readonly="true"
						value="' . $studentId . '" />';
      } else {
        $year = transactionYearComboOption();
        $selectedYear = isset($postData['financial_year']) ? $postData['financial_year'] : '';
        $cmbYearAttr = array('id' => 'cmbYear', 'class' => 'form-control input-sm');
        echo form_dropdown('financial_year', $year, $selectedYear, $cmbYearAttr);
      }
      ?>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtAmount"><?php echo lang('trxspp_monthly_amount'); ?> <span
          class="sign sign-mandatory">*</span></label>
      <input id="txtAmount" name="real_amount" type="text" class="form-control input-sm" style="text-align:right;"
        <?php if ($editorState == 'EDIT') {
          echo 'readonly="true"';
        } ?>
             value="<?php echo isset($postData['real_amount']) ? number_format((float)$postData['real_amount'], 2, '.', ',') : ''; ?>"/>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtDiskon"><?php echo lang('trxspp_monthly_discount'); ?></label>
      <input id="txtDiskon" type="text" name="diskon" class="form-control input-sm" style="text-align:right;"
        <?php if ($editorState == 'EDIT') {
          echo 'readonly="true"';
        } ?>
             value="<?php echo isset($postData['diskon']) ? number_format((float)$postData['diskon'], 2, '.', ',') : ''; ?>">
    </div>
  </div>
</div><!-- /.row -->

<div class="row">
  <div class="col-md-6 col-xs-12">
    <div class="panel-upper-buttons">
      <button <?php if ($editorState == 'EDIT') echo 'disabled="disabled"'; ?>
        id="btnPilihBulan" type="button" class="btn btn-primary btn-sm"
        title="<?php echo lang('trxspp_choose_month'); ?>">
        <?php echo lang('trxspp_choose_month'); ?>
      </button>
    </div>
  </div>
</div><!-- /.row -->

<div class="row">
  <div class="col-md-6 col-xs-12">
    <div class="form-group">
      <div id="chkMonth">
      </div>
    </div>
  </div>
</div><!-- /.row -->

<div class="row">
  <div class="col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtTotal"><?php echo lang('trxspp_total_amount'); ?></label>
      <input id="txtTotal" type="text" readonly="readonly" name="total_amount" class="form-control input-sm"
             maxlength="18" style="text-align:right;"
             value="<?php echo isset($postData['total_amount']) ? number_format((float)$postData['total_amount'], 2, '.', ',') : ''; ?>">
    </div>
  </div>
  <div class="col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtRekark"><?php echo lang('trxspp_description'); ?></label>
      <input id="txtRemark" type="text" name="remark" class="form-control input-sm"
             value="<?php echo isset($postData['remark']) ? $postData['remark'] : ''; ?>">
    </div>
  </div>
</div><!-- /.row -->

<?php include_once 'popups/single_result_student_lookup_dialog.php'; ?>
