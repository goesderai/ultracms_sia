<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>SIA Invoice</title>
    <?php
    echo linkTag('assets/themes/sb-admin/css/bootstrap.min.css');
    echo linkTag('assets/themes/sb-admin/font-awesome/css/font-awesome.min.css');
    echo linkTag('assets/themes/sb-admin/css/sia-admin.min.css');
    echo linkTag('assets/themes/sb-admin/css/sia-invoice.min.css');
    ?>
  </head>
  <body>
    <?php
    if(isset($invoiceData)):
        $isCopy = $invoiceData->isCopy();
        
        // invoice body
        include 'includes/transaction_invoice.php';
        
        // invoice body (copy) 
    /*echo '<p>&nbsp;</p><p>&nbsp;</p>';
    $isCopy = true;
    include 'includes/transaction_invoice.php';*/
    else:
    ?>
      <div class="alert alert-danger"><b>Error!</b> Gagal membentuk invoice!</div>
    <?php endif;?>
  </body>
</html>
