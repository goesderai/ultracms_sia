<!DOCTYPE html>
<html lang="en">
<head>
  <?php echo generateHeadTags($pageTitle); ?>
</head>
<body>

<div id="wrapper">
  <?php include_once 'includes/nav.php'; ?>
  <div id="page-wrapper">
    <div class="container-fluid">
      <?php echo renderPageHeading($pageTitle, $pageIcon); ?>
      <!-- BEGIN After-save message -->
      <div class="row">
        <div class="col-md-6 col-md-offset-3 col-xs-12">
          <div class="row">
            <div class="col-md-12 col-xs-12">
              <div class="alert <?php echo $alertType; ?>" role="alert">
                <h4><?php echo $alertMessage; ?></h4>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-xs-6">
              <a class="btn btn-primary btn-sm btn-block" target="_blank" href="<?php echo $invoiceUrl; ?>">Cetak Invoice</a>
            </div>
            <div class="col-md-6 col-xs-6">
              <a class="btn btn-warning btn-sm btn-block" href="<?php echo $alertProceedUrl; ?>">Tutup</a>
            </div>
          </div>
        </div>
      </div>
      <!-- END After-save message -->

    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

</body>
</html>
