<?php echo form_open('sia_trx_gaji/index', 'method="get"'); ?>

<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <label for="cmbEmployee">Karyawan <span class="sign sign-mandatory">*</span></label>
    <?php
    $employeeOptions = array('-- Pilih --') + $DAO_Employee->getSelectOptionsForUserMapping();
    $selectedEmployee = isset($postData['employee_id']) ? $postData['employee_id'] : '';
    $cmbEmployeeAttr = array('id' => 'cmbEmployee', 'class' => 'form-control input-sm');
    echo form_dropdown('employee_id', $employeeOptions, $selectedEmployee, $cmbEmployeeAttr);
    ?>
  </div>
  <div class="col-lg-6 col-md-6 col-xs-12">
    <label>Tanggal Pembayaran</label>
    <div class="form-horizontal">
      <div class="form-group">
        <div class="col-lg-5 col-md-5 col-xs-12">
          <div id="trxDatePickerFrom" class="input-group date">
            <input id="txtTrxDateFrom" name="txtTrxDateFrom" type="text" class="form-control input-sm"
                   value="<?php echo isset($postData['transaction_date_from']) ? formatDateForDisplay($postData['transaction_date_from']) : ''; ?>"
                   readonly="readonly"/>
            <span class="input-group-addon">
															<span class="fa fa-calendar"></span>
														</span>
          </div>
          <input id="transaction_date_from" name="transaction_date_from" type="hidden"
                 value="<?php echo isset($postData['transaction_date_from']) ? $postData['transaction_date_from'] : ''; ?>"/>
        </div>
        <div class="col-lg-2 col-md-2 col-xs-12 text-center">
          <label>s/d</label>
        </div>
        <div class="col-lg-5 col-md-5 col-xs-12">
          <div id="trxDatePickerTo" class="input-group date">
            <input id="txtTrxDateTo" name="txtTrxDateTo" type="text" class="form-control input-sm"
                   value="<?php echo isset($postData['transaction_date_to']) ? formatDateForDisplay($postData['transaction_date_to']) : ''; ?>"
                   readonly="readonly"/>
            <span class="input-group-addon">
															<span class="fa fa-calendar"></span>
														</span>
          </div>
          <input id="transaction_date_to" name="transaction_date_to" type="hidden"
                 value="<?php echo isset($postData['transaction_date_to']) ? $postData['transaction_date_to'] : ''; ?>"/>
        </div>
      </div>
    </div>
  </div>
</div><!-- /.row -->

<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtKeterangan">Keterangan</label>
      <input id="txtKeterangan" name="remark" type="text" class="form-control input-sm"
             value="<?php echo isset($postData['remark']) ? $postData['remark'] : ''; ?>"/>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel-upper-buttons">
      <button id="btnCari" type="submit" class="btn btn-primary btn-sm"
              onclick="javascript:ultracms.siaTransaksiPembayaranGajiList.validateForm()">
        <i class="fa fa-search"></i> Cari
      </button>
      <button id="btnReset" type="button" class="btn btn-primary btn-sm">
        <i class="fa fa-undo"></i> Reset
      </button>
      <a href="<?php echo site_url('sia_trx_gaji/insert'); ?>" class="btn btn-primary btn-sm" title="Tambah data baru">
        <i class="fa fa-plus"></i> Tambah
      </a>
    </div>
  </div>
</div><!-- /.row -->

<!-- Search Result -->
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="table-responsive">
      <table class="table table-bordered table-hover table-striped">
        <thead>
        <tr>
          <th class="text-center" width="5%">No</th>
          <th class="text-center" width="15%">Nomor Transaksi</th>
          <th class="text-center" width="25%">Pegawai</th>
          <th class="text-center" width="5%">Bulan</th>
          <th class="text-center" width="5%">Tahun</th>
          <th class="text-center" width="20%">Keterangan</th>
          <th class="text-center" width="15%">Tanggal Transaksi</th>
          <th class="text-center" width="15%">Total Gaji</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $baseUrlParams = array(QSPARAM_PAGE => $listPage, QSPARAM_PAGE_SIZE => $listPageSize) + $_GET;
        if (isset($recordset)):
          $i = $rownumStart + 1;
          foreach ($recordset as $row):
            $urlParams = $baseUrlParams + array(QSPARAM_REC_ID => $row->id);
            $editUrl = siteUrl('sia_trx_gaji/update', $urlParams);
            ?>
            <tr>
              <td><?php echo $i; ?></td>
              <td><a href="<?php echo $editUrl; ?>"><?php echo $row->transaction_number; ?></a></td>
              <td><?php echo $DAO_Employee->getEmployeeCaptionById($row->employee_id); ?></td>
              <td><?php echo $row->financial_month; ?></td>
              <td align="right"><?php echo $row->financial_year; ?></td>
              <td><?php echo $row->remark; ?></td>
              <td align="left"><?php echo date("d F Y", strtotime($row->transaction_date)); ?></td>
              <td align="right"><?php echo number_format($row->total_gaji); ?></td>
            </tr>
            <?php
            unset($urlParams, $editUrl);
            $i++;
          endforeach;
        endif;
        ?>
        </tbody>
      </table>

      <?php if (isset($recordcount)):
        $pagingLinks = generatePagingLinks('sia_trx_gaji/index', $baseUrlParams, $recordcount, $listPageSize, $listPage);
        ?>
        <div class="pull-right"><?php echo $pagingLinks; ?></div>
      <?php endif; ?>
    </div>
  </div>
</div><!-- /.row -->
<?php echo form_close(); ?>

