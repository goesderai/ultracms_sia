<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $pageTitle . ' | ' . $appName; ?></title>
  <link
    href="<?php echo base_url('assets/themes/sb-admin/css/bootstrap.min.css'); ?>"
    rel="stylesheet" type="text/css"/>
  <link
    href="<?php echo base_url('assets/themes/sb-admin/font-awesome/css/font-awesome.min.css'); ?>"
    rel="stylesheet" type="text/css"/>
  <link
    href="<?php echo base_url('assets/themes/sb-admin/css/sia-report.min.css'); ?>"
    rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="container"><!-- BEGIN report -->
  <div class="report-section report-header">
    <h3><?php echo $COMPANY_NAME; ?></h3>
    <p><?php echo sprintf("%s | %s | %s<br>%s", $COMPANY_ADDRESS, $COMPANY_PHONE, $COMPANY_FAX, $COMPANY_EMAIL); ?></p>
  </div>
  <div class="row">
    <div align="center">
      <h3><label>Laporan Transaksi Akun</label></h3>
      <h5><label>Periode <?php echo $periode . formatDateForReport($dateFrom) ?>
          s/d <?php echo formatDateForReport($dateTo) ?></label></h5>
    </div>
  </div>

  <div class="report-section report-body">
    <div class="row">
      <div style="float: right">
        <h5><label></label></h5>
      </div>
    </div>
    <div class="row">
      <table width="100%">
        <tr>
          <td width="49%" valign="top">
            <table class="table table-bordered table-hover table-striped">
              <thead>
              <tr>
                <th class="text-center">Kode Akun</th>
                <th class="text-center">Nama Akun</th>
                <th class="text-center">Debet</th>
                <th class="text-center">Kredit</th>
              </tr>
              </thead>
              <tbody>
              <?php
              $sumDebet = 0;
              $sumKredit = 0;
              $recordset = $DAO_Main_Account->getMainAccountTransaksi($dateFrom, $dateTo);

              $i = 1;
              foreach ($recordset as $row):
                ?>
                <tr>
                  <td align="left"><?php echo isset($row->account_code) ? $row->account_code : '-' ?></td>
                  <td align="left"><?php echo isset($row->name) ? $row->name : '-' ?></td>
                  <td
                    align="right"><?php echo isset($row->debet) ? number_format((float)$row->debet, 2, '.', ',') : 0 ?></td>
                  <td
                    align="right"><?php echo isset($row->kredit) ? number_format((float)$row->kredit, 2, '.', ',') : 0 ?></td>
                </tr>
                <?php
                $sumDebet = $sumDebet + $row->debet;
                $sumKredit = $sumKredit + $row->kredit;
                $i++;
              endforeach;
              ?>
              <tr>
                <td align="left" colspan="2"><b>Jumlah</b></td>
                <td align="right"><b><?php echo number_format((float)$sumDebet, 2, '.', ',') ?></b></td>
                <td align="right"><b><?php echo number_format((float)$sumKredit, 2, '.', ',') ?></b></td>
              </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </table>
    </div>

  </div>
  <div class="report-section">
    <button type="button" class="btn btn-primary btn-sm hidden-print"
            onclick="javascript:window.print();"><i class="fa fa-print"></i> Print
    </button>
  </div>
  <!-- END report --></div>
</body>
</html>
