<!DOCTYPE html>
<html lang="en">
	<head>
    <?php echo generateHeadTags($pageTitle);?>
	</head>
	<body>
	
    <div id="wrapper">
        <?php include_once 'includes/nav.php'; ?>
        <div id="page-wrapper">
            <div class="container-fluid">
              <?php echo renderPageHeading($pageTitle, $pageIcon); ?>
							<!-- BEGIN After-save message -->
							<div class="row">
								<div class="col-lg-6 col-md-6 col-xs-12 col-lg-offset-3 col-md-offset-3">
									<div class="alert <?php echo $alertType;?>" role="alert">
                    <h4><?php echo $alertMessage; ?></h4>
									</div>
									<?php if(isset($alertProceedUrl) && !empty($alertProceedUrl)):?>
									<a class="btn btn-primary btn-sm btn-block" href="<?php echo $alertProceedUrl;?>">Lanjut</a>
									<?php endif;?>
								</div>
							</div>
							<!-- END After-save message -->
							
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    
	</body>
</html>
