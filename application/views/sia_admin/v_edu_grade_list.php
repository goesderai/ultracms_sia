<!-- Page Content -->
<?php echo form_open('sia_edu_grade/index', 'method="get"'); ?>

<!-- Search Filter -->
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel-upper-buttons">
      <a href="<?php echo site_url('sia_edu_grade/insert'); ?>"
         class="btn btn-primary btn-sm" title="<?php echo lang('label_add_new_hint'); ?>">
        <i class="fa fa-plus"></i> <?php echo lang('label_add_new'); ?>
      </a>
    </div>
  </div>
</div><!-- /.row -->

<!-- Search Result -->
<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="table-responsive">
      <table class="table table-bordered table-hover table-striped">
        <thead>
        <tr>
          <th width="5%" class="text-center"><?php echo lang('label_rownum'); ?></th>
          <th width="50%" class="text-center"><?php echo lang('edugrd_grade'); ?></th>
          <th width="35%" class="text-center"><?php echo lang('edugrd_level_count'); ?></th>
          <th width="10%" class="text-center"><?php echo lang('label_action'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $baseUrlParams = $_GET;
        if (isset($recordset)) :
          $i = $rownumStart + 1;
          foreach ($recordset as $row) :
            $urlParams = $baseUrlParams + array(QSPARAM_REC_ID => $row->id);
            $editUrl = siteUrl('sia_edu_grade/update', $urlParams);
            $deleteUrl = siteUrl('sia_edu_grade/delete', $urlParams);
            ?>
            <tr>
              <td><?php echo $i; ?></td>
              <td><a href="<?php echo $editUrl; ?>" title="Edit"><?php echo $row->name; ?></a></td>
              <td class="text-right"><?php echo $row->classrooms; ?></td>
              <td class="text-center">
                <a href="javascript:void(0);"
                   onclick="siaJS_confirm('questions.confirm_delete', '<?php echo $deleteUrl; ?>');"
                   title="<?php echo lang('label_delete'); ?>" class="btn btn-danger btn-xs">
                  <i class="fa fa-times"></i>
                </a>
              </td>
            </tr>
            <?php
            unset($urlParams, $editUrl);
            $i++;
          endforeach;
        endif;
        ?>
        </tbody>
      </table>
    </div>
  </div>
</div><!-- /.row -->
<?php echo form_close(); ?>
<!-- /Page Content -->
