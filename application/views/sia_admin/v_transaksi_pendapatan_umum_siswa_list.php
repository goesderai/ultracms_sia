<?php echo form_open('sia_transaksi_pendapatan_umum_siswa/index', 'method="get"'); ?>

<div class="row">
  <div class="col-lg-3 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtNomor">Nomor Transaksi</label>
      <input id="txtNomor" type="text" name="nomor" class="form-control input-sm"/>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtNama">Nama Transaksi</label>
      <input id="txtNama" type="text" name="nama" class="form-control input-sm"/>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtNis">Nis</label>
      <input id="txtNis" type="text" name="nis" class="form-control input-sm"/>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtNamaSiswa">Nama Siswa</label>
      <input id="txtNamaSiswa" type="text" name="siswa" class="form-control input-sm"/>
    </div>
  </div>
  <div class="col-lg-6 col-md-6 col-xs-12">
    <label>Tanggal Pembayaran</label>
    <div class="form-horizontal">
      <div class="form-group">
        <div class="col-lg-5 col-md-5 col-xs-12">
          <div id="trxDatePickerFrom" class="input-group date">
            <input id="txtTrxDateFrom" name="txtTrxDateFrom" type="text" class="form-control input-sm"
                   value="<?php echo isset($postData['transaction_date_from']) ? formatDateForDisplay($postData['transaction_date_from']) : ''; ?>"
                   readonly="readonly"/>
            <span class="input-group-addon">
															<span class="fa fa-calendar"></span>
														</span>
          </div>
          <input id="transaction_date_from" name="transaction_date_from" type="hidden"
                 value="<?php echo isset($postData['transaction_date_from']) ? $postData['transaction_date_from'] : ''; ?>"/>
        </div>
        <div class="col-lg-2 col-md-2 col-xs-12 text-center">
          <label>s/d</label>
        </div>
        <div class="col-lg-5 col-md-5 col-xs-12">
          <div id="trxDatePickerTo" class="input-group date">
            <input id="txtTrxDateTo" name="txtTrxDateTo" type="text" class="form-control input-sm"
                   value="<?php echo isset($postData['transaction_date_to']) ? formatDateForDisplay($postData['transaction_date_to']) : ''; ?>"
                   readonly="readonly"/>
            <span class="input-group-addon">
															<span class="fa fa-calendar"></span>
														</span>
          </div>
          <input id="transaction_date_to" name="transaction_date_to" type="hidden"
                 value="<?php echo isset($postData['transaction_date_to']) ? $postData['transaction_date_to'] : ''; ?>"/>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-6 col-md-6 col-xs-12">&nbsp;</div>
</div><!-- /.row -->
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel-upper-buttons">
      <button id="btnCari" type="submit" class="btn btn-primary btn-sm"
              onclick="javascript:ultracms.siaTransaksiPendapatanUmumSiswaList.validateForm()">
        <i class="fa fa-search"></i> Cari
      </button>
      <button id="btnReset" type="button" class="btn btn-primary btn-sm">
        <i class="fa fa-undo"></i> Reset
      </button>
      <a href="<?php echo site_url('sia_transaksi_pendapatan_umum_siswa/insert'); ?>" class="btn btn-primary btn-sm"
         title="Tambah data baru">
        <i class="fa fa-plus"></i> Tambah
      </a>
    </div>
  </div>
</div><!-- /.row -->

<!-- Search Result -->
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="table-responsive">
      <table class="table table-bordered table-hover table-striped">
        <thead>
        <tr>
          <th class="text-center" width="5%">No</th>
          <th class="text-center" width="12%">Nomor Transaksi</th>
          <th class="text-center" width="15%">Nama Transaksi</th>
          <th class="text-center" width="15%">Nama Siswa</th>
          <th class="text-center" width="20%">Keterangan</th>
          <th class="text-center" width="10%">Tanggal Transaksi</th>
          <th class="text-center" width="7%">Jumlah Transaksi</th>
          <th colspan="2" class="text-center" width="5%">Aksi</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $baseUrlParams = array(QSPARAM_PAGE => $listPage, QSPARAM_PAGE_SIZE => $listPageSize) + $_GET;
        if (isset($recordset)):
          $i = $rownumStart + 1;
          foreach ($recordset as $row):
            $urlParams = $baseUrlParams + array(QSPARAM_REC_ID => $row->id);
            $editUrl = siteUrl('sia_transaksi_pendapatan_umum_siswa/update', $urlParams);
            $koreksiUrl = siteUrl('sia_transaksi_pendapatan_umum_siswa/koreksi', $urlParams);
            $invoiceUrl = site_url('sia_transaksi_pendapatan_umum_siswa/show_invoice/' . $row->id);
            ?>
            <tr>
              <td align="center"><?php echo $i; ?></td>
              <td><a href="<?php echo $editUrl; ?>" title="Edit"><?php echo $row->transaction_number; ?></a></td>
              <td><?php echo $row->nama_tr; ?></td>
              <td><?php echo $row->nama_siswa; ?></td>
              <td><?php echo $row->remark; ?></td>
              <td align="left"><?php echo date("d F Y", strtotime($row->transaction_date)); ?></td>
              <td align="right"><?php echo number_format((float)$row->jumlah_transaksi, 2, '.', ','); ?></td>
              <td align="center"><a href="<?php echo $invoiceUrl; ?>" target="_blank" title="Cetak Invoice"
                                    class="btn btn-primary btn-sm"><i class="fa fa-print"></i></a></td>
              <td align="center">
                <?php if ((userHasRoles(array(ROLEID_ADMIN)) || userHasRoles(array(ROLEID_ROOT))) && visibleCorrectionButton($row->transaction_number, $row->id)): ?>
                  <a href="<?php echo $koreksiUrl; ?>" title="Koreksi" class="btn btn-danger btn-sm"><i
                      class="fa fa-edit"></i></a>
                <?php endif; ?>
              </td>

            </tr>
            <?php
            unset($urlParams, $editUrl);
            $i++;
          endforeach;
        endif;
        ?>
        </tbody>
      </table>

      <?php if (isset($recordcount)):
        $pagingLinks = generatePagingLinks('sia_transaksi_pendapatan_umum_siswa/index', $baseUrlParams, $recordcount, $listPageSize, $listPage);
        ?>
        <div class="pull-right"><?php echo $pagingLinks; ?></div>
      <?php endif; ?>
    </div>
  </div>
</div><!-- /.row -->
<?php echo form_close(); ?>
