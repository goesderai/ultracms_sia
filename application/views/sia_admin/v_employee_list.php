<!-- Page Content -->
<?php echo form_open('sia_employees/index', 'method="get"'); ?>

<!-- Search Filter -->
<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtNama"><?php echo lang('employee_name'); ?></label>
      <input id="txtNama" name="nama" type="text" class="form-control input-sm"
             value="<?php echo isset($postData['nama']) ? $postData['nama'] : ''; ?>"/>
    </div>
  </div>
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtIdNumber"><?php echo lang('employee_identity_number'); ?></label>
      <input id="txtIdNumber" name="id_number" type="text" class="form-control input-sm"
             value="<?php echo isset($postData['id_number']) ? $postData['id_number'] : ''; ?>"/>
    </div>
  </div>
</div><!-- /.row -->
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel-upper-buttons">
      <button id="btnCari" type="submit" class="btn btn-primary btn-sm">
        <i class="fa fa-search"></i> <?php echo lang('label_search'); ?>
      </button>
      <button id="btnReset" type="button" class="btn btn-primary btn-sm">
        <i class="fa fa-undo"></i> <?php echo lang('label_reset'); ?>
      </button>
      <a href="<?php echo site_url('sia_employees/insert'); ?>" class="btn btn-primary btn-sm"
         title="<?php echo lang('label_add_new_hint'); ?>">
        <i class="fa fa-plus"></i> <?php echo lang('label_add_new'); ?>
      </a>
    </div>
  </div>
</div><!-- /.row -->

<!-- Search Result -->
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="table-responsive">
      <table class="table table-bordered table-hover table-striped">
        <thead>
        <tr>
          <th width="5%" class="text-center"><?php echo lang('label_rownum'); ?></th>
          <th width="20%" class="text-center"><?php echo lang('employee_name'); ?></th>
          <th width="25%" class="text-center"><?php echo lang('employee_address'); ?></th>
          <th width="20%" class="text-center"><?php echo lang('employee_identity_number'); ?></th>
          <th width="20%" class="text-center"><?php echo lang('employee_phone'); ?></th>
          <th width="10%" class="text-center"><?php echo lang('label_action'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $baseUrlParams = array(QSPARAM_PAGE => $listPage, QSPARAM_PAGE_SIZE => $listPageSize) + $_GET;
        if (isset($recordset)):
          $i = $rownumStart + 1;
          foreach ($recordset as $row):
            $urlParams = $baseUrlParams + array(QSPARAM_REC_ID => $row->id);
            $editUrl = siteUrl('sia_employees/update', $urlParams);
            $deleteUrl = siteUrl('sia_employees/delete', $urlParams);
            ?>
            <tr>
              <td><?php echo $i; ?></td>
              <td><a href="<?php echo $editUrl; ?>" title="Edit"><?php echo $row->name; ?></a></td>
              <td><?php echo $row->address; ?></td>
              <td><?php echo $row->id_number; ?></td>
              <td><?php echo $row->phone_number; ?></td>
              <td class="text-center">
                <a href="javascript:void(0);" title="Hapus" class="btn btn-danger btn-xs"
                   onclick="siaJS_confirm('questions.confirm_delete', '<?php echo $deleteUrl; ?>');">
                  <i class="fa fa-times"></i>
                </a>
              </td>
            </tr>
            <?php
            unset($urlParams, $editUrl);
            $i++;
          endforeach;
        endif;
        ?>
        </tbody>
      </table>

      <?php if (isset($recordcount)):
        $pagingLinks = generatePagingLinks('sia_employees/index', $baseUrlParams, $recordcount, $listPageSize, $listPage);
        ?>
        <div class="pull-right"><?php echo $pagingLinks; ?></div>
      <?php endif; ?>
    </div>
  </div>
</div><!-- /.row -->
<?php echo form_close(); ?>
