<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $pageTitle . ' | ' . $appName; ?></title>
  <link
    href="<?php echo base_url('assets/themes/sb-admin/css/bootstrap.min.css'); ?>"
    rel="stylesheet" type="text/css"/>
  <link
    href="<?php echo base_url('assets/themes/sb-admin/font-awesome/css/font-awesome.min.css'); ?>"
    rel="stylesheet" type="text/css"/>
  <link
    href="<?php echo base_url('assets/themes/sb-admin/css/sia-report.min.css'); ?>"
    rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="container-fluid"><!-- BEGIN report -->
  <div class="report-section report-header">
    <h3><?php echo $COMPANY_NAME; ?></h3>
    <p><?php rptPrintCompanyInfo($COMPANY_ADDRESS, $COMPANY_PHONE, $COMPANY_FAX, $COMPANY_EMAIL, $COMPANY_FACEBOOK, $COMPANY_INSTAGRAM); ?></p>
  </div>
  <div class="row">
    <div class="col-md-12 col-xs-12">
      <h3 class="text-center"><?php echo $pageTitle; ?></h3>
      <h5 class="text-right"><?php echo $ket . formatDateForReport($tanggal); ?></h5>
    </div>
  </div>

  <div class="report-section report-body">
    <div class="table-responsive">
      <table class="table table-bordered table-hover table-striped">
        <thead>
        <tr>
          <th class="text-center" style="width: 3%">No</th>
          <th class="text-center" style="width: 10%">No.Transaksi</th>
          <th class="text-center" style="width: 17%">Jenis Transaksi</th>
          <th class="text-center" style="width: 32%">Keterangan</th>
          <th class="text-center" style="width: 7%">Pendapatan</th>
          <th class="text-center" style="width: 7%">Pengeluaran</th>
          <th class="text-center" style="width: 7%">Penarikan</th>
          <th class="text-center" style="width: 7%">Penyetoran</th>
          <th class="text-center" style="width: 10%">Jumlah</th>
        </tr>
        </thead>
        <tbody>
        <!-- Kas Awal Hari ini -->
        <?php
        $sumPendapatan    = 0;
        $sumPengeluaran   = 0;
        $sumPenarikan     = 0;
        $sumPenyetoran    = 0;
        $sql              = $DAO_Transaksi->getKas($filtersKasAwal);
        $recordsetKasAwal = $DAO_Transaksi->db->query($sql);
        $i                = 1;
        foreach ($recordsetKasAwal->result() as $row):
          ?>
          <tr>
            <td/>
            <td colspan="3" class="text-left"><b>Kas Awal</b></td>
            <td colspan="5" class="text-right">
              <b><?php echo isset($row->sisa) ? number_format((float)$row->sisa, 2, '.', ',') : 0 ?></b>
            </td>
          </tr>
          <?php
          $i++;
        endforeach;
        ?>

        <!-- Transaksi -->
        <?php
        $sql       = $DAO_Transaksi->getRptKasHarian($filters);
        $recordset = $DAO_Transaksi->db->query($sql);
        $i         = 1;
        foreach ($recordset->result() as $row):
          ?>
          <tr>
            <td class="text-center"><?php echo $i ?></td>
            <td><?php echo $row->transaction_number ?></td>
            <td><?php echo $row->transaction_type . ' - ' . $row->akun ?></td>
            <td><?php echo $row->keterangan ?></td>
            <td class="text-right"><?php echo number_format((float)$row->debet, 2, '.', ',') ?></td>
            <td class="text-right"><?php echo number_format((float)$row->kredit, 2, '.', ',') ?></td>
            <td class="text-right"><?php echo number_format((float)$row->penarikan, 2, '.', ',') ?></td>
            <td class="text-right"><?php echo number_format((float)$row->setoran, 2, '.', ',') ?></td>
            <td/>

          </tr>
          <?php
          $sumPengeluaran = $sumPengeluaran + $row->kredit;
          $sumPendapatan  = $sumPendapatan + $row->debet;
          $sumPenarikan   = $sumPenarikan + $row->penarikan;
          $sumPenyetoran  = $sumPenyetoran + $row->setoran;
          $i++;
        endforeach;
        ?>

        <!-- Kas Akhir Hari ini -->
        <?php

        $sql               = $DAO_Transaksi->getKas($filtersKasAkhir);
        $recordsetKasAkhir = $DAO_Transaksi->db->query($sql);
        $i                 = 1;
        foreach ($recordsetKasAkhir->result() as $row):
          ?>
          <tr>
            <td/>
            <td colspan="3" align="left"><b>Kas Akhir</b></td>
            <td class="text-right"><b><?php echo number_format((float)$sumPendapatan, 2, '.', ',') ?></b></td>
            <td class="text-right"><b><?php echo number_format((float)$sumPengeluaran, 2, '.', ',') ?></b></td>
            <td class="text-right"><b><?php echo number_format((float)$sumPenarikan, 2, '.', ',') ?></b></td>
            <td class="text-right"><b><?php echo number_format((float)$sumPenyetoran, 2, '.', ',') ?></b></td>
            <td class="text-right"><b><?php echo isset($row->sisa) ? number_format((float)$row->sisa, 2, '.', ',') : 0 ?></b>
            </td>
          </tr>
          <?php
          $i++;
        endforeach;
        ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="report-section">
    <div class="row">
      <div class="col-md-12 col-xs-12 form-group">
        <button type="button" class="btn btn-primary btn-sm hidden-print"
                onclick="javascript:window.print();"><i class="fa fa-print"></i> Print
        </button>
      </div>
    </div>
  </div>
  <!-- END report --></div>
</body>
</html>
