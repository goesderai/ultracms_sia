<!DOCTYPE html>
<html lang="en">

	<head>
      <?php
        $js = array('assets/js/ultracms_sia_login.min.js');
        echo generateHeadTags($pageTitle, null, null, $js);
      ?>
      <script src="https://www.google.com/recaptcha/api.js" type="text/javascript"></script>
      <style type="text/css">
        body{padding-top:10%;}
      </style>
      <script type="text/javascript">
        $(document).ready(function(){
        	ultracms.siaLogin.init();
        });
      </script>
	</head>
	
	<body>
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-xs-12 col-md-offset-4">
					<?php echo form_open('sia_login/index', array('id' => 'loginForm'));?>
						<div class="container-fluid">
							<?php if(isset($errMsg)): ?>
							<div class="row">
								<div class="col-md-12 col-xs-12">
									<div class="alert alert-danger" role="alert">
										<b><?php echo $errMsg;?></b>
									</div>
								</div>
							</div>
							<?php endif; ?>
							<div class="row">
								<div class="col-md-12 col-xs-12">
									<h4 style="color: white"><b><?php echo $pageTitle;?></b></h4>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 col-xs-12">
									<div class="form-group">
										<div class="input-group input-group-sm">
											<span class="input-group-addon" id="sizing-addon1" >
												<i class="fa fa-user" aria-hidden="true"></i>
											</span>
											<input id="txtUsername" name="username" type="text" class="form-control" 
												aria-describedby="sizing-addon1" placeholder="<?php echo lang('label_username'); ?>" tabindex="1" />
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 col-xs-12">
									<div class="form-group">
										<div class="input-group input-group-sm">
											<span class="input-group-addon" id="sizing-addon2">
												<i class="fa fa-key" aria-hidden="true"></i>
											</span>
											<input id="txtPassword" name="password" type="password" class="form-control" 
												aria-describedby="sizing-addon2" placeholder="<?php echo lang('label_password'); ?>" tabindex="2" />
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 col-xs-12">
									<div class="form-group">
										<div class="g-recaptcha" style="width: 304px; margin: 0 auto;" 
											data-sitekey="<?php echo $reCaptchaPublicKey;?>"></div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 col-xs-12">
									<button type="submit" class="btn btn-primary btn-sm btn-block"><?php echo lang('label_login'); ?></button>
								</div>
							</div>
						</div>
					<?php echo form_close();?>
				</div>
			</div>
		</div>
	</body>

</html>