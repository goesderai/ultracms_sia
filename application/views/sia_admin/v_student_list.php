<!-- BEGIN Search Filter -->
<?php echo form_open('sia_students/index', array('name' => 'formFilter', 'method' => 'get')); ?>
<div class="row">
  <div class="col-md-4 col-xs-12">
    <div class="form-group">
      <label for="txtNis"><?php echo lang('student_nis'); ?></label>
      <input id="txtNis" name="nis" type="text"
             class="form-control input-sm"
             value="<?php echo isset($postData['nis']) ? $postData['nis'] : ''; ?>"/>
    </div>
  </div>
  <div class="col-md-4 col-xs-12">
    <div class="form-group">
      <label for="txtNama"><?php echo lang('student_name'); ?></label>
      <input id="txtNama" name="nama" type="text"
             class="form-control input-sm"
             value="<?php echo isset($postData['nama']) ? $postData['nama'] : ''; ?>"/>
    </div>
  </div>
  <div class="col-md-4 col-xs-12">
    <div class="form-group">
      <label for="cmbStatus"><?php echo lang('student_parent_name'); ?></label>
      <input id="txtParentName" name="parent_name" type="text"
             class="form-control input-sm"
             value="<?php echo isset($postData['parent_name']) ? $postData['parent_name'] : ''; ?>"/>
    </div>
  </div>
</div>
<!-- /.row -->
<div class="row">
  <div class="col-md-4 col-xs-12">
    <div class="form-group">
      <label for="cmbGrade"><?php echo lang('student_educational_level'); ?></label>
      <?php
      $grades = array(lang('student_choose')) + $DAO_Edu->getGradeSelectOptions();
      $selectedGrade = isset($postData['grade']) ? $postData['grade'] : '';
      $cmbGradeAttr = array('id' => 'cmbGrade', 'class' => 'form-control input-sm');
      echo form_dropdown('grade', $grades, $selectedGrade, $cmbGradeAttr);
      ?>
    </div>
  </div>
  <div class="col-md-4 col-xs-12">
    <div class="form-group">
      <label for="cmbClassroom"><?php echo lang('student_grade'); ?></label>
      <select id="cmbClassroom" name="classroom"
              class="form-control imput-sm">
        <option value="0" disabled="disabled"
                selected="selected"><?php echo lang('student_choose_educational_level'); ?></option>
      </select> <input type="hidden"
                       id="cmbClassroomInitialValue"
                       value="{&quot;value&quot;: &quot;0&quot;, &quot;label&quot;: &quot;<?php echo lang('student_choose_educational_level'); ?>&quot;}"/>
    </div>
  </div>
  <div class="col-md-4 col-xs-12">
    <div class="form-group">
      <label for="cmbStatus"><?php echo lang('student_status'); ?></label>
      <?php
      $statuses = array(lang('student_choose')) + $DAO_Lov->getLovs('STATUS_SISWA', 'sequence');
      $selectedStatus = isset($postData['status']) ? $postData['status'] : '';
      $cmbStatusAttr = array('id' => 'cmbStatus', 'class' => 'form-control input-sm');
      echo form_dropdown('status', $statuses, $selectedStatus, $cmbStatusAttr);
      ?>
    </div>
  </div>
</div>
<!-- /.row -->
<div class="row">
  <div class="col-md-12 col-xs-12">
    <div class="panel-upper-buttons">
      <button id="btnCari" type="submit"
              class="btn btn-primary btn-sm">
        <i class="fa fa-search"></i> <?php echo lang('label_search'); ?>
      </button>
      <button id="btnReset" type="button"
              class="btn btn-primary btn-sm">
        <i class="fa fa-undo"></i> <?php echo lang('label_reset'); ?>
      </button>
      <a href="<?php echo site_url('sia_students/insert'); ?>"
         class="btn btn-primary btn-sm"
         title="<?php echo lang('label_add_new_hint'); ?>"> <i
          class="fa fa-plus"></i> <?php echo lang('label_add_new'); ?>
      </a>
    </div>
  </div>
</div>
<!-- /.row -->
<?php echo form_close(); ?>
<!-- END Search Filter -->

<!-- BEGIN Search Result -->
<?php echo form_open('sia_students/index', array('name' => 'formBulk', 'method' => 'post')); ?>
<div class="row">
  <div class="col-md-12 col-xs-12">
    <div class="table-responsive">
      <table
        class="table table-bordered table-hover table-striped">
        <thead>
        <tr>
          <th width="5%" class="text-center"><?php echo lang('label_rownum'); ?></th>
          <th width="15%" class="text-center"><?php echo lang('student_nis'); ?></th>
          <th width="20%" class="text-center"><?php echo lang('student_name'); ?></th>
          <th width="20%" class="text-center"><?php echo lang('student_educational_level'); ?></th>
          <th width="15%" class="text-center"><?php echo lang('student_grade'); ?></th>
          <th width="15%" class="text-center"><?php echo lang('student_status'); ?></th>
          <th width="10%" class="text-center"><?php echo lang('label_action'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $baseUrlParams = array(QSPARAM_PAGE => $listPage, QSPARAM_PAGE_SIZE => $listPageSize) + $_GET;
        if (isset($recordset)) :
          $i = $rownumStart + 1;
          foreach ($recordset as $row) :
            $urlParams = $baseUrlParams + array(QSPARAM_REC_ID => $row->id);
            $editUrl = siteUrl('sia_students/update', $urlParams);
            $deleteUrl = siteUrl('sia_students/delete', $urlParams);
            ?>
            <tr>
              <td><?php echo $i; ?></td>
              <td><a href="<?php echo $editUrl; ?>" title="Edit"><?php echo $row->nis; ?></a></td>
              <td><?php echo $row->name; ?></td>
              <td><?php echo $row->grade_name; ?></td>
              <td><?php echo $row->classroom_name; ?></td>
              <td><?php echo getLovLabel($row->status, $statuses); ?></td>
              <td class="text-center">
                <a href="javascript:void(0);" title="Hapus" class="btn btn-danger btn-xs"
                   onclick="siaJS_confirm('questions.confirm_delete', '<?php echo $deleteUrl; ?>');">
                  <i class="fa fa-times"></i>
                </a>
              </td>
            </tr>
            <?php
            unset($urlParams, $editUrl);
            $i++;
          endforeach;
        endif;
        ?>
        </tbody>
      </table>
      <?php
      if (isset($recordcount)) :
        $pagingLinks = generatePagingLinks('sia_students/index', $baseUrlParams, $recordcount, $listPageSize, $listPage);
        ?>
        <div class="pull-right"><?php echo $pagingLinks; ?></div>
      <?php endif; ?>
    </div>
  </div>
</div><!-- /.row -->
<?php echo form_close(); ?>
<!-- END Search Result -->
