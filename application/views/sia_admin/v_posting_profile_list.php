<!-- Page Content -->
<?php echo form_open('sia_posting_profile/index', 'method="get"'); ?>

<!-- Search Filter -->
<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtAkun"><?php echo lang('pstngprfl_account_debit'); ?></label>
      <?php
      $mainAccount = $DAO_Main_Account->getAllAsArray();
      $selectedMainAccount = isset($postData['account_debet']) ? $postData['account_debet'] : '';
      $cmbMainAccountAttr = array('id' => 'account_debet', 'class' => 'form-control input-sm');
      echo form_dropdown('account_debet', $mainAccount, $selectedMainAccount, $cmbMainAccountAttr);
      ?>
    </div>
  </div>
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtParent"><?php echo lang('pstngprfl_account_credit'); ?></label>
      <?php
      $balance = $DAO_Main_Account->getAllAsArray();
      $selectedBalance = isset($postData['account_credit']) ? $postData['account_credit'] : '';
      $cmbBalanceAttr = array('id' => 'account_credit', 'class' => 'form-control input-sm');
      echo form_dropdown('account_credit', $balance, $selectedBalance, $cmbBalanceAttr);
      ?>
    </div>
  </div>
</div><!-- /.row -->
<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="cmbTipeTransaksi"><?php echo lang('pstngprfl_trx_type'); ?></label>
      <?php
      $tipeTransaksi = $DAO_TipeTransaksi->getAllTrxTypeAsArray(array(emptyComboOption(lang('label_all'))));
      $selectedtipeTransaksi = isset($postData['trx_type']) ? $postData['trx_type'] : '';
      $cmbTipeTransaksiAttr = array('id' => 'cmbTipeTransaksi', 'class' => 'form-control input-sm');
      echo form_dropdown('trx_type', $tipeTransaksi, $selectedtipeTransaksi, $cmbTipeTransaksiAttr);
      ?>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel-upper-buttons">
      <button id="btnCari" type="submit" class="btn btn-primary btn-sm">
        <i class="fa fa-search"></i> <?php echo lang('label_search'); ?>
      </button>
      <button id="btnReset" type="button" class="btn btn-primary btn-sm">
        <i class="fa fa-undo"></i> <?php echo lang('label_reset'); ?>
      </button>
      <a href="<?php echo site_url('sia_posting_profile/insert'); ?>" class="btn btn-primary btn-sm"
         title="<?php echo lang('label_add_new_hint'); ?>">
        <i class="fa fa-plus"></i> <?php echo lang('label_add_new'); ?>
      </a>
    </div>
  </div>
</div><!-- /.row -->

<!-- Search Result -->
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="table-responsive">
      <table class="table table-bordered table-hover table-striped">
        <thead>
        <tr>
          <th width="5%" class="text-center"><?php echo lang('label_rownum'); ?></th>
          <th width="25%" class="text-center"><?php echo lang('pstngprfl_trx_type'); ?></th>
          <th width="35%" class="text-center"><?php echo lang('pstngprfl_account_debit'); ?></th>
          <th width="35%" class="text-center"><?php echo lang('pstngprfl_account_credit'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $baseUrlParams = array(QSPARAM_PAGE => $listPage, QSPARAM_PAGE_SIZE => $listPageSize) + $_GET;
        if (isset($recordset)):
          $i = $rownumStart + 1;
          foreach ($recordset as $row):
            $urlParams = $baseUrlParams + array(QSPARAM_REC_ID => $row->id);
            $editUrl = siteUrl('sia_posting_profile/update', $urlParams);
            ?>
            <tr>
              <td><?php echo $i; ?></td>
              <td><a href="<?php echo $editUrl; ?>"
                     title="Edit"><?php $typeTrx = $DAO_TipeTransaksi->getRow($row->trx_type);
                  echo $typeTrx['transaction_type'] . ' ' . $typeTrx['nama'] ?></a></td>
              <td><?php $accountDebet = $DAO_Main_Account->getMainAccountName($row->account_debet);
                echo $accountDebet; ?></td>
              <td><?php $accountCredit = $DAO_Main_Account->getMainAccountName($row->account_credit);
                echo $accountCredit; ?></td>
            </tr>
            <?php
            unset($urlParams, $editUrl);
            $i++;
          endforeach;
        endif;
        ?>
        </tbody>
      </table>

      <?php if (isset($recordcount)):
        $pagingLinks = generatePagingLinks('sia_posting_profile/index', $baseUrlParams, $recordcount, $listPageSize, $listPage);
        ?>
        <div class="pull-right"><?php echo $pagingLinks; ?></div>
      <?php endif; ?>
    </div>
  </div>
</div><!-- /.row -->
<?php echo form_close(); ?>
