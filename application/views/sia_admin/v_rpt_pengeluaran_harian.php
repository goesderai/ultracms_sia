<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $pageTitle . ' | ' . $appName; ?></title>
  <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css"/>
  <link rel="stylesheet" type="text/css"
        href="<?php echo base_url('assets/themes/sb-admin/font-awesome/css/font-awesome.min.css'); ?>"/>
  <link rel="stylesheet" type="text/css"
        href="<?php echo base_url('assets/themes/sb-admin/css/sia-report.min.css'); ?>"/>
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script type="text/javascript"
          src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
  <!-- BEGIN report -->
  <div class="report-section report-header">
    <h3><?php echo $COMPANY_NAME; ?></h3>
    <p><?php echo sprintf("%s | %s | %s<br>%s", $COMPANY_ADDRESS, $COMPANY_PHONE, $COMPANY_FAX, $COMPANY_EMAIL); ?></p>
  </div>
  <div class="report-section report-body">
    <h4><?php echo $pageTitle; ?></h4>
    <p>Tahun: <b><?php echo $year; ?></b></p>
    <p>Bulan: <b><?php $dateObj = DateTime::createFromFormat('!m', $month); echo $dateObj->format('F'); ?></b></p>
    <div class="form-group">
      <div class="table-responsive">
        <table id="reportTable" class="table table-bordered table-condensed table-striped display">
          <thead>
          <tr>
            <th style="text-align:center;" >Tanggal</th>
            <?php
            foreach ($headerData as $head) {
              echo sprintf('<th class="text-center">%s</th>', $head->nama);
            }
            ?>
            <th style="text-align:center;" >Jumlah</th>
          </tr>
          </thead>
          <tbody>
          <?php
          //Building data skeleton
          $rows = [];
          $columnTotal = [];
          for ($i = 0; $i < 31; $i++) {
            $rows[$i + 1] = [];
            foreach ($headerData as $head) {
              $rows[$i + 1][$head->id] = 0;
              $columnTotal[$head->id] = 0;
            }
          }

          //Loading data
          foreach ($bodyData as $body) {
            $day = (integer) date('d', strtotime($body->transaction_date));
            $rows[$day][$body->id] = [$body->id, $body->amount];
          }

          //Render rows
          $i = 0;
          $cols = 0;
          $grandTotal = 0;
          foreach ($rows as $row => $rowVal) {
            $totalAmount = 0;
            echo sprintf('<tr><td class="text-center">%s</td>', $row);
            if ($i < 1) $cols++;
            foreach ($rowVal as $value) {
              echo sprintf('<td class="text-right">%s</td>', number_format($value[1], 2));
              $totalAmount += $value[1];
              if (isset($columnTotal[$value[0]])) $columnTotal[$value[0]] += $value[1];
              if ($i < 1) $cols++;
            }
            echo sprintf('<td class="text-right">%s</td></tr>', number_format($totalAmount, 2));
            if ($i < 1) $cols++;
            $grandTotal += $totalAmount;
            $i++;
          }
          ?>
          </tbody>
          <tfoot>
          <tr>
            <td class="text-center"><b>Total</b></td>
            <?php
            $columnGrandTotal = 0;
            foreach ($columnTotal as $colTtl) {
              echo sprintf('<td class="text-right"><b>%s</b></td>', number_format($colTtl, 2));
              $columnGrandTotal += $colTtl;
            }
            ?>
            <td class="text-right"><b><?php echo number_format($columnGrandTotal, 2); ?></b></td>
          </tr>
          <!--<tr>
            <td class="text-center" colspan="<?php echo $cols - 1; ?>">Total</td>
            <td class="text-right"><b><?php echo number_format($grandTotal, 2); ?></b></td>
          </tr>
          --></tfoot>
        </table>
      </div>
    </div>
    <div class="form-group">
      <button id="btnPrint" type="button" class="btn btn-primary hidden-print" onclick="javascript: window.print()"><i
          class="fa fa-print"></i> Print
      </button>
    </div>
  </div>
  <!-- END report -->
</div>
</body>
</html>
