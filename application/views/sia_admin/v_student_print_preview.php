<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo $pageTitle . ' | ' . $appName;?></title>
		<link href="<?php echo base_url('assets/themes/sb-admin/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url('assets/themes/sb-admin/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url('assets/themes/sb-admin/css/sia-report.min.css');?>" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<div class="container">
			<!-- BEGIN report -->
			<div class="report-section report-header">
				<h3><?php echo $COMPANY_NAME;?></h3>
				<p><?php echo sprintf("%s | %s | %s<br>%s", $COMPANY_ADDRESS, $COMPANY_PHONE, $COMPANY_FAX, $COMPANY_EMAIL);?></p>
			</div>
			<div class="report-section report-body">
				<!-- BEGIN Student info -->
				<div class="container-fluid">
					
					<!-- BEGIN General info -->
					<div class="row">
						<div class="col-lg-12 col-md-12 col-xs-6">
							<h4>Informasi Umum</h4>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtNis">NIS</label>
								<input id="txtNis" name="nis" type="text" class="form-control input-sm" 
									value="<?php echo isset($postData['nis'])? $postData['nis'] : '';?>" />
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtNama">Nama</label>
								<input id="txtNama" name="name" type="text" class="form-control input-sm" 
									value="<?php echo isset($postData['name'])? $postData['name'] : '';?>" />
							</div>
						</div>
					</div><!-- /.row -->
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtNickname">Nama Panggilan</label>
								<input id="txtNickname" name="nickname" type="text" class="form-control input-sm"
									value="<?php echo isset($postData['nickname'])? $postData['nickname'] : '';?>" />
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtGender">Jenis Kelamin</label>
								<input id="txtGender" name="gender" type="text" class="form-control input-sm"
									value="<?php echo isset($postData['gender_str'])? $postData['gender_str'] : '';?>" />
							</div>
						</div>
					</div><!-- /.row -->
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtBirthPlace">Tempat Lahir</label>
								<input id="txtBirthPlace" name="birth_place" type="text" class="form-control input-sm"
									value="<?php echo isset($postData['birth_place'])? $postData['birth_place'] : '';?>" />
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtBirthDate">Tanggal Lahir</label>
								<input id="txtBirthDate" name="txtBirthDate" type="text" class="form-control input-sm" 
									value="<?php echo isset($postData['birth_date'])? formatDateForDisplay($postData['birth_date']) : '';?>"/>
							</div>
						</div>
					</div><!-- /.row -->
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtWeight">Berat Badan (kg)</label>
								<input id="txtWeight" name="weight" type="text" class="form-control input-sm"
									value="<?php echo isset($postData['weight'])? $postData['weight'] : '';?>" />
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtHeight">Tinggi Badan (cm)</label>
								<input id="txtHeight" name="height" type="text" class="form-control input-sm"
									value="<?php echo isset($postData['height'])? $postData['height'] : '';?>" />
							</div>
						</div>
					</div><!-- /.row -->
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtBloodType">Golongan Darah</label>
								<input id="txtBloodType" name="blood_type" type="text" class="form-control input-sm"
									value="<?php echo isset($postData['blood_type'])? $postData['blood_type'] : '';?>" />
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtIllnessHistory">Penyakit yang Pernah Diderita</label>
								<input id="txtIllnessHistory" name="illness_history" type="text" class="form-control input-sm"
									value="<?php echo isset($postData['illness_history'])? $postData['illness_history'] : '';?>" />
							</div>
						</div>
					</div><!-- /.row -->
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtNthChild">Anak Nomor Ke</label>
								<input id="txtNthChild" name="nth_child" type="text" class="form-control input-sm"
									value="<?php echo isset($postData['nth_child'])? $postData['nth_child'] : '';?>" />
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtNSiblings">Jumlah Saudara Kandung</label>
								<input id="txtNSiblings" name="n_siblings" type="text" class="form-control input-sm"
									value="<?php echo isset($postData['n_siblings'])? $postData['n_siblings'] : '';?>" />
							</div>
						</div>
					</div><!-- /.row -->
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtNStepSiblings">Jumlah Saudara Tiri</label>
								<input id="txtNStepSiblings" name="n_step_siblings" type="text" class="form-control input-sm"
									value="<?php echo isset($postData['n_step_siblings'])? $postData['n_step_siblings'] : '';?>" />
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtNFosterSiblings">Jumlah Saudara Angkat</label>
								<input id="txtNFosterSiblings" name="n_foster_siblings" type="text" class="form-control input-sm"
									value="<?php echo isset($postData['n_foster_siblings'])? $postData['n_foster_siblings'] : '';?>" />
							</div>
						</div>
					</div><!-- /.row -->
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtReligion">Agama</label>
								<input id="txtReligion" type="text" class="form-control input-sm"
									value="<?php echo isset($postData['religion'])? $postData['religion'] : '';?>" />
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtNationality">Kewarganegaraan</label>
								<input id="txtNationality" name="nationality" type="text" class="form-control input-sm"
									value="<?php echo isset($postData['nationality'])? $postData['nationality'] : '';?>" />
							</div>
						</div>
					</div><!-- /.row -->
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtAddress">Alamat</label>
								<input id="txtAddress" name="address" type="text" class="form-control input-sm" 
									value="<?php echo isset($postData['address'])? $postData['address'] : '';?>" />
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtPhoneNumber">No. Telephone</label>
								<input id="txtPhoneNumber" name="phone_number" type="text" class="form-control input-sm"
									value="<?php echo isset($postData['phone_number'])? $postData['phone_number'] : '';?>" />
							</div>
						</div>
					</div><!-- /.row -->
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtDailyLanguage">Bahasa Sehari-Hari</label>
								<input id="txtDailyLanguage" name="daily_language" type="text" class="form-control input-sm"
									value="<?php echo isset($postData['daily_language'])? $postData['daily_language'] : '';?>" />
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtStayedAt">Bertempat Tinggal Pada</label>
								<input id="txtStayedAt" type="text" class="form-control input-sm"
									value="<?php echo isset($postData['stayed_at'])? $postData['stayed_at'] : '';?>" />
							</div>
						</div>
					</div><!-- /.row -->
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtGrade">Tingkat</label>
								<input id="txtGrade" type="text" class="form-control input-sm"
									value="<?php echo isset($postData['grade'])? $postData['grade'] : '';?>" />
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtClassroom">Ruang Kelas</label>
								<input id="txtClassroom" type="text" class="form-control input-sm"
									value="<?php echo isset($postData['classroom'])? $postData['classroom'] : '';?>" />
							</div>
						</div>
					</div>
					<!-- END General info -->
					
					<!-- BEGIN Parent info -->
					<div class="row">
						<div class="col-lg-12 col-md-12 col-xs-6">
							<h4>Informasi Orang Tua / Wali</h4>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtFatherName">Nama Ayah Kandung</label>
								<input id="txtFatherName" name="father_name" type="text" class="form-control input-sm" 
									value="<?php echo isset($postData['father_name'])? $postData['father_name'] : '';?>" />
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtMotherName">Nama Ibu Kandung</label>
								<input id="txtMotherName" name="mother_name" type="text" class="form-control input-sm" 
									value="<?php echo isset($postData['mother_name'])? $postData['mother_name'] : '';?>" />
							</div>
						</div>
					</div><!-- /.row -->
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtFatherLastEdu">Pendidikan Tertinggi Ayah Kandung</label>
								<input id="txtFatherLastEdu" name="father_last_education" type="text" class="form-control input-sm" 
									value="<?php echo isset($postData['father_last_education'])? $postData['father_last_education'] : '';?>" />
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtMotherLastEdu">Pendidikan Tertinggi Ibu Kandung</label>
								<input id="txtMotherLastEdu" name="mother_last_education" type="text" class="form-control input-sm" 
									value="<?php echo isset($postData['mother_last_education'])? $postData['mother_last_education'] : '';?>" />
							</div>
						</div>
					</div><!-- /.row -->
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtParentOcupation">Pekerjaan Orang Tua (Ayah/Ibu)</label>
								<input id="txtParentOcupation" name="parent_ocupation" type="text" class="form-control input-sm" 
									value="<?php echo isset($postData['parent_ocupation'])? $postData['parent_ocupation'] : '';?>" />
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtGuardianName">Nama Wali Siswa (Jika Ada)</label>
								<input id="txtGuardianName" name="guardian_name" type="text" class="form-control input-sm" 
									value="<?php echo isset($postData['guardian_name'])? $postData['guardian_name'] : '';?>" />
							</div>
						</div>
					</div><!-- /.row -->
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtGuardianLastEdu">Pendidikan Tertinggi Wali Siswa</label>
								<input id="txtGuardianLastEdu" name="guardian_last_education" type="text" class="form-control input-sm" 
									value="<?php echo isset($postData['guardian_last_education'])? $postData['guardian_last_education'] : '';?>" />
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtGuardianRelationship">Hubungan Terhadap Anak</label>
								<input id="txtGuardianRelationship" name="guardian_relationship" type="text" class="form-control input-sm" 
									value="<?php echo isset($postData['guardian_relationship'])? $postData['guardian_relationship'] : '';?>" />
							</div>
						</div>
					</div><!-- /.row -->
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtGuardianOcupation">Pekerjaan Wali Siswa</label>
								<input id="txtGuardianOcupation" name="guardian_ocupation" type="text" class="form-control input-sm" 
									value="<?php echo isset($postData['guardian_ocupation'])? $postData['guardian_ocupation'] : '';?>" />
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-6">&nbsp;</div>
					</div><!-- /.row -->
					<!-- END Parent info -->
					
					<!-- BEGIN Origin info -->
					<div class="row">
						<div class="col-lg-12 col-md-12 col-xs-6">
							<h4>Informasi Lainnya</h4>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtEnteredAs">Masuk Sekolah ini Sebagai</label>
								<input id="txtEnteredAs" type="text" class="form-control input-sm" 
									value="<?php echo isset($postData['entered_school_as'])? $postData['entered_school_as'] : '';?>" />
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtStudentOrigin">Asal Anak</label>
								<input id="txtStudentOrigin" type="text" class="form-control input-sm" 
									value="<?php echo isset($postData['student_origin'])? $postData['student_origin'] : '';?>" />
							</div>
						</div>
					</div><!-- /.row -->
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtPlayGroupName">Nama Kelompok Bermain</label>
								<input id="txtPlayGroupName" type="text" class="form-control input-sm" 
									value="<?php echo isset($postData['play_group_name'])? $postData['play_group_name'] : '';?>" />
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtPlayGroupLocation">Lokasi Kelompok Bermain</label>
								<input id="txtPlayGroupLocation" type="text" class="form-control input-sm" 
									value="<?php echo isset($postData['play_group_location'])? $postData['play_group_location'] : '';?>" />
							</div>
						</div>
					</div><!-- /.row -->
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtNomorSttb">Tahun & Nomor STTB</label>
								<input id="txtNomorSttb" type="text" class="form-control input-sm" 
									value="<?php echo isset($postData['tahun_dan_nomor_sttb'])? $postData['tahun_dan_nomor_sttb'] : '';?>" />
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtYearsOfLearning">Lama Belajar</label>
								<input id="txtYearsOfLearning" type="text" class="form-control input-sm" 
									value="<?php echo isset($postData['years_of_learning'])? $postData['years_of_learning'] : '';?>" />
							</div>
						</div>
					</div><!-- /.row -->
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtNamaSekolahAsal">Nama Sekolah Asal</label>
								<input id="txtNamaSekolahAsal" type="text" class="form-control input-sm" 
									value="<?php echo isset($postData['origin_school_name'])? $postData['origin_school_name'] : '';?>" />
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtTanggalPindah">Tanggal Pindah Dari Sekolah Asal</label>
								<input id="txtTanggalPindah" name="txtTanggalPindah" type="text" class="form-control input-sm" 
									value="<?php echo isset($postData['transfer_date'])? formatDateForDisplay($postData['transfer_date']) : '';?>"  />
							</div>
						</div>
					</div><!-- /.row -->
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtPindahDariTingkat">Pindah Dari Tingkat</label>
								<input id="txtPindahDariTingkat" name="transfer_from_grade" type="text" class="form-control input-sm" 
									value="<?php echo isset($postData['transfer_from_grade'])? $postData['transfer_from_grade'] : '';?>" />
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtAcceptedDate">Tanggal Diterima</label>
								<input id="txtAcceptedDate" name="txtAcceptedDate" type="text" class="form-control input-sm" 
									value="<?php echo isset($postData['accepted_date'])? formatDateForDisplay($postData['accepted_date']) : '';?>"  />
							</div>
						</div>
					</div><!-- /.row -->
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-6">
							<div class="form-group">
								<label for="txtAcceptedGrade">Diterima di Tingkat</label>
								<input id="txtAcceptedGrade" name="accepted_grade" type="text" class="form-control input-sm" 
									value="<?php echo isset($postData['accepted_grade'])? $postData['accepted_grade'] : '';?>" />
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-6">&nbsp;</div>
					</div><!-- /.row -->
					<!-- END Origin info -->
					
				</div>
				<!-- END Student info -->
			</div>
			<div class="report-section">
				<button type="button" class="btn btn-primary btn-sm hidden-print" onclick="javascript:window.print();">
					<i class="fa fa-print"></i> Print
				</button>
			</div>
			<!-- END report -->
		</div>
    <script type="text/javascript" src="<?php echo base_url('assets/themes/sb-admin/js/jquery-2.2.4.min.js');?>"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        $('input.form-control').attr('readonly', 'readonly');
      });
    </script>
	</body>
</html>
