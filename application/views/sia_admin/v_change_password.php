<!-- BEGIN Form -->
<?php
echo form_open($formActionUrl, 'id="changePassForm"');
if (isset($errorMessages) && !empty($errorMessages)) {
  $errHeading = lang('message_error_mandatory_fields');
  echo createErrorMessages($errHeading, $errorMessages);
}
?>
<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtPass">Password <span class="sign sign-mandatory">*</span></label>
      <input id="txtPass" name="pass" type="password" class="form-control input-sm"
             placeholder="password saat ini"/>
    </div>
  </div>
</div><!-- /.row -->
<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtNewPass">Password Baru <span class="sign sign-mandatory">*</span></label>
      <input id="txtNewPass" name="newPass" type="password" class="form-control input-sm"/>
    </div>
  </div>
</div><!-- /.row -->
<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtConfirmNewPass">Konfirmasi Password Baru <span class="sign sign-mandatory">*</span></label>
      <input id="txtConfirmNewPass" name="confirmNewPass" type="password" class="form-control input-sm"/>
    </div>
  </div>
</div><!-- /.row -->
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel-upper-buttons">
      <button id="btnSimpan" type="button" class="btn btn-primary btn-sm"
              title="<?php echo lang('label_save_hint'); ?>">
        <i class="fa fa-save"></i> <?php echo lang('label_save'); ?>
      </button>
      <button id="btnReset" type="reset" class="btn btn-primary btn-sm" title="Reset">
        <i class="fa fa-undo"></i> <?php echo lang('label_reset'); ?>
      </button>
    </div>
  </div>
</div><!-- /.row -->
<?php echo form_close(); ?>
<!-- END Form -->
