<!-- Page Content -->
<?php echo form_open('sia_modules/index', 'method="get"'); ?>

<!-- Search Filter -->
<div class="row">
  <div class="col-lg-4 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="cmbParent"><?php echo lang('module_parent'); ?></label>
      <?php
      $selectedParent = isset($postData['parent_id']) ? $postData['parent_id'] : '';
      echo form_dropdown('parent_id', $parentOptions, $selectedParent,
        array('id' => 'cmbParent', 'class' => 'form-control input-sm'));
      ?>
    </div>
  </div>
  <div class="col-lg-4 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtClassName"><?php echo lang('module_class_name'); ?></label>
      <input id="txtClassName" name="class_name" type="text" class="form-control input-sm"
             value="<?php echo isset($postData['class_name']) ? $postData['class_name'] : ''; ?>"/>
    </div>
  </div>
  <div class="col-lg-4 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtCaption"><?php echo lang('module_caption'); ?></label>
      <input id="txtCaption" name="caption" type="text" class="form-control input-sm"
             value="<?php echo isset($postData['caption']) ? $postData['caption'] : ''; ?>"/>
    </div>
  </div>
</div><!-- /.row -->
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel-upper-buttons">
      <button id="btnCari" type="submit" class="btn btn-primary btn-sm">
        <i class="fa fa-search"></i> <?php echo lang('label_search'); ?>
      </button>
      <button id="btnReset" type="button" class="btn btn-primary btn-sm">
        <i class="fa fa-undo"></i> <?php echo lang('label_reset'); ?>
      </button>
      <a href="<?php echo site_url('sia_modules/insert'); ?>" class="btn btn-primary btn-sm"
         title="<?php echo lang('label_add_new_hint'); ?>">
        <i class="fa fa-plus"></i> <?php echo lang('label_add_new'); ?>
      </a>
    </div>
  </div>
</div><!-- /.row -->

<!-- Search Result -->
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="table-responsive">
      <table class="table table-bordered table-hover table-striped">
        <thead>
        <tr>
          <th width="5%" class="text-center"><?php echo lang('label_rownum'); ?></th>
          <th width="20%" class="text-center"><?php echo lang('module_parent'); ?></th>
          <th width="25%" class="text-center"><?php echo lang('module_class_name'); ?></th>
          <th width="20%" class="text-center"><?php echo lang('module_caption'); ?></th>
          <th width="20%" class="text-center"><?php echo lang('module_description'); ?></th>
          <th width="10%" class="text-center"><?php echo lang('label_action'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $baseUrlParams = array(QSPARAM_PAGE => $listPage, QSPARAM_PAGE_SIZE => $listPageSize) + $_GET;
        if (isset($recordset)):
          $i = $rownumStart + 1;
          foreach ($recordset as $row):
            $urlParams = $baseUrlParams + array(QSPARAM_REC_ID => $row->id);
            $editUrl = siteUrl('sia_modules/update', $urlParams);
            $deleteUrl = siteUrl('sia_modules/delete', $urlParams);
            ?>
            <tr>
              <td><?php echo $i; ?></td>
              <td><?php echo printEmpty($row->parent_name, 'NULL'); ?></td>
              <td><?php echo printEmpty($row->class_name, 'NULL'); ?></td>
              <td><a href="<?php echo $editUrl; ?>"
                     title="<?php echo lang('label_edit'); ?>"><?php echo $row->caption; ?></a></td>
              <td><?php echo $row->description; ?></td>
              <td class="text-center">
                <a href="javascript:void(0);" title="<?php echo lang('label_delete'); ?>" class="btn btn-danger btn-xs"
                   onclick="siaJS_confirm('questions.confirm_delete', '<?php echo $deleteUrl; ?>');">
                  <i class="fa fa-times"></i>
                </a>
              </td>
            </tr>
            <?php
            unset($urlParams, $editUrl);
            $i++;
          endforeach;
        endif;
        ?>
        </tbody>
      </table>

      <?php if (isset($recordcount)):
        $pagingLinks = generatePagingLinks('sia_modules/index', $baseUrlParams, $recordcount, $listPageSize, $listPage);
        ?>
        <div class="pull-right"><?php echo $pagingLinks; ?></div>
      <?php endif; ?>
    </div>
  </div>
</div><!-- /.row -->
<?php echo form_close(); ?>
