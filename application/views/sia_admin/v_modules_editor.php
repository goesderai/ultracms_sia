<!-- BEGIN Form -->
<?php
echo form_open($formActionUrl, 'id="modulesForm"');
if (isset($errorMessages) && !empty($errorMessages)) {
  $errHeading = lang('message_error_mandatory_fields');
  echo createErrorMessages($errHeading, $errorMessages);
}
?>
<div class="row">
  <div class="col-md-4 col-xs-12">
    <div class="form-group">
      <label for="cmbParent"><?php echo lang('module_parent'); ?></label>
      <?php
      $parentId = isset($postData['parent_id']) ? $postData['parent_id'] : 0;
      echo form_dropdown('parent_id', $parentOptions, $parentId, array('id' => 'cmbParent', 'class' => 'form-control input-sm'));
      ?>
    </div>
  </div>
  <div class="col-md-4 col-xs-12">
    <div class="form-group">
      <label for="txtClassName"><?php echo lang('module_class_name'); ?></label>
      <input type="text" id="txtClassName" name="class_name" maxlength="50"
             class="form-control input-sm"
             value="<?php echo isset($postData['class_name']) ? $postData['class_name'] : ''; ?>"/>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-4 col-xs-12">
    <div class="form-group">
      <label for="txtDescription"><?php echo lang('module_description'); ?></label>
      <input type="text" id="txtDescription" name="description" maxlength="50"
             class="form-control input-sm"
             value="<?php echo isset($postData['description']) ? $postData['description'] : ''; ?>"/>
    </div>
  </div>
  <div class="col-md-4 col-xs-12">
    <div class="form-group">
      <label for="txtCaption"><?php echo lang('module_caption'); ?> <span
          class="sign sign-mandatory">*</span></label>
      <input type="text" id="txtCaption" name="caption" maxlength="25"
             class="form-control input-sm"
             value="<?php echo isset($postData['caption']) ? $postData['caption'] : ''; ?>"/>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-4 col-xs-12">
    <div class="form-group">
      <label for="txtIcon"><?php echo lang('module_icon_class'); ?> <span
          class="sign sign-mandatory">*</span></label>
      <input type="text" id="txtIcon" name="icon" maxlength="25"
             class="form-control input-sm"
             value="<?php echo isset($postData['icon']) ? $postData['icon'] : 'fa-folder'; ?>"/>
    </div>
  </div>
  <div class="col-md-2 col-xs-12">
    <div class="form-group">
      <label for="cmbTreeLevel"><?php echo lang('module_tree_level'); ?> <span
          class="sign sign-mandatory">*</span></label>
      <?php
      $treeLevel = isset($postData['tree_level']) ? $postData['tree_level'] : 0;
      echo form_dropdown('tree_level', array(0, 1), $treeLevel, array('id' => 'cmbTreeLevel', 'class' => 'form-control input-sm'));
      ?>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-2 col-xs-12">
    <div class="form-group">
      <label for="txtDisplayOrder"><?php echo lang('module_display_order'); ?></label>
      <input type="number" id="txtDisplayOrder" name="display_order"
             class="form-control input-sm"
             value="<?php echo isset($postData['display_order']) ? $postData['display_order'] : '0'; ?>"/>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 col-xs-12">
    <div class="panel-lower-buttons">
      <button id="btnSimpan" type="button" class="btn btn-primary btn-sm"
              title="<?php echo lang('label_save_hint'); ?>">
        <i class="fa fa-save"></i> <?php echo lang('label_save'); ?>
      </button>
      <a href="javascript:void(0)" class="btn btn-primary btn-sm" title="<?php echo lang('label_close_hint'); ?>"
         onclick="siaJS_confirm('questions.confirm_close', '<?php echo site_url('sia_modules/index'); ?>')">
        <i class="fa fa-minus-circle"></i> <?php echo lang('label_close'); ?>
      </a>
    </div>
  </div>
</div>

<input type="hidden" id="id" name="id"
       value="<?php echo isset($postData['id']) ? $postData['id'] : ''; ?>"/>
<?php echo form_close(); ?>
<!-- END Form -->
