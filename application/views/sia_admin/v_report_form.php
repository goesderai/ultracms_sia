<?php
echo form_open($formActionUrl, 'id="reportForm" target="_blank"');

// Load report parameter form
$formFile = sprintf('includes/reports/%s.php', $reportForm);
include_once $formFile;
?>
<div id="reportFormButtonContainer" class="row">
  <div class="col-md-3 col-xs-12">
    <div class="form-group">
      <button id="btnSubmit" type="submit" class="btn btn-primary btn-block btn-sm"><i class="fa fa-play"></i> OK</button>
    </div>
  </div>
  <div class="col-md-3 col-xs-12">
    <div class="form-group">
      <button id="btnReset" type="reset" class="btn btn-primary btn-block btn-sm"><i class="fa fa-undo"></i> Reset</button>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
<script type="text/javascript">
  $(function () {
    try {
      $('#reportForm').submit(function () {
        $('#reportFormButtonContainer').html('<p class="text-center text-info"><i class="fa fa-info-circle"></i> Press F5 to generate another report.</p>');
      });
    } catch (e) {
      console.error(e);
    }
  });
</script>
