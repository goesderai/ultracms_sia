<?php
if(isset($recordset)):
	$i = $rownumStart + 1;
	foreach ($recordset as $row):
	$studentObject = array("id" => $row->id,
	    "nis" => $row->nis,
	    "name" => $row->name,
	    "grade_name" => $row->grade_name,
	    "classroom_name" => $row->classroom_name);
	$rowJson = htmlentities(json_encode($studentObject), ENT_QUOTES);
?>
<tr data-value="<?php echo $rowJson;?>">
	<td>
		<input type="radio" name="rdStudent" class="singleResultStudentLookupRadio" 
          id="singleResultStudentLookupRadio-<?php echo $i;?>" 
		  value="<?php echo $row->id;?>" />
	</td>
	<td><?php echo $i;?></td>
	<td><?php echo $row->nis;?></td>
	<td><?php echo $row->name;?></td>
	<td><?php echo $row->grade_name;?></td>
	<td><?php echo $row->classroom_name;?></td>
</tr>
<?php
		$i++;
	endforeach;
endif;
?>