<!-- Page Content -->
<?php echo form_open('sia_salary/index', 'method="get"'); ?>

<!-- Search Filter -->
<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <label for="cmbEmployee">Karyawan <span class="sign sign-mandatory">*</span></label>
    <?php
    $employeeOptions = array('-- Pilih --') + $DAO_Employee->getSelectOptionsForUserMapping();
    $selectedEmployee = isset($postData['employee_id']) ? $postData['employee_id'] : '';
    $cmbEmployeeAttr = array('id' => 'cmbEmployee', 'class' => 'form-control input-sm');
    echo form_dropdown('employee_id', $employeeOptions, $selectedEmployee, $cmbEmployeeAttr);
    ?>
  </div>
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtGapok">Gaji Pokok</label>
      <input id="txtGapok" name="gaji_pokok" type="text" class="form-control input-sm"
             value="<?php echo isset($postData['gaji_pokok']) ? $postData['gaji_pokok'] : ''; ?>"/>
    </div>
  </div>
</div><!-- /.row -->
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel-upper-buttons">
      <button id="btnCari" type="submit" class="btn btn-primary btn-sm">
        <i class="fa fa-search"></i> Cari
      </button>
      <button id="btnReset" type="button" class="btn btn-primary btn-sm">
        <i class="fa fa-undo"></i> Reset
      </button>
      <a href="<?php echo site_url('sia_salary/insert'); ?>" class="btn btn-primary btn-sm" title="Tambah data baru">
        <i class="fa fa-plus"></i> Tambah
      </a>
    </div>
  </div>
</div><!-- /.row -->

<!-- Search Result -->
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="table-responsive">
      <table class="table table-bordered table-hover table-striped">
        <thead>
        <tr>
          <th width="5%" class="text-center"><?php echo lang('label_rownum');?></th>
          <th width="20%" class="text-center"><?php echo lang('employee_name'); ?></th>
          <th width="25%" class="text-center"><?php echo lang('employee_nett_salary'); ?></th>
          <th width="25%" class="text-center"><?php echo lang('employee_total_salary'); ?></th>
          <th width="10%" class="text-center"><?php echo lang('label_action');?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $baseUrlParams = array(QSPARAM_PAGE => $listPage, QSPARAM_PAGE_SIZE => $listPageSize) + $_GET;
        if (isset($recordset)):
          $i = $rownumStart + 1;
          foreach ($recordset as $row):
            $urlParams = $baseUrlParams + array(QSPARAM_REC_ID => $row->id);
            $editUrl = siteUrl('sia_salary/update', $urlParams);
            $deleteUrl = siteUrl('sia_salary/delete', $urlParams);
            ?>
            <tr>
              <td><?php echo $i; ?></td>
              <td><a href="<?php echo $editUrl; ?>"
                     title="Edit"><?php echo $DAO_Employee->getEmployeeCaptionById($row->employee_id); ?></a></td>
              <td><?php echo number_format($row->gaji_pokok); ?></td>
              <td><?php echo number_format($row->total_gaji); ?></td>
              <td class="text-center">
                <a href="javascript:void(0)"
                   onclick="siaJS_confirm('questions.confirm_delete', '<?php echo $deleteUrl; ?>');"
                   title="<?php echo lang('label_delete'); ?>" class="btn btn-danger btn-xs">
                  <i class="fa fa-times"></i>
                </a>
              </td>
            </tr>
            <?php
            unset($urlParams, $editUrl);
            $i++;
          endforeach;
        endif;
        ?>
        </tbody>
      </table>

      <?php if (isset($recordcount)):
        $pagingLinks = generatePagingLinks('sia_salary/index', $baseUrlParams, $recordcount, $listPageSize, $listPage);
        ?>
        <div class="pull-right"><?php echo $pagingLinks; ?></div>
      <?php endif; ?>
    </div>
  </div>
</div><!-- /.row -->
<?php echo form_close(); ?>
