<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $pageTitle . ' | ' . $appName; ?></title>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/themes/sb-admin/font-awesome/css/font-awesome.min.css'); ?>"/>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css"/>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.4/css/buttons.bootstrap.min.css"/>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.5.0/css/colReorder.bootstrap.min.css"/>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.bootstrap.min.css"/>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.4/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.4/js/buttons.bootstrap.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.4/js/buttons.html5.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.4/js/buttons.print.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.5.0/js/dataTables.colReorder.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
  <!-- BEGIN report -->
  <div class="report-section report-header">
    <h3><?php echo $COMPANY_NAME; ?></h3>
    <p><?php rptPrintCompanyInfo($COMPANY_ADDRESS, $COMPANY_PHONE, $COMPANY_FAX, $COMPANY_EMAIL, $COMPANY_FACEBOOK, $COMPANY_INSTAGRAM); ?></p>
  </div>
  <div class="report-section report-body">
    <?php
    if (isset($reportGen)) {
      $reportGen->generateReport();
    } else {
      print '<p style="text-align: center; font-weight: bold; color: red">ERROR!<br>
                  Report generator not loaded!</p>';
      die();
    }
    ?>
  </div>
  <!-- END report -->
</div>
<script type="text/javascript">
  $(document).ready(function () {
    $('#reportTable').DataTable({
                                  colReorder: true,
                                  responsive: true,
                                  ordering: <?php $ordering = $reportGen->show_number_column ? "false" : "true"; echo $ordering; ?>,
                                  searching:  false,
                                  dom:        'lrtipB',
                                  buttons:    ['print']
                                });
  });
</script>
</body>
</html>
