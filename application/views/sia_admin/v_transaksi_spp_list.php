<?php echo form_open('sia_transaksi_spp/index', 'method="get"'); ?>
<div class="row">
  <div class="col-lg-3 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtNis"><?php echo lang('trxspp_nis'); ?></label>
      <input id="txtNis" name="nis" type="text" class="form-control input-sm"
             value="<?php echo isset($postData['nis']) ? $postData['nis'] : ''; ?>"/>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtNis"><?php echo lang('trxspp_student_name'); ?></label>
      <input id="txtNama" name="nama" type="text" class="form-control input-sm"
             value="<?php echo isset($postData['nama']) ? $postData['nama'] : ''; ?>"/>
    </div>
  </div>
  <div class="col-lg-6 col-md-6 col-xs-12">
    <label><?php echo lang('trxspp_transaction_date'); ?></label>
    <div class="form-horizontal">
      <div class="form-group">
        <div class="col-lg-5 col-md-5 col-xs-12">
          <div id="trxDatePickerFrom" class="input-group date">
            <input id="txtTrxDateFrom" name="txtTrxDateFrom" type="text" class="form-control input-sm"
                   value="<?php echo isset($postData['transaction_date_from']) ? formatDateForDisplay($postData['transaction_date_from']) : ''; ?>"
                   readonly="readonly"/>
            <span class="input-group-addon">
                    <span class="fa fa-calendar"></span>
                  </span>
          </div>
          <input id="transaction_date_from" name="transaction_date_from" type="hidden"
                 value="<?php echo isset($postData['transaction_date_from']) ? $postData['transaction_date_from'] : ''; ?>"/>
        </div>
        <div class="col-lg-2 col-md-2 col-xs-12 text-center">
          <label><?php echo lang('trxspp_until'); ?></label>
        </div>
        <div class="col-lg-5 col-md-5 col-xs-12">
          <div id="trxDatePickerTo" class="input-group date">
            <input id="txtTrxDateTo" name="txtTrxDateTo" type="text" class="form-control input-sm"
                   value="<?php echo isset($postData['transaction_date_to']) ? formatDateForDisplay($postData['transaction_date_to']) : ''; ?>"
                   readonly="readonly"/>
            <span class="input-group-addon">
                    <span class="fa fa-calendar"></span>
                  </span>
          </div>
          <input id="transaction_date_to" name="transaction_date_to" type="hidden"
                 value="<?php echo isset($postData['transaction_date_to']) ? $postData['transaction_date_to'] : ''; ?>"/>
        </div>
      </div>
    </div>
  </div>
</div><!-- /.row -->

<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtKeterangan"><?php echo lang('trxspp_description'); ?></label>
      <input type="text" id="txtKeterangan" name="remark" class="form-control input-sm"
             value="<?php echo isset($postData['remark']) ? $postData['remark'] : ''; ?>"/>
    </div>
  </div>

  <div class="col-lg-6 col-md-6 col-xs-12">&nbsp;</div>
</div><!-- /.row -->

<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel-upper-buttons">
      <button id="btnCari" type="submit" class="btn btn-primary btn-sm"
              onclick="javascript:ultracms.siaTransaksiSppList.validateForm()">
        <i class="fa fa-search"></i>&nbsp;<?php echo lang('label_search'); ?>
      </button>
      <button id="btnReset" type="button" class="btn btn-primary btn-sm">
        <i class="fa fa-undo"></i>&nbsp;<?php echo lang('label_reset'); ?>
      </button>
      <a href="<?php echo site_url('sia_transaksi_spp/insert'); ?>" class="btn btn-primary btn-sm"
         title="<?php echo lang('label_add_new_hint'); ?>">
        <i class="fa fa-plus"></i>&nbsp;<?php echo lang('label_add_new'); ?>
      </a>
    </div>
  </div>
</div><!-- /.row -->

<!-- Search Result -->
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="table-responsive">
      <table class="table table-bordered table-hover table-striped">
        <thead>
        <tr>
          <th class="text-center" width="5%"><?php echo lang('label_rownum'); ?></th>
          <th class="text-center" width="15%"><?php echo lang('trxspp_nis'); ?></th>
          <th class="text-center" width="25%"><?php echo lang('trxspp_student_name'); ?></th>
          <th class="text-center" width="15%"><?php echo lang('trxspp_transaction_date'); ?></th>
          <th class="text-center" width="10%"><?php echo lang('trxspp_year'); ?></th>
          <th class="text-center" width="20%"><?php echo lang('trxspp_description'); ?></th>
          <th class="text-center" width="20%"><?php echo lang('trxspp_amount'); ?></th>
          <th class="text-center" width="5%"><?php echo lang('label_action'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $baseUrlParams = array(QSPARAM_PAGE => $listPage, QSPARAM_PAGE_SIZE => $listPageSize) + $_GET;
        if (isset($recordset)):
          $i = $rownumStart + 1;
          foreach ($recordset as $row):
            $urlParams = $baseUrlParams + array(QSPARAM_REC_ID => $row->id);
            $editUrl = siteUrl('sia_transaksi_spp/update', $urlParams);
            $invoiceUrl = site_url('Sia_transaksi_spp/show_invoice/' . $row->id);
            $siswa = $DAO_Siswa->getRow($row->student_id);
            ?>
            <tr>
              <td><?php echo $i; ?></td>
              <td><a href="<?php echo $editUrl; ?>" title="<?php echo lang('label_edit'); ?>">
                  <?php echo $siswa['nis']; ?></a>
              </td>
              <td><?php echo $siswa['name']; ?></td>
              <td><?php echo date("d F Y", strtotime($row->transaction_date)); ?></td>
              <td><?php echo $row->financial_year; ?></td>
              <td><?php echo $row->remark; ?></td>
              <td align="right"><?php echo number_format((float)$row->total_amount, 2, '.', ','); ?></td>
              <td align="center">
                <a href="<?php echo $invoiceUrl; ?>" target="_blank" title="Cetak Invoice"
                   class="btn btn-primary btn-sm"><i class="fa fa-print"></i></a>
              </td>
            </tr>
            <?php
            unset($urlParams, $editUrl, $siswa);
            $i++;
          endforeach;
        endif;
        ?>
        </tbody>
      </table>

      <?php if (isset($recordcount)):
        $pagingLinks = generatePagingLinks('sia_transaksi_spp/index', $baseUrlParams, $recordcount, $listPageSize, $listPage);
        ?>
        <div class="pull-right"><?php echo $pagingLinks; ?></div>
      <?php endif; ?>
    </div>
  </div>
</div><!-- /.row -->
<?php echo form_close(); ?>
