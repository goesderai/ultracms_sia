<table id="classroomGrid" class="table table-bordered table-hover table-striped">
	<thead>
		<tr>
			<th width="5%" class="text-center"><?php echo lang('label_rownum');?></th>
			<th width="60%" class="text-center"><?php echo lang('edugrd_level_name');?></th>
			<th width="25%" class="text-center"><?php echo lang('edugrd_student_count');?></th>
			<th width="10%" class="text-center"><?php echo lang('label_action');?></th>
		</tr>
	</thead>
	<tbody>
	<?php
		$baseUrlParams = $_GET;
		if(isset($recordset)):
			$i = 1;
			foreach ($recordset as $row):
	?>
		<tr>
			<td><?php echo $i;?></td>
			<td>
				<input name="classroomId[]" type="hidden" value="<?php echo $row->id;?>"/>
				<a id="classroomEditLink-<?php echo $row->id;?>" class="classroomEditLink" 
					href="javascript:void(0);" title="Edit"><?php echo $row->name;?></a>
			</td>
			<td class="text-right"><?php echo $row->students;?></td>
			<td class="text-center actionColumn">
				<!-- BEGIN Delete button -->
				<?php if($row->students <= 0):?>
				<button id="btnDelClassroom-<?php echo $row->id;?>" type="button" title="<?php echo lang('label_delete');?>" class="btn btn-danger btn-xs">
					<i class="fa fa-times"></i>
				</button>
				<?php else:?>
				<button disabled="disabled" type="button" class="btn btn-danger btn-xs">
					<i class="fa fa-times"></i>
				</button>
				<?php endif;?>
				<!-- END Delete button -->
			</td>
		</tr>
	<?php
				unset($urlParams, $editUrl);
				$i++;
			endforeach;
		endif;
	?>
	</tbody>
</table>
