<!-- BEGIN Content -->
<div class="container-fluid">
  <div class="row">
    <div class="col-md-4 col-xs-12">
      <div class="form-group">
        <label for="mutationDate">Tanggal Proses Kenaikan Jenjang</label>
        <input id="mutationDate" type="text" class="form-control input-sm" readonly="readonly"
               value="<?php echo formatDateForDisplay($headerData->mutation_date); ?>"/>
      </div>
    </div>
    <div class="col-md-4 col-xs-12">
      <div class="form-group">
        <label for="cmbDestGradeId">Jenjang Tujuan</label>
        <input id="cmbDestGradeId" type="text" class="form-control input-sm" readonly="readonly"
               value="<?php echo $headerData->grade_name; ?>"/>
      </div>
    </div>
    <div class="col-md-4 col-xs-12">
      <div class="form-group">
        <label for="cmbDestClassroomId">Tingkatan Tujuan</label>
        <input id="cmbDestClassroomId" type="text" class="form-control input-sm" readonly="readonly"
               value="<?php echo $headerData->classroom_name; ?>"/>
      </div>
    </div>
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12 col-md-12 col-xs-12">
      <div class="form-group">
        <label for="tblStudents">Daftar Siswa</label>
        <table id="tblStudents" class="table table-bordered table-hover table-striped">
          <thead>
          <tr>
            <th width="5%" class="text-center">No</th>
            <th width="20%" class="text-center">NIS</th>
            <th width="35%" class="text-center">Nama</th>
            <th width="20%" class="text-center">Jenjang Asal</th>
            <th width="20%" class="text-center">Tingkatan Asal</th>
          </tr>
          </thead>
          <tbody id="tblStudentsBody">
          <?php
          if (isset($rsDetail)):
            $i = 0;
            foreach ($rsDetail->result() as $row):
              $rowNum = $i + 1;
              ?>
              <tr>
                <td><?php echo $rowNum; ?></td>
                <td><?php echo $row->nis; ?></td>
                <td><?php echo $row->name; ?></td>
                <td><?php echo $row->gradeName; ?></td>
                <td><?php echo $row->classroomName; ?></td>
              </tr>
              <?php
              $i++;
            endforeach;
          endif;
          ?>
          </tbody>
        </table>
      </div>
    </div>
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12 col-md-12 col-xs-12">
      <div class="form-group">
        <a href="javascript:void(0)" class="btn btn-primary btn-sm" title="<?php echo lang('label_close_hint'); ?>"
           onclick="siaJS_confirm('questions.confirm_close', '<?php echo site_url('sia_student_grade_mutation/index'); ?>')">
          <i class="fa fa-minus-circle"></i> <?php echo lang('label_close'); ?>
        </a>
      </div>
    </div>
  </div><!-- /.row -->
</div>
<!-- END Content -->
