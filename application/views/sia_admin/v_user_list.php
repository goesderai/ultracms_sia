<!-- Page Content -->
<?php echo form_open('sia_users/index', 'method="get"'); ?>

<!-- Search Filter -->
<div class="row">
  <div class="col-md-4 col-xs-12">
    <div class="form-group">
      <label for="txtUsername"><?php echo lang('user_username'); ?></label>
      <input id="txtUsername" name="username" type="text" class="form-control input-sm"
             value="<?php echo isset($postData['username']) ? $postData['username'] : ''; ?>"/>
    </div>
  </div>
  <div class="col-md-4 col-xs-12">
    <div class="form-group">
      <label for="txtNama"><?php echo lang('user_name'); ?></label>
      <input id="txtNama" name="name" type="text" class="form-control input-sm"
             value="<?php echo isset($postData['name']) ? $postData['name'] : ''; ?>"/>
    </div>
  </div>
  <div class="col-md-4 col-xs-12">
    <div class="form-group">
      <label for="cmbStatus"><?php echo lang('user_status'); ?></label>
      <?php
      $statusOptions = array('0' => lang('user_choice_all'),
        'Y' => lang('user_active'),
        'N' => lang('user_inactive')
      );
      $selectedStatus = isset($postData['status']) ? $postData['status'] : '0';
      $cmbStatusAttr = array('id' => 'cmbStatus', 'class' => 'form-control input-sm');
      echo form_dropdown('status', $statusOptions, $selectedStatus, $cmbStatusAttr);
      ?>
    </div>
  </div>
</div><!-- /.row -->
<div class="row">
  <div class="col-md-12 col-xs-12">
    <div class="panel-upper-buttons">
      <button id="btnCari" type="submit" class="btn btn-primary btn-sm">
        <i class="fa fa-search"></i> <?php echo lang('label_search'); ?>
      </button>
      <button id="btnReset" type="button" class="btn btn-primary btn-sm">
        <i class="fa fa-undo"></i> <?php echo lang('label_reset'); ?>
      </button>
      <a href="<?php echo site_url('sia_users/insert'); ?>" class="btn btn-primary btn-sm"
         title="<?php echo lang('label_add_new_hint'); ?>">
        <i class="fa fa-plus"></i> <?php echo lang('label_add_new'); ?>
      </a>
    </div>
  </div>
</div><!-- /.row -->

<!-- Search Result -->
<div class="row">
  <div class="col-md-12 col-xs-12">
    <div class="table-responsive">
      <table class="table table-bordered table-hover table-striped">
        <thead>
        <tr>
          <th width="5%" class="text-center"><?php echo lang('label_rownum'); ?></th>
          <th width="35%" class="text-center"><?php echo lang('user_username'); ?></th>
          <th width="35%" class="text-center"><?php echo lang('user_name'); ?></th>
          <th width="15%" class="text-center"><?php echo lang('user_status'); ?></th>
          <th width="10%" class="text-center"><?php echo lang('label_action'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $baseUrlParams = array(QSPARAM_PAGE => $listPage, QSPARAM_PAGE_SIZE => $listPageSize) + $_GET;
        if (isset($recordset)):
          $i = $rownumStart + 1;
          foreach ($recordset as $row):
            $activeLabel = ($row->active == 'Y') ? 'Aktif' : 'Non-aktif';
            $urlParams = $baseUrlParams + array(QSPARAM_REC_ID => $row->id);
            $editUrl = siteUrl('sia_users/update', $urlParams);
            $actionBtnUrl = ($row->active == 'Y') ? siteUrl('sia_users/deactivate', $urlParams) :
              siteUrl('sia_users/activate', $urlParams);
            $actionBtnCssClass = ($row->active == 'Y') ? 'btn-danger' : 'btn-success';
            $actionBtnTooltip = ($row->active == 'Y') ? 'Non-aktifkan!' : 'Aktifkan!';
            ?>
            <tr>
              <td><?php echo $i; ?></td>
              <td><a href="<?php echo $editUrl; ?>" title="Edit"><?php echo $row->username; ?></a></td>
              <td><?php echo $row->name; ?></td>
              <td><?php echo $activeLabel; ?></td>
              <td class="text-center">
                <a href="javascript:void(0)" class="btn <?php echo $actionBtnCssClass; ?> btn-xs"
                   onclick="siaJS_confirm('questions.confirm_action', '<?php echo $actionBtnUrl; ?>');"
                   title="<?php echo $actionBtnTooltip; ?>">
                  <i class="fa fa-power-off"></i>
                </a>
              </td>
            </tr>
            <?php
            unset($urlParams, $editUrl);
            $i++;
          endforeach;
        endif;
        ?>
        </tbody>
      </table>

      <?php if (isset($recordcount)):
        $pagingLinks = generatePagingLinks('sia_users/index', $baseUrlParams, $recordcount, $listPageSize, $listPage);
        ?>
        <div class="pull-right"><?php echo $pagingLinks; ?></div>
      <?php endif; ?>
    </div>
  </div>
</div><!-- /.row -->
<?php echo form_close(); ?>
