<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $pageTitle . ' | ' . $appName; ?></title>
  <link
    href="<?php echo base_url('assets/themes/sb-admin/css/bootstrap.min.css'); ?>"
    rel="stylesheet" type="text/css"/>
  <link
    href="<?php echo base_url('assets/themes/sb-admin/font-awesome/css/font-awesome.min.css'); ?>"
    rel="stylesheet" type="text/css"/>
  <link
    href="<?php echo base_url('assets/themes/sb-admin/css/sia-report.min.css'); ?>"
    rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="container"><!-- BEGIN report -->
  <div class="report-section report-header">
    <h3><?php echo $COMPANY_NAME; ?></h3>
    <p><?php echo sprintf("%s | %s | %s<br>%s", $COMPANY_ADDRESS, $COMPANY_PHONE, $COMPANY_FAX, $COMPANY_EMAIL); ?></p>
  </div>
  <div class="row">
    <div align="center">
      <h3><label>Laporan Neraca</label></h3>
      <h5><label>Periode <?php echo $periode ?></label></h5>
    </div>
  </div>

  <div class="report-section report-body">
    <div class="row">
      <div style="float: right">
        <h5><label></label></h5>
      </div>
    </div>
    <div class="row">
      <table width="100%">
        <tr>
          <td width="49%" valign="top">
            <table class="table table-bordered table-hover table-striped">
              <thead>
              <tr>
                <th class="text-center" colspan="2">AKTIVA</th>
              </tr>
              </thead>
              <tbody>
              <!-- Aktiva Lancar -->
              <tr>
                <td align="left" colspan="2"><b>Aktiva Lancar</b></td>
              </tr>
              <?php
              $sumAktivaLancar = 0;
              $sql = $DAO_Transaksi->getAktivaLancar($filters);
              $recordsetAktivaLancar = $DAO_Transaksi->db->query($sql);
              $i = 1;
              foreach ($recordsetAktivaLancar->result() as $row):
                ?>
                <tr>
                  <td align="left"><?php echo isset($row->nama) ? $row->nama : '-' ?></td>
                  <td
                    align="right"><?php echo isset($row->saldo) ? number_format((float)$row->saldo, 2, '.', ',') : 0 ?></td>
                </tr>
                <?php
                $sumAktivaLancar = $sumAktivaLancar + $row->saldo;
                $i++;
              endforeach;
              ?>
              <tr>
                <td align="left"><b>Jumlah (1)</b></td>
                <td align="right"><b><?php echo number_format((float)$sumAktivaLancar, 2, '.', ',') ?></b></td>
              </tr>
              <tr>
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr>
                <td align="left" colspan="2"><b>Aktiva Tetap</b></td>
              </tr>
              <?php
              $sumAktivaTetap = 0;
              $sql = $DAO_Transaksi->getAktivaTetap($filters);
              $recordsetAktivaTetap = $DAO_Transaksi->db->query($sql);
              $i = 1;
              foreach ($recordsetAktivaTetap->result() as $row):
                ?>
                <tr>
                  <td align="left"><?php echo isset($row->nama) ? $row->nama : '-' ?></td>
                  <td
                    align="right"><?php echo isset($row->saldo) ? number_format((float)$row->saldo, 2, '.', ',') : 0 ?></td>
                </tr>
                <?php
                $sumAktivaTetap = $sumAktivaTetap + $row->saldo;
                $i++;
              endforeach;
              ?>
              <tr>
                <td align="left"><b>Jumlah (2)</b></td>
                <td align="right"><b><?php echo number_format((float)$sumAktivaTetap, 2, '.', ',') ?></b></td>
              </tr>

              <tr>
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr>
                <td align="left"><b>Jumlah Aktiva (1)+(2)</b></td>
                <td align="right">
                  <b><?php echo number_format((float)$sumAktivaLancar + $sumAktivaTetap, 2, '.', ',') ?></b></td>
              </tr>
              </tbody>
            </table>
          </td>
          <td width="2%"></td>
          <td width="49%" valign="top">
            <table class="table table-bordered table-hover table-striped">
              <thead>
              <tr>
                <th class="text-center" colspan="2">PASIVA</th>
              </tr>
              </thead>
              <tbody>

              <tr>
                <td align="left" colspan="2"><b>Hutang Jangka Pendek</b></td>
              </tr>
              <?php
              $sumHutangPendek = 0;
              $sql = $DAO_Transaksi->getHutangPendek($filters);
              $recordsetHutangPendek = $DAO_Transaksi->db->query($sql);
              $i = 1;
              foreach ($recordsetHutangPendek->result() as $row):
                ?>
                <tr>
                  <td align="left"><?php echo isset($row->nama) ? $row->nama : '-' ?></td>
                  <td
                    align="right"><?php echo isset($row->saldo) ? number_format((float)$row->saldo, 2, '.', ',') : 0 ?></td>
                </tr>
                <?php
                $sumHutangPendek = $sumHutangPendek + $row->saldo;
                $i++;
              endforeach;
              ?>
              <tr>
                <td align="left"><b>Jumlah (1)</b></td>
                <td align="right"><b><?php echo number_format((float)$sumHutangPendek, 2, '.', ',') ?></b></td>
              </tr>
              <tr>
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr>
                <td align="left" colspan="2"><b>Hutang Jangka Panjang</b></td>
              </tr>
              <?php
              $sumHutangPanjang = 0;
              $sql = $DAO_Transaksi->getHutangPanjang($filters);
              $recordsetHutangPanjang = $DAO_Transaksi->db->query($sql);
              $i = 1;
              foreach ($recordsetHutangPanjang->result() as $row):
                ?>
                <tr>
                  <td align="left"><?php echo isset($row->nama) ? $row->nama : '-' ?></td>
                  <td
                    align="right"><?php echo isset($row->saldo) ? number_format((float)$row->saldo, 2, '.', ',') : 0 ?></td>
                </tr>
                <?php
                $sumHutangPanjang = $sumHutangPanjang + $row->saldo;
                $i++;
              endforeach;
              ?>
              <tr>
                <td align="left"><b>Jumlah (2)</b></td>
                <td align="right"><b><?php echo number_format((float)$sumHutangPanjang, 2, '.', ',') ?></b></td>
              </tr>
              </tbody>
            </table>

            <!-- Modal -->
            <table class="table table-bordered table-hover table-striped">
              <thead>
              <tr>
                <th class="text-center" colspan="2">EKUITAS</th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td align="left" colspan="2"><b>Modal</b></td>
              </tr>
              <?php
              $sumModal = 0;
              $sql = $DAO_Transaksi->getModal($filters);
              $recordsetModal = $DAO_Transaksi->db->query($sql);
              $i = 1;
              foreach ($recordsetModal->result() as $row):
                ?>
                <tr>
                  <td align="left"><?php echo isset($row->nama) ? $row->nama : '-' ?></td>
                  <td
                    align="right"><?php echo isset($row->saldo) ? number_format((float)$row->saldo, 2, '.', ',') : 0 ?></td>
                </tr>
                <?php
                $sumModal = $sumModal + $row->saldo;
                $i++;
              endforeach;
              ?>
              <tr>
                <?php
                $sumLabaBerjalan = 0;
                $sqlPendapatan = $DAO_Transaksi->getPendapatan($filters);
                $recordsetPendapatan = $DAO_Transaksi->db->query($sqlPendapatan);

                $sqlBeban = $DAO_Transaksi->getBeban($filters);
                $recordsetBeban = $DAO_Transaksi->db->query($sqlBeban);

                foreach ($recordsetPendapatan->result() as $row):
                  $sumLabaBerjalan = $sumLabaBerjalan + $row->saldo;
                endforeach;

                foreach ($recordsetBeban->result() as $row):
                  $sumLabaBerjalan = $sumLabaBerjalan - $row->saldo;
                endforeach;

                ?>
                <td align="left">Laba Berjalan</td>
                <td align="right"><?php echo number_format((float)$sumLabaBerjalan, 2, '.', ',') ?></td>
              </tr>
              <tr>
                <td align="left"><b>Jumlah (3)</b></td>
                <td align="right"><b><?php echo number_format((float)$sumModal + $sumLabaBerjalan, 2, '.', ',') ?></b>
                </td>
              </tr>
              <tr>
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr>
                <td align="left"><b>Jumlah Pasiva dan Ekuitas (1)+(2)+(3)</b></td>
                <td align="right">
                  <b><?php echo number_format((float)$sumHutangPendek + $sumHutangPanjang + $sumModal + $sumLabaBerjalan, 2, '.', ',') ?></b>
                </td>
              </tr>
              </tbody>
            </table>

          </td>
        </tr>
      </table>
    </div>

  </div>
  <div class="report-section">
    <button type="button" class="btn btn-primary btn-sm hidden-print"
            onclick="javascript:window.print();"><i class="fa fa-print"></i> Print
    </button>
  </div>
  <!-- END report --></div>
</body>
</html>
