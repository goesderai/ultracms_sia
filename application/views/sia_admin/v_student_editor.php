<!-- BEGIN Form -->
<?php echo form_open($formActionUrl, 'id="studentForm"'); ?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <?php
    if (isset($errorMessages) && !empty($errorMessages)) {
      $errHeading = lang('message_error_mandatory_fields');
      echo createErrorMessages($errHeading, $errorMessages);
    }
    ?>
    <!-- BEGIN Tabs -->
    <div>
      <!-- Tab navs -->
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a
            href="#tabChildData" aria-controls="tabChildData"
            role="tab" data-toggle="tab"><?php echo lang('student_tab_person_info'); ?></a>
        </li>
        <li role="presentation"><a href="#tabParentData"
                                   aria-controls="tabParentData" role="tab"
                                   data-toggle="tab"><?php echo lang('student_tab_parent_info'); ?></a>
        </li>
        <li role="presentation"><a href="#tabChildOriginData"
                                   aria-controls="tabChildOriginData" role="tab"
                                   data-toggle="tab"><?php echo lang('student_tab_origin_info'); ?></a>
        </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="tabChildData">
          <?php include_once 'includes/student_main_form.php'; ?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="tabParentData">
          <?php include_once 'includes/student_parent_form.php'; ?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="tabChildOriginData">
          <?php include_once 'includes/student_origin_form.php'; ?>
        </div>
      </div>
    </div>
    <!-- END Tabs -->
  </div>
</div>
<!-- /.row -->
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel-lower-buttons">
      <!-- Save button -->
      <button id="btnSimpan" type="button" class="btn btn-primary btn-sm" title="Simpan perubahan">
        <i class="fa fa-save"></i> <?php echo lang('label_save'); ?>
      </button>
      <!-- Print preview button -->
      <?php
      if (isset($postData['id'])) :
        $previewUrl = sprintf("sia_students/printview?%s=%d", QSPARAM_REC_ID, $postData['id']);
        ?>
        <a href="<?php echo site_url($previewUrl); ?>"
           class="btn btn-primary btn-sm" target="_blank"> <i
            class="fa fa-print"></i> <?php echo lang('label_print_preview'); ?>
        </a>
      <?php endif; ?>
      <!-- Close button -->
      <a href="javascript:void(0);" class="btn btn-primary btn-sm"
         onclick="siaJS_confirm('questions.confirm_close', '<?php echo site_url('sia_students/index'); ?>');">
        <i class="fa fa-minus-circle"></i> <?php echo lang('label_close'); ?>
      </a>
    </div>
  </div>
</div><!-- /.row -->
<input type="hidden" name="id"
       value="<?php echo isset($postData['id']) ? $postData['id'] : ''; ?>"/>
<?php echo form_close(); ?>
<!-- END Form -->
