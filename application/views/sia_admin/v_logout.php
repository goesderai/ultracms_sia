<!DOCTYPE html>
<html lang="en">

	<head>
		<?php echo generateHeadTags($pageTitle, null, null, null); ?>
	</head>
	
	<body style="padding-top: 10%">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-xs-12 col-md-offset-4">
					<div class="alert alert-success text-center">
						<p><?php echo lang('message_common_logout_successful');?><br>
						  <a href="<?php echo site_url('sia_login');?>" 
						     title="<?php echo lang('message_common_goto_login');?>">
						     <?php echo lang('label_relogin');?>
						  </a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</body>

</html>