<!-- BEGIN Form -->
<?php
echo form_open($formActionUrl, 'id="mainAccountForm"');

if (isset($errorMessages) && !empty($errorMessages)) {
  $errHeading = lang('message_error_mandatory_fields');
  echo createErrorMessages($errHeading, $errorMessages);
}

include_once 'includes/main_account_main_form.php';
?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel-lower-buttons">
      <button id="btnSimpan" type="button" class="btn btn-primary btn-sm"
              title="<?php echo lang('label_save_hint'); ?>">
        <i class="fa fa-save"></i> <?php echo lang('label_save'); ?>
      </button>
      <a href="javascript:void(0);" class="btn btn-primary btn-sm" title="<?php echo lang('label_close_hint'); ?>"
         onclick="siaJS_confirm('questions.confirm_close', '<?php echo site_url('sia_main_account/index'); ?>');">
        <i class="fa fa-minus-circle"></i> <?php echo lang('label_close'); ?>
      </a>
    </div>
  </div>
</div><!-- /.row -->
<input type="hidden" name="id" value="<?php echo isset($postData['id']) ? $postData['id'] : ''; ?>"/>
<?php echo form_close(); ?>
<!-- END Form -->
