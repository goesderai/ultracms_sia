<?php echo form_open('', 'id="reportForm" target="_blank"'); ?>
<div class="row">
  <div class="col-md-3 col-xs-12">
    <div class="form-group">
      <label for="cmbYear">Tahun</label>
      <?php
      $years = [
        date('Y') - 2 => date('Y') - 2,
        date('Y') - 1 => date('Y') - 1,
        date('Y') => date('Y')
      ];
      echo form_dropdown('year', $years, date('Y'), ['id'=>'cmbYear', 'class'=>'form-control']);
      ?>
    </div>
  </div>
  <div class="col-md-3 col-xs-12">
    <div class="form-group">
      <label for="cmbMonth">Bulan</label>
      <?php
      echo form_dropdown('month', monthComboOptions(false), (int) date('m'), ['id'=>'cmbMonth', 'class'=>'form-control']);
      ?>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-3 col-xs-12">
    <div class="form-group">
      <button id="btnSubmit" type="submit" class="btn btn-primary btn-block btn-sm"><i class="fa fa-play"></i> OK</button>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
<script type="text/javascript">
  $(document).ready(function () {
    $('#reportForm').submit(function () {
      $('#btnSubmit').attr('disabled', 'disabled').text('Refresh page to generate another report')
    });
  });
</script>
