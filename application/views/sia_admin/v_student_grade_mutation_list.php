<!-- Page Content -->
<?php echo form_open('sia_student_grade_mutation/index', 'method="get"'); ?>

<!-- Search Filter -->
<div class="row">
  <div class="col-md-4 col-xs-12">
    <div class="form-group">
      <label for="mutationDate">Tanggal Proses Kenaikan Jenjang</label>
      <div id="mutation_date_picker" class="input-group date">
        <input id="mutationDate" type="text" class="form-control input-sm"
               value="<?php echo isset($postData['mutation_date']) ? formatDateForDisplay($postData['mutation_date']) : ''; ?>"
               readonly="readonly"/>
        <span class="input-group-addon">
													<span class="fa fa-calendar"></span>
												</span>
      </div>
      <input id="mutation_date" name="mutation_date" type="hidden"
             value="<?php echo isset($postData['mutation_date']) ? $postData['mutation_date'] : ''; ?>"/>
    </div>
  </div>
  <div class="col-md-4 col-xs-12">
    <div class="form-group">
      <label for="cmbGradeId">Jenjang Tujuan</label>
      <?php
      $gradeOptions = array('-- Pilih --') + $DAO_Edu->getGradeSelectOptions();
      $selectedGrade = isset($postData['grade_id']) ? $postData['grade_id'] : '';
      $cmbGradeAttr = array('id' => 'cmbGradeId', 'class' => 'form-control input-sm');
      echo form_dropdown('grade_id', $gradeOptions, $selectedGrade, $cmbGradeAttr);
      ?>
    </div>
  </div>
  <div class="col-md-4 col-xs-12">
    <div class="form-group">
      <label for="cmbDestClassroomId">Tingkatan Tujuan</label>
      <?php
      $cmbClassroomAttr = array('id' => 'cmbDestClassroomId', 'class' => 'form-control input-sm');
      if (!empty($selectedGrade)) {
        $classroomOptions = $DAO_Edu->getClassroomSelectOptions($selectedGrade);
        $selectedClassroom = isset($postData['classroom_id']) ? $postData['classroom_id'] : '';
        echo form_dropdown('classroom_id', $classroomOptions, $selectedClassroom, $cmbClassroomAttr);
      } else {
        $classroomOptions = array('-- Pilih jenjang terlebih dahulu --');
        echo form_dropdown('classroom_id', $classroomOptions, 0, $cmbClassroomAttr);
      }
      ?>
    </div>
  </div>
</div><!-- /.row -->
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel-upper-buttons">
      <button id="btnCari" type="submit" class="btn btn-primary btn-sm">
        <i class="fa fa-search"></i> Cari
      </button>
      <button id="btnReset" type="button" class="btn btn-primary btn-sm">
        <i class="fa fa-undo"></i> Reset
      </button>
      <a href="<?php echo site_url('sia_student_grade_mutation/insert'); ?>" class="btn btn-primary btn-sm"
         title="Tambah data baru">
        <i class="fa fa-plus"></i> Tambah
      </a>
    </div>
  </div>
</div><!-- /.row -->

<!-- Search Result -->
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="table-responsive">
      <table class="table table-bordered table-hover table-striped">
        <thead>
        <tr>
          <th width="5%" class="text-center"><?php echo lang('label_rownum'); ?></th>
          <th width="35%" class="text-center">Tanggal Proses Kenaikan Jenjang</th>
          <th width="30%" class="text-center">Jenjang Tujuan</th>
          <th width="30%" class="text-center">Tingkatan Tujuan</th>
          <!-- <th width="10%" class="text-center">Aksi</th> -->
        </tr>
        </thead>
        <tbody>
        <?php
        $baseUrlParams = array(QSPARAM_PAGE => $listPage, QSPARAM_PAGE_SIZE => $listPageSize) + $_GET;
        if (isset($recordset)):
          $i = $rownumStart + 1;
          foreach ($recordset as $row):
            $urlParams = $baseUrlParams + array(QSPARAM_REC_ID => $row->id);
            $viewUrl = siteUrl('sia_student_grade_mutation/view', $urlParams);
            ?>
            <tr>
              <td><?php echo $i; ?></td>
              <td><a href="<?php echo $viewUrl; ?>" title="Lihat"><?php echo $row->mutation_date; ?></a></td>
              <td><?php echo $row->grade_name; ?></td>
              <td><?php echo $row->classroom_name; ?></td>
            </tr>
            <?php
            unset($urlParams, $viewUrl);
            $i++;
          endforeach;
        endif;
        ?>
        </tbody>
      </table>

      <?php if (isset($recordcount)):
        $pagingLinks = generatePagingLinks('sia_student_grade_mutation/index', $baseUrlParams, $recordcount, $listPageSize, $listPage);
        ?>
        <div class="pull-right"><?php echo $pagingLinks; ?></div>
      <?php endif; ?>
    </div>
  </div>
</div><!-- /.row -->
<?php echo form_close(); ?>
