<!-- BEGIN Form -->
<?php echo form_open($formActionUrl, 'id="employeeForm"'); ?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <?php
    if (isset($errorMessages) && !empty($errorMessages)) {
      $errHeading = lang('message_error_mandatory_fields');
      echo createErrorMessages($errHeading, $errorMessages);
    }
    ?>
    <!-- BEGIN Tabs -->
    <div>
      <!-- Tab navs -->
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a
            href="#tabGeneralInfo" aria-controls="tabGeneralInfo"
            role="tab" data-toggle="tab">Informasi Umum</a></li>
        <?php if (userHasRoles(array(ROLEID_CASHIER))): ?>
          <li role="presentation"><a href="#tabSalary"
                                     aria-controls="tabSalary" role="tab" data-toggle="tab">Gaji</a>
          </li>
        <?php endif; ?>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="tabGeneralInfo">
          <div class="tabpanel-content">
            <?php include_once 'includes/employee_main_form.php'; ?>
          </div>
        </div>
        <?php if (userHasRoles(array(ROLEID_CASHIER))): ?>
          <div role="tabpanel" class="tab-pane fade" id="tabSalary">
            <div class="tabpanel-content">
              <?php include_once 'includes/salary_main_form.php'; ?>
            </div>
          </div>
        <?php else: ?>
          <input type="hidden" name="gaji_pokok"
                 value="<?php echo isset($postData['gaji_pokok']) ? $postData['gaji_pokok'] : '0'; ?>"/>
          <input type="hidden" name="tunjangan"
                 value="<?php echo isset($postData['tunjangan']) ? $postData['tunjangan'] : '0'; ?>"/>
          <input type="hidden" name="lain_lain"
                 value="<?php echo isset($postData['lain_lain']) ? $postData['lain_lain'] : '0'; ?>"/>
        <?php endif; ?>
      </div>
    </div>
    <!-- END Tabs -->
  </div>
</div><!-- /.row -->
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel-lower-buttons">
      <button id="btnSimpan" type="button"
              class="btn btn-primary btn-sm">
        <i class="fa fa-save"></i> <?php echo lang('label_save'); ?>
      </button>
      <a href="javascript:void(0);"
         class="btn btn-primary btn-sm"
         onclick="siaJS_confirm('questions.confirm_close', '<?php echo site_url('sia_employees/index'); ?>');">
        <i class="fa fa-minus-circle"></i> <?php echo lang('label_close'); ?>
      </a>
    </div>
  </div>
</div><!-- /.row -->
<input type="hidden" id="id" name="id"
       value="<?php echo isset($postData['id']) ? $postData['id'] : ''; ?>"/>
<input type="hidden" id="isCashier" name="isCashier"
       value="<?php echo userHasRoles(array(ROLEID_CASHIER)) ? 1 : 0; ?>"/>
<?php echo form_close(); ?>
<!-- END Form -->
