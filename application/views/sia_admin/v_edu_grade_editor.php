<!-- Page Content -->
<?php echo form_open($formActionUrl, array('id' => 'formEduGrade')); ?>

<!-- Grade name -->
<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtNamaTingkat"><?php echo lang('edugrd_grade_name'); ?> <span
          class="sign sign-mandatory">*</span></label>
      <input id="gradeId" name="id" type="hidden" value="<?php echo $gradeId; ?>"/>
      <input id="txtNamaTingkat" name="name" type="text" class="form-control input-sm"
             value="<?php echo isset($postData['name']) ? $postData['name'] : ''; ?>"/>
    </div>
  </div>
</div>

<!-- Grade classrooms -->
<h4><?php echo lang('edugrd_level'); ?></h4>
<?php if ($gradeId == 0): ?>
  <p><i class="fa fa-info-circle"></i> <?php echo lang('edugrd_save_level_hint'); ?></p>
<?php else: ?>
  <div class="row">
    <div class="col-lg-6 col-md-6 col-xs-12">
      <div class="panel-upper-buttons">
        <button id="btnAddClassroom" type="button" class="btn btn-primary btn-sm"
                title="<?php echo lang('edugrd_add_level_hint'); ?>">
          <i class="fa fa-plus"></i> <?php echo lang('edugrd_add_level'); ?>
        </button>
      </div>
    </div>
  </div><!-- /.row -->
  <div class="row">
    <div class="col-lg-6 col-md-6 col-xs-12">
      <div id="classroomsContainer" class="table-responsive"></div>
    </div>
  </div><!-- /.row -->
<?php endif; ?>

<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel-lower-buttons">
      <button id="btnSimpan" type="button" class="btn btn-primary btn-sm"
              title="<?php echo lang('label_save_hint'); ?>">
        <i class="fa fa-save"></i> <?php echo lang('label_save'); ?>
      </button>
      <a href="javascript:void(0);" class="btn btn-primary btn-sm" title="<?php echo lang('label_close_hint'); ?>"
         onclick="siaJS_confirm('questions.confirm_close', '<?php echo site_url('sia_edu_grade/index'); ?>');">
        <i class="fa fa-minus-circle"></i> <?php echo lang('label_close'); ?></a>
    </div>
  </div>
</div><!-- /.row -->
<?php echo form_close(); ?>
<!-- /Page Content -->
