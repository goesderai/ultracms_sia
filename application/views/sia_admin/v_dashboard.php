<!--Header (logo & company info)-->
<div class="row">
  <div class="col-md-12 col-xs-12 text-center">
    <img src="<?php echo base_url('assets/img/Logo.png'); ?>"/>
    <br/>
    <br/>
    <p>
      <?php
      $companyInfoTemplate = '%s | Telp. %s | Fax %s<br>' .
                             '<i class="fa fa-envelope"></i> %s | <i class="fa fa-facebook-square"></i> %s | <i class="fa fa-instagram"></i> %s';
      echo sprintf($companyInfoTemplate, $COMPANY_ADDRESS, $COMPANY_PHONE, $COMPANY_FAX, $COMPANY_EMAIL, $COMPANY_FACEBOOK, $COMPANY_INSTAGRAM);
      ?>
    </p>
  </div>
</div>

<div class="row">
  <div class="col-md-6 col-xs-12">
    <div id="studentByGenderChart"></div>
  </div>
  <div class="col-md-6 col-xs-12">
    <div id="studentByReligionChart"></div>
  </div>
</div>

<!-- Google chart scripts -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  // Load the Visualization API and the corechart package.
  google.charts.load('current', {'packages': ['corechart']});

  // Set a callback to run when the Google Visualization API is loaded.
  google.charts.setOnLoadCallback(drawChart);

  // Callback that creates and populates a data table,
  // instantiates the pie chart, passes in the data and
  // draws it.
  function drawChart() {
    // Student by gender chart
    const data = google.visualization.arrayToDataTable([
                                                         ['Gender', 'Students'],
                                                         ['Laki-Laki', <?php echo $daoSiswa->countByCriteria("`gender`='L' and `active`='Y' and `status`=2"); ?>],
                                                         ['Perempuan', <?php echo $daoSiswa->countByCriteria("`gender`='P' and `active`='Y' and `status`=2"); ?>]
                                                       ]);
    const chart = new google.visualization.PieChart(document.getElementById('studentByGenderChart'));
    chart.draw(data, {'title': 'Segmentasi Siswa Berdasarkan Jenis Kelamin', 'width': 500, 'height': 500});

    // Student by religion chart
    const data2 = google.visualization.arrayToDataTable([
                                                          ['Religion', 'Students'],
                                                          ['Budha',  <?php echo $daoSiswa->countByCriteria("`religion_id`=1 and `active`='Y' and `status`=2"); ?>],
                                                          ['Hindu',  <?php echo $daoSiswa->countByCriteria("`religion_id`=2 and `active`='Y' and `status`=2"); ?>],
                                                          ['Islam',  <?php echo $daoSiswa->countByCriteria("`religion_id`=5 and `active`='Y' and `status`=2"); ?>],
                                                          ['Katholik', <?php echo $daoSiswa->countByCriteria("`religion_id`=4 and `active`='Y' and `status`=2"); ?>],
                                                          ['Kristen',  <?php echo $daoSiswa->countByCriteria("`religion_id`=3 and `active`='Y' and `status`=2"); ?>]
                                                        ]);
    const chart2 = new google.visualization.PieChart(document.getElementById('studentByReligionChart'));
    chart2.draw(data2, {'title': 'Segmentasi Siswa Berdasarkan Agama', 'width': 500, 'height': 500});
  }
</script>
