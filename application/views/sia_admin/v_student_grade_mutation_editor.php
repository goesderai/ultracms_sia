<?php

// BEGIN Form
echo form_open($formActionUrl, 'id="mutationForm"');

// Error message (if any)
if (isset($errorMessages) && !empty($errorMessages)) {
  $errHeading = lang('message_error_mandatory_fields');
  echo createErrorMessages($errHeading, $errorMessages);
}

// The form -->
include_once 'includes/student_grade_mutation_form.php';

// END Form -->
echo form_close();
