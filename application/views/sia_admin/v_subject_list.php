<!-- Page Content -->
<?php
if (isset($errorMessages) && !empty($errorMessages)):
  $errHeading = lang('message_error_mandatory_fields');
  echo createErrorMessages($errHeading, $errorMessages);
else:
  echo form_open('sia_subjects/index', 'method="get"');
  ?>
  <!-- Search Filter -->
  <div class="row">
    <div class="col-md-6 col-xs-12">
      <div class="form-group">
        <label for="txtNama">Nama</label>
        <input id="txtNama" name="name" type="text" class="form-control input-sm"
               value="<?php echo isset($postData['name']) ? $postData['name'] : ''; ?>"/>
      </div>
    </div>
  </div><!-- /.row -->
  <div class="row">
    <div class="col-md-12 col-xs-12">
      <div class="panel-upper-buttons">
        <button id="btnCari" type="submit" class="btn btn-primary btn-sm">
          <i class="fa fa-search"></i> <?php echo lang('label_search');?>
        </button>
        <button id="btnReset" type="button" class="btn btn-primary btn-sm">
          <i class="fa fa-undo"></i> <?php echo lang('label_reset');?>
        </button>
        <button id="btnTambah" type="button" class="btn btn-primary btn-sm" title="<?php echo lang('label_add_new_hint');?>">
          <i class="fa fa-plus"></i> <?php echo lang('label_add_new');?>
        </button>
      </div>
    </div>
  </div><!-- /.row -->

  <!-- Search Result -->
  <div class="row">
    <div class="col-md-12 col-xs-12">
      <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
          <thead>
          <tr>
            <th width="5%" class="text-center"><?php echo lang('label_rownum');?></th>
            <th width="85%" class="text-center">Nama</th>
            <th width="10%" class="text-center"><?php echo lang('label_action');?></th>
          </tr>
          </thead>
          <tbody>
          <?php
          $baseUrlParams = array(QSPARAM_PAGE => $listPage, QSPARAM_PAGE_SIZE => $listPageSize) + $_GET;
          if (isset($recordset)):
            $i = $rownumStart + 1;
            foreach ($recordset as $row):
              $urlParams = $baseUrlParams + array(QSPARAM_REC_ID => $row->id);
              $editUrl = siteUrl('sia_subjects/update', $urlParams);
              $editedData = htmlentities(json_encode(array('id' => $row->id, 'name' => $row->name)));
              $deleteUrl = siteUrl('sia_subjects/delete', $urlParams);
              ?>
              <tr>
                <td><?php echo $i; ?></td>
                <td>
                  <a href="javascript:void(0);" title="Edit"
                     onclick="javascript:ultracms.siaSubjectList.editRow('<?php echo $editUrl; ?>', '<?php echo $editedData; ?>');">
                    <?php echo $row->name; ?>
                  </a>
                </td>
                <td class="text-center">
                  <a href="javascript:void(0)"
                     onclick="siaJS_confirm('questions.confirm_action', '<?php echo $deleteUrl; ?>');"
                     title="<?php echo lang('label_delete'); ?>" class="btn btn-danger btn-xs">
                    <i class="fa fa-times"></i>
                  </a>
                </td>
              </tr>
              <?php
              $i++;
            endforeach;
          endif;
          ?>
          </tbody>
        </table>

        <?php if (isset($recordcount)):
          $pagingLinks = generatePagingLinks('sia_subjects/index', $baseUrlParams, $recordcount, $listPageSize, $listPage);
          ?>
          <div class="pull-right"><?php echo $pagingLinks; ?></div>
        <?php endif; ?>
      </div>
    </div>
  </div><!-- /.row -->
  <?php
  echo form_close();
endif;
?>

<!-- editor form -->
<div class="modal fade" id="subjectEditorForm" tabindex="-1" role="dialog" aria-labelledby="subjectModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <?php echo form_open('#', array('id' => 'formEditorSubject', 'method' => 'post')); ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="subjectModalLabel">Mata Pelajaran</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label for="subjectName" class="control-label">Nama</label>
          <input id="subjectName" name="name" type="text" class="form-control input-sm"/>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary btn-sm">
          <i class="fa fa-save"></i> <?php echo lang('label_save');?>
        </button>
        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">
          <i class="fa fa-minus-circle"></i> <?php echo lang('label_close');?>
        </button>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>
