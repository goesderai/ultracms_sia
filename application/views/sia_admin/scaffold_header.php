<!DOCTYPE html>
<html lang="en">
<head>
  <?php
  echo generateHeadTags($pageTitle, null, $pageCss, $pageJs);
  if (!empty($pageJsInitFunc)) {
    echo sprintf('<script type="text/javascript">$(document).ready(function(){%s;});</script>', $pageJsInitFunc);
  }
  ?>
</head>
<body>
<div id="wrapper">
  <?php
  include_once 'includes/nav.php';
  echo '<div id="page-wrapper">';
  echo '<div class="container-fluid">';
  echo renderPageHeading($pageTitle, $pageIcon);
  ?>
