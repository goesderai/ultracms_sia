<!-- Page Content -->
<?php echo form_open('sia_transaction_type/index', 'method="get"'); ?>

<!-- Search Filter -->
<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="cmbTipeTransaksi"><?php echo lang('trxtype_type'); ?></label>
      <?php
      $tipeTransaksi = $DAO_TipeTransaksi->getAllTrxTypeAsArray(array(emptyComboOption(lang('label_all'))));
      $selectedtipeTransaksi = isset($postData['transaction_type']) ? $postData['transaction_type'] : '';
      $cmbTipeTransaksiAttr = array('id' => 'cmbTipeTransaksi', 'class' => 'form-control input-sm');
      echo form_dropdown('transaction_type', $tipeTransaksi, $selectedtipeTransaksi, $cmbTipeTransaksiAttr);
      ?>
    </div>
  </div>
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="txtNama"><?php echo lang('trxtype_name'); ?></label>
      <input id="txtNama" name="nama" type="text" class="form-control input-sm"
             value="<?php echo isset($postData['nama']) ? $postData['nama'] : ''; ?>"/>
    </div>
  </div>
</div><!-- /.row -->
<div class="row">
  <div class="col-lg-6 col-md-6 col-xs-12">
    <div class="form-group">
      <label for="cmbDocType"><?php echo lang('trxtype_doc_type'); ?></label>
      <?php
      $docTypes = $DAO_Document_Type->getAllSelectOptions(array(emptyComboOption(lang('label_all'))));
      $selectedDocType = isset($postData['document_type_id']) ? $postData['document_type_id'] : '';
      $cmbDocTypeAttr = array('id' => 'cmbDocType', 'class' => 'form-control input-sm');
      echo form_dropdown('document_type_id', $docTypes, $selectedDocType, $cmbDocTypeAttr);
      ?>
    </div>
  </div>
  <div class="col-lg-3 col-md-3 col-xs-12">
    <div class="form-group">
      <label for="cmbStatus"><?php echo lang('trxtype_active_inactive'); ?></label>
      <?php
      echo form_dropdown("active",
        array("0" => "-- " . lang('label_all') . " --", "Y" => lang("trxtype_active"), "N" => lang("trxtype_inactive")),
        isset($postData["active"]) ? $postData["active"] : "0",
        array("id" => "cmbStatus", "class" => "form-control input-sm"));
      ?>
    </div>
  </div>
  <div class="col-lg-3 col-md-3 col-xs-12">
    <div class="form-group">
      <label for="cmbStatusDC"><?php echo lang('trxtype_debit_credit'); ?></label>
      <?php
      echo form_dropdown("transaction_flow",
        array("0" => "-- " . lang('label_all') . " --", "D" => lang('trxtype_debit'), "C" => lang("trxtype_credit")),
        isset($postData["transaction_flow"]) ? $postData["transaction_flow"] : "0",
        array("id" => "cmbStatusDC", "class" => "form-control input-sm"));
      ?>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel-upper-buttons">
      <button id="btnCari" type="submit" class="btn btn-primary btn-sm">
        <i class="fa fa-search"></i> <?php echo lang('label_search'); ?>
      </button>
      <button id="btnReset" type="button" class="btn btn-primary btn-sm">
        <i class="fa fa-undo"></i> <?php echo lang('label_reset'); ?>
      </button>
      <a href="<?php echo site_url('sia_transaction_type/insert'); ?>" class="btn btn-primary btn-sm"
         title="<?php echo lang('label_add_new_hint'); ?>">
        <i class="fa fa-plus"></i> <?php echo lang('label_add_new'); ?>
      </a>
    </div>
  </div>
</div><!-- /.row -->

<!-- Search Result -->
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="table-responsive">
      <table class="table table-bordered table-hover table-striped">
        <thead>
        <tr>
          <th width="5%" class="text-center"><?php echo lang('label_rownum'); ?></th>
          <th width="25%" class="text-center"><?php echo lang('trxtype_code'); ?></th>
          <th width="30%" class="text-center"><?php echo lang('trxtype_name'); ?></th>
          <th width="15%" class="text-center"><?php echo lang('trxtype_debit_credit'); ?></th>
          <th width="15%" class="text-center"><?php echo lang('trxtype_active_inactive'); ?></th>
          <th width="10%" class="text-center"><?php echo lang('label_action'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $baseUrlParams = array(QSPARAM_PAGE => $listPage, QSPARAM_PAGE_SIZE => $listPageSize) + $_GET;
        if (isset($recordset)):
          $i = $rownumStart + 1;
          foreach ($recordset as $row):
            $urlParams = $baseUrlParams + array(QSPARAM_REC_ID => $row->id);
            $editUrl = siteUrl('sia_transaction_type/update', $urlParams);
            $deleteUrl = siteUrl('sia_transaction_type/delete', $urlParams);
            ?>
            <tr>
              <td><?php echo $i; ?></td>
              <td><a href="<?php echo $editUrl; ?>" title="<?php echo lang('label_edit'); ?>">
                  <?php echo $row->transaction_type; ?></a>
              </td>
              <td><?php echo $row->nama; ?></td>
              <td><?php echo ($row->transaction_flow == "D") ? lang("trxtype_debit") : lang("trxtype_credit"); ?></td>
              <td><?php echo ($row->active == "Y") ? lang("trxtype_active") : lang("trxtype_inactive"); ?></td>
              <td class="text-center">
                <a href="javascript:void(0);" title="<?php echo lang('label_delete'); ?>"
                   class="btn btn-danger btn-xs"
                   onclick="siaJS_confirm('questions.confirm_delete', '<?php echo $deleteUrl; ?>');">
                  <i class="fa fa-times"></i>
                </a>
              </td>
            </tr>
            <?php
            unset($urlParams, $editUrl);
            $i++;
          endforeach;
        endif;
        ?>
        </tbody>
      </table>

      <?php if (isset($recordcount)):
        $pagingLinks = generatePagingLinks('sia_transaction_type/index', $baseUrlParams, $recordcount, $listPageSize, $listPage);
        ?>
        <div class="pull-right"><?php echo $pagingLinks; ?></div>
      <?php endif; ?>
    </div>
  </div>
</div><!-- /.row -->
<?php echo form_close(); ?>
