<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseBackEndController.php';
require_once 'IBasicController.php';

/**
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
class Sia_student_grade_mutation extends BaseBackEndController implements IBasicController
{

  var $recId;
  var $viewList = 'v_student_grade_mutation_list';
  var $viewEditor = 'v_student_grade_mutation_editor';
  var $viewReadOnly = 'v_student_grade_mutation_view';

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array("DAO_Edu"));
    $this->recId = $this->getRecId();
    $this->setViewData("DAO_Edu", $this->DAO_Edu);
  }

  function getPageTitle()
  {
    return 'Kenaikan Jenjang';
  }

  function getPageIcon()
  {
    return 'fa-graduation-cap';
  }

  function getJsFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/themes/sb-admin/js/moment-with-locales.min.js',
        'assets/themes/sb-admin/js/bootstrap-datetimepicker.min.js',
        'assets/js/ultracms_sia_student_grade_mutation_list.min.js');
    } elseif ($this->getControllerAction() === 'view') {
      return array();
    } else {
      return array('assets/themes/sb-admin/js/moment-with-locales.min.js',
        'assets/themes/sb-admin/js/bootstrap-datetimepicker.min.js',
        'assets/js/ultracms_comp_MultipleResultStudentLookupDialog.min.js',
        'assets/js/ultracms_sia_student_grade_mutation_editor.min.js');
    }
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaStudentGradeMutationList.init()';
    } elseif ($this->getControllerAction() === 'view') {
      return '';
    } else {
      return 'ultracms.siaStudentGradeMutationEditor.init()';
    }
  }

  function getCssFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/themes/sb-admin/css/bootstrap-datetimepicker.min.css');
    } elseif ($this->getControllerAction() === 'view') {
      return array();
    } else {
      return array('assets/themes/sb-admin/css/bootstrap-datetimepicker.min.css');
    }
  }

  public function index()
  {
    $mutation_date = isset($_GET['mutation_date']) ? $_GET['mutation_date'] : '';
    $classroom_id = isset($_GET['classroom_id']) ? $_GET['classroom_id'] : 0;
    $grade_id = isset($_GET['grade_id']) ? $_GET['grade_id'] : 0;
    $filters = array();
    if (!empty($mutation_date)) {
      $filters[] = array(QRYFILTER_FIELD => 'm.mutation_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $mutation_date);
    }
    if (!empty($classroom_id)) {
      $filters[] = array(QRYFILTER_FIELD => 'cr.id', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $classroom_id);
    }
    if (!empty($grade_id)) {
      $filters[] = array(QRYFILTER_FIELD => 'grd.id', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $grade_id);
    }
    $rs = $this->DAO_Edu->getGradeMutationList($filters, null, $this->getFirstRowPos(), $this->pageSize);
    $recordCount = $this->DAO_Edu->getGradeMutationListCount($filters);

    $this->setViewData(VDATAKEY_LIST_PAGE, $this->page);
    $this->setViewData(VDATAKEY_LIST_PAGE_SIZE, $this->pageSize);
    $this->setViewData(VDATAKEY_POST_DATA, $_GET);
    $this->setViewData(VDATAKEY_ROWNUM_START, $this->getFirstRowPos());
    $this->setViewData(VDATAKEY_RECORDSET, $rs);
    $this->setViewData(VDATAKEY_RECORDCOUNT, $recordCount);
    $this->loadView($this->viewList);
  }

  public function insert()
  {
    $this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
    $this->setViewData(VDATAKEY_EDITOR_STATE, $this->getControllerAction());
    if ($this->isPostRequest()) {
      $validateResult = $this->validate();
      if ($validateResult[0] === true) {
        // save data
        $data = $this->getFormData();
        $header = $data[0];
        $details = $data[1];
        $header['created_date'] = $this->getCurrentDateTime();
        $header['created_by'] = $this->userData['id'];
        $this->DAO_Edu->insertGradeMutation($header, $details);
        $this->showSuccessMessage('Data berhasil disimpan', 'sia_student_grade_mutation/index');
      } else {
        // show error & form
        $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
        $this->setViewData(VDATAKEY_POST_DATA, $_POST);
        $this->loadView($this->viewEditor);
      }
    } else {
      $dummyData = array();
      $this->loadView($this->viewEditor);
    }
  }

  public function view()
  {
    $headerData = $this->DAO_Edu->getGradeMutation($this->recId);
    $rsDetail = $this->DAO_Edu->getGradeMutationDetails($this->recId);
    $this->setViewData("headerData", $headerData);
    $this->setViewData("rsDetail", $rsDetail);
    $this->loadView($this->viewReadOnly);
  }

  private function getFormData()
  {
    $data = array();

    // Header data
    $data[0] = array(
      'mutation_date' => $_POST['mutation_date'],
      'dest_classroom_id' => $_POST['dest_classroom_id']
    );

    // Detail data
    $data[1] = isset($_POST['studentId']) ? $_POST['studentId'] : array();

    return $data;
  }

  private function validate()
  {
    $errMsg = array();
    $data = $this->getFormData();
    $headData = $data[0];
    $detailData = $data[1];
    $editorState = $this->getControllerAction();

    // validating...
    if (empty($headData['mutation_date'])) {
      $errMsg[] = 'Tanggal proses kenaikan jenjang belum diisi';
    }
    if (empty($headData['dest_classroom_id'])) {
      $errMsg[] = 'Tingkatan tujuan belum diisi';
    }
    if (count($detailData) <= 0 || empty($detailData)) {
      $errMsg[] = 'Daftar siswa belum diisi';
    }

    if (count($errMsg) > 0) {
      return array(false, $errMsg);
    } else {
      return array(true, $errMsg);
    }
  }

}
