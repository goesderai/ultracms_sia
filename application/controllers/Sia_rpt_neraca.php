<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseReportController.php';
require_once 'IReportController.php';

/**
 * Controller laporan Neraca
 *
 * @author <a href="mailto:agung3ka@gmail.com">Turah Eka</a>
 */
class Sia_rpt_neraca extends BaseReportController implements IReportController{

  const V_RPT = 'v_neraca_report';
  const V_RPT_FORM = 'v_rpt_neraca_form';
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array('DAO_Transaksi'));
	}

  function getPageTitle()
  {
    return 'Laporan Neraca';
  }

  function getJsFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/themes/sb-admin/js/moment-with-locales.min.js',
        'assets/themes/sb-admin/js/bootstrap-datetimepicker.min.js',
        'assets/js/ultracms_sia_rpt_neraca.min.js');
    } else {
      return array();
    }
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaRptNeraca.init()';
    } else {
      return '';
    }
  }

  function getCssFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/themes/sb-admin/css/bootstrap-datetimepicker.min.css');
    } else {
      return array();
    }
  }

  public function index()
  {
		$this->setViewData('DAO_Transaksi', $this->DAO_Transaksi);
		$this->loadReportForm(self::V_RPT_FORM);
	}
	
	public function generate_report(){
		/*$tanggalDari = isset($_POST['date_from'])? $_POST['date_from'] : '';
		$tanggalSampai = isset($_POST['date_to'])? $_POST['date_to'] : '';*/
		$periode = isset($_POST['financial_year'])? $_POST['financial_year'] : '';
		$tanggalSampai = $periode.'-12-31';
		$filters = array();

		if(!empty($tanggalSampai)){
			$filters[] = array(QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_IS_LESS_THAN_OR_EQUAL_TO, QRYFILTER_VALUE => $tanggalSampai);
		}
		$validateResult = $this->validate();
		if($validateResult[0]===true){
			// Show report
      $this->setViewData(VDATAKEY_PAGE_TITLE, $this->getPageTitle());
			$this->setViewData('DAO_Transaksi', $this->DAO_Transaksi);
			$this->setViewData('filters', $filters);
			$this->setViewData('periode', $periode);
			$this->setViewData('tanggalSampai', $tanggalSampai);
			$this->setViewData('ket', 'Periode ');
			$this->loadCompanyInfo();
			$this->loadPlainView(self::V_RPT);
		}else{
			// show error & form
			$this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
			$this->setViewData(VDATAKEY_POST_DATA, $_POST);
			$this->loadView(self::V_RPT_FORM);
		}
	}
	
	private function validate(){
		$errMsg = array();
		
		if(count($errMsg) > 0){
			return array(false, $errMsg);
		}else{
			return array(true, $errMsg);
		}
	}
	
}
