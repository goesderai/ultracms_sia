<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseReportController.php';
require_once 'IReportController.php';

/**
 * Controller laporan tunggakan pembayaran buku
 *
 * @author <a href="mailto:agung3ka@gmail.com">Turah Eka</a>
 */
class Sia_rpt_tunggakan_uang_buku extends BaseReportController implements IReportController{

  const V_RPT_FORM = 'v_rpt_tunggakan_uang_buku_form';

	public function __construct(){
		parent::__construct();
		$this->load->model(array('DAO_Siswa', 'DAO_Transaksi','DAO_Edu','DAO_Lov'));
	}

  function getPageTitle()
  {
    if ($this->isPostRequest()) {
      $year = $_POST['financial_year'];
      return "Laporan Tunggakan Uang Buku Siswa Tahun {$year}";
    } else {
      return "Laporan Tunggakan Uang Buku Siswa";
    }
  }

  function getJsFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/js/ultracms_sia_rpt_tunggakan_uang_buku.min.js');
    } else {
      return array();
    }
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaRptTransaksiTunggakanUangBuku.init()';
    } else {
      return '';
    }
  }

  function getCssFiles()
  {
    return array();
  }

  public function index()
  {
		$this->setViewData('DAO_Siswa', $this->DAO_Siswa);
		$this->setViewData('DAO_Transaksi', $this->DAO_Transaksi);
		$this->setViewData('DAO_Edu', $this->DAO_Edu);
		$this->setViewData('DAO_Lov', $this->DAO_Lov);
		$this->loadReportForm(self::V_RPT_FORM);
	}
	
	public function generate_report(){
			$grade = 3;
			$classroom = isset($_POST['classroom'])? $_POST['classroom'] : '';
			$year = isset($_POST['financial_year'])? $_POST['financial_year'] : '';
			$validateResult = $this->validate();
			if($validateResult[0]===true){
				// Show report
				/*if($classroom == 0 && $grade == 0){
					$sql = 'SELECT s.nis as `NIS`, s.name as `Nama Siswa`, g.name AS `Jenjang`, e.name as `Tingkatan`, s.phone_number as `No. Telepon` FROM m_student s INNER JOIN m_edu_classroom e ON s.classroom_id=e.id INNER JOIN m_edu_grade g ON e.grade_id = g.id WHERE s.active = "Y" AND s.status = 2 AND s.id NOT IN (SELECT v.student_id FROM v_rpt_bayar_uang_buku v WHERE v.tahun_ajaran = ?) and s.efective_date < Date("'.$year.'-07-02") order by e.name, s.nis';
					$qry = $this->db->query($sql, array($year));
				}if($classroom != 0 && $grade == 0){
					$sql = 'SELECT s.nis as `NIS`, s.name as `Nama Siswa`, g.name AS `Jenjang`, e.name as `Tingkatan`, s.phone_number as `No. Telepon` FROM m_student s INNER JOIN m_edu_classroom e ON s.classroom_id=e.id INNER JOIN m_edu_grade g ON e.grade_id = g.id WHERE s.active = "Y" AND s.status = 2 AND s.id NOT IN (SELECT v.student_id FROM v_rpt_bayar_uang_buku v WHERE v.tahun_ajaran = ? and v.classroom_id = ?) and s.efective_date < Date("'.$year.'-07-02") and e.id = ? order by e.name, s.nis';
					$qry = $this->db->query($sql, array($year,$classroom,$classroom));
				}if($classroom == 0 && $grade != 0){
					$sql = 'SELECT s.nis as `NIS`, s.name as `Nama Siswa`, g.name AS `Jenjang`, e.name as `Tingkatan`, s.phone_number as `No. Telepon` FROM m_student s INNER JOIN m_edu_classroom e ON s.classroom_id=e.id INNER JOIN m_edu_grade g ON e.grade_id = g.id WHERE s.active = "Y" AND s.status = 2 AND s.id NOT IN (SELECT v.student_id FROM v_rpt_bayar_uang_buku v WHERE v.tahun_ajaran = ? and v.grade_id = ?) and s.efective_date < Date("'.$year.'-07-02") and g.id = ? order by e.name, s.nis';
					$qry = $this->db->query($sql, array($year,$grade,$grade));
				}if($classroom != 0 && $grade != 0){
					$sql = 'SELECT s.nis as `NIS`, s.name as `Nama Siswa`, g.name AS `Jenjang`, e.name as `Tingkatan`, s.phone_number as `No. Telepon` FROM m_student s INNER JOIN m_edu_classroom e ON s.classroom_id=e.id INNER JOIN m_edu_grade g ON e.grade_id = g.id WHERE s.active = "Y" AND s.status = 2 AND s.id NOT IN (SELECT v.student_id FROM v_rpt_bayar_uang_buku v WHERE v.tahun_ajaran = ? and v.grade_id = ? and v.classroom_id = ?) and s.efective_date < Date("'.$year.'-07-02") and e.id = ? and g.id = ? order by e.name, s.nis';
					$qry = $this->db->query($sql, array($year,$grade,$classroom,$classroom,$grade));
				}*/
				
				if($classroom == 0 && $grade != 0){
					$sql = 'SELECT s.nis as `NIS`, s.nama_siswa as `Nama Siswa`, s.grade_name AS `Jenjang`, s.class_name as `Tingkatan`, s.phone_number as `No. Telepon` FROM v_student_history_mutations s WHERE s.student_id NOT IN (SELECT v.student_id FROM v_exclude_tunggakan_uang_buku v WHERE v.tahun_ajaran = ?) and s.year = ? and s.grade_id = ? order by s.grade_name, s.nis';
					$qry = $this->db->query($sql, array($year,$year,$grade));
				}if($classroom != 0 && $grade != 0){
					$sql = 'SELECT s.nis as `NIS`, s.nama_siswa as `Nama Siswa`, s.grade_name AS `Jenjang`, s.class_name as `Tingkatan`, s.phone_number as `No. Telepon` FROM v_student_history_mutations s WHERE s.student_id NOT IN (SELECT v.student_id FROM v_exclude_tunggakan_uang_buku v WHERE v.tahun_ajaran = ?) and s.year = ? and s.grade_id = ? and s.class_id = ? order by s.grade_name, s.nis';
					$qry = $this->db->query($sql, array($year, $year, $grade, $classroom));
				}
				$this->setReportGenField(RPTGEN_HEADER, $this->getPageTitle());
				$this->setReportGenField(RPTGEN_MYSQL_RESOURCE, $qry);
				$this->setReportGenField(RPTGEN_DATE_FIELDS, array('Tanggal Transaksi' => $this->appConfig['dateFormatForDisplay']));
				$this->loadReport();
			}else{
				// show error & form
				$this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
				$this->setViewData(VDATAKEY_POST_DATA, $_POST);
				$this->loadView(self::V_RPT_FORM);
			}
	}
	
	private function validate(){
		$errMsg = array();
		$year = isset($_POST['financial_year'])? $_POST['financial_year'] : '';

    // Tahun
		if($year == ''){
			$errMsg[] = "Tahun Ajaran Harus diisi!";
		}
		
		if(count($errMsg) > 0){
			return array(false, $errMsg);
		}else{
			return array(true, $errMsg);
		}
	}
	
}
