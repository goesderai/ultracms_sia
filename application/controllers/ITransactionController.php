<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'IBasicController.php';

/**
 * Transaction controller interface
 * 
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
interface ITransactionController extends IBasicController
{
    function showAfterSaveMessage($invoiceUrl, $proceedUrl);
    function show_invoice($transactionId);
    function generateInvoiceData($transactionId);
}
