<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseBackEndController.php';
require_once 'IBasicController.php';
require_once 'ITransactionController.php';
require_once APPPATH . 'models/InvoiceHeader.php';
require_once APPPATH . 'models/InvoiceDetail.php';

/**
 * Sia_transaksi_pendapatan_umum_siswa.php
 *
 * @author <a href="mailto:agung3ka@gmail.com">Turah Eka</a>
 */
class Sia_transaksi_pendapatan_umum_siswa extends BaseBackEndController implements IBasicController
{

  var $recId;
  var $viewList   = 'v_transaksi_pendapatan_umum_siswa_list';
  var $viewEditor = 'v_transaksi_pendapatan_umum_siswa_editor';

  public function __construct()
  {
    parent::__construct();

    $this->load->model(['DAO_System_Parameter', 'DAO_Security', 'DAO_Transaksi_Pendapatan', 'DAO_Transaksi', 'DAO_Siswa', 'DAO_Lov', 'DAO_Sequence', 'DAO_TipeTransaksi', 'DAO_Posting_Profile']);
    $this->recId = $this->getRecId();
    $this->load->library(['Number_generator']);
    $this->setViewData('DAO_Siswa', $this->DAO_Siswa);
    $this->setViewData('DAO_TipeTransaksi', $this->DAO_TipeTransaksi);
    $this->setViewData('DAO_System_Parameter', $this->DAO_System_Parameter);
    $this->setViewData('DAO_Lov', $this->DAO_Lov);
    $this->setViewData('DAO_Posting_Profile', $this->DAO_Posting_Profile);
    $this->setViewData('DAO_Security', $this->DAO_Security);
    $this->setViewData('userData', $this->userData);
  }

  function getPageTitle()
  {
    return 'Transaksi Pendapatan Umum Siswa';
  }

  function getJsFiles()
  {
    $js = ['assets/themes/sb-admin/js/moment-with-locales.min.js',
           'assets/themes/sb-admin/js/bootstrap-datetimepicker.min.js'];
    if ($this->isIndexAction()) {
      $js[] = 'assets/js/ultracms_sia_transaksi_pendapatan_umum_siswa_list.min.js';
    } else {
      $js[] = 'assets/themes/sb-admin/js/jquery.number.min.js';
      $js[] = 'assets/js/ultracms_comp_SingleResultStudentLookupDialog.min.js';
      $js[] = 'assets/js/ultracms_sia_transaksi_pendapatan_umum_siswa_editor.min.js';
    }
    return $js;
  }

  function getCssFiles()
  {
    return ['assets/themes/sb-admin/css/bootstrap-datetimepicker.min.css'];
  }

  function getPageIcon()
  {
    return 'fa-usd';
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaTransaksiPendapatanUmumSiswaList.init()';
    } else {
      return 'ultracms.siaTransaksiPendapatanUmumSiswaEditor.init()';
    }
  }

  public function index()
  {
    $transaction_type      = isset($_GET['nama']) ? $_GET['nama'] : '';
    $nomor                 = isset($_GET['nomor']) ? $_GET['nomor'] : '';
    $siswa                 = isset($_GET['siswa']) ? $_GET['siswa'] : '';
    $nis                   = isset($_GET['nis']) ? $_GET['nis'] : '';
    $remark                = isset($_GET['remark']) ? $_GET['remark'] : '';
    $transaction_date_from = isset($_GET['transaction_date_from']) ? $_GET['transaction_date_from'] : '';
    $transaction_date_to   = isset($_GET['transaction_date_to']) ? $_GET['transaction_date_to'] : '';
    $filters               = [
      [QRYFILTER_FIELD => 'transaction_number', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $nomor],
      [QRYFILTER_FIELD => 'nama_tr', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $transaction_type],
      [QRYFILTER_FIELD => 'remark', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $remark],
      [QRYFILTER_FIELD => 'nama_siswa', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $siswa],
      [QRYFILTER_FIELD => 'nis', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $nis]
    ];

    if (!empty($transaction_date_from)) {
      if (!empty($transaction_date_to)) {
        $filters[] = [QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_BETWEEN, QRYFILTER_VALUE => [$transaction_date_from, $transaction_date_to]];
      } else {
        $filters[] = [QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_MORE_THAN_OR_EQUAL_TO, QRYFILTER_VALUE => $transaction_date_from];
      }
    }

    /*if($transaction_type>0){
              $filters[] = array(QRYFILTER_FIELD => 'transaction_type', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $transaction_type);
          }*/

    $rs          = $this->DAO_Transaksi_Pendapatan->getListTransaksi($filters, null, $this->getFirstRowPos(), $this->pageSize);
    $recordCount = $this->DAO_Transaksi_Pendapatan->getListCount($filters);

    $this->setViewData(VDATAKEY_LIST_PAGE, $this->page);
    $this->setViewData(VDATAKEY_LIST_PAGE_SIZE, $this->pageSize);
    $this->setViewData(VDATAKEY_POST_DATA, $_GET);
    $this->setViewData(VDATAKEY_ROWNUM_START, $this->getFirstRowPos());
    $this->setViewData(VDATAKEY_RECORDSET, $rs);
    $this->setViewData(VDATAKEY_RECORDCOUNT, $recordCount);

    $this->loadView($this->viewList);
  }

  public function insert()
  {
    $this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
    $this->setViewData(VDATAKEY_EDITOR_STATE, 'ADD');
    if ($this->isPostRequest()) {
      $validateResult = $this->validate();
      if ($validateResult[0] === true) {
        // save
        $pendapatan                       = $this->getFormData();
        $pendapatan['transaction_number'] = $this->generateTransactionNumber();
        $jurnal                           = $this->getFormDataForJurnal();


        $id = $this->DAO_Transaksi_Pendapatan->insert($pendapatan);
        /**
         * TODO insert ke tabel jurnal debet kredit masih pertanyaan sisa pembayaran dijadikan utang apa piutang??
         */
        $jurnal['transaction_number'] = $pendapatan['transaction_number'];
        $this->DAO_Transaksi->insertJurnalWithPiutang($jurnal, parseFloat($_POST["sisa_bayar"]));
        $this->showAfterSaveMessage(site_url("sia_transaksi_pendapatan_umum_siswa/show_invoice/{$id}"),
                                    site_url('sia_transaksi_pendapatan_umum_siswa/index'));
      } else {
        // show error & form
        $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
        $this->setViewData(VDATAKEY_POST_DATA, $_POST);
        $this->loadView($this->viewEditor);
      }
    } else {

      $this->loadView($this->viewEditor);
    }
  }

  public function update()
  {
    $this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
    $this->setViewData(VDATAKEY_EDITOR_STATE, 'EDIT');
    if ($this->isPostRequest()) {
      $validateResult = $this->validate();
      if ($validateResult[0] === true) {
        // update
        $this->DAO_Transaksi_Pendapatan->update($this->getFormDataEditePendapatanSiswa(), $this->recId);
        $this->DAO_Transaksi->updatePendapatan($this->getFormData(), $_POST['transaction_number']);
        $this->showSuccessMessage('Data berhasil disimpan', 'sia_transaksi_pendapatan_umum_siswa/index');
      } else {
        // show error & form
        $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
        $this->setViewData(VDATAKEY_POST_DATA, $_POST);
        $this->loadView($this->viewEditor);
      }
    } else {
      $transaksi = $this->DAO_Transaksi_Pendapatan->getRow($this->recId);
      $this->setViewData(VDATAKEY_POST_DATA, $transaksi);
      $this->loadView($this->viewEditor);
    }
  }

  public function koreksi()
  {
    $this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
    $this->setViewData(VDATAKEY_EDITOR_STATE, 'KOREKSI');
    if ($this->isPostRequest()) {
      $validateResult = $this->validate();
      if ($validateResult[0] === true) {
        // save
        $newTransactionNumber = $this->generateTransactionNumber();
        $this->DAO_Transaksi_Pendapatan->insertKoreksi($this->recId, $newTransactionNumber);
        /**
         * TODO insert ke tabel jurnal debet kredit masih pertanyaan sisa pembayaran dijadikan utang apa piutang??
         */
        $this->DAO_Transaksi->insertJurnalKoreksi($_POST['transaction_number'], $newTransactionNumber);


        $this->setViewData(VDATAKEY_ALERT_TYPE, ALERTTYPE_SUCCESS);
        $this->setViewData(VDATAKEY_ALERT_MESSAGE, 'Data berhasil disimpan');
        $this->setViewData(VDATAKEY_ALERT_PROCEED_URL, site_url('sia_transaksi_pendapatan_umum_siswa/index'));
        $this->loadView($this->viewAfterSaveMessage);
      } else {
        // show error & form
        $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
        $this->setViewData(VDATAKEY_POST_DATA, $_POST);
        $this->loadView($this->viewEditor);
      }
    } else {
      $transaksi = $this->DAO_Transaksi_Pendapatan->getRow($this->recId);
      $this->setViewData(VDATAKEY_POST_DATA, $transaksi);
      $this->loadView($this->viewEditor);
    }
  }

  private function getFormData()
  {
    $currentAction  = $this->uri->segment(2, 0);
    $typTrx         = $_POST["trx_typ"];
    $trxDate        = strtotime($_POST["trx_date"]);
    $trxDateFormat  = date('Y-m-d', $trxDate);
    $numMonth       = date("n", $trxDate);
    $numYear        = date("Y", $trxDate);
    $postingProfile = $this->DAO_Posting_Profile->getPostingProfileByType($_POST["sub_transaction_type"]);
    $accountCode    = null;
    $jmlTrx         = parseFloat(isset($_POST["jumlah_transaksi"]) ? $_POST["jumlah_transaksi"] : 0);
    $diskon         = parseFloat(isset($_POST["diskon"]) ? $_POST["diskon"] : 0);
    $totalTrx       = parseFloat(isset($_POST["total_bayar"]) ? $_POST["total_bayar"] : 0);
    $dibayar        = parseFloat(isset($_POST["dibayar"]) ? $_POST["dibayar"] : 0);
    $sisa           = parseFloat(isset($_POST["sisa_bayar"]) ? $_POST["sisa_bayar"] : 0);
    $tahunAjaran    = isset($_POST["tahun_ajaran"]) ? $_POST["tahun_ajaran"] : '';


    if ($postingProfile != null) {
      $accountCode = $postingProfile['account_credit'];
    }

    $data = [
      'transaction_type' => $_POST["sub_transaction_type"],
      'transaction_date' => $trxDateFormat,
      'remark'           => $_POST["remark"],
      'student_id'       => $_POST["student_id"],
      'jumlah_transaksi' => $jmlTrx,
      'diskon'           => $diskon,
      'total_bayar'      => $totalTrx,
      'dibayar'          => $dibayar,
      'sisa_bayar'       => $sisa,
      'tahun_ajaran'     => $tahunAjaran,
      'remark'           => $_POST["remark"]
    ];

    //Set audit trails
    if (!empty($currentAction)) {
      if ($currentAction == 'insert') {
        $data['created_by']   = $this->getCurrentUserId();
        $data['created_date'] = $this->getCurrentDateTime();
      }
      if ($currentAction == 'update') {
        $data['modified_by']   = $this->getCurrentUserId();
        $data['modified_date'] = $this->getCurrentDateTime();
      }
    }
    return $data;
  }

  private function getFormDataEditePendapatanSiswa()
  {
    $data           = $this->DAO_Transaksi_Pendapatan->getRow($this->recId);
    $data['remark'] = $_POST["remark"];
    return $data;
  }

  private function getFormDataForJurnal()
  {
    $currentAction  = $this->uri->segment(2, 0);
    $typTrx         = $_POST["trx_typ"];
    $trxDate        = strtotime($_POST["trx_date"]);
    $trxDateFormat  = date('Y-m-d', $trxDate);
    $numMonth       = date("n", $trxDate);
    $numYear        = date("Y", $trxDate);
    $postingProfile = $this->DAO_Posting_Profile->getPostingProfileByType($_POST["sub_transaction_type"]);
    $accountCode    = null;
    /*$jmlTrx = $_POST["jumlah_transaksi"];
        $diskon = $_POST["diskon"];*/
    $totalTrx = parseFloat($_POST["total_bayar"]);
    /*$totalBayar = $_POST["dibayar"];*/


    if ($postingProfile != null) {
      $accountCode = $postingProfile['account_credit'];
    }

    $data = [
      'transaction_date' => $trxDateFormat,
      'remark'           => $_POST["remark"],
      'transaction_type' => $_POST["sub_transaction_type"],
      'financial_year'   => $numYear,
      'financial_month'  => $numMonth,
      'account_code'     => $accountCode,
      'created_date'     => date('Y-m-d H:i:s', time())
    ];
    if ($_POST["student_id"] > 0) {
      $data['student_id'] = $_POST["student_id"];
    }
    $data['amount_credit'] = $totalTrx;
    if (!empty($currentAction) && $currentAction == 'insert') {
      $data['modified_date'] = date('Y-m-d H:i:s', time());
    }
    return $data;
  }

  /*private function getFormDataForJurnalPiutang($transactionNumber) {
        $currentAction = $this->uri->segment(2, 0);
        $trNumber = $this->generateTransactionNumberPiutang();
        $type = $_POST["sub_transaction_type"];
        if($type == 144 || $type=145){
            $type = $_POST["sub_transaction_type"];
        }else{
            $type = 'LAIN';
        }
        $sysParamKey = 'PIUTANG-'.$type;
        $typTrx = $this->DAO_System_Parameter->getValue($sysParamKey);
        $trxDate = strtotime($_POST["trx_date"]);
        $trxDateFormat = date('Y-m-d', $trxDate);
        $numMonth = date("n", $trxDate);
        $numYear = date("Y", $trxDate);
        $postingProfile = $this->DAO_Posting_Profile->getPostingProfileByType($typTrx);
        $remark = 'Piutang '.$transactionNumber;
        $accountCode = null;
        if($postingProfile != null){
            $accountCode = $postingProfile['account_debet'];
        }
        $jmlTrx = $_POST["sisa_bayar"];


        $data = array(
            'transaction_number' => $trNumber,
            'transaction_date' => $trxDateFormat,
            'remark' => $remark,
            'transaction_type' => $typTrx,
            'financial_year' => $numYear,
            'financial_month' => $numMonth,
            'account_code' => $accountCode,
            'created_date'=> date('Y-m-d H:i:s', time())
        );
        if($_POST["student_id"]>0){
            $data['student_id'] = $_POST["student_id"];
        }
        $data['amount_debet'] = $jmlTrx;
        if(!empty($currentAction) && $currentAction=='insert'){
            $data['modified_date'] = date('Y-m-d H:i:s', time());
        }
        return $data;
    }*/

  private function validate()
  {
    $errMsg        = [];
    $currentAction = $this->uri->segment(2, 0);
    if (!empty($currentAction) && $currentAction == 'insert') {
      $trxTypeId = isset($_POST["sub_transaction_type"]) ? $_POST["sub_transaction_type"] : '';
      $this->setViewData(VDATAKEY_EDITOR_STATE, 'ADD');
      $postingProfile = $this->DAO_Posting_Profile->getPostingProfileByType($trxTypeId);
      if ($postingProfile == null) {
        $errMsg[] = 'Type transaksi belum memiliki posting profile';
      }

      /*
             * 	347 Pengembalian talangan
                352 Pengembalian uang pangkal
                353 pengembalian piutang akademik
                354 bon karyawan
                355 piutang lainnya
                358 piutang uang buku
                selain ini PDD
             */

      if ($trxTypeId == 318 || $trxTypeId == 319 || $trxTypeId == 320 || $trxTypeId == 321
          || $trxTypeId == 347 || $trxTypeId == 352 || $trxTypeId == 353 || $trxTypeId == 354
          || $trxTypeId == 355 || $trxTypeId == 358) {
        if (parseFloat($_POST["sisa_bayar"]) > 0) {
          $errMsg[] = 'Transaksi berikut tidak bole ada sisa!';
        }
      }

      $tahunAjaran = isset($_POST["tahun_ajaran"]) ? $_POST["tahun_ajaran"] : '';
      if ($trxTypeId == 145 || $trxTypeId == 353 || $trxTypeId == 150 || $trxTypeId == 358) {
        if ($tahunAjaran == '') {
          $errMsg[] = 'Tahun Ajaran Harus diisi!';
        }
      }

      /*
             * validasi di db local 326 dan 327 adalah pdd
             * if($trxTypeId == 326 || $trxTypeId == 327){
                if(parseFloat($_POST["sisa_bayar"]) > 0){
                    $errMsg[] = 'Pendapatan dibayar dimuka tidak bole ada sisa!';
                }
            }*/
    } else {
      $this->setViewData(VDATAKEY_EDITOR_STATE, 'EDIT');
    }
    if (count($errMsg) > 0) {
      return [false, $errMsg];
    } else {
      return [true, $errMsg];
    }
  }

  public function getMonth($year)
  {
    $monthArray = [];

    $monthArray[1]  = "Jan";
    $monthArray[2]  = "Feb";
    $monthArray[3]  = "Mar";
    $monthArray[4]  = "Apr";
    $monthArray[5]  = "Mei";
    $monthArray[6]  = "Jun";
    $monthArray[7]  = "Jul";
    $monthArray[8]  = "Ags";
    $monthArray[9]  = "Sep";
    $monthArray[10] = "Okt";
    $monthArray[11] = "Nop";
    $monthArray[12] = "Des";

    $renderMonthHtml = "";
    foreach ($monthArray as $monthVal => $month) {
      $renderMonthHtml .= '<div class="checkbox"><label><input type="checkbox" value="' . $monthVal . '"> ' . $month . ' </label></div>';
    }

    echo $renderMonthHtml;
  }


  private function generateTransactionNumber()
  {
    $year      = date('Y');
    $month     = date('m');
    $seqName   = sprintf("TRX-D-%s-%s", $year, $month);
    $seqRemark = sprintf("Sequence TRX Pendapatan tahun %s bulan %s", $year, $month);
    $seqNum    = $this->DAO_Sequence->getSequence($seqName, $seqRemark);
    $trxNo     = sprintf("TD%s-%s-%04d", $year, $month, $seqNum);
    return $trxNo;
  }

  private function generateTransactionNumberPiutang()
  {
    $year      = date('Y');
    $month     = date('m');
    $seqName   = sprintf("TRX-C-%s-%s", $year, $month);
    $seqRemark = sprintf("Sequence TRX Pembayaran tahun %s bulan %s", $year, $month);
    $seqNum    = $this->DAO_Sequence->getSequence($seqName, $seqRemark);
    $trxNo     = sprintf("TC%s-%s-%04d", $year, $month, $seqNum);
    return $trxNo;
  }

  public function get_sub_trx_type($parent, $subType)
  {
    $renderMonthHtml = "";
    if ($parent != null) {
      $subTrxType = $this->DAO_TipeTransaksi->getPembayaranTransaction('D', 2, $parent, $subType);
      if (!empty($subTrxType)) {
        foreach ($subTrxType as $subTypeVal => $subTypeView) {
          $renderMonthHtml .= '<option value="' . $subTypeVal . '">' . $subTypeView . '</option>';
        }
      }
    }
    echo $renderMonthHtml;
  }

  public function show_invoice($transactionId)
  {
    $this->loadCompanyInfo();
    $invoiceData = $this->generateInvoiceData($transactionId);
    $this->setViewData(VDATAKEY_INVOICE_DATA, $invoiceData);
    $this->setViewData(VDATAKEY_PAGE_TITLE, $this->getPageTitle());
    $this->loadPlainView('v_transaction_invoice');
  }

  public function generateInvoiceData($transactionId)
  {
    // Prep data
    $company        = $this->getCompanyInfo();
    $trx            = $this->DAO_Transaksi->getRowByCriteria('v_list_pendapatan_siswa', ['id' => $transactionId]);
    $trxType        = $this->DAO_Transaksi->getRowByCriteria('gen_md_trx_type', ['id' => $trx['transaction_type']]);
    $trxTypeParent1 = $this->DAO_Transaksi->getRowByCriteria('gen_md_trx_type', ['id' => $trxType['parent_type']]);
    $trxTypeParent2 = $this->DAO_Transaksi->getRowByCriteria('gen_md_trx_type', ['id' => $trxTypeParent1['parent_type']]);
    $target         = null;
    $targetData     = null;
    $targetIdNumber = null;
    if (empty($trx['employee_id']) && !empty($trx['student_id'])) {
      $targetData     = $this->DAO_Siswa->getRow($trx['student_id']);
      $target         = 'S';
      $targetIdNumber = $targetData['nis'];
    }
    if (!empty($trx['employee_id']) && empty($trx['student_id'])) {
      $targetData     = $this->DAO_Transaksi->getRowByCriteria('m_employee', ['id' => $trx['employee_id']]);
      $target         = 'E';
      $targetIdNumber = $targetData['id_number'];
    }

    // Create new invoice data object
    $data = new InvoiceHeader();
    $data->setCompanyAddress($company['COMPANY_ADDRESS']);
    $data->setCompanyPhone($company['COMPANY_PHONE']);
    $data->setCompanyEmail($company['COMPANY_EMAIL']);
    $data->setNumber($this->number_generator->generateInvoiceNumber());
    $data->setDate(date($this->appConfig['dateFormatForDisplay']));
    $data->setSub($trxTypeParent2['nama']);
    $data->setTrx($trxTypeParent1['nama']);
    $data->setTarget($target);
    $data->setTargetName($targetData['name']);
    $data->setTargetIdNumber($targetIdNumber);

    // Create invoice details
    $details = [];
    $detail  = new InvoiceDetail();
    $detail->setTrxNumber($trx['transaction_number']);
    $detail->setTrxType($trxType['nama']);
    $detail->setTrxAmount($trx['dibayar']);
    $detail->setTrxBalance($trx['sisa_bayar']);
    $detail->setRemark($trx['remark']);
    $details[] = $detail;
    $data->setDetails($details);

    return $data;
  }

  public function showAfterSaveMessage($invoiceUrl, $proceedUrl)
  {
    $this->setViewData(VDATAKEY_PAGE_TITLE, $this->getPageTitle());
    $this->setViewData(VDATAKEY_PAGE_ICON, $this->getPageIcon());
    $this->setViewData(VDATAKEY_INVOICE_URL, $invoiceUrl);
    $this->setViewData(VDATAKEY_ALERT_PROCEED_URL, $proceedUrl);
    $this->setViewData(VDATAKEY_ALERT_TYPE, ALERTTYPE_SUCCESS);
    $this->setViewData(VDATAKEY_ALERT_MESSAGE, "Data berhasil disimpan.");
    $this->loadPlainView('v_after_save_transaction');
  }

  public function getSisaHutangByStudentId($studentId, $transactionType)
  {
    $retval         = null;
    $dataSisaHutang = $this->DAO_Transaksi->getSisaHutangByStudentIdAndTransactionType($studentId, $transactionType);
    if ($dataSisaHutang != null) {
      $retval = $dataSisaHutang;
    }
    $sisaHutang = $dataSisaHutang[0]->sisa_hutang;
    echo $sisaHutang;
  }
}
