<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseReportController.php';
require_once 'IReportController.php';

/**
 * Controller laporan tunggakan pembayaran akademik
 *
 * @author <a href="mailto:agung3ka@gmail.com">Turah Eka</a>
 */
class Sia_rpt_tunggakan_uang_akademik extends BaseReportController implements IReportController
{

  const V_RPT_FORM = 'v_rpt_tunggakan_uang_akademik_form';

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('DAO_Siswa', 'DAO_Transaksi', 'DAO_Edu', 'DAO_Lov'));
  }

  function getPageTitle()
  {
    if ($this->isPostRequest()) {
      $year = $_POST['financial_year'];
      return 'Laporan Tunggakan Akademik Siswa Tahun ' . $year;
    } else {
      return 'Laporan Tunggakan Akademik Siswa';
    }
  }

  function getJsFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/js/ultracms_sia_rpt_tunggakan_uang_akademik.min.js');
    } else {
      return array();
    }
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaRptTransaksiTunggakanUangAkademik.init()';
    } else {
      return '';
    }
  }

  function getCssFiles()
  {
    return array();
  }

  public function index()
  {
    $this->setViewData('DAO_Siswa', $this->DAO_Siswa);
    $this->setViewData('DAO_Transaksi', $this->DAO_Transaksi);
    $this->setViewData('DAO_Edu', $this->DAO_Edu);
    $this->setViewData('DAO_Lov', $this->DAO_Lov);
    $this->loadReportForm(self::V_RPT_FORM);
  }

  public function generate_report()
  {
    $grade = isset($_POST['grade']) ? $_POST['grade'] : '';
    $classroom = isset($_POST['classroom']) ? $_POST['classroom'] : '';
    $year = isset($_POST['financial_year']) ? $_POST['financial_year'] : '';
    $validateResult = $this->validate();
    if ($validateResult[0] === true) {
      // Show report
      if ($classroom == 0 && $grade == 0) {
        $sql = 'SELECT s.nis as `NIS`, s.nama_siswa as `Nama Siswa`, s.grade_name AS `Jenjang`, s.class_name as `Tingkatan`, s.phone_number as `No. Telepon` FROM v_student_history_mutations s WHERE s.student_id NOT IN (SELECT v.student_id FROM v_exclude_tunggakan_uang_akademik v WHERE v.tahun_ajaran = ?) and s.year = ? order by s.grade_name, s.class_name, s.nis asc';
        $qry = $this->db->query($sql, array($year, $year));
      }
      if ($classroom != 0 && $grade == 0) {
        $sql = 'SELECT s.nis as `NIS`, s.nama_siswa as `Nama Siswa`, s.grade_name AS `Jenjang`, s.class_name as `Tingkatan`, s.phone_number as `No. Telepon` FROM v_student_history_mutations s WHERE s.student_id NOT IN (SELECT v.student_id FROM v_exclude_tunggakan_uang_akademik v WHERE v.tahun_ajaran = ?) and s.year = ? and s.class_id = ? order by s.grade_name, s.class_name, s.nis asc';
        $qry = $this->db->query($sql, array($year, $year, $classroom));
      }
      if ($classroom == 0 && $grade != 0) {
        $sql = 'SELECT s.nis as `NIS`, s.nama_siswa as `Nama Siswa`, s.grade_name AS `Jenjang`, s.class_name as `Tingkatan`, s.phone_number as `No. Telepon` FROM v_student_history_mutations s WHERE s.student_id NOT IN (SELECT v.student_id FROM v_exclude_tunggakan_uang_akademik v WHERE v.tahun_ajaran = ?) and s.year = ? and s.grade_id = ? order by s.grade_name, s.class_name, s.nis asc';
        $qry = $this->db->query($sql, array($year, $year, $grade));
      }
      if ($classroom != 0 && $grade != 0) {
        $sql = 'SELECT s.nis as `NIS`, s.nama_siswa as `Nama Siswa`, s.grade_name AS `Jenjang`, s.class_name as `Tingkatan`, s.phone_number as `No. Telepon` FROM v_student_history_mutations s WHERE s.student_id NOT IN (SELECT v.student_id FROM v_exclude_tunggakan_uang_akademik v WHERE v.tahun_ajaran = ?) and s.year = ? and s.grade_id = ? and s.class_id = ? order by s.grade_name, s.class_name, s.nis asc';
        $qry = $this->db->query($sql, array($year, $year, $grade, $classroom));
      }
      $this->setReportGenField(RPTGEN_HEADER, $this->getPageTitle());
      $this->setReportGenField(RPTGEN_MYSQL_RESOURCE, $qry);
      $this->setReportGenField(RPTGEN_DATE_FIELDS, array('Tanggal Transaksi' => $this->appConfig['dateFormatForDisplay']));
      $this->loadReport();
    } else {
      // show error & form
      $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
      $this->setViewData(VDATAKEY_POST_DATA, $_POST);
      $this->loadView(self::V_RPT_FORM);
    }
  }

  private function validate()
  {
    $errMsg = array();
    $year = isset($_POST['financial_year']) ? $_POST['financial_year'] : '';

    // Tahun
    if ($year == '') {
      $errMsg[] = "Tahun Ajaran Harus diisi!";
    }

    if (count($errMsg) > 0) {
      return array(false, $errMsg);
    } else {
      return array(true, $errMsg);
    }
  }

}
