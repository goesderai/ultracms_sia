<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseReportController.php';
require_once 'IReportController.php';

/**
 * Controller laporan Piutang
 *
 * @author <a href="mailto:agung3ka@gmail.com">Turah Eka</a>
 */
class Sia_rpt_piutang extends BaseReportController implements IReportController
{

  const V_RPT_FORM = 'v_rpt_piutang_form';

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('DAO_TipeTransaksi', 'DAO_Transaksi', 'DAO_Posting_Profile'));
  }

  function getPageTitle()
  {
    return 'Laporan Piutang';
  }

  function getJsFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/js/ultracms_sia_transaksi_piutang.min.js');
    } else {
      return array();
    }
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaRptPiutang.init()';
    } else {
      return '';
    }
  }

  function getCssFiles()
  {
    return array();
  }

  public function index()
  {
    $this->setViewData('DAO_TipeTransaksi', $this->DAO_TipeTransaksi);
    $this->setViewData('DAO_Transaksi', $this->DAO_Transaksi);
    $this->setViewData('DAO_Posting_Profile', $this->DAO_Posting_Profile);
    $this->loadReportForm(self::V_RPT_FORM);
  }

  public function generate_report()
  {
    $tipeTransaksi = isset($_POST['trx_type']) ? $_POST['trx_type'] : '';
    $financialYear = isset($_POST['financial_year']) ? $_POST['financial_year'] : '';
    $filters = array();

    if (!empty($tipeTransaksi)) {
      $postingProfile = $this->DAO_Posting_Profile->getPostingProfileByType($tipeTransaksi);

      $filters[] = array(QRYFILTER_FIELD => 'account_id', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $postingProfile['account_debet']);
    }
    $sql = null;
    if (!empty($financialYear)) {
      $filters[] = array(QRYFILTER_FIELD => 'financial_year', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $financialYear);
      $sql = $this->DAO_Transaksi->getRptPiutang($filters);
    } else {
      $sql = $this->DAO_Transaksi->getRptPiutangTanpaTahun($filters);
    }
    
    $qry = $this->db->query($sql);
    $this->setReportGenField(RPTGEN_HEADER, $this->getPageTitle());
    $this->setReportGenField(RPTGEN_MYSQL_RESOURCE, $qry);
    $this->setReportGenField(RPTGEN_DATE_FIELDS, array('Tanggal Transaksi' => $this->appConfig['dateFormatForDisplay']));
    $this->setReportGenField(RPTGEN_NUMBER_FIELDS, array("Piutang", "Dibayar", "Sisa"));
    $this->loadReport();
  }

}
