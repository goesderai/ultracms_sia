<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'IBasicController.php';

/**
 * Report controller interface
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
interface IReportController extends IBasicController
{
	function generate_report();
}
