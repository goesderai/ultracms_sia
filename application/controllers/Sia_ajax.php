<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseSessionAwareController.php';
require_once 'IBasicController.php';

/**
 * Controller for ajax request/response
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
class Sia_ajax extends BaseSessionAwareController implements IBasicController
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array("DAO_Edu", "DAO_Siswa"));
  }

  public function index()
  {
    show_404();
  }

  /**
   * Get classroom select options by grade id
   * @param integer $gradeId The grade id
   */
  public function classroom_select_options($gradeId)
  {
    if (empty($gradeId)) {
      echo '<option value="0" disabled="disabled" selected="selected">-- Pilih jenjang terlebih dahulu --</option>';
    } else {
      $classrooms = $this->DAO_Edu->getClassroomSelectOptions($gradeId);
      $selectOptions = "<option value=\"0\">-- Pilih --</option>";
      foreach ($classrooms as $id => $name) {
        $selectOptions .= "<option value=\"{$id}\">{$name}</option>";
      }
      echo $selectOptions;
    }
  }

  /**
   * Get language by its key
   * @param string $key The language key
   */
  public function get_lang($key)
  {
    echo lang($key);
  }

  public function student_lookup_dlg_list($singleResult = '0')
  {
    if ($this->isPostRequest()) {
      show_404();
    } else {
      $page = (isset($_GET[QSPARAM_PAGE]) && !empty($_GET[QSPARAM_PAGE]) && is_numeric($_GET[QSPARAM_PAGE]) && intval($_GET[QSPARAM_PAGE]) > 0) ? $_GET[QSPARAM_PAGE] : 1;
      $pageSize = (isset($_GET[QSPARAM_PAGE_SIZE]) && !empty($_GET[QSPARAM_PAGE_SIZE]) && is_numeric($_GET[QSPARAM_PAGE_SIZE]) && intval($_GET[QSPARAM_PAGE_SIZE]) > 0) ? $_GET[QSPARAM_PAGE_SIZE] : $this->appConfig['listPageSize'];
      $nis = $_GET['nis'];
      $nama = $_GET['nama'];
      $srcClassroomId = isset($_GET['srcClassroomId']) ? $_GET['srcClassroomId'] : 0;
      $destClassroomId = isset($_GET['destClassroomId']) ? $_GET['destClassroomId'] : 0;
      $excludeIds = isset($_GET['excludeIds']) ? $_GET['excludeIds'] : '';
      $firstRowPos = ($page - 1) * $pageSize;
      $filters = array();
      $filters[] = array(QRYFILTER_FIELD => 's.active', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => 'Y');
      $filters[] = array(QRYFILTER_FIELD => 's.nis', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => trim($nis));
      $filters[] = array(QRYFILTER_FIELD => 's.name', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => trim($nama));
      if (!empty($srcClassroomId)) {
        $filters[] = array(QRYFILTER_FIELD => 's.classroom_id', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => trim($srcClassroomId));
      }
      if (!empty($destClassroomId)) {
        $filters[] = array(QRYFILTER_FIELD => 's.classroom_id', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_NOT_EQUAL, QRYFILTER_VALUE => trim($destClassroomId));
      }
      if (!empty($excludeIds)) {
        $filters[] = array(QRYFILTER_FIELD => 's.id', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_NOT_IN, QRYFILTER_VALUE => trim($excludeIds));
      }
      $rs = $this->DAO_Siswa->getList($filters, null, $firstRowPos, $pageSize);
      $rsCount = $this->DAO_Siswa->getListCount($filters);
      $this->setViewData(VDATAKEY_ROWNUM_START, $firstRowPos);
      $this->setViewData(VDATAKEY_RECORDSET, $rs);
      if ($singleResult == '1') {
        $html = $this->load->view('sia_admin/chunks/single_result_student_lookup_dialog_grid', $this->viewData, true);
      } else {
        $html = $this->load->view('sia_admin/chunks/student_lookup_dialog_grid', $this->viewData, true);
      }

      $resp = array();
      $resp["page"] = $page;
      $resp["pages"] = ceil($rsCount / $pageSize);
      $resp["html"] = $html;
      echo json_encode($resp);
    }
  }

}
