<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseBackEndController.php';

/**
 * Sia_rpt_pengeluaran_bulanan.php
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
class Sia_rpt_pengeluaran_bulanan extends BaseBackEndController
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('DAO_Transaksi'));
  }

  function getPageTitle()
  {
    return 'Laporan Pengeluaran Bulanan';
  }

  function getPageIcon()
  {
    return 'fa-file';
  }

  function getJsFiles()
  {
    return array();
  }

  function getJsInitFunction()
  {
    return '';
  }

  function getCssFiles()
  {
    return array();
  }

  function index()
  {
    if ($this->isPostRequest()) {
      $year = isset($_POST['year'])? $_POST['year'] : '';
      if (!empty($year)) {
        $rs = $this->DAO_Transaksi->getRsRptPengeluaranBulanan($year);
        $this->loadCompanyInfo();
        $this->setViewData(VDATAKEY_PAGE_TITLE, $this->getPageTitle());
        $this->setViewData('year', $year);
        $this->setViewData('headerData', $rs['header']);
        $this->setViewData('bodyData', $rs['body']);
        $this->loadPlainView('v_rpt_pengeluaran_bulanan');
      }
    } else {
      $this->loadView('v_rpt_pengeluaran_bulanan_form');
    }
  }

}
