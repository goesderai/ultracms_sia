<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseSessionAwareController.php';

/**
 * Base back-end controller
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
abstract class BaseBackEndController extends BaseSessionAwareController
{

  public $sidebar_menu_generator; //Sidebar_menu_generator instance

  protected $page;
  protected $pageSize;
  protected $viewAfterSaveMessage = 'v_after_save';

  public function __construct()
  {
    parent::__construct();

    // Load required basic library, helper, etc
    $this->load->library(['Sidebar_menu_generator']);
    $this->load->helper(['ultracms']);

    // init class fields
    $this->setViewData(VDATAKEY_APP_NAME, $this->appConfig['appName']);
    $this->setViewData(VDATAKEY_APP_VERSION, $this->appConfig['appVersion']);
    $this->setViewData(VDATAKEY_APP_LANGUAGE, $this->appConfig['appLanguage']);
    $this->setViewData(VDATAKEY_SIDEBAR_MENU_GENERATOR, $this->sidebar_menu_generator);
    if (isset($_GET[QSPARAM_PAGE]) &&
        !empty($_GET[QSPARAM_PAGE]) &&
        is_numeric($_GET[QSPARAM_PAGE]) &&
        intval($_GET[QSPARAM_PAGE]) > 0) {
      $this->page = $_GET[QSPARAM_PAGE];
    } else {
      $this->page = 1;
    }
    if (isset($_GET[QSPARAM_PAGE_SIZE]) &&
        !empty($_GET[QSPARAM_PAGE_SIZE]) &&
        is_numeric($_GET[QSPARAM_PAGE_SIZE]) &&
        intval($_GET[QSPARAM_PAGE_SIZE]) > 0) {
      $this->pageSize = $_GET[QSPARAM_PAGE_SIZE];
    } else {
      $this->pageSize = $this->appConfig['listPageSize'];
    }
  }

  /**
   * Get page title
   * @return string
   */
  abstract function getPageTitle();

  /**
   * Get page icon (in font awesome format), eg: 'fa-users'
   * @return string
   */
  abstract function getPageIcon();

  /**
   * Get javascript file paths
   * @return array
   */
  abstract function getJsFiles();

  /**
   * Get js init function, eg: 'somePage.init()'
   * @return string
   */
  abstract function getJsInitFunction();

  /**
   * Get css file paths
   * @return array
   */
  abstract function getCssFiles();

  protected function getFirstRowPos()
  {
    $retval = ($this->page - 1) * $this->pageSize;
    return $retval;
  }

  /**
   * Get current date and time in mysql format (yyyy-mm-dd hh:mm:ss).
   *
   * @return string Current date & time in mysql format
   */
  protected function getCurrentDateTime()
  {
    return date($this->appConfig['dateTimeFormatForSaving'], time());
  }

  /**
   * Get current user ID
   *
   * @return integer Current user ID
   */
  protected function getCurrentUserId()
  {
    return $this->userData['id'];
  }

  /**
   * Get current record id
   *
   * @return mixed Record ID
   */
  protected function getRecId()
  {
    if (isset($_GET[QSPARAM_REC_ID]) && !empty($_GET[QSPARAM_REC_ID])) {
      return $_GET[QSPARAM_REC_ID];
    } else if (isset($_POST[QSPARAM_REC_ID]) && !empty($_POST[QSPARAM_REC_ID])) {
      return $_POST[QSPARAM_REC_ID];
    } else {
      return 0;
    }
  }

  /**
   * Show after save/update/delete message
   * @param String $message The message you want to show
   * @param String $proceedUrl (Optional) The url (relative path) to navigate when user clicks the 'Proceed' button. Default is empty.
   */
  protected function showSuccessMessage($message, $proceedUrl = '')
  {
    if (empty($message)) {
      return;
    } else {
      $this->setViewData(VDATAKEY_PAGE_TITLE, $this->getPageTitle());
      $this->setViewData(VDATAKEY_PAGE_ICON, $this->getPageIcon());
      $this->setViewData(VDATAKEY_ALERT_TYPE, ALERTTYPE_SUCCESS);
      $this->setViewData(VDATAKEY_ALERT_MESSAGE, $message);
      if (!empty($proceedUrl)) {
        $this->setViewData(VDATAKEY_ALERT_PROCEED_URL, site_url($proceedUrl));
      } else {
        $this->setViewData(VDATAKEY_ALERT_PROCEED_URL, $proceedUrl);
      }
      $this->loadPlainView($this->viewAfterSaveMessage);
    }
  }

  /**
   * View loader
   * @param String $view <br> The view name
   */
  protected function loadView($view)
  {
    $this->setViewData(VDATAKEY_PAGE_TITLE, $this->getPageTitle());
    $this->setViewData(VDATAKEY_PAGE_ICON, $this->getPageIcon());
    $this->setViewData(VDATAKEY_PAGE_CSS, $this->getCssFiles());
    $this->setViewData(VDATAKEY_PAGE_JS, $this->getJsFiles());
    $this->setViewData(VDATAKEY_PAGE_JS_INITFUNC, $this->getJsInitFunction());
    $this->load->view('sia_admin/scaffold_header', $this->viewData);
    $this->load->view('sia_admin/' . $view, $this->viewData);
    $this->load->view('sia_admin/scaffold_footer', $this->viewData);
  }

  /**
   * Loads company's info into view data
   */
  protected function loadCompanyInfo()
  {
    $sysParams = $this->DAO_System_Parameter->getParamGroup("COMPANY_INFO");
    foreach ($sysParams as $param) {
      $this->setViewData($param->key, $param->value);
    }
  }

  /**
   * Get company's info as array
   */
  protected function getCompanyInfo()
  {
    $sysParams = $this->DAO_System_Parameter->getParamGroup("COMPANY_INFO");
    $retval    = [];
    foreach ($sysParams as $param) {
      $retval[$param->key] = $param->value;
    }
    return $retval;
  }

  protected function isIndexAction()
  {
    return ($this->getControllerAction() === 'index');
  }

}
