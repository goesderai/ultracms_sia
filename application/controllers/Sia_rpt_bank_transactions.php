<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseReportController.php';
require_once 'IReportController.php';

/**
 * Controller laporan transaksi bank
 *
 * @author <a href="mailto:agung3ka@gmail.com">Turah Eka</a>
 */
class Sia_rpt_bank_transactions extends BaseReportController implements IReportController{

	public function __construct(){
		parent::__construct();
		$this->load->model(array('DAO_Transaksi', 'DAO_Main_Account'));
	}

  function getPageTitle()
  {
    return 'Laporan Transaksi Bank';
  }

  function getJsFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/themes/sb-admin/js/moment-with-locales.min.js',
        'assets/themes/sb-admin/js/bootstrap-datetimepicker.min.js',
        'assets/js/ultracms_sia_rpt_bank_transactions.min.js');
    } else {
      return array();
    }
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaRptBankTransactions.init()';
    } else {
      return array();
    }
  }

  function getCssFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/themes/sb-admin/css/bootstrap-datetimepicker.min.css');
    } else {
      return array();
    }
  }

  public function index()
  {
		$this->setViewData('DAO_Transaksi', $this->DAO_Transaksi);
		$this->setViewData('DAO_Main_Account', $this->DAO_Main_Account);
		$this->loadReportForm('v_rpt_bank_transactions_form');
	}
	
	public function generate_report(){
			$transaction_date_from = isset($_POST['transaction_date_from'])? $_POST['transaction_date_from'] : '';
			$transaction_date_to = isset($_POST['transaction_date_to'])? $_POST['transaction_date_to'] : '';
			$bankId = isset($_POST['account_code'])?$_POST['account_code']:'';
			$filters = array();
			if(!empty($transaction_date_from)){
				if(!empty($transaction_date_to)){
					$filters[] = array(QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_BETWEEN, QRYFILTER_VALUE => array($transaction_date_from, $transaction_date_to));
				}else{
					$filters[] = array(QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_BETWEEN, QRYFILTER_VALUE => array($transaction_date_from, $transaction_date_from));
				}
			}
			if(!empty($bankId)){
				$filters[] = array(QRYFILTER_FIELD => 'account_code', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $bankId);
			}
			$sql = $this->DAO_Transaksi->getRptBankTransactions($filters);
			
			$qry = $this->db->query($sql);
    $this->setReportGenField(RPTGEN_HEADER, $this->getPageTitle());
			$this->setReportGenField(RPTGEN_MYSQL_RESOURCE, $qry);
			$this->setReportGenField(RPTGEN_DATE_FIELDS, array('Tanggal Transaksi' => $this->appConfig['dateFormatForDisplay']));
			$this->setReportGenField(RPTGEN_NUMBER_FIELDS, array("Penyetoran", "Penarikan", "Sisa"));
			$this->loadReport();
	}
	
}
