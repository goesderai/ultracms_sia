<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseBackEndController.php';
require_once 'IBasicController.php';

/**
 * Students controller
 *
 * @author <a href="mailto:agung3ka@gmail.com">Eka D.</a>
 */
class Sia_posting_profile extends BaseBackEndController implements IBasicController
{
  var $recId;
  var $viewList = 'v_posting_profile_list';
  var $viewEditor = 'v_posting_profile_editor';

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('DAO_Posting_Profile', 'DAO_Main_Account', 'DAO_TipeTransaksi'));
    $this->lang->load('sia_posting_profile', $this->getCurrentLocale());
    $this->recId = $this->getRecId();
    $this->setViewData('DAO_Posting_Profile', $this->DAO_Posting_Profile);
    $this->setViewData('DAO_Main_Account', $this->DAO_Main_Account);
    $this->setViewData('DAO_TipeTransaksi', $this->DAO_TipeTransaksi);
  }

  function getPageTitle()
  {
    return 'Posting Profile';
  }

  function getPageIcon()
  {
    return 'fa-link';
  }

  function getJsFiles()
  {
    $js = array('assets/3rd-party/select2/js/select2.min.js');
    if ($this->isIndexAction()) {
      $js[] = 'assets/js/ultracms_sia_posting_profile_list.min.js';
    } else {
      $js[] = 'assets/js/ultracms_sia_posting_profile_editor.min.js';
    }
    return $js;
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaPostingProfileList.init()';
    } else {
      return 'ultracms.siaPostingProfileEditor.init()';
    }
  }

  function getCssFiles()
  {
    return array('assets/3rd-party/select2/css/select2.min.css');
  }

  public function index()
  {
    $filters = array();

    if (isset($_GET['account_debet']) && !empty($_GET['account_debet'])) {
      $filters[] = array(QRYFILTER_FIELD => 'account_debet', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $_GET['account_debet']);
    };
    if (isset($_GET['account_credit']) && !empty($_GET['account_credit'])) {
      $filters[] = array(QRYFILTER_FIELD => 'account_credit', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $_GET['account_credit']);
    };

    if (isset($_GET['trx_type']) && !empty($_GET['trx_type'])) {
      $filters[] = array(QRYFILTER_FIELD => 'trx_type', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $_GET['trx_type']);
    }

    $rs = $this->DAO_Posting_Profile->getList($filters, null, $this->getFirstRowPos(), $this->pageSize);
    $recordCount = $this->DAO_Posting_Profile->getListCount($filters);

    $this->setViewData(VDATAKEY_LIST_PAGE, $this->page);
    $this->setViewData(VDATAKEY_LIST_PAGE_SIZE, $this->pageSize);
    $this->setViewData(VDATAKEY_POST_DATA, $_GET);
    $this->setViewData(VDATAKEY_ROWNUM_START, $this->getFirstRowPos());
    $this->setViewData(VDATAKEY_RECORDSET, $rs);
    $this->setViewData(VDATAKEY_RECORDCOUNT, $recordCount);
    $this->loadView($this->viewList);
  }

  private function getFormData()
  {
    $data = array(
      'account_debet' => $_POST['account_debet'],
      'account_credit' => $_POST['account_credit'],
      'trx_type' => $_POST['trx_type']

    );

    return $data;
  }

  public function insert()
  {
    $this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
    $this->setViewData(VDATAKEY_EDITOR_STATE, 'ADD');
    if ($this->isPostRequest()) {
      $validateResult = $this->validate();
      if ($validateResult[0] === true) {
        // save
        $mainAccount = $this->getFormData();
        $this->DAO_Posting_Profile->insert($mainAccount);
        $this->setViewData(VDATAKEY_ALERT_TYPE, ALERTTYPE_SUCCESS);
        $this->setViewData(VDATAKEY_ALERT_MESSAGE, 'Data berhasil disimpan');
        $this->setViewData(VDATAKEY_ALERT_PROCEED_URL, site_url('sia_posting_profile/index'));
        $this->loadView($this->viewAfterSaveMessage);
      } else {
        // show error & form
        $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
        $this->setViewData(VDATAKEY_POST_DATA, $_POST);
        $this->loadView($this->viewEditor);
      }
    } else {
      $this->loadView($this->viewEditor);
    }
  }

  public function update()
  {
    $this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
    $this->setViewData(VDATAKEY_EDITOR_STATE, 'EDIT');
    if ($this->isPostRequest()) {
      $validateResult = $this->validate();
      if ($validateResult[0] === true) {
        // update
        $this->DAO_Posting_Profile->update($this->getFormData(), $this->recId);
        $this->setViewData(VDATAKEY_ALERT_TYPE, ALERTTYPE_SUCCESS);
        $this->setViewData(VDATAKEY_ALERT_MESSAGE, 'Data berhasil disimpan');
        $this->setViewData(VDATAKEY_ALERT_PROCEED_URL, site_url('sia_posting_profile/index'));
        $this->loadView($this->viewAfterSaveMessage);
      } else {
        // show error & form
        $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
        $this->setViewData(VDATAKEY_POST_DATA, $_POST);
        $this->loadView($this->viewEditor);
      }
    } else {
      $mainAccount = $this->DAO_Posting_Profile->getRow($this->recId);
      $this->setViewData(VDATAKEY_POST_DATA, $mainAccount);
      $this->loadView($this->viewEditor);
    }

  }

  private function validate()
  {
    $errMsg = array();
    $currentAction = $this->uri->segment(2, 0);
    if ((!empty($currentAction) && $currentAction == 'insert')) {
      if (isset($_POST['trx_type']) && !empty($_POST['trx_type'])) {
        $isTypeExis = $this->DAO_Posting_Profile->isTypeExis($_POST['trx_type']);
        if ($isTypeExis) {
          $errMsg[] = 'Type transaksi sudah memiliki posting profile';
        }
      } else {
        $errMsg[] = 'Type transaksi harus diisi';
      }
      if (count($errMsg) > 0) {
        return array(false, $errMsg);
      } else {
        return array(true, $errMsg);
      }
    } else {
      return array(true, $errMsg);
    }
  }
}
