<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseBackEndController.php';
require_once 'IBasicController.php';

/**
 * Pembayaran controller
 *
 * @author <a href="mailto:agung3ka@gmail.com">Turah Eka</a>
 */
class Sia_transaksi_pembayaran extends BaseBackEndController implements IBasicController
{

  var $recId;
  var $viewList = 'v_transaksi_pembayaran_list';
  var $viewEditor = 'v_transaksi_pembayaran_editor';

  public function __construct()
  {
    parent::__construct();

    $this->load->model(array('DAO_Security', 'DAO_Employee', 'DAO_Transaksi', 'DAO_Siswa', 'DAO_Lov', 'DAO_Sequence', 'DAO_TipeTransaksi', 'DAO_Posting_Profile'));
    $this->recId = $this->getRecId();
    $this->setViewData('DAO_Siswa', $this->DAO_Siswa);
    $this->setViewData('DAO_TipeTransaksi', $this->DAO_TipeTransaksi);
    $this->setViewData('DAO_Lov', $this->DAO_Lov);
    $this->setViewData('DAO_Posting_Profile', $this->DAO_Posting_Profile);
    $this->setViewData('DAO_Employee', $this->DAO_Employee);
    $this->setViewData('DAO_Security', $this->DAO_Security);
  }

  function getPageTitle()
  {
    return 'Transaksi Pengeluaran';
  }

  function getJsFiles()
  {
    $js = array('assets/themes/sb-admin/js/moment-with-locales.min.js',
      'assets/themes/sb-admin/js/bootstrap-datetimepicker.min.js',);
    if ($this->isIndexAction()) {
      $js[] = 'assets/js/ultracms_sia_transaksi_pembayaran_list.min.js';
    } else {
      $js[] = 'assets/themes/sb-admin/js/jquery.number.min.js';
      $js[] = 'assets/js/ultracms_sia_transaksi_pembayaran_editor.min.js';
    }
    return $js;
  }

  function getCssFiles()
  {
    return array('assets/themes/sb-admin/css/bootstrap-datetimepicker.min.css');
  }

  function getPageIcon()
  {
    return 'fa-usd';
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaTransaksiPembayaranList.init()';
    } else {
      return 'ultracms.siaTransaksiPembayaranEditor.init()';
    }
  }

  public function index()
  {
    $nomor = isset($_GET['nomor']) ? $_GET['nomor'] : '';
    $nama = isset($_GET['nama']) ? $_GET['nama'] : '';
    $remark = isset($_GET['remark']) ? $_GET['remark'] : '';
    $transaction_date_from = isset($_GET['transaction_date_from']) ? $_GET['transaction_date_from'] : '';
    $transaction_date_to = isset($_GET['transaction_date_to']) ? $_GET['transaction_date_to'] : '';
    $filters = array(
      array(QRYFILTER_FIELD => 'transaction_number', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $nomor),
      array(QRYFILTER_FIELD => 'nama_tr', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $nama),
      array(QRYFILTER_FIELD => 'remark', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $remark),
      array(QRYFILTER_FIELD => 'ref_transaction_id', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_ISNULL)
    );

    if (!empty($transaction_date_from)) {
      if (!empty($transaction_date_to)) {
        $filters[] = array(QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_BETWEEN, QRYFILTER_VALUE => array($transaction_date_from, $transaction_date_to));
      } else {
        $filters[] = array(QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_MORE_THAN_OR_EQUAL_TO, QRYFILTER_VALUE => $transaction_date_from);
      }
    }


    $rs = $this->DAO_Transaksi->getListTransaksiPembayaran($filters, null, $this->getFirstRowPos(), $this->pageSize);
    $recordCount = $this->DAO_Transaksi->getListCountPembayaran($filters);
    $this->setViewData(VDATAKEY_LIST_PAGE, $this->page);
    $this->setViewData(VDATAKEY_LIST_PAGE_SIZE, $this->pageSize);
    $this->setViewData(VDATAKEY_POST_DATA, $_GET);
    $this->setViewData(VDATAKEY_ROWNUM_START, $this->getFirstRowPos());
    $this->setViewData(VDATAKEY_RECORDSET, $rs);
    $this->setViewData(VDATAKEY_RECORDCOUNT, $recordCount);
    $this->loadView($this->viewList);
  }

  public function insert()
  {
    $this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
    $this->setViewData(VDATAKEY_EDITOR_STATE, $this->getControllerAction());
    if ($this->isPostRequest()) {
      $validateResult = $this->validate();
      if ($validateResult[0] === true) {
        // save
        $pembayaran = $this->getFormData();
        $pembayaran['transaction_number'] = $this->generateTransactionNumber();
        $pembayaran['created_by'] = $this->userData['id'];
        $this->DAO_Transaksi->insert($pembayaran);
        $this->showSuccessMessage('Data berhasil disimpan', 'sia_transaksi_pembayaran/index');
      } else {
        // show error & form
        $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
        $this->setViewData(VDATAKEY_POST_DATA, $_POST);
        $this->loadView($this->viewEditor);
      }
    } else {
      $this->setViewData('userData', $this->userData);
      $this->loadView($this->viewEditor);
    }
  }

  public function update()
  {
    $this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
    $this->setViewData(VDATAKEY_EDITOR_STATE, $this->getControllerAction());
    if ($this->isPostRequest()) {
      $validateResult = $this->validate();
      if ($validateResult[0] === true) {
        // update
        $pembayaran = $this->getFormData();
        $pembayaran['modified_by'] = $this->userData['id'];
        $this->DAO_Transaksi->update($pembayaran, $this->recId);
        $this->showSuccessMessage('Data berhasil disimpan', 'sia_transaksi_pembayaran/index');
      } else {
        // show error & form
        $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
        $this->setViewData(VDATAKEY_POST_DATA, $_POST);
        $this->loadView($this->viewEditor);
      }
    } else {
      $transaksi = $this->DAO_Transaksi->getRow($this->recId);
      $this->setViewData(VDATAKEY_POST_DATA, $transaksi);
      $this->loadView($this->viewEditor);
    }
  }

  public function koreksi()
  {
    $this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
    $this->setViewData(VDATAKEY_EDITOR_STATE, $this->getControllerAction());
    if ($this->isPostRequest()) {
      $validateResult = $this->validate();
      if ($validateResult[0] === true) {
        $newTransactionNumber = $this->generateTransactionNumber();
        $this->DAO_Transaksi->insertJurnalKoreksi($_POST['transaction_number'], $newTransactionNumber);

        $this->setViewData(VDATAKEY_ALERT_TYPE, ALERTTYPE_SUCCESS);
        $this->setViewData(VDATAKEY_ALERT_MESSAGE, 'Data berhasil disimpan');
        $this->setViewData(VDATAKEY_ALERT_PROCEED_URL, site_url('sia_transaksi_pembayaran/index'));
        $this->loadView($this->viewAfterSaveMessage);
      } else {
        // show error & form
        $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
        $this->setViewData(VDATAKEY_POST_DATA, $_POST);
        $this->loadView($this->viewEditor);
      }
    } else {
      $transaksi = $this->DAO_Transaksi->getRow($this->recId);
      $this->setViewData(VDATAKEY_POST_DATA, $transaksi);
      $this->loadView($this->viewEditor);
    }
  }

  private function getFormData()
  {
    $currentAction = $this->uri->segment(2, 0);
    $trxDate = strtotime($_POST["trx_date"]);
    $trxDateFormat = date('Y-m-d', $trxDate);
    $numMonth = date("n", $trxDate);
    $numYear = date("Y", $trxDate);

    $transaction_type = isset($_POST['transaction_type']) ? $_POST['transaction_type'] : 0;
    $sub_transaction_type = isset($_POST['sub_transaction_type']) ? $_POST['sub_transaction_type'] : $_POST['jenis_transaction'];
    $trxTypeSave = "";
    if ($sub_transaction_type == 0) {
      $trxTypeSave = $transaction_type;
    } else {
      $trxTypeSave = $sub_transaction_type;
    }
    $postingProfile = $this->DAO_Posting_Profile->getPostingProfileByType($trxTypeSave);
    $accountCode = null;
    if ($postingProfile != null) {
      $accountCode = $postingProfile['account_debet'];
    }
    $data = array(
      'transaction_date' => $trxDateFormat,
      'remark' => $_POST["remark"],
      'transaction_type' => $trxTypeSave,
      'financial_year' => $numYear,
      'financial_month' => $numMonth,
      'account_code' => $accountCode,
      'created_date' => date('Y-m-d H:i:s', time()),
      'amount_debet' => parseFloat($_POST["debet_kredit"])
    );
    if ($_POST["employee_id"] > 0) {
      $data['employee_id'] = $_POST["employee_id"];
    } else {
      $data['employee_id'] = null;
    }
    if (!empty($currentAction) && $currentAction == 'insert') {
      $data['modified_date'] = date('Y-m-d H:i:s', time());
    }
    return $data;
  }

  private function validate()
  {
    $errMsg = array();
    $currentAction = $this->uri->segment(2, 0);
    $transaction_type = isset($_POST['transaction_type']) ? $_POST['transaction_type'] : 0;
    $sub_transaction_type = isset($_POST['sub_transaction_type']) ? $_POST['sub_transaction_type'] : $_POST['jenis_transaction'];
    $trxTypeSave = "";
    if ($sub_transaction_type == 0) {
      $trxTypeSave = $transaction_type;
    } else {
      $trxTypeSave = $sub_transaction_type;
    }

    if (!empty($currentAction) && $currentAction == 'insert') {
      $this->setViewData(VDATAKEY_EDITOR_STATE, 'ADD');
      $postingProfile = $this->DAO_Posting_Profile->getPostingProfileByType($trxTypeSave);
      if ($postingProfile == null) {
        $errMsg[] = 'Type transaksi belum memiliki posting profile';
      }
    } else {
      $this->setViewData(VDATAKEY_EDITOR_STATE, 'EDIT');
    }
    if (count($errMsg) > 0) {
      return array(false, $errMsg);
    } else {
      return array(true, $errMsg);
    }
  }

  public function getMonth($year)
  {
    $monthArray = array();

    $monthArray[1] = "Jan";
    $monthArray[2] = "Feb";
    $monthArray[3] = "Mar";
    $monthArray[4] = "Apr";
    $monthArray[5] = "Mei";
    $monthArray[6] = "Jun";
    $monthArray[7] = "Jul";
    $monthArray[8] = "Ags";
    $monthArray[9] = "Sep";
    $monthArray[10] = "Okt";
    $monthArray[11] = "Nop";
    $monthArray[12] = "Des";

    $renderMonthHtml = "";
    foreach ($monthArray as $monthVal => $month) {
      $renderMonthHtml .= '<div class="checkbox"><label><input type="checkbox" value="' . $monthVal . '"> ' . $month . ' </label></div>';
    }
    echo $renderMonthHtml;
  }

  private function generateTransactionNumber()
  {
    $year = date('Y');
    $month = date('m');
    $seqName = sprintf("TRX-C-%s-%s", $year, $month);
    $seqRemark = sprintf("Sequence TRX Pembayaran tahun %s bulan %s", $year, $month);
    $seqNum = $this->DAO_Sequence->getSequence($seqName, $seqRemark);
    $trxNo = sprintf("TC%s-%s-%04d", $year, $month, $seqNum);
    return $trxNo;
  }

  public function get_sub_trx_type($parent, $subType)
  {
    $renderMonthHtml = '<option value="0">-- Pilih --</option>';
    if ($parent != null) {
      $subTrxType = $this->DAO_TipeTransaksi->getPembayaranTransaction('C', 4, $parent, $subType);
      if (!empty($subTrxType)) {
        foreach ($subTrxType as $subTypeVal => $subTypeView) {
          $renderMonthHtml .= '<option value="' . $subTypeVal . '">' . $subTypeView . '</option>';
        }
      }
    }
    echo $renderMonthHtml;
  }
}
