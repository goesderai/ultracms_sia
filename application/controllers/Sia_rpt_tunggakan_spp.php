<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseReportController.php';
require_once 'IReportController.php';

/**
 * Controller laporan tunggakan pembayaran SPP
 *
 * @author <a href="mailto:agung3ka@gmail.com">Turah Eka</a>
 */
class Sia_rpt_tunggakan_spp extends BaseReportController implements IReportController
{

  const V_RPT_FORM = 'v_rpt_tunggakan_spp_form';

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('DAO_Siswa', 'DAO_Transaksi_Spp'));
  }

  function getPageTitle()
  {
    if ($this->isPostRequest()) {
      $month = isset($_POST['financial_month']) ? $_POST['financial_month'] : '';
      $year = isset($_POST['financial_year']) ? $_POST['financial_year'] : '';
      $monthNum = $month;
      $dateObj = DateTime::createFromFormat('!m', $monthNum);
      $monthName = $dateObj->format('F');
      return 'Laporan Tunggakan SPP Bulan ' . $monthName . ' Tahun ' . $year;
    } else {
      return 'Laporan Tunggakan SPP';
    }
  }

  function getJsFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/js/ultracms_sia_rpt_tunggakan_spp.min.js');
    } else {
      return array();
    }
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaRptTransaksiTunggakanSpp.init()';
    } else {
      return '';
    }
  }

  function getCssFiles()
  {
    return array();
  }

  public function index()
  {
    $this->setViewData('DAO_Siswa', $this->DAO_Siswa);
    $this->setViewData('DAO_Transaksi_Spp', $this->DAO_Transaksi_Spp);
    $this->loadReportForm(self::V_RPT_FORM);
  }

  public function generate_report()
  {
    $month = isset($_POST['financial_month']) ? $_POST['financial_month'] : '';
    $year = isset($_POST['financial_year']) ? $_POST['financial_year'] : '';

    $filters = array();

    if (!empty($year)) {
      $filters[] = array(QRYFILTER_FIELD => 'financial_year', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $year);
    }

    if (!empty($month)) {
      $filters[] = array(QRYFILTER_FIELD => 'financial_month', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $month);
    }
    $validateResult = $this->validate();
    if ($validateResult[0] === true) {
      // Show report
      $sql = $this->DAO_Transaksi_Spp->getRptTunggakanSppQuery($month, $year);
      $qry = $this->db->query($sql);
      $this->setReportGenField(RPTGEN_HEADER, $this->getPageTitle());
      $this->setReportGenField(RPTGEN_MYSQL_RESOURCE, $qry);
      $this->setReportGenField(RPTGEN_DATE_FIELDS, array('Tanggal Transaksi' => $this->appConfig['dateFormatForDisplay']));
      $this->loadReport();
    } else {
      // show error & form
      $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
      $this->setViewData(VDATAKEY_POST_DATA, $_POST);
      $this->loadView(self::V_RPT_FORM);
    }
  }

  private function validate()
  {
    $errMsg = array();
    $month = isset($_POST['financial_month']) ? $_POST['financial_month'] : '';
    $year = isset($_POST['financial_year']) ? $_POST['financial_year'] : '';

    // Bulan
    if ($month == '') {
      $errMsg[] = "Bulan Harus Diisi!";
    }

    // Tahun
    if ($year == '') {
      $errMsg[] = "Tahun Harus diisi!";
    }

    if (count($errMsg) > 0) {
      return array(false, $errMsg);
    } else {
      return array(true, $errMsg);
    }
  }

}
