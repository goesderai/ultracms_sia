<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseReportController.php';
require_once 'IReportController.php';

/**
 * Controller laporan tunggakan pembayaran SPP
 *
 * @author <a href="mailto:agung3ka@gmail.com">Turah Eka</a>
 */
class Sia_rpt_kas extends BaseReportController implements IReportController
{

  const V_RPT_FORM = 'v_rpt_kas_form';
  const V_RPT_KAS = 'v_kas_report';

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('DAO_Transaksi'));
  }

  function getPageTitle()
  {
    return 'Laporan Kas Harian';
  }

  function getJsFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/themes/sb-admin/js/moment-with-locales.min.js',
        'assets/themes/sb-admin/js/bootstrap-datetimepicker.min.js',
        'assets/js/ultracms_sia_rpt_kas.min.js');
    } else {
      return array();
    }
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaRptKas.init()';
    } else {
      return '';
    }
  }

  function getCssFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/themes/sb-admin/css/bootstrap-datetimepicker.min.css');
    } else {
      return array();
    }
  }

  public function index()
  {
    $this->setViewData('DAO_Transaksi', $this->DAO_Transaksi);
    $this->loadReportForm(self::V_RPT_FORM);
  }

  public function generate_report()
  {
    $tanggal = isset($_POST['transaction_date']) ? $_POST['transaction_date'] : '';
    $filters = array();
    $filtersKasAwal = array();
    $filtersKasAkhir = array();

    if (!empty($tanggal)) {
      $filters[] = array(QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $tanggal);
      $filtersKasAwal[] = array(QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_SMALLER, QRYFILTER_VALUE => $tanggal);
      $filtersKasAkhir[] = array(QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_IS_LESS_THAN_OR_EQUAL_TO, QRYFILTER_VALUE => $tanggal);
    }

    $validateResult = $this->validate();
    if ($validateResult[0] === true) {
      // Show report
      $this->setViewData(VDATAKEY_PAGE_TITLE, $this->getPageTitle());
      $this->setViewData('DAO_Transaksi', $this->DAO_Transaksi);
      $this->setViewData('filters', $filters);
      $this->setViewData('tanggal', $tanggal);
      $this->setViewData('ket', 'Tanggal ');
      $this->setViewData('filtersKasAwal', $filtersKasAwal);
      $this->setViewData('filtersKasAkhir', $filtersKasAkhir);
      $this->loadCompanyInfo();
      $this->loadPlainView(self::V_RPT_KAS);
    } else {
      // show error & form
      $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
      $this->setViewData(VDATAKEY_POST_DATA, $_POST);
      $this->loadPlainView(self::V_RPT_FORM);
    }
  }

  private function validate()
  {
    $errMsg = array();

    if (count($errMsg) > 0) {
      return array(false, $errMsg);
    } else {
      return array(true, $errMsg);
    }
  }

}
