<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseReportController.php';
require_once 'IReportController.php';

/**
 * Controller laporan pembayaran
 *
 * @author <a href="mailto:agung3ka@gmail.com">Turah Eka</a>
 */
class Sia_rpt_main_account_transaksi extends BaseReportController implements IReportController
{

  const V_RPT = 'v_main_account_trx_report';
  const V_RPT_FORM = 'v_rpt_main_account_trx_form';

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('DAO_Main_Account'));
  }

  function getPageTitle()
  {
    return 'Laporan Transaksi Main Account';
  }

  function getJsFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/themes/sb-admin/js/moment-with-locales.min.js',
        'assets/themes/sb-admin/js/bootstrap-datetimepicker.min.js',
        'assets/js/ultracms_sia_rpt_main_account_trx.min.js');
    } else {
      return array();
    }
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaRptMainAccountTrx.init()';
    } else {
      return '';
    }
  }

  function getCssFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/themes/sb-admin/css/bootstrap-datetimepicker.min.css');
    } else {
      return array();
    }
  }

  public function index()
  {
    $this->setViewData('DAO_Main_Account', $this->DAO_Main_Account);
    $this->loadReportForm(self::V_RPT_FORM);
  }

  public function generate_report()
  {
    $transaction_date_from = isset($_POST['transaction_date_from']) ? $_POST['transaction_date_from'] : '';
    $transaction_date_to = isset($_POST['transaction_date_to']) ? $_POST['transaction_date_to'] : '';

    $validateResult = $this->validate();
    if ($validateResult[0] === true) {
      // Show report
      $this->setViewData(VDATAKEY_PAGE_TITLE, $this->getPageTitle());
      $this->setViewData('DAO_Main_Account', $this->DAO_Main_Account);
      $this->setViewData('periode', 'Periode ');
      $this->setViewData('ket', 'Tanggal ');
      $this->setViewData('dateFrom', $transaction_date_from);
      $this->setViewData('dateTo', $transaction_date_to);
      $this->loadCompanyInfo();
      $this->loadPlainView(self::V_RPT);
    } else {
      // show error & form
      $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
      $this->setViewData(VDATAKEY_POST_DATA, $_POST);
      $this->loadView(self::V_RPT_FORM);
    }
  }

  private function validate()
  {
    $errMsg = array();

    if (count($errMsg) > 0) {
      return array(false, $errMsg);
    } else {
      return array(true, $errMsg);
    }
  }

}
