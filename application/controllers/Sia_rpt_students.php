<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseReportController.php';
require_once 'IReportController.php';

/**
 * Controller laporan siswa
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
class Sia_rpt_students extends BaseReportController implements IReportController
{

  var $reportTypes = array('01' => 'Data Semua Siswa', '02' => 'Data Siswa Berdasarkan Parameter');

  public function __construct()
  {
    parent::__construct();
    $this->lang->load('sia_students', $this->getCurrentLocale());
  }

  function getPageTitle()
  {
    $title = 'Laporan Siswa';
    if ($this->isPostRequest()) {
      return sprintf('%s - %s', $title, $this->reportTypes[$_POST['reportType']]);
    } else {
      return $title;
    }
  }

  function getJsFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/themes/sb-admin/js/moment-with-locales.min.js',
        'assets/themes/sb-admin/js/bootstrap-datetimepicker.min.js',
        '/assets/js/ultracms_sia_rpt_student.min.js');
    } else {
      return array();
    }
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaRptStudent.init()';
    } else {
      return '';
    }
  }

  function getCssFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/themes/sb-admin/css/bootstrap-datetimepicker.min.css');
    } else {
      return array();
    }
  }

  public function index()
  {
    $this->setViewData("reportTypes", $this->reportTypes);
    $this->loadReportForm('v_rpt_students_form');
  }

  /**
   * Display report filter by report type
   * @param String $reportType The report type
   * @category Ajax response
   */
  public function reports($reportType)
  {
    if (is_null($reportType) || empty($reportType)) {
      echo "";
    } else {
      switch ($reportType) {
        case '02':
          $this->load->model(array('DAO_Lov', 'DAO_Agama', 'DAO_Edu'));
          $this->setViewData('DAO_Lov', $this->DAO_Lov);
          $this->setViewData('DAO_Agama', $this->DAO_Agama);
          $this->setViewData('DAO_Edu', $this->DAO_Edu);
          $filters = $this->load->view('sia_admin/includes/reports/v_rpt_students_form_02', $this->viewData, true);
          echo $filters;
          break;
        default:
          echo "";
      }
    }
  }

  public function generate_report()
  {
    if ($this->isPostRequest()) {
      $reportType = isset($_POST['reportType']) ? $_POST['reportType'] : '';
      if (!empty($reportType)) {
        $dateFormat = $this->appConfig['dateFormatForDisplay'];
        $selectFields = "`nis` AS `NIS`,
                         `name` AS `Nama`,
                         `birth_place` AS `Tempat Lahir`,
                         `birth_date` AS `Tanggal Lahir`,
                         `gender_str` AS `Jenis Kelamin`,
                         `religion_name` AS `Agama`,
                         `accepted_date` AS `Tanggal Diterima`,
                         `grade_name` AS `Jenjang`,
                         `classroom_name` AS `Tingkatan`";
        $this->setReportGenField(RPTGEN_HEADER, $this->getPageTitle());
        $this->setReportGenField(RPTGEN_DATE_FIELDS, array('Tanggal Lahir' => $dateFormat, 'Tanggal Diterima' => $dateFormat));
        $this->setReportGenField(RPTGEN_SHOW_NUMBER_COLUMN, false);

        switch ($reportType) {
          case '01':
            // All
            $sql = "select {$selectFields} from `v_rpt_all_students` where `active` = 'Y' order by `name`";
            $qry = $this->db->query($sql);
            $this->setReportGenField(RPTGEN_MYSQL_RESOURCE, $qry);
            break;

          case '02':
            // By parameters
            $birth_date_from = isset($_POST['birth_date_from']) ? $_POST['birth_date_from'] : null;
            $birth_date_to = isset($_POST['birth_date_to']) ? $_POST['birth_date_to'] : null;
            $gender = isset($_POST['gender']) ? $_POST['gender'] : null;
            $religion_id = isset($_POST['religion_id']) ? $_POST['religion_id'] : null;
            $grade = isset($_POST['grade']) ? $_POST['grade'] : null;
            $classroom = isset($_POST['classroom']) ? $_POST['classroom'] : null;
            $status = isset($_POST['status']) ? $_POST['status'] : 2;

            // Build filters
            $filters = array();
            if (!empty($birth_date_from) && !empty($birth_date_to)) {
              $filters[] = array(QRYFILTER_FIELD => 'birth_date',
                QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_BETWEEN,
                QRYFILTER_VALUE => array($birth_date_from, $birth_date_to));
            }
            if (!empty($gender)) {
              $filters[] = array(QRYFILTER_FIELD => 'gender',
                QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL,
                QRYFILTER_VALUE => $gender);
            }
            if (!empty($religion_id)) {
              $filters[] = array(QRYFILTER_FIELD => 'religion_id',
                QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL,
                QRYFILTER_VALUE => $religion_id);
            }
            if (!empty($grade)) {
              $filters[] = array(QRYFILTER_FIELD => 'grade_id',
                QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL,
                QRYFILTER_VALUE => $grade);
            }
            if (!empty($classroom)) {
              $filters[] = array(QRYFILTER_FIELD => 'classroom_id',
                QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL,
                QRYFILTER_VALUE => $classroom);
            }
            $filters[] = array(QRYFILTER_FIELD => 'active',
              QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL,
              QRYFILTER_VALUE => 'Y');
            $filters[] = array(QRYFILTER_FIELD => 'status',
              QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL,
              QRYFILTER_VALUE => $status);

            // Execute query
            $this->load->model('BaseDAO');
            $sql = "select {$selectFields} from `v_rpt_all_students` s where 1=1 " .
              $this->BaseDAO->buildWhereSql('s', $filters) .
              " order by s.name";
            $qry = $this->db->query($sql);
            $this->setReportGenField(RPTGEN_MYSQL_RESOURCE, $qry);
            break;
        }
        $this->loadReport();
      } else {
        show_404();
      }
    } else {
      show_404();
    }
  }

}
