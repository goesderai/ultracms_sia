<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseBackEndController.php';
require_once 'IBasicController.php';

/**
 * Change password controller
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
class Sia_change_password extends BaseBackEndController implements IBasicController
{
	
	var $formChangePassword = 'v_change_password';
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array('DAO_Security'));
  }

  function getPageTitle()
  {
    return 'Change Password';
  }

  function getPageIcon()
  {
    return 'fa-gear';
  }

  function getJsFiles()
  {
    return array('assets/js/ultracms_sia_change_pass.min.js');
  }

  function getJsInitFunction()
  {
    return 'ultracms.siaChangePass.init()';
  }

  function getCssFiles()
  {
    return array();
  }

  public function index()
  {
		$this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
		if($this->isPostRequest()){
			$validateResult = $this->validate();
			if($validateResult[0] === true){
				$data = $this->getFormData();
				$this->DAO_Security->changePassword($this->userData['id'], $data['newPass']);
				$this->showSuccessMessage('Data berhasil disimpan, password baru akan berlaku pada saat login berikutnya.');
			}else{
				$this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
				$this->loadView($this->formChangePassword);
			}
		}else{
			$this->loadView($this->formChangePassword);
		}
	}
	
	private function getFormData(){
		$data = array(
			'pass' => $_POST['pass'],
			'newPass' => $_POST['newPass'],
			'confirmNewPass' => $_POST['confirmNewPass']
		);
		return $data;
	}
	
	private function validate(){
		$errMsg = array();
		
		// Validate
		$data = $this->getFormData();
		if(empty($data['pass'])){
			$errMsg[] = "Password saat ini harus diisi";
		}
		if(empty($data['newPass'])){
			$errMsg[] = "Password baru harus diisi";
		}
		if(empty($data['confirmNewPass'])){
			$errMsg[] = "Konfirmasi password baru harus diisi";
		}
		if(!empty($data['pass']) && (md5($data['pass']) != $this->userData['password'])){
			$errMsg[] = "Password saat ini salah";
		}
		if((!empty($data['newPass']) && !empty($data['confirmNewPass'])) && ($data['newPass'] != $data['confirmNewPass'])){
			$errMsg[] = "Konfirmasi password baru salah";
		}
		
		if(!empty($errMsg)){
			return array(false, $errMsg);
		}else{
			return array(true, $errMsg);
		}
	}
	
}
