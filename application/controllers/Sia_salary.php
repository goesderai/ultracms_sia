<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseBackEndController.php';
require_once 'IBasicController.php';

/**
 * Employees controller
 *
 * @author <a href="mailto:agung3ka@gmail.com">Eka D.</a>
 */
class Sia_salary extends BaseBackEndController implements IBasicController
{
	
	var $recId;
	var $viewList = 'v_salary_list';
	var $viewEditor = 'v_salary_editor';
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array('DAO_Salary','DAO_Employee'));
		$this->recId = $this->getRecId();
		$this->setViewData('DAO_Employee', $this->DAO_Employee);
		$this->lang->load(array('sia_employees'), $this->getCurrentLocale());
	}

  function getPageTitle()
  {
    return 'Gaji Karyawan';
  }

  function getPageIcon()
  {
    return 'fa-users';
  }

  function getJsFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/js/ultracms_sia_salary_list.min.js');
    } else {
      return array('assets/themes/sb-admin/js/moment-with-locales.min.js',
        'assets/themes/sb-admin/js/bootstrap-datetimepicker.min.js',
        'assets/js/ultracms_sia_salary_editor.min.js');
    }
  }

  function getCssFiles()
  {
    if ($this->isIndexAction()) {
      return array();
    } else {
      return array('assets/themes/sb-admin/css/bootstrap-datetimepicker.min.css');
    }
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaSalaryList.init()';
    } else {
      return 'ultracms.siaSalaryEditor.init()';
    }
  }

  public function index()
  {
		$employee_id = isset($_GET['employee_id'])? $_GET['employee_id'] : '';
		$gapok = isset($_GET['gaji_pokok'])? $_GET['gaji_pokok'] : '';
		$filters = array(
			array(QRYFILTER_FIELD => 'active', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => 'Y'),
			array(QRYFILTER_FIELD => 'gaji_pokok', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $gapok)
		);
		if(!empty($employee_id) || $employee_id != 0){
				$filters[] = array(QRYFILTER_FIELD => 'employee_id', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $employee_id);
			}
		$rs = $this->DAO_Salary->getList($filters, null, $this->getFirstRowPos(), $this->pageSize);
		$recordCount = $this->DAO_Salary->getListCount($filters);
		
		$this->setViewData(VDATAKEY_LIST_PAGE, $this->page);
		$this->setViewData(VDATAKEY_LIST_PAGE_SIZE, $this->pageSize);
		$this->setViewData(VDATAKEY_POST_DATA, $_GET);
		$this->setViewData(VDATAKEY_ROWNUM_START, $this->getFirstRowPos());
		$this->setViewData(VDATAKEY_RECORDSET, $rs);
		$this->setViewData(VDATAKEY_RECORDCOUNT, $recordCount);
		$this->loadView($this->viewList);
	}
	
	public function insert(){
		$this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
		$this->setViewData(VDATAKEY_EDITOR_STATE, 'ADD');
		if($this->isPostRequest()){
			$validateResult = $this->validate();
			if($validateResult[0]===true){
				// save
				$existData = $this->DAO_Salary->getRowByEmployeeId($_POST["employee_id"]);
				if (empty($existData) && is_null($existData)){
					$salary = $this->getFormData();
					$salary['created_date'] = $this->getCurrentDateTime();
					$salary['created_by'] = $this->userData['id'];
					$this->DAO_Salary->insert($salary);
				}else{
					$employee = $this->getFormData();
					$employee['modified_by'] = $this->userData['id'];
					$this->DAO_Salary->update($employee, $existData['id']);
				}
				$this->showSuccessMessage('Data berhasil disimpan', 'sia_salary/index');
			}else{
				// show error & form
				$this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
				$this->setViewData(VDATAKEY_POST_DATA, $_POST);
				$this->loadView($this->viewEditor);
			}
		}else{
			$this->loadView($this->viewEditor);
		}
	}
	
	public function update(){
		$this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
		$this->setViewData(VDATAKEY_EDITOR_STATE, 'EDIT');
		if($this->isPostRequest()){
			$validateResult = $this->validate();
			if($validateResult[0]===true){
				// update
				$employee = $this->getFormData();
				$employee['modified_by'] = $this->userData['id'];
				$this->DAO_Salary->update($employee, $this->recId);
				$this->showSuccessMessage('Data berhasil disimpan', 'sia_salary/index');
			}else{
				// show error & form
				$this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
				$this->setViewData(VDATAKEY_POST_DATA, $_POST);
				$this->loadView($this->viewEditor);
			}
		}else{
			$student = $this->DAO_Salary->getRow($this->recId);
			$this->setViewData(VDATAKEY_POST_DATA, $student);
			$this->loadView($this->viewEditor);
		}
	}
	
	public function delete(){
		if($this->isPostRequest()){
			// Show error
			show_error('Request method not supported!');
		}else{
			$data = array('active' => 'N', 'modified_by' => $this->userData['id']);
			$this->DAO_Salary->update($data, $this->recId);
			$this->showSuccessMessage('Data berhasil dihapus', 'sia_salary/index');
		}
	}
	
	private function getFormData(){
		$gajiTotal = $_POST["gaji_pokok"] + $_POST["tunjangan"] + $_POST["lain_lain"];
		$data = array(
			'employee_id' => $_POST["employee_id"],
			'gaji_pokok' => $_POST["gaji_pokok"],
			'tunjangan' => $_POST["tunjangan"],
			'lain_lain' => $_POST["lain_lain"],
			'total_gaji' => $gajiTotal,
			'active' => 'Y'
		);
		return $data;
	}
	
	private function validate(){
		$errMsg = array();
		$data = $this->getFormData();
    $editorState = $this->getControllerAction();
		
		// Validasi
		if((!empty($editorState) && $editorState=='insert') && $this->DAO_Salary->isExists($data)){
			$this->setViewData(VDATAKEY_EDITOR_STATE, 'ADD');
			$errMsg[] = "Data karyawan dengan nama yang sama sudah terdaftar";
		}else{
			$this->setViewData(VDATAKEY_EDITOR_STATE, 'EDIT');
		}
		
		if(count($errMsg) > 0){
			return array(false, $errMsg);
		}else{
			return array(true, $errMsg);
		}
	}
	
}
