<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseBackEndController.php';
require_once 'IBasicController.php';

/**
 * Employees controller
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
class Sia_employees extends BaseBackEndController implements IBasicController
{
	
	var $recId;
	var $viewList = 'v_employee_list';
	var $viewEditor = 'v_employee_editor';
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array('DAO_Employee', 'DAO_Salary', 'DAO_Lov'));
		$this->lang->load('sia_employees');
		$this->recId = $this->getRecId();
		$this->setViewData('DAO_Lov', $this->DAO_Lov);
	}

  function getPageTitle()
  {
    return lang('employee_title');
  }

  function getPageIcon()
  {
    return 'fa-users';
  }

  function getJsFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/js/ultracms_sia_employee_list.min.js');
    } else {
      return array('assets/themes/sb-admin/js/moment-with-locales.min.js',
        'assets/themes/sb-admin/js/bootstrap-datetimepicker.min.js',
        'assets/js/ultracms_sia_employee_editor.min.js');
    }
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaEmployeeList.init()';
    } else {
      return 'ultracms.siaEmployeeEditor.init()';
    }
  }

  function getCssFiles()
  {
    if ($this->isIndexAction()) {
      return array();
    } else {
      return array('assets/themes/sb-admin/css/bootstrap-datetimepicker.min.css');
    }
  }

  public function index()
  {
		$id_number = isset($_GET['id_number'])? $_GET['id_number'] : '';
		$nama = isset($_GET['nama'])? $_GET['nama'] : '';
		$filters = array(
			array(QRYFILTER_FIELD => 'id_number', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $id_number),
			array(QRYFILTER_FIELD => 'name', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $nama),
			array(QRYFILTER_FIELD => 'active', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => 'Y')
		);
		$rs = $this->DAO_Employee->getList($filters, null, $this->getFirstRowPos(), $this->pageSize);
		$recordCount = $this->DAO_Employee->getListCount($filters);
		
		$this->setViewData(VDATAKEY_LIST_PAGE, $this->page);
		$this->setViewData(VDATAKEY_LIST_PAGE_SIZE, $this->pageSize);
		$this->setViewData(VDATAKEY_POST_DATA, $_GET);
		$this->setViewData(VDATAKEY_ROWNUM_START, $this->getFirstRowPos());
		$this->setViewData(VDATAKEY_RECORDSET, $rs);
		$this->setViewData(VDATAKEY_RECORDCOUNT, $recordCount);
		$this->loadView($this->viewList);
	}
	
	public function insert(){
		$this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
		if($this->isPostRequest()){
			$validateResult = $this->validate();
			if($validateResult[0]===true){
				// save
				$employee = $this->getFormData();
				$employee['created_date'] = $this->getCurrentDateTime();
				$employee['created_by'] = $this->userData['id'];
				$this->DAO_Employee->insert($employee);
				
				// save salary
				$salary = $this->getSalaryData();
				$salary['employee_id'] = $this->db->insert_id();
				$salary['created_date'] = $this->getCurrentDateTime();
				$salary['created_by'] = $this->userData['id'];
				$this->DAO_Salary->insert($salary);
				
				$this->showSuccessMessage('Data berhasil disimpan', 'sia_employees/index');
			}else{
				// show error & form
				$this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
				$this->setViewData(VDATAKEY_POST_DATA, $_POST);
				$this->loadView($this->viewEditor);
			}
		}else{
			$this->loadView($this->viewEditor);
		}
	}
	
	public function update(){
		$this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
		if($this->isPostRequest()){
			$validateResult = $this->validate();
			if($validateResult[0]===true){
				// update
				$employee = $this->getFormData();
				$employee['modified_by'] = $this->userData['id'];
				$this->DAO_Employee->update($employee, $this->recId);
				
				// insert/update salary
				$salary = $this->getSalaryData();
				if($this->DAO_Salary->isExists(array('employee_id' => $this->recId))){
					$salary['modified_by'] = $this->userData['id'];
					$this->DAO_Salary->update($salary, $this->recId);
				}else{
					$salary['employee_id'] = $this->recId;
					$salary['created_date'] = $this->getCurrentDateTime();
					$salary['created_by'] = $this->userData['id'];
					$this->DAO_Salary->insert($salary);
				}
				
				$this->showSuccessMessage('Data berhasil disimpan', 'sia_employees/index');
			}else{
				// show error & form
				$this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
				$this->setViewData(VDATAKEY_POST_DATA, $_POST);
				$this->loadView($this->viewEditor);
			}
		}else{
			$employee = $this->DAO_Employee->getRow($this->recId);
			$salary = $this->DAO_Salary->getRow($this->recId);
			$formData = is_null($salary)? $employee : array_merge($employee, $salary);
			$this->setViewData(VDATAKEY_POST_DATA, $formData);
			$this->loadView($this->viewEditor);
		}
	}
	
	public function delete(){
		if($this->isPostRequest()){
			// Show error
			show_error('Request method not supported!');
		}else{
			$data = array('active' => 'N', 'modified_by' => $this->userData['id']);
			$this->DAO_Employee->update($data, $this->recId);
			$this->showSuccessMessage('Data berhasil dihapus', 'sia_employees/index');
		}
	}
	
	private function getFormData(){
		$data = array(
			'name' => trim($_POST["name"]),
			'nickname' => trim($_POST["nickname"]),
			'gender' => $_POST["gender"],
			'id_number' => $_POST["id_number"],
			'birth_date' => $_POST["birth_date"],
			'birth_place' => $_POST["birth_place"],
			'address' => trim($_POST["address"]),
			'phone_number' => trim($_POST["phone_number"]),
			'jabatan' => $_POST["jabatan"]
		);
		return $data;
	}
	
	private function getSalaryData(){
		$data = array(
			'gaji_pokok' => $_POST['gaji_pokok'],
			'tunjangan' => $_POST['tunjangan'],
			'lain_lain' => $_POST['lain_lain']
		);
		$data['total_gaji'] = $data['gaji_pokok'] + $data['tunjangan'] + $data['lain_lain'];
		
		return $data;
	}
	
	private function validate(){
		$errMsg = array();
		$data = $this->getFormData();
    $editorState = $this->getControllerAction();
		
		// Validasi
		if((!empty($editorState) && $editorState=='insert') && $this->DAO_Employee->isExists($data)){
			$errMsg[] = "Data karyawan dengan nama dan no. identitas yang sama sudah terdaftar";
		}
		
		if(count($errMsg) > 0){
			return array(false, $errMsg);
		}else{
			return array(true, $errMsg);
		}
	}
	
}
