<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseBackEndController.php';
require_once 'IBasicController.php';

/**
 * Modules controller
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
class Sia_modules extends BaseBackEndController implements IBasicController
{
	
    var $recId;
    var $viewList = 'v_modules_list';
    var $viewEditor = 'v_modules_editor';
    var $indexUrl = 'sia_modules/index';
    const PARENT_OPTIONS = 'parentOptions';
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array("DAO_Modules"));
		$this->lang->load('sia_modules', $this->getCurrentLocale());
		$this->recId = $this->getRecId();
  }

  function getPageTitle()
  {
    return lang('module_title');
  }

  function getPageIcon()
  {
    return 'fa-cubes';
  }

  function getJsFiles()
  {
    $js = array('assets/3rd-party/select2/js/select2.min.js');
    if ($this->isIndexAction()) {
      $js[] = 'assets/js/ultracms_sia_modules_list.min.js';
    } else {
      $js[] = 'assets/js/ultracms_sia_modules_editor.min.js';
    }
    return $js;
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaModulesList.init()';
    } else {
      return 'ultracms.siaModulesEditor.init()';
    }
  }

  function getCssFiles()
  {
    return array('assets/3rd-party/select2/css/select2.min.css');
  }

  public function index()
  {
	    $parentId = isset($_GET['parent_id'])? $_GET['parent_id'] : '';
	    $className = isset($_GET['class_name'])? $_GET['class_name'] : '';
	    $caption = isset($_GET['caption'])? $_GET['caption'] : '';
	    $filters = array();
	    $filters[] = array(QRYFILTER_FIELD => 'caption', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $caption);
	    if(!empty($className)){
	       $filters[] = array(QRYFILTER_FIELD => 'class_name', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $className);
	    }
	    if (!empty($parentId)) {
	        $filters[] = array(QRYFILTER_FIELD => 'parent_id', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $parentId);
	    } else {
	        $filters[] = array(QRYFILTER_FIELD => 'parent_id', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_ISNULL);
	    }
	    $rs = $this->DAO_Modules->getList($filters, null, $this->getFirstRowPos(), $this->pageSize);
	    $recordCount = $this->DAO_Modules->getListCount($filters);
	    
	    $this->setViewData(VDATAKEY_LIST_PAGE, $this->page);
	    $this->setViewData(VDATAKEY_LIST_PAGE_SIZE, $this->pageSize);
	    $this->setViewData(VDATAKEY_POST_DATA, $_GET);
	    $this->setViewData(VDATAKEY_ROWNUM_START, $this->getFirstRowPos());
	    $this->setViewData(VDATAKEY_RECORDSET, $rs);
	    $this->setViewData(VDATAKEY_RECORDCOUNT, $recordCount);
	    $this->setViewData(self::PARENT_OPTIONS, $this->createParentOptions());
		$this->loadView($this->viewList);
	}
	
	public function insert(){
	    $this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
	    $this->setViewData(VDATAKEY_EDITOR_STATE, EDITOR_STATE_INSERT);
	    $this->setViewData(self::PARENT_OPTIONS, $this->createParentOptions());
	    if($this->isPostRequest()){
	        $validateResult = $this->validate();
	        if($validateResult[0]===true){
	            // save
	            $module = $this->getFormData();
	            $this->DAO_Modules->insert($module);
	            $this->showSuccessMessage('Data berhasil disimpan', $this->indexUrl);
	        }else{
	            // show error & form
	            $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
	            $this->setViewData(VDATAKEY_POST_DATA, $_POST);
	            $this->loadView($this->viewEditor);
	        }
	    }else{
	        $this->loadView($this->viewEditor);
	    }
	}
	
	public function update(){
	    $this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
	    $this->setViewData(VDATAKEY_EDITOR_STATE, EDITOR_STATE_UPDATE);
	    $this->setViewData(self::PARENT_OPTIONS, $this->createParentOptions());
	    if($this->isPostRequest()){
	        $validateResult = $this->validate();
	        if($validateResult[0]===true){
	            // update
	            $module = $this->getFormData();
	            $this->DAO_Modules->update($module, $this->recId);
	            $this->showSuccessMessage('Data berhasil disimpan', $this->indexUrl);
	        }else{
	            // show error & form
	            $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
	            $this->setViewData(VDATAKEY_POST_DATA, $_POST);
	            $this->loadView($this->viewEditor);
	        }
	    }else{
	        $module = $this->DAO_Modules->getRow($this->recId);
	        $this->setViewData(VDATAKEY_POST_DATA, $module);
	        $this->loadView($this->viewEditor);
	    }
	}
	
	public function delete(){
	    if($this->isPostRequest()){
	        // Show error
	        show_error('Request method not supported!');
	    }else{
	        $this->DAO_Modules->delete($this->recId);
	        $this->showSuccessMessage('Data berhasil dihapus', $this->indexUrl);
	    }
	}
	
	private function getFormData(){
	    $data = array(
	        'parent_id' => $_POST['parent_id'],
	        'class_name' => strtolower(trim($_POST['class_name'])),
	        'description' => trim($_POST['description']),
	        'caption' => trim($_POST['caption']),
	        'icon' => strtolower(trim($_POST['icon'])),
	        'tree_level' => $_POST['tree_level'],
	        'display_order' => $_POST['display_order']
	    );
	    return $data;
	}
	
	private function createParentOptions(){
	    $retval = array(lang('module_option_root'));
	    $rs = $this->DAO_Modules->getRoots();
	    foreach ($rs as $row){
	        $retval[$row->id] = $row->caption;
	    }
	    return $retval;
	}
	
	private function validate(){
	    $errMsg = array();
	    $data = $this->getFormData();
    $editorState = $this->getControllerAction();
	    
	    // Validasi
	    if((!empty($editorState) && $editorState=='insert') && $this->DAO_Modules->isClassNameExists($data['class_name'])){
	        $errMsg[] = "Data module dengan Class Name yang sama sudah terdaftar";
	    }
	    if(empty($data['caption'])){
	        $errMsg[] = "Caption tidak boleh kosong";
	    }
	    if(empty($data['icon'])){
	        $errMsg[] = "Icon Class tidak boleh kosong";
	    }
	    if(!($data['tree_level']==='0' || $data['tree_level']==='1')){
	        $errMsg[] = "Tree Level tidak valid";
	    }
	    
	    if(count($errMsg) > 0){
	        return array(false, $errMsg);
	    }else{
	        return array(true, $errMsg);
	    }
	}
	
}
