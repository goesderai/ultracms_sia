<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseReportController.php';
require_once 'IReportController.php';

/**
 * Controller laporan tunggakan pembayaran Uang pangkal atau uang gedung
 *
 * @author <a href="mailto:agung3ka@gmail.com">Turah Eka</a>
 */
class Sia_rpt_tunggakan_uang_pangkal extends BaseReportController implements IReportController
{

  const V_RPT_FORM = 'v_rpt_tunggakan_uang_pangkal_form';

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('DAO_Siswa', 'DAO_Transaksi', 'DAO_Edu'));
  }

  function getPageTitle()
  {
    return 'Laporan Tunggakan Uang Pangkal / Gedung';
  }

  function getJsFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/js/ultracms_sia_rpt_tunggakan_uang_pangkal.min.js');
    } else {
      return array();
    }
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaRptTransaksiTunggakanUangPangkal.init()';
    } else {
      return '';
    }
  }

  function getCssFiles()
  {
    array();
  }

  public function index()
  {
    $this->setViewData('DAO_Siswa', $this->DAO_Siswa);
    $this->setViewData('DAO_Transaksi', $this->DAO_Transaksi);
    $this->setViewData('DAO_Edu', $this->DAO_Edu);
    $this->loadReportForm(self::V_RPT_FORM);
  }

  public function generate_report()
  {
    $grade = isset($_POST['grade']) ? $_POST['grade'] : '';
    $classroom = isset($_POST['classroom']) ? $_POST['classroom'] : '';
    $validateResult = $this->validate();
    if ($validateResult[0] === true) {
      // Show report
      if ($classroom == 0 && $grade == 0) {
        $sql = 'SELECT v.nis as `NIS`, v.student_name as `Nama Siswa`, v.grade_name AS `Jenjang`, v.class_name as `Tingkatan`, v.phone_number as `No. Telepon` FROM v_rpt_tunggakan_uang_pangkal v';
        $qry = $this->db->query($sql);
      }
      if ($classroom != 0 && $grade == 0) {
        $sql = 'SELECT v.nis as `NIS`, v.student_name as `Nama Siswa`, v.grade_name AS `Jenjang`, v.class_name as `Tingkatan`, v.phone_number as `No. Telepon` FROM v_rpt_tunggakan_uang_pangkal v WHERE v.class_id = ?';
        $qry = $this->db->query($sql, array($classroom));
      }
      if ($classroom == 0 && $grade != 0) {
        $sql = 'SELECT v.nis as `NIS`, v.student_name as `Nama Siswa`, v.grade_name AS `Jenjang`, v.class_name as `Tingkatan`, v.phone_number as `No. Telepon` FROM v_rpt_tunggakan_uang_pangkal v WHERE v.grade_id = ?';
        $qry = $this->db->query($sql, array($grade));
      }
      if ($classroom != 0 && $grade != 0) {
        $sql = 'SELECT v.nis as `NIS`, v.student_name as `Nama Siswa`, v.grade_name AS `Jenjang`, v.class_name as `Tingkatan`, v.phone_number as `No. Telepon` FROM v_rpt_tunggakan_uang_pangkal v WHERE v.class_id = ? and v.grade_id = ?';
        $qry = $this->db->query($sql, array($classroom, $grade));
      }
      $this->setReportGenField(RPTGEN_HEADER, $this->getPageTitle());
      $this->setReportGenField(RPTGEN_MYSQL_RESOURCE, $qry);
      $this->setReportGenField(RPTGEN_DATE_FIELDS, array('Tanggal Transaksi' => $this->appConfig['dateFormatForDisplay']));
      $this->loadReport();
    } else {
      // show error & form
      $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
      $this->setViewData(VDATAKEY_POST_DATA, $_POST);
      $this->loadView(self::V_RPT_FORM);
    }
  }

  private function validate()
  {
    $errMsg = array();

    //Validation(s) here

    if (count($errMsg) > 0) {
      return array(false, $errMsg);
    } else {
      return array(true, $errMsg);
    }
  }

}
