<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseBackEndController.php';
require_once 'IBasicController.php';

/**
 * Tipe Transaksi controller
 *
 * @author <a href="mailto:agung3ka@gmail.com">Turah Eka</a>
 */
class Sia_transaction_type extends BaseBackEndController implements IBasicController
{
	var $recId;
	var $viewList = 'v_transaction_type_list';
	var $viewEditor = 'v_transaction_type_editor';
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array('DAO_TipeTransaksi', 'DAO_Document_Type'));
		$this->lang->load('sia_transaction_type', $this->getCurrentLocale());
		$this->recId = $this->getRecId();
		$this->setViewData('DAO_TipeTransaksi', $this->DAO_TipeTransaksi);
		$this->setViewData('DAO_Document_Type', $this->DAO_Document_Type);
	}

  function getPageTitle()
  {
    return lang('trxtype_type');
  }

  function getJsFiles()
  {
    $js = array('assets/3rd-party/select2/js/select2.min.js');
    if ($this->isIndexAction()) {
      $js[] = 'assets/js/ultracms_sia_transaction_type_list.min.js';
    } else {
      $js[] = 'assets/js/ultracms_sia_transaction_type_editor.min.js';
    }
    return $js;
  }

  function getCssFiles()
  {
    $css = array('assets/3rd-party/select2/css/select2.min.css');
    if ($this->isIndexAction()) {
      // no addition
    } else {
      $css[] = 'assets/3rd-party/pretty-checkbox/pretty-checkbox.min.css';
    }
    return $css;
  }

  function getPageIcon()
  {
    return 'fa-sitemap';
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaTransactionTypeList.init()';
    } else {
      return 'ultracms.siaTransactionTypeEditor.init()';
    }
  }

  public function index()
  {
		$transactionType = isset($_GET['transaction_type'])? $_GET['transaction_type'] : '';
		$docType = isset($_GET['document_type_id'])? $_GET['document_type_id'] : '';
		$active = isset($_GET['active'])? $_GET['active'] : '';
		$nama = isset($_GET['nama'])? $_GET['nama'] : '';
		$flow = isset($_GET['transaction_flow'])? $_GET['transaction_flow'] : '';
		$filters = array(
			array(QRYFILTER_FIELD => 'nama', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $nama)	
		);
		if(!empty($active)){
		    $filters[] = array(QRYFILTER_FIELD => 'active', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $active);
		}
		if(!empty($flow)){
		    $filters[] = array(QRYFILTER_FIELD => 'transaction_flow', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $flow);
		}
		if($transactionType > 0){
			$filters[] = array(QRYFILTER_FIELD => 'id', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $transactionType);
		};
		if($docType > 0){
			$filters[] = array(QRYFILTER_FIELD => 'document_type_id', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $docType);
		}
		
		$rs = $this->DAO_TipeTransaksi->getList($filters, null, $this->getFirstRowPos(), $this->pageSize);
		$recordCount = $this->DAO_TipeTransaksi->getListCount($filters);
		
		$this->setViewData(VDATAKEY_LIST_PAGE, $this->page);
		$this->setViewData(VDATAKEY_LIST_PAGE_SIZE, $this->pageSize);
		$this->setViewData(VDATAKEY_POST_DATA, $_GET);
		$this->setViewData(VDATAKEY_ROWNUM_START, $this->getFirstRowPos());
		$this->setViewData(VDATAKEY_RECORDSET, $rs);
		$this->setViewData(VDATAKEY_RECORDCOUNT, $recordCount);
		$this->loadView($this->viewList);
	}
	
	public function insert(){
		$this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
		$this->setViewData(VDATAKEY_EDITOR_STATE, 'ADD');
		if($this->isPostRequest()){
			$validateResult = $this->validate();
			if($validateResult[0]===true){
				// save
				$transactionType = $this->getFormData();
				$this->DAO_TipeTransaksi->insert($transactionType);
				$this->setViewData(VDATAKEY_ALERT_TYPE, ALERTTYPE_SUCCESS);
				$this->setViewData(VDATAKEY_ALERT_MESSAGE, lang('message_common_save_success'));
				$this->setViewData(VDATAKEY_ALERT_PROCEED_URL, site_url('sia_transaction_type/index'));
				$this->loadView($this->viewAfterSaveMessage);
			}else{
				// show error & form
				$this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
				$this->setViewData(VDATAKEY_POST_DATA, $_POST);
				$this->loadView($this->viewEditor);
			}
		}else{
			$this->loadView($this->viewEditor);
		}
	}
	
	private function getFormData(){
		$subType = isset($_POST['is_sub_type'])? $_POST['is_sub_type'] : 0;
		$active = isset($_POST['active'])? $_POST['active'] : 'N';
		$data = array(
			'transaction_type' => $_POST['transaction_type'],
			'document_type_id' => $_POST['document_type_id'],
			'active' => $active,
			'transaction_flow' => $_POST['transaction_flow'],
			'nama' => $_POST['nama'],
			'is_sub_type' => $subType
		);
		if ($_POST['parent_type']>0){
			$data['parent_type'] = $_POST['parent_type'];
		}else{
			$data['parent_type'] = null;
		}
		
		return $data;
	}
	
	public function update(){
		$this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
		$this->setViewData(VDATAKEY_EDITOR_STATE, 'EDIT');
		if($this->isPostRequest()){
			$validateResult = $this->validate();
			if($validateResult[0]===true){
				// update
				$this->DAO_TipeTransaksi->update($this->getFormData(), $this->recId);
				$this->setViewData(VDATAKEY_ALERT_TYPE, ALERTTYPE_SUCCESS);
				$this->setViewData(VDATAKEY_ALERT_MESSAGE, lang('message_common_save_success'));
				$this->setViewData(VDATAKEY_ALERT_PROCEED_URL, site_url('sia_transaction_type/index'));
				$this->loadView($this->viewAfterSaveMessage);
			}else{
				// show error & form
				$this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
				$this->setViewData(VDATAKEY_POST_DATA, $_POST);
				$this->loadView($this->viewEditor);
			}
		}else{
			$transactionType = $this->DAO_TipeTransaksi->getRow($this->recId);
			$this->setViewData(VDATAKEY_POST_DATA, $transactionType);
			$this->loadView($this->viewEditor);
		}
	}
	
	private function validate(){
		$errMsg = array();
		$currentAction = $this->uri->segment(2, 0);
		if (!empty($currentAction) && $currentAction=='insert'){
			$this->setViewData(VDATAKEY_EDITOR_STATE, 'ADD');
			$trxTypTmp = isset($_POST['transaction_type'])? $_POST['transaction_type'] : '';
			$tipeTransaksi = $this->DAO_TipeTransaksi->getTipeTransaksi($trxTypTmp);
			
			if($tipeTransaksi != null){
				$errMsg[] = 'Tipe Transaksi Sudah Terdaftar';
				return array(false, $errMsg);
			}else{
				return array(true, $errMsg);
			}
		}else{
			$this->setViewData(VDATAKEY_EDITOR_STATE, 'EDIT');
			return array(true, $errMsg);
		}
	}
	
	public function delete(){
		if($this->isPostRequest()){
			// Show error
			show_error(lang('message_error_invalid_request_method'));
		}else{
			$data = array('active' => 'N', 'modified_by' => $this->userData['id']);
			$this->DAO_TipeTransaksi->update($data, $this->recId);
			$this->showSuccessMessage(lang('message_common_delete_success'), 'sia_transaction_type/index');
		}
	}
}
