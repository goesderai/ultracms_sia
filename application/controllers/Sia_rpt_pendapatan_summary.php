<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseReportController.php';
require_once 'IReportController.php';

/**
 * Controller laporan pendapatan
 *
 * @author <a href="mailto:agung3ka@gmail.com">Turah Eka</a>
 */
class Sia_rpt_pendapatan_summary extends BaseReportController implements IReportController
{

	const V_RPT_FORM = 'v_rpt_pendapatan_summary_form';

	public function __construct()
	{
		parent::__construct();
		$this->load->model(['DAO_TipeTransaksi', 'DAO_Transaksi_Pendapatan']);
	}

	function getPageTitle()
	{
		return 'Laporan Summary Pendapatan';
	}

	function getJsFiles()
	{
		if ($this->isIndexAction()) {
			return ['assets/themes/sb-admin/js/moment-with-locales.min.js',
              'assets/themes/sb-admin/js/bootstrap-datetimepicker.min.js',
              'assets/js/ultracms_sia_rpt_transaksi_pendapatan_summary.min.js'];
		} else {
			return [];
		}
	}

	function getJsInitFunction()
	{
		if ($this->isIndexAction()) {
			return 'ultracms.siaRptPendapatanSummary.init()';
		} else {
			return '';
		}
	}

	function getCssFiles()
	{
		if ($this->isIndexAction()) {
			return ['assets/themes/sb-admin/css/bootstrap-datetimepicker.min.css'];
		} else {
			return [];
		}
	}

	public function index()
	{
		$this->setViewData('DAO_TipeTransaksi', $this->DAO_TipeTransaksi);
		$this->setViewData('DAO_Transaksi_Pendapatan', $this->DAO_Transaksi_Pendapatan);
		$this->loadReportForm(self::V_RPT_FORM);
	}

	public function generate_report()
	{
		$filters               = [];
		$transactionType       = null;
		$tipeTransaksi         = isset($_POST['trx_type']) ? $_POST['trx_type'] : '';
		
		$transaction_date_from = isset($_POST['transaction_date_from']) ? $_POST['transaction_date_from'] : '';
		$transaction_date_to   = isset($_POST['transaction_date_to']) ? $_POST['transaction_date_to'] : '';
		
		$financialMonth         = isset($_POST['financial_month']) ? $_POST['financial_month'] : '';
		$financialYear         = isset($_POST['financial_year']) ? $_POST['financial_year'] : '';
		
		/*var_dump($tipeTransaksi);
		var_dump($financialMonth);
		var_dump($financialYear);
		var_dump($transaction_date_from);
		var_dump($transaction_date_to);
		
		
		
		die();*/
		
		$trxGrouping = '';
		$periodeGrouping= '';
		$trxDateOrder = false;
		//Build filters
		if (!empty($tipeTransaksi)) {
			
			$transactionType = $this->DAO_TipeTransaksi->getRow($tipeTransaksi);
			
			/*if($transactionType['is_sub_type'] == 1){
				$trxGrouping = '';
				$filters[] = [QRYFILTER_FIELD => 'trx_type', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $tipeTransaksi];
			}else{
				if($transactionType['parent_type'] != null){
					$trxGrouping = 'trx_type';
					$filters[]       = [QRYFILTER_FIELD => 'parent_type_id', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $tipeTransaksi];
				}else{
					$trxGrouping = 'parent_type_id';
					$filters[]       = [QRYFILTER_FIELD => 'grand_parent_id', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $tipeTransaksi];
				}
			}*/
			

		}
		if (!empty($transaction_date_from)) {
			if (!empty($transaction_date_to)) {
				$filters[] = [QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_BETWEEN, QRYFILTER_VALUE => [$transaction_date_from, $transaction_date_to]];
			} else {
				$filters[] = [QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_BETWEEN, QRYFILTER_VALUE => [$transaction_date_from, $transaction_date_from]];
			}
			$periodeGrouping = '`s`.`transaction_date`';
			$trxDateOrder = true;
		}else{
			if(!empty($financialMonth)){
				if(!empty($financialYear)){
					$filters[] = [QRYFILTER_FIELD => 'financial_month', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $financialMonth];
					$filters[] = [QRYFILTER_FIELD => 'financial_year', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $financialYear];
					$periodeGrouping = '`s`.`financial_month`, `s`.`financial_year`';
				}
			}else{
			if(!empty($financialYear)){
					$filters[] = [QRYFILTER_FIELD => 'financial_year', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $financialYear];
					$periodeGrouping = '`s`.`financial_year`';	
				}
			}
		}

		//Fetch data
		$sql          = $this->DAO_Transaksi_Pendapatan->getRptQueryGrouping($filters, $transactionType, $periodeGrouping);
		$qry          = $this->db->query($sql);
		$newPageTitle = $this->getPageTitle();
		$allData      = $qry->result();

		//Preparing report data
		if (!empty($allData)) {
			$first   = reset($allData);
			$last    = end($allData);
			$periode = ' Periode : ';
			if (!empty($transaction_date_from)) {
				$dateStart = date($this->appConfig['dateFormatForDisplay'], strtotime($transaction_date_from));
				if (!empty($transaction_date_to)) {
					$endDate = date($this->appConfig['dateFormatForDisplay'], strtotime($transaction_date_to));
					$periode = $periode . $dateStart . ' s/d ' . $endDate;
				} else {
					$periode = $periode . $dateStart;
				}
			} else {
				if(!empty($financialMonth) && !empty($financialYear)){
					$dateObj   = DateTime::createFromFormat('!m', $financialMonth);
					$monthName = $dateObj->format('F');
					
					$periode   = $periode . $monthName . ' ' . $financialYear;
				}else{
					if(!empty($financialYear)){
						$periode   = $periode . ' Tahun ' . $financialYear;
					}
				}
				
				
			}
			$trxTypeLabel = !is_null($transactionType)? $transactionType['nama'] : 'Semua Tipe Transaksi';
			$newPageTitle      = $newPageTitle . " - " . $trxTypeLabel . "<br>" . $periode;
		}

		//Load report
		$this->setReportGenField(RPTGEN_HEADER, $newPageTitle);
		$this->setReportGenField(RPTGEN_MYSQL_RESOURCE, $qry);
		if($trxDateOrder){
			$this->setReportGenField(RPTGEN_DATE_FIELDS, ['Tanggal Transaksi' => $this->appConfig['dateFormatForDisplay']]);
		}
		$this->setReportGenField(RPTGEN_NUMBER_FIELDS, ["Jumlah"]);
		$this->loadReport();
	}

}
