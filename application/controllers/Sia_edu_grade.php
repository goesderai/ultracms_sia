<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseBackEndController.php';
require_once 'IBasicController.php';

/**
 * Grade & classroom controller
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
class Sia_edu_grade extends BaseBackEndController implements IBasicController
{
	
	var $recId;
	var $viewList = 'v_edu_grade_list';
	var $viewEditor = 'v_edu_grade_editor';
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array('DAO_Edu'));
		$this->lang->load('sia_edu_grade', $this->getCurrentLocale());
		$this->recId = $this->getRecId();
  }

  function getPageTitle()
  {
    return lang('edugrd_title');
  }

  function getPageIcon()
  {
    return 'fa-bank';
  }

  function getJsFiles()
  {
    if ($this->isIndexAction()) {
      return array();
    } else {
      return array('assets/js/ultracms_sia_edu_grade_editor.min.js');
    }
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return '';
    } else {
      return 'ultracms.siaEduGradeEditor.init()';
    }
  }

  function getCssFiles()
  {
    if ($this->isIndexAction()) {
      return array();
    } else {
      return array();
    }
  }

  public function index()
  {
		$rs = $this->DAO_Edu->getGradeList();
		$this->setViewData(VDATAKEY_ROWNUM_START, $this->getFirstRowPos());
		$this->setViewData(VDATAKEY_RECORDSET, $rs);
		$this->loadView($this->viewList);
	}
	
	public function insert(){
		$this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
		$this->setViewData("gradeId", $this->recId);
		if($this->isPostRequest()){
		$validateResult = $this->validate();
			if($validateResult[0]===true){
				// insert
				$grade = $this->getGradeFormData();
				$grade['created_by'] = $this->userData['id'];
				$grade['created_date'] = $this->getCurrentDateTime();
				$this->DAO_Edu->insertGrade($grade);
				$gradeId = $this->db->insert_id();
				$this->loadFormWithData($gradeId);
			}else{
				// show error & form
				$this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
				$this->setViewData(VDATAKEY_POST_DATA, $_POST);
				$this->loadView($this->viewEditor);
			}
		}else{
			$this->loadView($this->viewEditor);
		}
	}
	
	public function update(){
		$this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
		$this->setViewData("gradeId", $this->recId);
		if($this->isPostRequest()){
			$validateResult = $this->validate();
			if($validateResult[0]===true){
				// update
				$grade = $this->getGradeFormData();
				$grade['modified_by'] = $this->userData['id'];
				$this->DAO_Edu->updateGrade($grade, $this->recId);
				$this->showSuccessMessage('Data berhasil disimpan', 'sia_edu_grade/index');
			}else{
				// show error & form
				$this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
				$this->setViewData(VDATAKEY_POST_DATA, $_POST);
				$this->loadView($this->viewEditor);
			}
		}else{
			$this->loadFormWithData($this->recId);
		}
	}
	
	private function loadFormWithData($gradeId){
		$grade = $this->DAO_Edu->getGradeById($gradeId);
		$this->setViewData("gradeId", $gradeId);
		$this->setViewData(VDATAKEY_POST_DATA, $grade);
		$this->loadView($this->viewEditor);
	}
	
	public function delete(){
		if($this->isPostRequest()){
			// Show error
			show_error('Request method not supported!');
		}else{
			$data = array('active' => 'N', 'modified_by' => $this->userData['id']);
			$this->DAO_Edu->updateGrade($data, $this->recId);
			$this->showSuccessMessage('Data berhasil dihapus', 'sia_edu_grade/index');
		}
	}
	
	private function getGradeFormData(){
		$data = array(
			'name' => $_POST["name"]
		);
		return $data;
	}
	
	private function validate(){
		$errMsg = array();
		$data = $this->getGradeFormData();
    $editorState = $this->getControllerAction();
		
		// Validasi di sini
				
		if(count($errMsg) > 0){
			return array(false, $errMsg);
		}else{
			return array(true, $errMsg);
		}
	}
	
	/**
	 * Get classroom data by grade id
	 * @param Integer $gradeId The grade id
	 * @category Ajax response
	 */
	public function classrooms($gradeId){
		if(empty($gradeId)){
			echo "";
		}else{
			$sql = "SELECT cr.id, cr.grade_id, cr.name, " .
				"(SELECT COUNT(s.classroom_id) FROM m_student s WHERE s.classroom_id = cr.id) students " .
				"FROM m_edu_classroom cr WHERE cr.grade_id=?";
			$qry = $this->db->query($sql, array($gradeId));
			$rs = $qry->result();
			$this->setViewData(VDATAKEY_RECORDSET, $rs);
			$classroomsGrid = $this->load->view('sia_admin/v_edu_classroom_list', $this->viewData, true);
			echo $classroomsGrid;
		}
	}
	
	/**
	 * Add new classroom data
	 * @param Integer $gradeId The grade id
	 * @param String $classroomName The classroom name
	 * @category Ajax response
	 */
	public function add_classroom($gradeId, $classroomName){
		$resp = array('status' => 0, 'message' => '');
		
		if(!$this->isPostRequest()){
			if($this->DAO_Edu->isClassroomNameExists($classroomName)){
				$resp['message'] = 'Data ruang kelas gagal disimpan. Nama ruang kelas sudah terdaftar!';
			}else{
				$data = array('grade_id'=>$gradeId, 'name'=>urldecode($classroomName), 
					'created_by'=>$this->userData['id'], 'created_date'=>$this->getCurrentDateTime());
				$this->DAO_Edu->insertClassroom($data);
				$resp['status'] = 1;
				$resp['message'] = 'Data ruang kelas berhasil disimpan.';
			}
		}else{
			$resp['message'] = 'Operation not supported';
		}
		
		echo json_encode($resp);
	}
	
	/**
	 * Delete classroom data by id
	 * @param Integer $classroomId The record id
	 * @category Ajax response
	 */
	public function delete_classroom($classroomId){
		$resp = array('status' => 0, 'message' => '');
		if($this->isPostRequest()){
			$resp['message'] = 'Operation not supported';
		}else{
			$this->DAO_Edu->deleteClassroom($classroomId);
			$resp['status'] = 1;
			$resp['message'] = 'Data ruang kelas berhasil dihapus.';
		}
		echo json_encode($resp);
	}
	
	/**
	 * Update classroom name
	 * @param Integer $classroomId The record id
	 * @param String $newClassroomName The new classroom name
	 * @category Ajax response
	 */
	public function update_classroom($classroomId, $newClassroomName){
		$resp = array('status' => 0, 'message' => '');
		if($this->isPostRequest()){
			$resp['message'] = 'Operation not supported';
		}else{
			$data = array('name' => urldecode($newClassroomName), 'modified_by' => $this->userData['id']);
			$this->DAO_Edu->updateClassroom($data, $classroomId);
			$resp['status'] = 1;
			$resp['message'] = 'Data ruang kelas berhasil diedit.';
		}
		echo json_encode($resp);
	}
	
}
