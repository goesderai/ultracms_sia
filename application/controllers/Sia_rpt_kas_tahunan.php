<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseReportController.php';
require_once 'IReportController.php';

/**
 * Controller laporan tunggakan pembayaran SPP
 *
 * @author <a href="mailto:agung3ka@gmail.com">Turah Eka</a>
 */
class Sia_rpt_kas_tahunan extends BaseReportController implements IReportController
{

  const V_RPT_FORM = 'v_rpt_kas_tahunan_form';
  const V_RPT_ANNUALLY = 'v_kas_tahunan_report';

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('DAO_Transaksi'));
  }

  function getPageTitle()
  {
    return 'Laporan Kas Tahunan';
  }

  function getJsFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/js/ultracms_sia_rpt_kas_tahunan.min.js');
    } else {
      return array();
    }
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaRptKasTahunan.init()';
    } else {
      return '';
    }
  }

  function getCssFiles()
  {
    return array();
  }

  public function index()
  {
    $this->setViewData('DAO_Transaksi', $this->DAO_Transaksi);
    $this->loadReportForm(self::V_RPT_FORM);
  }

  public function generate_report()
  {
    $tahun = isset($_POST['financial_year']) ? $_POST['financial_year'] : '';
    $tanggalSebelumnya = $tahun . '-1-1';
    $tanggal = $tahun . '-12-31';

    $filters = array();
    $filtersKasAwal = array();
    $filtersKasAkhir = array();


    if (!empty($tahun)) {
      $filters[] = array(QRYFILTER_FIELD => 'financial_year', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $tahun);
      $filtersKasAwal[] = array(QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_SMALLER, QRYFILTER_VALUE => $tanggalSebelumnya);
      $filtersKasAkhir[] = array(QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_IS_LESS_THAN_OR_EQUAL_TO, QRYFILTER_VALUE => $tanggal);
    }
    $validateResult = $this->validate();
    if ($validateResult[0] === true) {
      // Show report
      $this->loadCompanyInfo();
      $this->setViewData(VDATAKEY_PAGE_TITLE, $this->getPageTitle());
      $this->setViewData('DAO_Transaksi', $this->DAO_Transaksi);
      $this->setViewData('filters', $filters);
      $this->setViewData('ket', 'Laporan Tahun ' . $tahun);
      $this->setViewData('filtersKasAwal', $filtersKasAwal);
      $this->setViewData('filtersKasAkhir', $filtersKasAkhir);
      $this->loadPlainView(self::V_RPT_ANNUALLY);
    } else {
      // show error & form
      $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
      $this->setViewData(VDATAKEY_POST_DATA, $_POST);
      $this->loadView(self::V_RPT_FORM);
    }
  }

  private function validate()
  {
    $errMsg = array();

    if (count($errMsg) > 0) {
      return array(false, $errMsg);
    } else {
      return array(true, $errMsg);
    }
  }

}
