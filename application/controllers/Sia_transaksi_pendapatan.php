<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseBackEndController.php';
require_once 'ITransactionController.php';
require_once APPPATH . 'models/InvoiceHeader.php';
require_once APPPATH . 'models/InvoiceDetail.php';

/**
 * Students controller
 *
 * @author <a href="mailto:agung3ka@gmail.com">Turah Eka</a>
 */
class Sia_transaksi_pendapatan extends BaseBackEndController implements ITransactionController
{

  var $recId;
  var $viewList   = 'v_transaksi_pendapatan_list';
  var $viewEditor = 'v_transaksi_pendapatan_editor';

  public function __construct()
  {
    parent::__construct();

    $this->load->model(['DAO_Security', 'DAO_Transaksi', 'DAO_Siswa', 'DAO_Lov', 'DAO_Sequence',
                        'DAO_TipeTransaksi', 'DAO_Posting_Profile', 'DAO_Employee']);
    $this->load->library(['Number_generator']);
    $this->recId = $this->getRecId();
    $this->setViewData('DAO_Siswa', $this->DAO_Siswa);
    $this->setViewData('DAO_TipeTransaksi', $this->DAO_TipeTransaksi);
    $this->setViewData('DAO_Lov', $this->DAO_Lov);
    $this->setViewData('DAO_Posting_Profile', $this->DAO_Posting_Profile);
    $this->setViewData('DAO_Security', $this->DAO_Security);
    $this->setViewData('DAO_Employee', $this->DAO_Employee);
  }

  function getPageTitle()
  {
    return 'Transaksi Pendapatan';
  }

  function getJsFiles()
  {
    $js = ['assets/themes/sb-admin/js/moment-with-locales.min.js',
           'assets/themes/sb-admin/js/bootstrap-datetimepicker.min.js'];
    if ($this->isIndexAction()) {
      $js[] = 'assets/js/ultracms_sia_transaksi_pendapatan_list.min.js';
    } else {
      $js[] = 'assets/themes/sb-admin/js/jquery.number.min.js';
      $js[] = 'assets/js/ultracms_sia_transaksi_pendapatan_editor.min.js';
    }
    return $js;
  }

  function getCssFiles()
  {
    return ['assets/themes/sb-admin/css/bootstrap-datetimepicker.min.css'];
  }

  function getPageIcon()
  {
    return 'fa-usd';
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaTransaksiPendapatanList.init()';
    } else {
      return 'ultracms.siaTransaksiPendapatanEditor.init()';
    }
  }

  public function index()
  {
    $nomor = isset($_GET['nomor']) ? $_GET['nomor'] : '';
    $nama  = isset($_GET['nama']) ? $_GET['nama'] : '';
    /*$account_code = isset($_GET['account_number'])? $_GET['account_number'] : '';*/
    $remark                = isset($_GET['remark']) ? $_GET['remark'] : '';
    $transaction_date_from = isset($_GET['transaction_date_from']) ? $_GET['transaction_date_from'] : '';
    $transaction_date_to   = isset($_GET['transaction_date_to']) ? $_GET['transaction_date_to'] : '';
    $filters               = [
      [QRYFILTER_FIELD => 'document_type_id', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => 3],
      [QRYFILTER_FIELD => 'transaction_number', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $nomor],
      [QRYFILTER_FIELD => 'nama_tr', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $nama],
      [QRYFILTER_FIELD => 'remark', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $remark],
      [QRYFILTER_FIELD => 'ref_transaction_id', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_ISNULL]
    ];

    if (!empty($transaction_date_from)) {
      if (!empty($transaction_date_to)) {
        $filters[] = [QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_BETWEEN, QRYFILTER_VALUE => [$transaction_date_from, $transaction_date_to]];
      } else {
        $filters[] = [QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_MORE_THAN_OR_EQUAL_TO, QRYFILTER_VALUE => $transaction_date_from];
      }
    }

    $rs          = $this->DAO_Transaksi->getListTransaksiPendapatan($filters, null, $this->getFirstRowPos(), $this->pageSize);
    $recordCount = $this->DAO_Transaksi->getListCountPendapatan($filters);

    $this->setViewData(VDATAKEY_LIST_PAGE, $this->page);
    $this->setViewData(VDATAKEY_LIST_PAGE_SIZE, $this->pageSize);
    $this->setViewData(VDATAKEY_POST_DATA, $_GET);
    $this->setViewData(VDATAKEY_ROWNUM_START, $this->getFirstRowPos());
    $this->setViewData(VDATAKEY_RECORDSET, $rs);
    $this->setViewData(VDATAKEY_RECORDCOUNT, $recordCount);

    $this->loadView($this->viewList);
  }

  public function insert()
  {
    $this->setViewData('userData', $this->userData);
    $this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
    $this->setViewData(VDATAKEY_EDITOR_STATE, 'ADD');
    if ($this->isPostRequest()) {
      $validateResult = $this->validate();
      if ($validateResult[0] === true) {
        // save
        $pendapatan                       = $this->getFormData();
        $pendapatan['transaction_number'] = $this->generateTransactionNumber();
        $id                               = $this->DAO_Transaksi->insert($pendapatan);
        $this->showAfterSaveMessage(site_url("sia_transaksi_pendapatan/show_invoice/{$id}"),
                                    site_url('sia_transaksi_pendapatan/index'));
      } else {
        // show error & form
        $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
        $this->setViewData(VDATAKEY_POST_DATA, $_POST);
        $this->loadView($this->viewEditor);
      }
    } else {

      $this->loadView($this->viewEditor);
    }
  }

  public function update()
  {
    $this->setViewData('userData', $this->userData);
    $this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
    $this->setViewData(VDATAKEY_EDITOR_STATE, 'EDIT');
    if ($this->isPostRequest()) {
      $validateResult = $this->validate();
      if ($validateResult[0] === true) {
        // update
        $this->DAO_Transaksi->updatePendapatan($this->getFormData(), $_POST['transaction_number']);
        $this->showSuccessMessage('Data berhasil disimpan', 'sia_transaksi_pendapatan/index');
      } else {
        // show error & form
        $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
        $this->setViewData(VDATAKEY_POST_DATA, $_POST);
        $this->loadView($this->viewEditor);
      }
    } else {
      $transaksi = $this->DAO_Transaksi->getRow($this->recId);
      $this->setViewData(VDATAKEY_POST_DATA, $transaksi);
      $this->loadView($this->viewEditor);
    }
  }

  public function koreksi()
  {
    $this->setViewData('userData', $this->userData);
    $this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
    $this->setViewData(VDATAKEY_EDITOR_STATE, 'KOREKSI');
    if ($this->isPostRequest()) {
      $validateResult = $this->validate();
      if ($validateResult[0] === true) {
        /**
         * TODO insert ke tabel jurnal debet kredit masih pertanyaan sisa pembayaran dijadikan utang apa piutang??
         */
        $newTransactionNumber = $this->generateTransactionNumber();
        $this->DAO_Transaksi->insertJurnalKoreksi($_POST['transaction_number'], $newTransactionNumber);
        $this->showSuccessMessage('Data berhasil disimpan', 'sia_transaksi_pendapatan/index');
      } else {
        // show error & form
        $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
        $this->setViewData(VDATAKEY_POST_DATA, $_POST);
        $this->loadView($this->viewEditor);
      }
    } else {
      $transaksi = $this->DAO_Transaksi->getRow($this->recId);
      $this->setViewData(VDATAKEY_POST_DATA, $transaksi);
      $this->loadView($this->viewEditor);
    }
  }

  private function getFormData()
  {
    $currentAction  = $this->uri->segment(2, 0);
    $typTrx         = $_POST["trx_typ"];
    $trxDate        = strtotime($_POST["trx_date"]);
    $trxDateFormat  = date('Y-m-d', $trxDate);
    $numMonth       = date("n", $trxDate);
    $numYear        = date("Y", $trxDate);
    $postingProfile = $this->DAO_Posting_Profile->getPostingProfileByType($_POST["sub_transaction_type"]);
    $accountCode    = null;
    if ($postingProfile != null) {
      $accountCode = $postingProfile['account_credit'];
    }

    $data = [
      'transaction_date' => $trxDateFormat,
      'remark'           => $_POST["remark"],
      'transaction_type' => $_POST["sub_transaction_type"],
      'financial_year'   => $numYear,
      'financial_month'  => $numMonth,
      'account_code'     => $accountCode,
      'created_date'     => date('Y-m-d H:i:s', time())
    ];

    if ($_POST["employee_id"] > 0) {
      $data['employee_id'] = $_POST["employee_id"];
    } else {
      $data['employee_id'] = null;
    }

    if ($typTrx == 'D') {
      $data['amount_credit'] = parseFloat($_POST["debet_kredit"]);
    } else {
      $data['amount_debet'] = parseFloat($_POST["debet_kredit"]);
    }
    if (!empty($currentAction) && $currentAction == 'insert') {
      $data['modified_date'] = date('Y-m-d H:i:s', time());
    }
    return $data;
  }

  private function validate()
  {
    $errMsg        = [];
    $currentAction = $this->uri->segment(2, 0);
    if (!empty($currentAction) && $currentAction == 'insert') {
      $this->setViewData(VDATAKEY_EDITOR_STATE, 'ADD');
      $postingProfile = $this->DAO_Posting_Profile->getPostingProfileByType($_POST["sub_transaction_type"]);
      if ($postingProfile == null) {
        $errMsg[] = 'Type transaksi belum memiliki posting profile';
      }
    } else {
      $this->setViewData(VDATAKEY_EDITOR_STATE, 'EDIT');
    }
    if (count($errMsg) > 0) {
      return [false, $errMsg];
    } else {
      return [true, $errMsg];
    }
  }

  public function getMonth($year)
  {
    $monthArray = [];

    $monthArray[1]  = "Jan";
    $monthArray[2]  = "Feb";
    $monthArray[3]  = "Mar";
    $monthArray[4]  = "Apr";
    $monthArray[5]  = "Mei";
    $monthArray[6]  = "Jun";
    $monthArray[7]  = "Jul";
    $monthArray[8]  = "Ags";
    $monthArray[9]  = "Sep";
    $monthArray[10] = "Okt";
    $monthArray[11] = "Nop";
    $monthArray[12] = "Des";

    $renderMonthHtml = "";
    foreach ($monthArray as $monthVal => $month) {
      $renderMonthHtml .= '<div class="checkbox"><label><input type="checkbox" value="' . $monthVal . '"> ' . $month . ' </label></div>';
    }

    echo $renderMonthHtml;
  }

  private function generateTransactionNumber()
  {
    $year      = date('Y');
    $month     = date('m');
    $seqName   = sprintf("TRX-D-%s-%s", $year, $month);
    $seqRemark = sprintf("Sequence TRX Pendapatan tahun %s bulan %s", $year, $month);
    $seqNum    = $this->DAO_Sequence->getSequence($seqName, $seqRemark);
    $trxNo     = sprintf("TD%s-%s-%04d", $year, $month, $seqNum);
    return $trxNo;
  }

  public function get_sub_trx_type($parent, $subType)
  {
    $renderMonthHtml = "";
    if ($parent != null) {
      $subTrxType = $this->DAO_TipeTransaksi->getPembayaranTransaction('D', 3, $parent, $subType);
      if (!empty($subTrxType)) {
        foreach ($subTrxType as $subTypeVal => $subTypeView) {
          $renderMonthHtml .= '<option value="' . $subTypeVal . '">' . $subTypeView . '</option>';
        }
      }
    }
    echo $renderMonthHtml;
  }

  public function showAfterSaveMessage($invoiceUrl, $proceedUrl)
  {
    $this->setViewData(VDATAKEY_PAGE_TITLE, $this->getPageTitle());
    $this->setViewData(VDATAKEY_PAGE_ICON, $this->getPageIcon());
    $this->setViewData(VDATAKEY_INVOICE_URL, $invoiceUrl);
    $this->setViewData(VDATAKEY_ALERT_PROCEED_URL, $proceedUrl);
    $this->setViewData(VDATAKEY_ALERT_TYPE, ALERTTYPE_SUCCESS);
    $this->setViewData(VDATAKEY_ALERT_MESSAGE, "Data berhasil disimpan.");
    $this->loadPlainView('v_after_save_transaction');
  }

  public function show_invoice($transactionId)
  {
    $this->loadCompanyInfo();
    $invoiceData = $this->generateInvoiceData($transactionId);
    $this->setViewData(VDATAKEY_INVOICE_DATA, $invoiceData);
    $this->setViewData(VDATAKEY_PAGE_TITLE, $this->getPageTitle());
    $this->loadPlainView('v_transaction_invoice');
  }

  public function generateInvoiceData($transactionId)
  {
    // Prep data
    $company        = $this->getCompanyInfo();
    $trx            = $this->DAO_Transaksi->getRowByCriteria('v_list_pendapatan', ['id' => $transactionId]);
    $trxType        = $this->DAO_Transaksi->getRowByCriteria('gen_md_trx_type', ['id' => $trx['transaction_type']]);
    $trxTypeParent1 = $this->DAO_Transaksi->getRowByCriteria('gen_md_trx_type', ['id' => $trxType['parent_type']]);
    $trxTypeParent2 = $this->DAO_Transaksi->getRowByCriteria('gen_md_trx_type', ['id' => $trxTypeParent1['parent_type']]);
    $target         = null;
    $targetData     = null;
    $targetIdNumber = null;
    if (empty($trx['employee_id']) && !empty($trx['student_id'])) {
      $targetData     = $this->DAO_Siswa->getRow($trx['student_id']);
      $target         = 'S';
      $targetIdNumber = $targetData['nis'];
    }
    if (!empty($trx['employee_id']) && empty($trx['student_id'])) {
      $targetData     = $this->DAO_Transaksi->getRowByCriteria('m_employee', ['id' => $trx['employee_id']]);
      $target         = 'E';
      $targetIdNumber = $targetData['id_number'];
    }

    // Create new invoice data object
    $data = new InvoiceHeader();
    $data->setCompanyAddress($company['COMPANY_ADDRESS']);
    $data->setCompanyPhone($company['COMPANY_PHONE']);
    $data->setCompanyEmail($company['COMPANY_EMAIL']);
    $data->setNumber($this->number_generator->generateInvoiceNumber());
    $data->setDate(date($this->appConfig['dateFormatForDisplay']));
    $data->setSub($trxTypeParent2['nama']);
    $data->setTrx($trxTypeParent1['nama']);
    $data->setTarget($target);
    $data->setTargetName($targetData['name']);
    $data->setTargetIdNumber($targetIdNumber);

    // Create invoice details
    $trxAmount = 0;
    $debet     = (int)$trx['amount_debet'];
    $kredit    = (int)$trx['amount_credit'];
    if (empty($debet) && !empty($kredit)) {
      $trxAmount = $kredit;
    }
    if (!empty($debet) && empty($kredit)) {
      $trxAmount = $debet;
    }
    $details = [];
    $detail  = new InvoiceDetail();
    $detail->setTrxNumber($trx['transaction_number']);
    $detail->setTrxType($trxType['nama']);
    $detail->setTrxAmount($trxAmount);
    $detail->setTrxBalance(0);
    $detail->setRemark($trx['remark']);
    $details[] = $detail;
    $data->setDetails($details);

    return $data;
  }

}
