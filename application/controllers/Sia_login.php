<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseFunctions.php';
require_once 'BaseSessionAwareController.php';
require_once 'IBasicController.php';
require_once getFilePath('assets/3rd-party/ReCaptcha/autoload.php');

/**
 * Login controller
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
class Sia_login extends BaseSessionAwareController implements IBasicController
{

  private $viewLogin = 'v_login';
  private $viewLogout = 'v_logout';

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('DAO_Security'));
    $this->load->helper(array('ultracms'));
    $this->setViewData("reCaptchaPublicKey", $this->appConfig["reCaptchaPublicKey"]);
    $this->setViewData(VDATAKEY_PAGE_TITLE, lang('label_applogin'));
  }

  public function index()
  {
    $viewKeyErrMsg = 'errMsg';
    if ($this->isPostRequest()) {
      // get form data
      $recaptchaResp = isset($_POST['g-recaptcha-response']) ? $_POST['g-recaptcha-response'] : '';
      $username = isset($_POST['username']) ? $_POST['username'] : null;
      $password = isset($_POST['password']) ? $_POST['password'] : null;

      // validate recaptcha
      $recaptcha = new \ReCaptcha\ReCaptcha($this->appConfig["reCaptchaSecretKey"]);
      $recaptchaResult = $recaptcha->verify($recaptchaResp, $_SERVER['REMOTE_ADDR']);
      if (!$recaptchaResult->isSuccess()) {
        // display error message
        $this->setViewData($viewKeyErrMsg, lang('message_error_invalid_recaptcha'));
        $this->load->view('sia_admin/' . $this->viewLogin, $this->viewData);
        return;
      }

      // validate user
      $validate = $this->DAO_Security->isValidUser($username, $password);
      $isValidUser = $validate['isValid'];
      $userData = $validate['userData'];
      if ($isValidUser) {
        // set session & redirect to dashboard
        $userData['granted_modules'] = $this->DAO_Security->getGrantedModules($userData['id']);
        $userData['assigned_roles'] = $this->DAO_Security->getUserRoles($userData['id']);
        $userData['locale'] = $this->appConfig['appLanguage'];
        $_SESSION[SESSKEY_USERDATA] = $userData;
        redirect($this->appConfig['defaultPage']);
      } else {
        // display error message
        $this->setViewData($viewKeyErrMsg, lang('message_error_invalid_login_credentials'));
        $this->load->view('sia_admin/' . $this->viewLogin, $this->viewData);
      }
    } else {
      $this->load->view('sia_admin/' . $this->viewLogin, $this->viewData);
    }
  }

  public function logout()
  {
    unset($_SESSION[SESSKEY_USERDATA]);
    $this->load->view('sia_admin/' . $this->viewLogout, $this->viewData);
  }

}
