<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseBackEndController.php';
require_once 'IBasicController.php';
require_once 'ITransactionController.php';
require_once APPPATH . 'models/InvoiceHeader.php';
require_once APPPATH . 'models/InvoiceDetail.php';

/**
 * Students controller
 *
 * @author <a href="mailto:agung3ka@gmail.com">Turah Eka</a>
 */
class Sia_transaksi_spp extends BaseBackEndController implements IBasicController
{

  var $recId;
  var $viewList           = 'v_transaksi_spp_list';
  var $viewEditor         = 'v_transaksi_spp_editor';
  var $viewEditModeEditor = 'v_transaksi_spp_editmode_editor';

  public function __construct()
  {
    parent::__construct();
    $this->load->model(['DAO_Transaksi_Spp', 'DAO_Edu', 'DAO_Transaksi', 'DAO_Siswa', 'DAO_Lov', 'DAO_Sequence',
                        'DAO_TipeTransaksi', 'DAO_Posting_Profile', 'DAO_System_Parameter']);
    $this->recId = $this->getRecId();
    $this->load->library(['Number_generator']);
    $this->lang->load('sia_transaksi_spp', $this->getCurrentLocale());
    $this->setViewData('DAO_Siswa', $this->DAO_Siswa);
    $this->setViewData('DAO_TipeTransaksi', $this->DAO_TipeTransaksi);
    $this->setViewData('DAO_Lov', $this->DAO_Lov);
    $this->setViewData('DAO_Posting_Profile', $this->DAO_Posting_Profile);
    $this->setViewData('DAO_System_Parameter', $this->DAO_System_Parameter);
    $this->setViewData('DAO_Edu', $this->DAO_Edu);
    $this->setViewData('DAO_Transaksi_Spp', $this->DAO_Transaksi_Spp);
  }

  function getPageTitle()
  {
    return lang('trxspp_page_title');
  }

  function getJsFiles()
  {
    $js = ['assets/themes/sb-admin/js/moment-with-locales.min.js',
           'assets/themes/sb-admin/js/bootstrap-datetimepicker.min.js'];
    if ($this->isIndexAction()) {
      $js[] = 'assets/js/ultracms_sia_transaksi_spp_list.min.js';
    } else {
      $js[] = 'assets/js/ultracms_comp_SingleResultStudentLookupDialog.min.js';
      $js[] = 'assets/themes/sb-admin/js/jquery.number.min.js';
      $js[] = 'assets/js/ultracms_sia_transaksi_spp_editor.min.js';
    }
    return $js;
  }

  function getCssFiles()
  {
    return ['assets/themes/sb-admin/css/bootstrap-datetimepicker.min.css'];
  }

  function getPageIcon()
  {
    return 'fa-usd';
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaTransaksiSppList.init()';
    } else {
      return 'ultracms.siaTransaksiSppEditor.init()';
    }
  }

  public function index()
  {
    $nis                   = isset($_GET['nis']) ? $_GET['nis'] : '';
    $nama                  = isset($_GET['nama']) ? $_GET['nama'] : '';
    $remark                = isset($_GET['remark']) ? $_GET['remark'] : '';
    $transaction_date_from = isset($_GET['transaction_date_from']) ? $_GET['transaction_date_from'] : '';
    $transaction_date_to   = isset($_GET['transaction_date_to']) ? $_GET['transaction_date_to'] : '';

    $filters = [
      [QRYFILTER_FIELD => 'nis', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $nis],
      [QRYFILTER_FIELD => 'remark', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $remark],
      [QRYFILTER_FIELD => 'name', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $nama]
    ];

    if (!empty($transaction_date_from)) {
      if (!empty($transaction_date_to)) {
        $filters[] = [QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_BETWEEN, QRYFILTER_VALUE => [$transaction_date_from, $transaction_date_to]];
      } else {
        $filters[] = [QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_BETWEEN, QRYFILTER_VALUE => [$transaction_date_from, $transaction_date_from]];
      }
    }

    $rs          = $this->DAO_Transaksi_Spp->getList($filters, null, $this->getFirstRowPos(), $this->pageSize);
    $recordCount = $this->DAO_Transaksi_Spp->getListCount($filters);
    $this->setViewData(VDATAKEY_LIST_PAGE, $this->page);
    $this->setViewData(VDATAKEY_LIST_PAGE_SIZE, $this->pageSize);
    $this->setViewData(VDATAKEY_POST_DATA, $_GET);
    $this->setViewData(VDATAKEY_ROWNUM_START, $this->getFirstRowPos());
    $this->setViewData(VDATAKEY_RECORDSET, $rs);
    $this->setViewData(VDATAKEY_RECORDCOUNT, $recordCount);
    $this->loadView($this->viewList);
  }

  public function insert()
  {
    $this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
    $this->setViewData(VDATAKEY_EDITOR_STATE, 'ADD');
    if ($this->isPostRequest()) {
      $validateResult = $this->validate();
      if ($validateResult[0] === true) {
        // save
        /*
                 * cari bulan yang sudah dibayar ditahun inputan
                 */
        $bulanDibayar = $this->DAO_Transaksi->getBulanDibayarByStudentId($_POST["student_id"], $_POST["financial_year"]);

        /*
                 * buat financial month yang diselect saat insert selain yang sudah terbayar
                 */
        $selectedFinancialMonth = "";
        foreach ($_POST["chkBulanDibayar"] as $financialMonth) {
          $isFinancialMontExist = false;
          foreach ($bulanDibayar as $row) {
            if ($financialMonth == $row->financial_month) {
              $isFinancialMontExist = true;
              break;
            }
          }
          if (!$isFinancialMontExist) {
            if ($selectedFinancialMonth == "") {
              $selectedFinancialMonth .= $financialMonth;
            } else {
              $selectedFinancialMonth .= "," . $financialMonth;
            }
          }

        }
        $spp                       = $this->getForm($selectedFinancialMonth);
        $spp['transaction_number'] = $this->generateTransactionNumberSPP("TR-SPP", "TDS");
        /*
         * simpan transaksi spp ke tabel transaksi SPP
         */
        $id = $this->DAO_Transaksi_Spp->insert($spp);

        /*
         * simpan transaksi spp ke tabel transaksi debet kredit
         */
        foreach ($_POST["chkBulanDibayar"] as $financialMonthTrx) {
          $isFinancialMontTrxExist = false;
          foreach ($bulanDibayar as $row) {
            if ($financialMonthTrx == $row->financial_month) {
              $isFinancialMontTrxExist = true;
              break;
            }
          }
          if (!$isFinancialMontTrxExist) {
            $trx                       = $this->getFormDataSpp($financialMonthTrx);
            $trx['transaction_number'] = $this->generateTransactionNumber();
            $this->DAO_Transaksi->insertSpp($trx);
          }

        }

        /*$this->setViewData(VDATAKEY_ALERT_TYPE, ALERTTYPE_SUCCESS);
                $this->setViewData(VDATAKEY_ALERT_MESSAGE, 'Data berhasil disimpan');
                $this->setViewData(VDATAKEY_ALERT_PROCEED_URL, site_url('sia_transaksi_spp/index'));
                $this->loadView($this->viewAfterSaveMessage);*/
        $this->showAfterSaveMessage(site_url("sia_transaksi_spp/show_invoice/{$id}"),
                                    site_url('sia_transaksi_spp/index'));
      } else {
        // show error & form
        $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
        $this->setViewData(VDATAKEY_POST_DATA, $_POST);
        $this->loadView($this->viewEditor);
      }
    } else {
      $this->loadView($this->viewEditor);
    }
  }

  public function update()
  {
    $this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
    $this->setViewData(VDATAKEY_EDITOR_STATE, 'EDIT');
    if ($this->isPostRequest()) {
      $validateResult = $this->validate();
      if ($validateResult[0] === true) {
        // update

        $this->DAO_Transaksi_Spp->update($this->getFormDataEditeSpp(), $this->recId);
        $this->showSuccessMessage('Data berhasil disimpan', 'sia_transaksi_spp/index');
      } else {
        // show error & form
        $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
        $this->setViewData(VDATAKEY_POST_DATA, $_POST);
        $this->loadView($this->viewEditor);
      }
    } else {
      $transaksi = $this->DAO_Transaksi_Spp->getRow($this->recId);
      $this->setViewData(VDATAKEY_POST_DATA, $transaksi);
      $this->loadView($this->viewEditor);
    }
  }

  private function getFormDataEditeSpp()
  {
    $data           = $this->DAO_Transaksi_Spp->getRow($this->recId);
    $data['remark'] = $_POST["remark"];
    return $data;
  }

  private function getForm($financialMonth)
  {
    $currentAction = $this->uri->segment(2, 0);
    $trxDate       = strtotime($_POST["transaction_date"]);
    $trxDateFormat = date('Y-m-d', $trxDate);
    $amount        = parseFloat($_POST["real_amount"]) - parseFloat($_POST["diskon"]);
    $data          = [
      'student_id'               => $_POST["student_id"],
      'real_amount'              => parseFloat($_POST["real_amount"]),
      'amount'                   => $amount,
      'selected_financial_month' => $financialMonth,
      'financial_year'           => $_POST["financial_year"],
      'diskon'                   => parseFloat($_POST["diskon"]),
      'total_amount'             => parseFloat($_POST["total_amount"]),
      'transaction_date'         => $trxDateFormat,
      'remark'                   => $_POST["remark"],
      'created_date'             => date('Y-m-d H:i:s', time())
    ];
    if (!empty($currentAction) && $currentAction == 'insert') {
      $data['modified_date'] = date('Y-m-d H:i:s', time());
    }
    return $data;
  }

  private function getFormDataSpp($financialMonth)
  {
    $currentAction  = $this->uri->segment(2, 0);
    $trxDate        = strtotime($_POST["transaction_date"]);
    $trxDateFormat  = date('Y-m-d', $trxDate);
    $strMonth       = date("F", mktime(0, 0, 0, $financialMonth, 10));
    $postingProfile = $this->DAO_Posting_Profile->getPostingProfileByType($this->DAO_System_Parameter->getValue('TRX_TYPE_SPP'));
    $accountCode    = null;
    if ($postingProfile != null) {
      $accountCode = $postingProfile['account_credit'];
    }
    $student = $this->DAO_Siswa->getRow($_POST["student_id"]);
    $amount  = parseFloat($_POST["real_amount"]) - parseFloat($_POST["diskon"]);
    $data    = [
      'student_id'       => $_POST["student_id"],
      'amount_credit'    => $amount,
      'transaction_date' => $trxDateFormat,
      'remark'           => 'Bayar Spp NIS : ' . $student['nis'] . '/' . $student['name'] . ' Bulan ' . $strMonth . ' Tahun ' . $_POST["financial_year"],
      'transaction_type' => $this->DAO_System_Parameter->getValue('TRX_TYPE_SPP'),
      'financial_year'   => $_POST["financial_year"],
      'financial_month'  => $financialMonth,
      'account_code'     => $accountCode,
      'created_date'     => date('Y-m-d H:i:s', time())
    ];

    if (!empty($currentAction) && $currentAction == 'insert') {
      $data['modified_date'] = date('Y-m-d H:i:s', time());
    }
    return $data;
  }

  private function validate()
  {
    $errMsg        = [];
    $currentAction = $this->uri->segment(2, 0);
    if (!empty($currentAction) && $currentAction == 'insert') {
      $this->setViewData(VDATAKEY_EDITOR_STATE, 'ADD');
      $postingProfile = $this->DAO_Posting_Profile->getPostingProfileByType($this->DAO_System_Parameter->getValue('TRX_TYPE_SPP'));
      if ($postingProfile == null) {
        $errMsg[] = 'Type transaksi belum memiliki posting profile';
      } else {
        if (!isset($_POST["chkBulanDibayar"])) {
          $errMsg[] = 'Pilih Bulan terlebih dahulu';
        }
      }
    } else {
      $this->setViewData(VDATAKEY_EDITOR_STATE, 'EDIT');
    }

    if (count($errMsg) > 0) {
      return [false, $errMsg];
    } else {
      return [true, $errMsg];
    }
  }

  public function get_month($studentId, $year)
  {
    $renderMonthHtml = "";
    if ($studentId > 0 && $year > 0) {
      $bulanDibayar = $this->DAO_Transaksi->getBulanDibayarByStudentId($studentId, $year);

      $monthArray     = [];
      $monthArray[1]  = "Jan";
      $monthArray[2]  = "Feb";
      $monthArray[3]  = "Mar";
      $monthArray[4]  = "Apr";
      $monthArray[5]  = "Mei";
      $monthArray[6]  = "Jun";
      $monthArray[7]  = "Jul";
      $monthArray[8]  = "Ags";
      $monthArray[9]  = "Sep";
      $monthArray[10] = "Okt";
      $monthArray[11] = "Nop";
      $monthArray[12] = "Des";

      foreach ($monthArray as $monthVal => $month) {
        $renderMonthHtml .= '<div class="checkbox"><label><input type="checkbox" name="chkBulanDibayar[]" value="' . $monthVal . '"';
        foreach ($bulanDibayar as $row) {
          if ($monthVal == $row->financial_month) {
            $renderMonthHtml .= ' disabled checked ';
          }
        }
        $renderMonthHtml .= ' /> ' . $month . ' </label></div>';
      }
    }
    echo $renderMonthHtml;
  }

  /*public function getMonthEditor($financialMonth){
        $renderMonthHtml = "";
        if($studentId >0 && $year > 0){

            $monthArray = array();
            $monthArray[1] = "Jan";
            $monthArray[2] = "Feb";
            $monthArray[3] = "Mar";
            $monthArray[4] = "Apr";
            $monthArray[5] = "Mei";
            $monthArray[6] = "Jun";
            $monthArray[7] = "Jul";
            $monthArray[8] = "Ags";
            $monthArray[9] = "Sep";
            $monthArray[10] = "Okt";
            $monthArray[11] = "Nop";
            $monthArray[12] = "Des";

            $monthArray = explode(',', $financialMonth);
            foreach ($monthArray as $monthVal => $month){
                $renderMonthHtml .= '<div class="checkbox"><label><input type="checkbox" name="chkBulanDibayar[]" value="'.$monthVal.'"';

                foreach ($monthArray as $financialMonthVal ){
                    if($monthVal == $financialMonthVal){
                        $renderMonthHtml .= ' disabled checked';
                    }
                }
                $renderMonthHtml .=' /> '.$month.' </label></div>';
            }
        }
        echo $renderMonthHtml;
    }*/

  private function generateTransactionNumberSPP($varSeq, $var)
  {
    $year  = date('Y');
    $month = date('m');
    /*TRX-SPP*/
    $seqName   = sprintf($varSeq . "-%s-%s", $year, $month);
    $seqRemark = sprintf("Sequence TRX SPP tahun %s bulan %s", $year, $month);
    $seqNum    = $this->DAO_Sequence->getSequence($seqName, $seqRemark);
    $trxNo     = sprintf($var . "%s-%s-%04d", $year, $month, $seqNum);
    return $trxNo;
  }

  private function generateTransactionNumber()
  {
    $year      = date('Y');
    $month     = date('m');
    $seqName   = sprintf("TRX-D-%s-%s", $year, $month);
    $seqRemark = sprintf("Sequence TRX Pendapatan tahun %s bulan %s", $year, $month);
    $seqNum    = $this->DAO_Sequence->getSequence($seqName, $seqRemark);
    $trxNo     = sprintf("TD%s-%s-%04d", $year, $month, $seqNum);
    return $trxNo;
  }

  public function get_data_class_by_id($id)
  {
    $siswa = $this->DAO_Siswa->getRow($id);

    $dataKelas = $this->DAO_Edu->getClassById($siswa['classroom_id']);
    $dataEdu   = $this->DAO_Edu->getGradeById($dataKelas['grade_id']);
    if (!is_null($dataKelas) && !is_null($dataEdu)) {
      echo $dataEdu['name'] . " - " . $dataKelas['name'];
    } else {
      echo "";
    }
  }

  public function get_financial_month_terbayar($studentId, $year)
  {
    $bulanDibayar           = $this->DAO_Transaksi->getBulanDibayarByStudentId($studentId, $year);
    $selectedFinancialMonth = "";
    foreach ($bulanDibayar as $row) {
      if ($selectedFinancialMonth == "") {
        $selectedFinancialMonth .= $row->financial_month;
      } else {
        $selectedFinancialMonth .= "," . $row->financial_month;
      }
    }
    echo $selectedFinancialMonth;
  }

  public function show_invoice($transactionId)
  {
    $this->loadCompanyInfo();
    $invoiceData = $this->generateInvoiceData($transactionId);
    $this->setViewData(VDATAKEY_INVOICE_DATA, $invoiceData);
    $this->setViewData(VDATAKEY_PAGE_TITLE, $this->getPageTitle());
    $this->loadPlainView('v_transaction_invoice');
  }

  public function generateInvoiceData($transactionId)
  {
    // Prep data
    $company = $this->getCompanyInfo();

    /*$trx = $this->DAO_Transaksi->getRowByCriteria('v_list_pendapatan_siswa', array('id' => $transactionId));
    $trxType = $this->DAO_Transaksi->getRowByCriteria('gen_md_trx_type', array('id' => $trx['transaction_type']));
    $trxTypeParent1 = $this->DAO_Transaksi->getRowByCriteria('gen_md_trx_type', array('id' => $trxType['parent_type']));
    $trxTypeParent2 = $this->DAO_Transaksi->getRowByCriteria('gen_md_trx_type', array('id' => $trxTypeParent1['parent_type']));*/

    $trx = $this->DAO_Transaksi_Spp->getRow($transactionId);

    $target         = null;
    $targetData     = null;
    $targetIdNumber = null;
    if (!empty($trx['student_id'])) {
      $targetData     = $this->DAO_Siswa->getRow($trx['student_id']);
      $target         = 'S';
      $targetIdNumber = $targetData['nis'];
    }

    // Create new invoice data object
    $data = new InvoiceHeader();
    $data->setCompanyAddress($company['COMPANY_ADDRESS']);
    $data->setCompanyPhone($company['COMPANY_PHONE']);
    $data->setCompanyEmail($company['COMPANY_EMAIL']);
    $data->setNumber($this->number_generator->generateInvoiceNumber());
    $data->setDate(date($this->appConfig['dateFormatForDisplay']));
    $data->setSub('PENDAPATAN');
    $data->setTrx('Pendapata SPP Siswa');
    $data->setTarget($target);
    $data->setTargetName($targetData['name']);
    $data->setTargetIdNumber($targetIdNumber);

    // Create invoice details
    /*$trxAmount = 0;
    if(empty($trx['amount_debet']) && !empty($trx['amount_credit'])){
        $trxAmount = $trx['amount_credit'];
    }
    if(!empty($trx['amount_debet']) && empty($trx['amount_credit'])){
        $trxAmount = $trx['amount_debet'];
    }
    var_dump($trxAmount);
    die();*/
    $details = [];
    $detail  = new InvoiceDetail();
    $detail->setTrxNumber($trx['transaction_number']);
    $detail->setTrxType('Pembayaran SPP');
    $detail->setTrxAmount($trx['total_amount']);
    $detail->setTrxBalance(0);
    $detail->setRemark($trx['remark']);
    $details[] = $detail;
    $data->setDetails($details);

    return $data;
  }

  public function showAfterSaveMessage($invoiceUrl, $proceedUrl)
  {
    $this->setViewData(VDATAKEY_PAGE_TITLE, $this->getPageTitle());
    $this->setViewData(VDATAKEY_PAGE_ICON, $this->getPageIcon());
    $this->setViewData(VDATAKEY_INVOICE_URL, $invoiceUrl);
    $this->setViewData(VDATAKEY_ALERT_PROCEED_URL, $proceedUrl);
    $this->setViewData(VDATAKEY_ALERT_TYPE, ALERTTYPE_SUCCESS);
    $this->setViewData(VDATAKEY_ALERT_MESSAGE, "Data berhasil disimpan.");
    $this->loadPlainView('v_after_save_transaction');
  }
}
