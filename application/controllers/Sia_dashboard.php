<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseBackEndController.php';
require_once 'IBasicController.php';

/**
 * Dashboard controller
 *
 * @author <a href="mailto:agung3ka@gmail.com">Turah Eka</a>
 */
class Sia_dashboard extends BaseBackEndController implements IBasicController
{
	
	var $defaultView = 'v_dashboard';
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array('DAO_Siswa'));
		$this->loadCompanyInfo();
  }

  function getPageTitle()
  {
    return 'Dashboard';
  }

  function getPageIcon()
  {
    return 'fa-dashboard';
  }

  function getJsFiles()
  {
    return array();
  }

  function getJsInitFunction()
  {
    return '';
  }

  function getCssFiles()
  {
    return array();
  }

  public function index()
  {
	  $this->setViewData('daoSiswa', $this->DAO_Siswa);
		$this->loadView($this->defaultView);
	}
	
}
