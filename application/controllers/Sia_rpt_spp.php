<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseReportController.php';
require_once 'IReportController.php';

/**
 * Controller laporan pembayaran SPP
 *
 * @author <a href="mailto:agung3ka@gmail.com">Turah Eka</a>
 */
class Sia_rpt_spp extends BaseReportController implements IReportController
{

  const V_RPT_FORM = 'v_rpt_spp_form';

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('DAO_Siswa', 'DAO_Transaksi_Spp'));
  }

  function getPageTitle()
  {
    return 'Laporan Pembayaran SPP';
  }

  function getJsFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/themes/sb-admin/js/moment-with-locales.min.js',
        'assets/themes/sb-admin/js/bootstrap-datetimepicker.min.js',
        'assets/js/ultracms_sia_transaksi_pendapatan_list.min.js');
    } else {
      return array();
    }
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaTransaksiPendapatanList.init()';
    } else {
      return '';
    }
  }

  function getCssFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/themes/sb-admin/css/bootstrap-datetimepicker.min.css');
    } else {
      return array();
    }
  }

  public function index()
  {
    $this->setViewData('DAO_Siswa', $this->DAO_Siswa);
    $this->setViewData('DAO_Transaksi_Spp', $this->DAO_Transaksi_Spp);
    $this->loadReportForm(self::V_RPT_FORM);
  }

  public function generate_report()
  {
    $studentId = isset($_POST['student_id']) ? $_POST['student_id'] : '';
    $month = isset($_POST['financial_month']) ? $_POST['financial_month'] : '';
    $year = isset($_POST['financial_year']) ? $_POST['financial_year'] : '';
    $transaction_date_from = isset($_POST['transaction_date_from']) ? $_POST['transaction_date_from'] : '';
    $transaction_date_to = isset($_POST['transaction_date_to']) ? $_POST['transaction_date_to'] : '';
    $filters = array();

    if ($year != '') {
      $filters[] = array(QRYFILTER_FIELD => 'financial_year', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $year);
    }

    if (!empty($studentId) || $studentId != 0) {
      $filters[] = array(QRYFILTER_FIELD => 'student_id', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $studentId);
    }

    if (!empty($month) || $month != 0) {
      $filters[] = array(QRYFILTER_FIELD => 'financial_month', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $month);
    }

    if (!empty($transaction_date_from)) {
      if (!empty($transaction_date_to)) {
        $filters[] = array(QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_BETWEEN, QRYFILTER_VALUE => array($transaction_date_from, $transaction_date_to));
      } else {
        $filters[] = array(QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_BETWEEN, QRYFILTER_VALUE => array($transaction_date_from, $transaction_date_from));
      }
    }

    $sql = $this->DAO_Transaksi_Spp->getRptSppQuery($filters);

    $qry = $this->db->query($sql);
    $this->setReportGenField(RPTGEN_HEADER, $this->getPageTitle());
    $this->setReportGenField(RPTGEN_MYSQL_RESOURCE, $qry);
    $this->setReportGenField(RPTGEN_DATE_FIELDS, array('Tanggal Transaksi' => $this->appConfig['dateFormatForDisplay']));
    $this->setReportGenField(RPTGEN_NUMBER_FIELDS, array("Jumlah"));
    $this->loadReport();
  }

}
