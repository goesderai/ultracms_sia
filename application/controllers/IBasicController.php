<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Back-end controller interface
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
interface IBasicController
{
	function __construct();
	function index();
}
