<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'BaseReportController.php';
require_once 'IReportController.php';

/**
 * Controller laporan Master Karyawan
 *
 * @author <a href="mailto:agung3ka@gmail.com">Turah Eka</a>
 */
class Sia_rpt_employee extends BaseReportController implements IReportController
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('DAO_Employee', 'DAO_Lov'));
  }

  function getPageTitle()
  {
    return 'Laporan Karyawan';
  }

  function getJsFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/js/ultracms_sia_employee_list.min.js');
    } else {
      return array();
    }
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaEmployeeList.init()';
    } else {
      return '';
    }
  }

  function getCssFiles()
  {
    return array();
  }

  public function index()
  {
    $this->setViewData('DAO_Employee', $this->DAO_Employee);
    $this->setViewData('DAO_Lov', $this->DAO_Lov);
    $this->loadReportForm('v_rpt_employee_form');
  }

  public function generate_report()
  {
    $nama = isset($_POST['nama']) ? $_POST['nama'] : '';
    $idNumber = isset($_POST['id_number']) ? $_POST['id_number'] : '';
    $alamat = isset($_POST['alamat']) ? $_POST['alamat'] : '';
    $gender = isset($_POST['gender']) ? $_POST['gender'] : null;
    $filters = array();

    if ($nama != '') {
      $filters[] = array(
        QRYFILTER_FIELD => 'name',
        QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE,
        QRYFILTER_VALUE => $nama
      );
    }

    if ($idNumber != '') {
      $filters[] = array(
        QRYFILTER_FIELD => 'id_number',
        QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE,
        QRYFILTER_VALUE => $idNumber
      );
    }

    if ($alamat != '') {
      $filters[] = array(
        QRYFILTER_FIELD => 'address',
        QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE,
        QRYFILTER_VALUE => $alamat
      );
    }

    if (!empty($gender)) {
      $filters[] = array(
        QRYFILTER_FIELD => 'gender',
        QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL,
        QRYFILTER_VALUE => $gender
      );
    }

    $sql = $this->DAO_Employee->getRptEmployeeQuery($filters);

    $qry = $this->db->query($sql);
    $this->setReportGenField(RPTGEN_HEADER, $this->getPageTitle());
    $this->setReportGenField(RPTGEN_MYSQL_RESOURCE, $qry);
    $this->setReportGenField(RPTGEN_DATE_FIELDS, array(
      'Tanggal Transaksi' => $this->appConfig['dateFormatForDisplay']
    ));
    $this->loadReport();
  }
}
