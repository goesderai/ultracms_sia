<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseController.php';

/**
 * BaseSessionAwareController.php
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
abstract class BaseSessionAwareController extends BaseController
{
  protected $userData;

  public function __construct()
  {
    parent::__construct();
    $this->load->library(array('session'));

    // Validate session
    $currentController = $this->uri->segment(1, 0);
    if (!empty($currentController) && $currentController !== 'sia_login') {
      if (isset($_SESSION[SESSKEY_USERDATA]) && !empty($_SESSION[SESSKEY_USERDATA])) {
        $this->userData = $_SESSION[SESSKEY_USERDATA];
      } else {
        redirect('sia_login');
      }
    }

    // load common language file
    $this->lang->load(array('sia_common_labels', 'sia_common_messages'), $this->getCurrentLocale());
  }

  protected function getCurrentLocale()
  {
    if (isset($_SESSION[SESSKEY_USERDATA])) {
      return isset($_SESSION[SESSKEY_USERDATA]['locale']) ? $_SESSION[SESSKEY_USERDATA]['locale'] : $this->config->item('language');
    } else {
      return $this->config->item('language');
    }
  }

  protected function setCurrentLocale($locale)
  {
    $_SESSION[SESSKEY_USERDATA]['locale'] = $locale;
  }

}
