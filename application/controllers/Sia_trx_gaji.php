<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseBackEndController.php';
require_once 'IBasicController.php';

/**
 * Pembayaran controller
 *
 * @author <a href="mailto:agung3ka@gmail.com">Turah Eka</a>
 */
class Sia_trx_gaji extends BaseBackEndController implements IBasicController
{
	
	var $recId;
	var $viewList = 'v_transaksi_pembayaran_gaji_list';
	var $viewEditor = 'v_transaksi_pembayaran_gaji_editor';
	
	public function __construct(){
		parent::__construct();
		
		$this->load->model(array('DAO_Transaksi','DAO_System_Parameter','DAO_Salary','DAO_Employee','DAO_Transaksi_Gaji',
			'DAO_Sequence', 'DAO_TipeTransaksi', 'DAO_Posting_Profile'));
		$this->recId = $this->getRecId();
		$this->setViewData('DAO_Transaksi_Gaji', $this->DAO_TipeTransaksi);
		$this->setViewData('DAO_Posting_Profile', $this->DAO_Posting_Profile);
		$this->setViewData('DAO_Employee', $this->DAO_Employee);
		$this->setViewData('DAO_TipeTransaksi', $this->DAO_TipeTransaksi);
	}

  function getPageTitle()
  {
    return 'Transaksi Pembayaran Gaji Karyawan';
  }

  function getJsFiles()
  {
    $js = array('assets/themes/sb-admin/js/moment-with-locales.min.js',
      'assets/themes/sb-admin/js/bootstrap-datetimepicker.min.js',
      'assets/js/ultracms_sia_transaksi_pembayaran_gaji_editor.min.js');
    if ($this->isIndexAction()) {
      $js[] = 'assets/js/ultracms_sia_transaksi_pembayaran_gaji_list.min.js';
    } else {
      $js[] = 'assets/js/ultracms_sia_transaksi_pembayaran_gaji_editor.min.js';
    }
    return $js;
  }

  function getCssFiles()
  {
    return array('assets/themes/sb-admin/css/bootstrap-datetimepicker.min.css');
  }

  function getPageIcon()
  {
    return 'fa-usd';
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaTransaksiPembayaranGajiList.init()';
    } else {
      return 'ultracms.siaTransaksiPembayaranGajiEditor.init()';
    }
  }

  public function index()
  {
			$transaction_date_from = isset($_GET['transaction_date_from'])? $_GET['transaction_date_from'] : '';
			$transaction_date_to = isset($_GET['transaction_date_to'])? $_GET['transaction_date_to'] : '';
			$remark = isset($_GET['remark'])? $_GET['remark'] : '';
			$employeeId = isset($_GET['employee_id'])? $_GET['employee_id'] : '';
			$filters = array(
				array(QRYFILTER_FIELD => 'remark', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $remark)
			);
			if(!empty($employeeId) || $employeeId != 0){
				$filters[] = array(QRYFILTER_FIELD => 'employee_id', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $employeeId);
			}
			if(!empty($transaction_date_from)){
				if(!empty($transaction_date_to)){
					$filters[] = array(QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_BETWEEN, QRYFILTER_VALUE => array($transaction_date_from, $transaction_date_to));
				}else{
					$filters[] = array(QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_BETWEEN, QRYFILTER_VALUE => array($transaction_date_from, $transaction_date_from));
				}
			}
			
			$rs = $this->DAO_Transaksi_Gaji->getList($filters, null, $this->getFirstRowPos(), $this->pageSize);
			$recordCount = $this->DAO_Transaksi_Gaji->getListCount($filters);
			$this->setViewData(VDATAKEY_LIST_PAGE, $this->page);
			$this->setViewData(VDATAKEY_LIST_PAGE_SIZE, $this->pageSize);
			$this->setViewData(VDATAKEY_POST_DATA, $_GET);
			$this->setViewData(VDATAKEY_ROWNUM_START, $this->getFirstRowPos());
			$this->setViewData(VDATAKEY_RECORDSET, $rs);
			$this->setViewData(VDATAKEY_RECORDCOUNT, $recordCount);
			$this->loadView($this->viewList);
			
	}
	
	public function insert(){
		$this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
		$this->setViewData(VDATAKEY_EDITOR_STATE, 'ADD');
		if($this->isPostRequest()){
			$validateResult = $this->validate();
			if($validateResult[0]===true){
				// save
				$trxNumber = $this->generateTransactionNumber();
				$trxGaji = $this->getFormDataGaji();
				$trxGaji['transaction_number'] = $trxNumber;
				/*
				 * insert ke tabel gaji
				 */
				$this->DAO_Transaksi_Gaji->insert($trxGaji);
				$trx = $this->getFormData();
				$trx['transaction_number'] = $trxNumber;
				/*
				 * insert ke tabel trx
				 */
				$this->DAO_Transaksi->insert($trx);
				
				if($_POST["bayar_kasbon"] > 0){
					$this->DAO_Transaksi->insert($this->addKasbonTrx($trx));
				}
				
				$this->setViewData(VDATAKEY_ALERT_TYPE, ALERTTYPE_SUCCESS);
				$this->setViewData(VDATAKEY_ALERT_MESSAGE, 'Data berhasil disimpan');
				$this->setViewData(VDATAKEY_ALERT_PROCEED_URL, site_url('sia_trx_gaji/index'));
				$this->loadView($this->viewAfterSaveMessage);
			}else{
				// show error & form
				$this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
				$this->setViewData(VDATAKEY_POST_DATA, $_POST);
				$this->loadView($this->viewEditor);
			}
		}else{
			
			$this->loadView($this->viewEditor);
		}
	}
	
	public function update(){
		$this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
		$this->setViewData(VDATAKEY_EDITOR_STATE, 'EDIT');
		if($this->isPostRequest()){
			$validateResult = $this->validate();
			if($validateResult[0]===true){
				// update
				$this->DAO_Transaksi_Gaji->update($this->getFormDataEditeGaji(), $this->recId);
				$this->setViewData(VDATAKEY_ALERT_TYPE, ALERTTYPE_SUCCESS);
				$this->setViewData(VDATAKEY_ALERT_MESSAGE, 'Data berhasil disimpan');
				$this->setViewData(VDATAKEY_ALERT_PROCEED_URL, site_url('sia_trx_gaji/index'));
				$this->loadView($this->viewAfterSaveMessage);
			}else{
				// show error & form
				$this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
				$this->setViewData(VDATAKEY_POST_DATA, $_POST);
				$this->loadView($this->viewEditor);
			}
		}else{
			$transaksi = $this->DAO_Transaksi_Gaji->getRow($this->recId);
			$this->setViewData(VDATAKEY_POST_DATA, $transaksi);
			$this->loadView($this->viewEditor);
		}
	}
	
	private function addKasbonTrx($trx){
		$data = $trx;
		$employee = $this->DAO_Employee->getRow($_POST["employee_id"]);
		$transaction_type = $this->DAO_System_Parameter->getValue('TRX_TYPE_KASBON_D');
		$postingProfile = $this->DAO_Posting_Profile->getPostingProfileByType($transaction_type);
		$accountCode = null;
		$trxDate = strtotime($_POST["trx_date"]);
		$trxDateFormat = date('Y-m-d', $trxDate);
		if($postingProfile != null){
			$accountCode = $postingProfile['account_credit'];
		}
		$namaKaryawan = "";
		if($employee !=null){
			$namaKaryawan = $employee['name'];
		}
		$data['transaction_number'] = $this->generateTransactionNumberKasbon();
		$data['transaction_date'] = $trxDateFormat;
		$data['transaction_type'] = $transaction_type;
		$data['account_code'] = $accountCode;
		$data['amount_debet'] = 0;
		$data['amount_credit'] = $_POST["bayar_kasbon"];
		$data['remark'] = "Pengembalian Pinjaman ".$namaKaryawan;
		return $data;
	}
	
	private function getFormData() {
		$currentAction = $this->uri->segment(2, 0);
		$trxDate = strtotime($_POST["trx_date"]);
		$trxDateFormat = date('Y-m-d', $trxDate);
		$numMonth = date("n", $trxDate);
		$numYear = date("Y", $trxDate);
		
		$transaction_type = $_POST["transaction_type"];
		$postingProfile = $this->DAO_Posting_Profile->getPostingProfileByType($transaction_type);
		
		$accountCode = null;
		if($postingProfile != null){
			$accountCode = $postingProfile['account_debet'];
		}
		$data = array(
			'transaction_date' => $trxDateFormat,
			'remark' => $_POST["remark"],
			'transaction_type' => $transaction_type,
			'financial_year' => $numYear,
			'financial_month' => $numMonth,
			'account_code' => $accountCode,
			'created_date'=> date('Y-m-d H:i:s', time()),
			'employee_id' => $_POST["employee_id"],
			'amount_debet' => $_POST["total_gaji"],
			'transaction_type' => $_POST["transaction_type"]
		);
		if(!empty($currentAction) && $currentAction=='insert'){
			$data['modified_date'] = date('Y-m-d H:i:s', time());
		}
		return $data;
	}
	
	private function getFormDataEditeGaji() {
		$data = $this->DAO_Transaksi_Gaji->getRow($this->recId);
		$data['remark'] = $_POST["remark"];
		return $data;
	}
	
	private function getFormDataGaji(){
		$dataTrx = $this->getFormData();
		$trxDate = strtotime($_POST["trx_date"]);
		$trxDateFormat = date('Y-m-d', $trxDate);
		$dataGaji = array(
			'employee_id' => $dataTrx['employee_id'],
			'financial_month' => $dataTrx['financial_month'],
			'financial_year' => $dataTrx['financial_year'],
			'gaji_pokok' => $_POST["gaji_pokok"],
			'harian' => $_POST["harian"],
			'tambahan' => $_POST["tambahan"],
			'total_gaji' => $_POST["total_gaji"],
			'total_kasbon' => $_POST["total_kasbon"],
			'bayar_kasbon' => $_POST["bayar_kasbon"],
			'total_gaji_dibayar' => $_POST["total_gaji_dibayar"],
			'transaction_date' => $trxDateFormat,
			'remark' => $dataTrx['remark'],
			'created_date' => $dataTrx['created_date'],
			'modified_date' => $dataTrx['modified_date'],
			'transaction_type' => $_POST["transaction_type"]
		);
		return $dataGaji;
	}
		
	private function validate(){
		$errMsg = array();
		$currentAction = $this->uri->segment(2, 0);
		if (!empty($currentAction) && $currentAction=='insert'){
			$this->setViewData(VDATAKEY_EDITOR_STATE, 'ADD');
			$postingProfile = $this->DAO_Posting_Profile->getPostingProfileByType($_POST["transaction_type"]);
			if($postingProfile == null){
				$errMsg[] = 'Type transaksi belum memiliki posting profile';
			}
		}else{
			$this->setViewData(VDATAKEY_EDITOR_STATE, 'EDIT');
		}
		if(count($errMsg)>0){
			return array(false, $errMsg);
		}else{
			return array(true, $errMsg);
		}
	}
	
	private function generateTransactionNumber(){
		$year = date('Y');
		$month = date('m');
		$seqName = sprintf("TRX-C-%s-%s", $year, $month);
		$seqRemark = sprintf("Sequence TRX Pembayaran tahun %s bulan %s", $year, $month);
		$seqNum = $this->DAO_Sequence->getSequence($seqName, $seqRemark);
		$trxNo = sprintf("TC%s-%s-%04d", $year, $month, $seqNum);
		return $trxNo;
	}
	
	private function generateTransactionNumberKasbon(){
		$year = date('Y');
		$month = date('m');
		$seqName = sprintf("TRX-C-%s-%s", $year, $month);
		$seqRemark = sprintf("Sequence TRX Pembayaran tahun %s bulan %s", $year, $month);
		$seqNum = $this->DAO_Sequence->getSequence($seqName, $seqRemark);
		$trxNo = sprintf("TD%s-%s-%04d", $year, $month, $seqNum);
		return $trxNo;
	}
	
	public function get_employee_data($id){
		$employee = $this->DAO_Employee->getRow($id);
		$salary = $this->DAO_Salary->getRowByEmployeeId($id);
		$kasbon = $this->DAO_Employee->getRowKasbon($id);
		if((!is_null($employee) && !is_null($employee)) || (!is_null($salary) && !is_null($salary))|| (!is_null($kasbon) && !is_null($kasbon))){
			$tmpGaji = 0;
			$tmpKasbon =0;
			if($salary != null){
				$tmpGaji = $salary['total_gaji'];
			}
			
			if($kasbon!=null){
				$tmpKasbon = $kasbon['sisa_hutang'];
			}
			$dataEmployee = array("jabatan"=>$employee['jabatan'], "gaji"=>$tmpGaji, "kasbon"=>$tmpKasbon);
			header('Content-Type: application/json');
			echo json_encode($dataEmployee);
		}else{
			echo "";
		}
	}
	
}
