<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseBackEndController.php';
require_once 'IBasicController.php';

/**
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
class Sia_subjects extends BaseBackEndController implements IBasicController
{
	
	/**
	 * Current record id
	 * @var Integer
	 */
	var $recId;
	
	/**
	 * List view
	 * @var String
	 */
	var $viewList = 'v_subject_list';
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array('DAO_Subjects'));
		$this->recId = $this->getRecId();
  }

  function getPageTitle()
  {
    return 'Mata Pelajaran';
  }

  function getJsFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/js/ultracms_sia_subject_list.min.js');
    } else {
      return array();
    }
  }

  function getCssFiles()
  {
    if ($this->isIndexAction()) {
      return array();
    } else {
      return array();
    }
  }

  function getPageIcon()
  {
    return 'fa-book';
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaSubjectList.init()';
    } else {
      return '';
    }
  }

  public function index()
  {
		$nama = isset($_GET['nama'])? $_GET['nama'] : '';
		$filters = array(
			array(QRYFILTER_FIELD => 'name', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $nama),
			array(QRYFILTER_FIELD => 'active', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => 'Y')
		);
		$sorts = array('name');
		$rs = $this->DAO_Subjects->getList($filters, $sorts, $this->getFirstRowPos(), $this->pageSize);
		$recordCount = $this->DAO_Subjects->getListCount($filters);
		
		$this->setViewData(VDATAKEY_LIST_PAGE, $this->page);
		$this->setViewData(VDATAKEY_LIST_PAGE_SIZE, $this->pageSize);
		$this->setViewData(VDATAKEY_POST_DATA, $_GET);
		$this->setViewData(VDATAKEY_ROWNUM_START, $this->getFirstRowPos());
		$this->setViewData(VDATAKEY_RECORDSET, $rs);
		$this->setViewData(VDATAKEY_RECORDCOUNT, $recordCount);
		$this->loadView($this->viewList);
	}
	
	public function insert(){
		$this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
		if($this->isPostRequest()){
			$validateResult = $this->validate();
			if($validateResult[0]===true){
				// save
				$subject = $this->getFormData();
				$subject['created_date'] = $this->getCurrentDateTime();
				$subject['created_by'] = $this->userData['id'];
				$this->DAO_Subjects->insert($subject);
				$this->showSuccessMessage("Data berhasil disimpan", "sia_subjects/index");
			}else{
				// show error & form
				$this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
				$this->setViewData(VDATAKEY_POST_DATA, $_POST);
				$this->loadView($this->viewList);
			}
		}else{
			show_404();
		}
	}
	
	public function update(){
		$this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
		if($this->isPostRequest()){
			$validateResult = $this->validate();
			if($validateResult[0]===true){
				// update
				$subject = $this->getFormData();
				$subject['modified_by'] = $this->userData['id'];
				$this->DAO_Subjects->update($subject, $this->recId);
				$this->showSuccessMessage("Data berhasil disimpan", "sia_subjects/index");
			}else{
				// show error & form
				$this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
				$this->setViewData(VDATAKEY_POST_DATA, $_POST);
				$this->loadView($this->viewList);
			}
		}else{
			show_404();
		}
	}
	
	private function validate(){
		$errMsg = array();
		$subject = $this->getFormData();

    if ($this->getControllerAction() == 'insert' && $this->DAO_Subjects->isExist($subject['name'])) {
			$errMsg[] = sprintf("Mata pelajaran '%s' sudah terdaftar", $subject['name']);
		}
		
		if(count($errMsg) > 0){
			return array(false, $errMsg);
		}else{
			return array(true, $errMsg);
		}
	}
	
	private function getFormData(){
		$data = array(
			"name" => trim($_POST["name"])
		);
		return $data;
	}
	
}
