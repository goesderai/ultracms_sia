<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * BaseController.php
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
abstract class BaseController extends CI_Controller
{

  protected $appConfig;
  protected $viewData = array();

  public function __construct()
  {
    parent::__construct();
    $this->load->helper(array('url', 'html', 'form', 'language'));
    $this->load->model(array('DAO_System_Parameter'));

    // Load config data
    $this->appConfig = $this->config->item('ultracms');
  }

  /**
   * Get current controller
   * @return string Current controller
   */
  protected function getController()
  {
    return $this->uri->segment(1, '');
  }

  /**
   * Get current controller action (the 2nd segment of current url)
   * @return string Current controller action
   */
  protected function getControllerAction()
  {
    $currentAction = $this->uri->segment(2, 'index');
    return strtolower($currentAction);
  }

  /**
   * Get system parameter value
   * @param $paramKey string Parameter key
   * @return mixed
   */
  protected function getSystemParameterValue($paramKey)
  {
    return $this->DAO_System_Parameter->getValue($paramKey);
  }

  /**
   * Get view data item by key
   * @param String $key <br> The view data key
   * @return mixed|null
   */
  protected function getViewData($key)
  {
    if (is_null($key) || empty($key)) {
      return null;
    } else {
      return $this->viewData[$key];
    }
  }

  /**
   * Set/add view data item
   * @param String $key <br> The view data key
   * @param Mixed $value <br> The view data value
   */
  protected function setViewData($key, $value)
  {
    if (!is_null($key) && !empty($key)) {
      $this->viewData[$key] = $value;
    }
  }

  protected function isPostRequest()
  {
    return ($_SERVER['REQUEST_METHOD'] === 'POST');
  }

  /**
   * Load view without scaffolding
   * @param $view string View name
   */
  protected function loadPlainView($view)
  {
    $this->load->view('sia_admin/' . $view, $this->viewData);
  }

}
