<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Base functions required by the controllers. Functions written here are functions required by the controller before the 
 * controller is initialized. 
 * 
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
 
// BEGIN Path related functions ---------------------------------------------------------------------------------------------

if (!function_exists('getFilePath')) {
    function getFilePath($pathToFile){
        if (is_null($pathToFile) || empty($pathToFile)) {
            return '';
        } else {
            $basePath = str_replace('system/', '', BASEPATH);
            return trim($basePath . $pathToFile);
        }
    }
}

// END Path related functions -----------------------------------------------------------------------------------------------