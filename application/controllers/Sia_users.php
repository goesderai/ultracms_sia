<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseBackEndController.php';
require_once 'IBasicController.php';

/**
 * Controller managemen user aplikasi
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
class Sia_users extends BaseBackEndController implements IBasicController
{
	
	var $recId;
	var $viewList = 'v_user_list';
	var $viewEditor = 'v_user_editor';
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array('DAO_Employee', 'DAO_Security'));
		$this->lang->load('sia_users');
		$this->recId = $this->getRecId();
		$this->setViewData('DAO_Employee', $this->DAO_Employee);
		$this->setViewData('DAO_Security', $this->DAO_Security);
    $this->setViewData(VDATAKEY_EDITOR_STATE, $this->getControllerAction());
  }

  function getPageTitle()
  {
    return lang('user_title');
  }

  function getJsFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/js/ultracms_sia_user_list.min.js');
    } else {
      return array('assets/3rd-party/select2/js/select2.min.js',
        'assets/js/ultracms_sia_user_editor.min.js');
    }
  }

  function getCssFiles()
  {
    if ($this->isIndexAction()) {
      return array();
    } else {
      return array('assets/3rd-party/select2/css/select2.min.css',
        'assets/3rd-party/pretty-checkbox/pretty-checkbox.min.css');
    }
  }

  function getPageIcon()
  {
    return 'fa-user';
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaUserList.init()';
    } else {
      return 'ultracms.siaUserEditor.init()';
    }
  }

  public function index()
  {
		$username = isset($_GET['username'])? $_GET['username'] : '';
		$name = isset($_GET['name'])? $_GET['name'] : '';
		$status = isset($_GET['status'])? $_GET['status'] : '';
		$filters = array(
			array(QRYFILTER_FIELD => 'username', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $username),
			array(QRYFILTER_FIELD => 'name', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $name)
		);
		if(!empty($status)){
			$filters[] = array(QRYFILTER_FIELD => 'active', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $status); 
		}
		$rs = $this->DAO_Security->getUserByFilter($filters, null, $this->getFirstRowPos(), $this->pageSize);
		$recordCount = $this->DAO_Security->getUserCountByFilter($filters);
		
		$this->setViewData(VDATAKEY_LIST_PAGE, $this->page);
		$this->setViewData(VDATAKEY_LIST_PAGE_SIZE, $this->pageSize);
		$this->setViewData(VDATAKEY_POST_DATA, $_GET);
		$this->setViewData(VDATAKEY_ROWNUM_START, $this->getFirstRowPos());
		$this->setViewData(VDATAKEY_RECORDSET, $rs);
		$this->setViewData(VDATAKEY_RECORDCOUNT, $recordCount);
		$this->loadView($this->viewList);
	}
	
	public function insert(){
		$this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
		if($this->isPostRequest()){
			$validateResult = $this->validate();
			if($validateResult[0] === true){
				$user = $this->getFormData();
				unset($user['confirmPassword']);
				$user['created_by'] = $this->userData['id'];
				$user['created_date'] = $this->getCurrentDateTime();
				$this->DAO_Security->insertUser($user);
				$this->showSuccessMessage('Data berhasil disimpan', 'sia_users/index');
			}else{
				// show error & form
				$this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
				$this->setViewData(VDATAKEY_POST_DATA, $_POST);
				$this->loadView($this->viewEditor);
			}
		}else{
			$this->loadView($this->viewEditor);
		}
	}
	
	public function update(){
		$this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
		if($this->isPostRequest()){
			$validateResult = $this->validate();
			if($validateResult[0] === true){
				$user = $this->getFormData();
				unset($user['confirmPassword']);
				if(empty($user['password'])){
					unset($user['password']);
				}
				$user['modified_by'] = $this->userData['id'];
				$this->DAO_Security->updateUser($user, $this->recId);
				$this->showSuccessMessage('Data berhasil disimpan', 'sia_users/index');
			}else{
				// show error & form
				$this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
				$this->setViewData(VDATAKEY_POST_DATA, $_POST);
				$this->loadView($this->viewEditor);
			}
		}else{
			$user = $this->DAO_Security->getUserById($this->recId);
			$user['user_roles'] = $this->DAO_Security->getUserRoles($this->recId);
			$this->setViewData(VDATAKEY_POST_DATA, $user);
			$this->loadView($this->viewEditor);
		}
	}
	
	public function activate(){
		if($this->isPostRequest()){
			// Show error
			show_error('Request method not supported!');
		}else{
			$data = array('active' => 'Y', 'modified_by' => $this->userData['id']);
			$this->DAO_Security->updateUser($data, $this->recId);
			$this->showSuccessMessage('Data berhasil diaktifkan', 'sia_users/index');
		}
	}
	
	public function deactivate(){
		if($this->isPostRequest()){
			// Show error
			show_error('Request method not supported!');
		}else{
			$data = array('active' => 'N', 'modified_by' => $this->userData['id']);
			$this->DAO_Security->updateUser($data, $this->recId);
			$this->showSuccessMessage('Data berhasil dinon-aktifkan', 'sia_users/index');
		}
	}
	
	private function getFormData(){
		$data = array(
			'employee_id' => $_POST['employee_id'],
			'username' => $_POST['username'],
			'password' => !empty($_POST['password'])? md5($_POST['password']) : '',
			'confirmPassword' => !empty($_POST['confirmPassword'])? md5($_POST['confirmPassword']) : '',
			'active' => isset($_POST['active'])? $_POST['active'] : 'N',
			'roles' => $_POST['role']
		);
		return $data;
	}
	
	private function validate(){
		$errMsg = array();
		$data = $this->getFormData();
    $editorState = $this->getControllerAction();
		
		// Validasi
		if((!empty($editorState) && $editorState=='insert') && $this->DAO_Security->isUsernameExists($data['username'])){
			$errMsg[] = "Data user dengan username yang sama sudah terdaftar";
		}
		if(!empty($data['password']) && !empty($data['confirmPassword']) && ($data['password'] != $data['confirmPassword'])){
			$errMsg[] = "Konfirmasi password salah";
		}
		
		if(count($errMsg) > 0){
			return array(false, $errMsg);
		}else{
			return array(true, $errMsg);
		}
	}
	
}
