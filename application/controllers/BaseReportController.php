<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseBackEndController.php';

/**
 * Base report controller
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
abstract class BaseReportController extends BaseBackEndController
{

  var $vReportForm;

  public function __construct()
  {
    parent::__construct();
    $this->load->library(array('PhpReportGenerator'));
    $this->setViewData("reportGen", $this->phpreportgenerator);
  }

  function getPageIcon()
  {
    return 'fa-file';
  }

  /**
   * Void setReportGenField($field, $value)<br>
   * Sets report generator field
   * @param String $field The field name
   * @param Mixed $value The field value
   */
  protected function setReportGenField($field, $value)
  {
    $this->phpreportgenerator->$field = $value;
  }

  /**
   * Display report parameter form
   * @param $vReportForm string Report form view
   */
  protected function loadReportForm($vReportForm)
  {
    $this->setViewData('reportJs', $this->getJsFiles());
    $this->setViewData('reportCss', $this->getCssFiles());
    $this->setViewData('onReadyCallback', $this->getJsInitFunction());
    $this->setViewData('reportForm', $vReportForm);
    $this->setViewData(VDATAKEY_FORM_ACTION_URL, $this->getController() . '/generate_report');
    $this->loadView('v_report_form');
  }

  protected function loadReport()
  {
    $this->loadCompanyInfo();
    $this->setViewData(VDATAKEY_PAGE_TITLE, $this->getPageTitle());
    $this->setViewData(VDATAKEY_PAGE_ICON, $this->getPageIcon());
    $this->setViewData(VDATAKEY_PAGE_CSS, $this->getCssFiles());
    $this->setViewData(VDATAKEY_PAGE_JS, $this->getJsFiles());
    $this->setViewData(VDATAKEY_PAGE_JS_INITFUNC, $this->getJsInitFunction());
    $this->loadPlainView('v_report');
  }

}
