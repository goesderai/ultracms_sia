<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseReportController.php';
require_once 'IReportController.php';

/**
 * Controller laporan pendapatan
 *
 * @author <a href="mailto:agung3ka@gmail.com">Turah Eka</a>
 */
class Sia_rpt_pendapatan extends BaseReportController implements IReportController
{

	const V_RPT_FORM = 'v_rpt_pendapatan_form';

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('DAO_TipeTransaksi', 'DAO_Transaksi_Pendapatan'));
	}

	function getPageTitle()
	{
		return 'Laporan Pendapatan';
	}

	function getJsFiles()
	{
		if ($this->isIndexAction()) {
			return array('assets/themes/sb-admin/js/moment-with-locales.min.js',
        'assets/themes/sb-admin/js/bootstrap-datetimepicker.min.js',
        'assets/js/ultracms_sia_transaksi_pendapatan_list.min.js');
		} else {
			return array();
		}
	}

	function getJsInitFunction()
	{
		if ($this->isIndexAction()) {
			return 'ultracms.siaTransaksiPendapatanList.init()';
		} else {
			return '';
		}
	}

	function getCssFiles()
	{
		if ($this->isIndexAction()) {
			return array('assets/themes/sb-admin/css/bootstrap-datetimepicker.min.css');
		} else {
			return array();
		}
	}

	public function index()
	{
		$this->setViewData('DAO_TipeTransaksi', $this->DAO_TipeTransaksi);
		$this->setViewData('DAO_Transaksi_Pendapatan', $this->DAO_Transaksi_Pendapatan);
		$this->loadReportForm(self::V_RPT_FORM);
	}

	public function generate_report()
	{
		$tipeTransaksi = isset($_POST['trx_type']) ? $_POST['trx_type'] : '';
		$transaction_date_from = isset($_POST['transaction_date_from']) ? $_POST['transaction_date_from'] : '';
		$transaction_date_to = isset($_POST['transaction_date_to']) ? $_POST['transaction_date_to'] : '';
		$filters = [];
		$transactionType = null;

		if (!empty($tipeTransaksi)) {
			$filters[] = array(QRYFILTER_FIELD => 'trx_type', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $tipeTransaksi);
			$transactionType = $this->DAO_TipeTransaksi->getRow($tipeTransaksi);
		}
		if (!empty($transaction_date_from)) {
			if (!empty($transaction_date_to)) {
				$filters[] = array(QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_BETWEEN, QRYFILTER_VALUE => array($transaction_date_from, $transaction_date_to));
			} else {
				$filters[] = array(QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_BETWEEN, QRYFILTER_VALUE => array($transaction_date_from, $transaction_date_from));
			}
		}
		$sql = $this->DAO_Transaksi_Pendapatan->getRptQuery($filters, 'D', 3);

		$qry = $this->db->query($sql);

		$newPageTitle = $this->getPageTitle();

		$allData = $qry->result();
			
		if(!empty($allData)){
			$first = reset($allData);
			$last = end($allData);

			$periode = ' Periode : ';
			if (!empty($transaction_date_from)) {
				$dateStart = date($this->appConfig['dateFormatForDisplay'], strtotime($transaction_date_from));
				if (!empty($transaction_date_to)) {
					$endDate = date($this->appConfig['dateFormatForDisplay'], strtotime($transaction_date_to));
					$periode = $periode.$dateStart.' s/d '.$endDate;
				} else {
					$periode = $periode.$dateStart;
				}
			}else{
				$dateStart = date($this->appConfig['dateFormatForDisplay'], strtotime($first->{'Tanggal Transaksi'}));
				$endDate = date($this->appConfig['dateFormatForDisplay'], strtotime($last->{'Tanggal Transaksi'}));
				$periode =$periode.$dateStart.' s/d '.$endDate;
			}

			$trxTypeLabel = !is_null($transactionType)? $transactionType['nama'] : 'Semua Tipe Transaksi';
			$newPageTitle = $newPageTitle." - ".$trxTypeLabel."<br>".$periode;

		}
		$this->setReportGenField(RPTGEN_HEADER, $newPageTitle);
		$this->setReportGenField(RPTGEN_MYSQL_RESOURCE, $qry);
		$this->setReportGenField(RPTGEN_DATE_FIELDS, array('Tanggal Transaksi' => $this->appConfig['dateFormatForDisplay']));
		$this->setReportGenField(RPTGEN_NUMBER_FIELDS, array("Jumlah"));
		$this->loadReport();
	}

}
