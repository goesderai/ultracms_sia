<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseBackEndController.php';
require_once 'IBasicController.php';

/**
 * Main account controller
 *
 * @author <a href="mailto:agung3ka@gmail.com">Eka D.</a>
 */
class Sia_main_account extends BaseBackEndController implements IBasicController
{
  var $recId;
  var $viewList = 'v_main_account_list';
  var $viewEditor = 'v_main_account_editor';

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('DAO_Main_Account', 'DAO_Posting_Profile'));
    $this->lang->load('sia_main_account');
    $this->recId = $this->getRecId();
    $this->setViewData('DAO_Main_Account', $this->DAO_Main_Account);
    $this->setViewData('DAO_Posting_Profile', $this->DAO_Posting_Profile);
  }

  function getPageTitle()
  {
    return lang('main_account_code');
  }

  function getPageIcon()
  {
    return 'fa-sitemap';
  }

  function getJsFiles()
  {
    $js = array('assets/3rd-party/select2/js/select2.min.js');
    if ($this->isIndexAction()) {
      $js[] = 'assets/js/ultracms_sia_main_account_list.min.js';
    } else {
      $js[] = 'assets/js/ultracms_sia_main_account_editor.min.js';
    }
    return $js;
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaMainAccountList.init()';
    } else {
      return 'ultracms.siaMainAccountEditor.init()';
    }
  }

  function getCssFiles()
  {
    return array('assets/3rd-party/select2/css/select2.min.css');
  }

  public function index()
  {
    $mainAccount = isset($_GET['account_code']) ? $_GET['account_code'] : '';
    $parent = isset($_GET['parent']) ? $_GET['parent'] : '';
    $name = isset($_GET['name']) ? $_GET['name'] : '';
    $filters = array(
      array(QRYFILTER_FIELD => 'name', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $name),
      array(QRYFILTER_FIELD => 'active', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => 'Y')
    );

    if ($mainAccount > 0) {
      $filters[] = array(QRYFILTER_FIELD => 'id', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $mainAccount);
    };

    if ($parent > 0) {
      $filters[] = array(QRYFILTER_FIELD => 'parent', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $parent);
    };
    $rs = $this->DAO_Main_Account->getList($filters, null, $this->getFirstRowPos(), $this->pageSize);
    $recordCount = $this->DAO_Main_Account->getListCount($filters);

    $this->setViewData(VDATAKEY_LIST_PAGE, $this->page);
    $this->setViewData(VDATAKEY_LIST_PAGE_SIZE, $this->pageSize);
    $this->setViewData(VDATAKEY_POST_DATA, $_GET);
    $this->setViewData(VDATAKEY_ROWNUM_START, $this->getFirstRowPos());
    $this->setViewData(VDATAKEY_RECORDSET, $rs);
    $this->setViewData(VDATAKEY_RECORDCOUNT, $recordCount);
    $this->loadView($this->viewList);
  }

  private function getFormData()
  {
    $data = array(
      'account_code' => $_POST['account_code'],
      'name' => $_POST['name']
    );
    if ($_POST['parent'] > 0) {
      $data['parent'] = $_POST['parent'];
    } else {
      $data['parent'] = null;
    }
    return $data;
  }

  public function insert()
  {
    $this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
    $this->setViewData(VDATAKEY_EDITOR_STATE, 'ADD');
    if ($this->isPostRequest()) {
      $validateResult = $this->validate();
      if ($validateResult[0] === true) {
        // save
        $mainAccount = $this->getFormData();
        $this->DAO_Main_Account->insert($mainAccount);
        $this->showSuccessMessage(lang('message_common_save_success'), 'sia_main_account/index');
      } else {
        // show error & form
        $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
        $this->setViewData(VDATAKEY_POST_DATA, $_POST);
        $this->loadView($this->viewEditor);
      }
    } else {
      $this->loadView($this->viewEditor);
    }
  }

  public function update()
  {
    $this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
    $this->setViewData(VDATAKEY_EDITOR_STATE, 'EDIT');
    if ($this->isPostRequest()) {
      $validateResult = $this->validate();
      if ($validateResult[0] === true) {
        // update
        $this->DAO_Main_Account->update($this->getFormData(), $this->recId);
        $this->showSuccessMessage(lang('message_common_save_success'), 'sia_main_account/index');
      } else {
        // show error & form
        $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
        $this->setViewData(VDATAKEY_POST_DATA, $_POST);
        $this->loadView($this->viewEditor);
      }
    } else {
      $mainAccount = $this->DAO_Main_Account->getRow($this->recId);
      $this->setViewData(VDATAKEY_POST_DATA, $mainAccount);
      $this->loadView($this->viewEditor);
    }
  }

  private function validate()
  {
    $errMsg = array();
    $currentAction = $this->uri->segment(2, 0);

    if (!empty($currentAction) && $currentAction == 'insert') {
      $this->setViewData(VDATAKEY_EDITOR_STATE, 'ADD');
      $acctTmp = isset($_POST['account_code']) ? $_POST['account_code'] : '';
      $mainAccount = $this->DAO_Main_Account->getMainAccount($acctTmp);

      if ($mainAccount != null) {
        $errMsg[] = lang('main_account_code_exists');
        return array(false, $errMsg);
      } else {
        return array(true, $errMsg);
      }
    } else {
      $this->setViewData(VDATAKEY_EDITOR_STATE, 'EDIT');
      return array(true, $errMsg);
    }
  }

  public function delete()
  {
    if ($this->isPostRequest()) {
      // Show error
      show_error(lang('message_error_invalid_request_method'));
    } else {
      $validateResult = $this->validateDeleteMA();
      if ($validateResult[0] === true) {
        $data = array('active' => 'N', 'modified_by' => $this->userData['id']);
        $this->DAO_Main_Account->update($data, $this->recId);
        $this->showSuccessMessage(lang('message_common_delete_success'), 'sia_main_account/index');
      } else {
        $mainAccount = isset($_GET['account_code']) ? $_GET['account_code'] : '';
        $parent = isset($_GET['parent']) ? $_GET['parent'] : '';
        $name = isset($_GET['name']) ? $_GET['name'] : '';
        $filters = array(
          array(QRYFILTER_FIELD => 'name', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $name),
          array(QRYFILTER_FIELD => 'active', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => 'Y')
        );

        if ($mainAccount > 0) {
          $filters[] = array(QRYFILTER_FIELD => 'id', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $mainAccount);
        };

        if ($parent > 0) {
          $filters[] = array(QRYFILTER_FIELD => 'parent', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $parent);
        };
        $rs = $this->DAO_Main_Account->getList($filters, null, $this->getFirstRowPos(), $this->pageSize);
        $recordCount = $this->DAO_Main_Account->getListCount($filters);

        $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
        $this->setViewData(VDATAKEY_LIST_PAGE, $this->page);
        $this->setViewData(VDATAKEY_LIST_PAGE_SIZE, $this->pageSize);
        $this->setViewData(VDATAKEY_POST_DATA, $_GET);
        $this->setViewData(VDATAKEY_ROWNUM_START, $this->getFirstRowPos());
        $this->setViewData(VDATAKEY_RECORDSET, $rs);
        $this->setViewData(VDATAKEY_RECORDCOUNT, $recordCount);
        $this->loadView($this->viewList);
      }

    }
  }

  private function validateDeleteMA()
  {
    $errMsg = array();
    $filters = array(
      array(QRYFILTER_FIELD => 'parent', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $this->recId),
      array(QRYFILTER_FIELD => 'active', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => 'Y')
    );
    $recordCount = $this->DAO_Main_Account->getListCount($filters);

    $recordCountExistPP = $this->DAO_Main_Account->getListMainAccountExist($this->recId);

    if ($recordCount > 0) {
      $errMsg[] = lang('main_account_has_children');
    }
    if ($recordCountExistPP > 0) {
      $errMsg[] = lang('main_account_has_posting_profile');
    }
    if (!empty($errMsg)) {
      return array(false, $errMsg);
    } else {
      return array(true, $errMsg);
    }
  }
}
