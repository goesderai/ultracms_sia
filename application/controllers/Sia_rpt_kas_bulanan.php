<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseReportController.php';
require_once 'IReportController.php';

/**
 * Controller laporan tunggakan pembayaran SPP
 *
 * @author <a href="mailto:agung3ka@gmail.com">Turah Eka</a>
 */
class Sia_rpt_kas_bulanan extends BaseReportController implements IReportController
{

  const V_RPT_FORM = 'v_rpt_kas_bulanan_form';
  const V_RPT_MONTHLY = 'v_kas_bulanan_report';

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('DAO_Transaksi'));
  }

  function getPageTitle()
  {
    return 'Laporan Kas Bulanan';
  }

  function getJsFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/js/ultracms_sia_rpt_kas_bulanan.min.js');
    } else {
      return array();
    }
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaRptKasBulanan.init()';
    } else {
      return '';
    }
  }

  function getCssFiles()
  {
    return array();
  }

  public function index()
  {
    $this->setViewData('DAO_Transaksi', $this->DAO_Transaksi);
    $this->loadReportForm(self::V_RPT_FORM);
  }

  public function generate_report()
  {
    $bulan = isset($_POST['financial_month']) ? $_POST['financial_month'] : '';
    $tahun = isset($_POST['financial_year']) ? $_POST['financial_year'] : '';
    $bulanSebelumnya = $bulan;
    $tahunSebelumnya = $tahun;
    if ($bulan == 1) {
      $bulanSebelumnya = 12;
      $tahunSebelumnya = $tahunSebelumnya - 1;
    } else {
      $bulanSebelumnya = $bulanSebelumnya - 1;

    }
    $tanggalSebelumnya = $tahunSebelumnya . '-' . $bulanSebelumnya . '-31';
    $tanggal = $tahun . '-' . $bulan . '-31';
    $filters = array();
    $filtersKasAwal = array();
    $filtersKasAkhir = array();

    if (!empty($bulan)) {
      $filters[] = array(QRYFILTER_FIELD => 'financial_month', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $bulan);
    }
    if (!empty($tahun)) {
      $filters[] = array(QRYFILTER_FIELD => 'financial_year', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => $tahun);
    }
    if (!empty($bulan) && !empty($tahun)) {
      $filtersKasAwal[] = array(QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_SMALLER, QRYFILTER_VALUE => $tanggalSebelumnya);
      $filtersKasAkhir[] = array(QRYFILTER_FIELD => 'transaction_date', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_IS_LESS_THAN_OR_EQUAL_TO, QRYFILTER_VALUE => $tanggal);
    }
    $validateResult = $this->validate();
    if ($validateResult[0] === true) {
      // Show report
      $this->loadCompanyInfo();
      $this->setViewData(VDATAKEY_PAGE_TITLE, $this->getPageTitle());
      $this->setViewData('DAO_Transaksi', $this->DAO_Transaksi);
      $this->setViewData('filters', $filters);
      $this->setViewData('bulan', $bulan);
      $this->setViewData('tahun', $tahun);
      $this->setViewData('ket', 'Bulan ');
      $this->setViewData('filtersKasAwal', $filtersKasAwal);
      $this->setViewData('filtersKasAkhir', $filtersKasAkhir);
      $this->loadPlainView(self::V_RPT_MONTHLY);
    } else {
      // show error & form
      $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
      $this->setViewData(VDATAKEY_POST_DATA, $_POST);
      $this->loadView(self::V_RPT_FORM);
    }
  }

  private function validate()
  {
    $errMsg = array();

    if (count($errMsg) > 0) {
      return array(false, $errMsg);
    } else {
      return array(true, $errMsg);
    }
  }

}
