<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseBackEndController.php';
require_once 'IBasicController.php';

//Current controller constants (available only in current controller & views)
defined('GRADE_PAUD_BARU_ID') or define('GRADE_PAUD_BARU_ID', 11);
defined('GRADE_SD_BARU_ID')   or define('GRADE_SD_BARU_ID', 15);
defined('STATUS_CALON')   or define('STATUS_CALON', 1);

/**
 * Students controller
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
class Sia_students extends BaseBackEndController implements IBasicController
{

  /**
   * Current record id
   * @var Integer
   */
  var $recId;

  /**
   * Student list view
   * @var String
   */
  var $viewList = 'v_student_list';

  /**
   * Student editor view
   * @var String
   */
  var $viewEditor = 'v_student_editor';

  /**
   * Student print view
   * @var String
   */
  var $viewPrint = 'v_student_print_preview';

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('DAO_Siswa', 'DAO_Agama', 'DAO_Lov', 'DAO_Sequence', 'DAO_Edu'));
    $this->lang->load('sia_students', $this->getCurrentLocale());
    $this->recId = $this->getRecId();
    $this->setViewData('DAO_Agama', $this->DAO_Agama);
    $this->setViewData('DAO_Lov', $this->DAO_Lov);
    $this->setViewData('DAO_Edu', $this->DAO_Edu);
    $this->setViewData(VDATAKEY_EDITOR_STATE, $this->getControllerAction());
  }

  function getPageTitle()
  {
    return lang('student_title');
  }

  function getPageIcon()
  {
    return 'fa-users';
  }


  function getJsFiles()
  {
    if ($this->isIndexAction()) {
      return array('assets/js/ultracms_sia_student_list.min.js');
    } else {
      return array('assets/themes/sb-admin/js/moment-with-locales.min.js',
        'assets/themes/sb-admin/js/bootstrap-datetimepicker.min.js',
        'assets/js/ultracms_sia_student_editor.min.js'
      );
    }
  }

  function getJsInitFunction()
  {
    if ($this->isIndexAction()) {
      return 'ultracms.siaStudentList.init()';
    } else {
      return 'ultracms.siaStudentEditor.init()';
    }
  }

  function getCssFiles()
  {
    if ($this->isIndexAction()) {
      return array();
    } else {
      return array('assets/themes/sb-admin/css/bootstrap-datetimepicker.min.css');
    }
  }

  public function index()
  {
    $nis = isset($_GET['nis']) ? $_GET['nis'] : '';
    $nama = isset($_GET['nama']) ? $_GET['nama'] : '';
    $grade = isset($_GET['grade']) ? $_GET['grade'] : '';
    $classroom = isset($_GET['classroom']) ? $_GET['classroom'] : '';
    $status = isset($_GET['status']) ? $_GET['status'] : '';
    $parentName = isset($_GET['parent_name']) ? $_GET['parent_name'] : '';
    $filters = array(
      array(QRYFILTER_FIELD => 's.nis', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $nis),
      array(QRYFILTER_FIELD => 's.name', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_LIKE, QRYFILTER_VALUE => $nama),
      array(QRYFILTER_FIELD => 's.active', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL, QRYFILTER_VALUE => 'Y')

    );
    if (!empty($status)) {
      $filters[] = array(QRYFILTER_FIELD => 's.status', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL,
        QRYFILTER_VALUE => $status);
    }
    if (!empty($parentName)) {
      $filters[] = array(
        QRYFILTER_FIELD => array('s.father_name', 's.mother_name'),
        QRYFILTER_OPERATOR => array(QRYFILTER_OPERATOR_OR, QRYFILTER_OPERATOR_LIKE, QRYFILTER_OPERATOR_LIKE),
        QRYFILTER_VALUE => array($parentName, $parentName)
      );
    }
    if (!empty($classroom)) {
      $filters[] = array(QRYFILTER_FIELD => 'cr.id', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL,
        QRYFILTER_VALUE => $classroom);
    }
    if (!empty($grade)) {
      $filters[] = array(QRYFILTER_FIELD => 'grd.id', QRYFILTER_OPERATOR => QRYFILTER_OPERATOR_EQUAL,
        QRYFILTER_VALUE => $grade);
    }
    $rs = $this->DAO_Siswa->getList($filters, null, $this->getFirstRowPos(), $this->pageSize);
    $recordCount = $this->DAO_Siswa->getListCount($filters);

    $this->setViewData(VDATAKEY_LIST_PAGE, $this->page);
    $this->setViewData(VDATAKEY_LIST_PAGE_SIZE, $this->pageSize);
    $this->setViewData(VDATAKEY_POST_DATA, $_GET);
    $this->setViewData(VDATAKEY_ROWNUM_START, $this->getFirstRowPos());
    $this->setViewData(VDATAKEY_RECORDSET, $rs);
    $this->setViewData(VDATAKEY_RECORDCOUNT, $recordCount);
    $this->loadView($this->viewList);
  }

  public function insert()
  {
    $this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
    if ($this->isPostRequest()) {
      $validateResult = $this->validate();
      if ($validateResult[0] === true) {
        // save
        $siswa = $this->getFormData();
        //$siswa['nis'] = $this->generateNis();
        $siswa['created_date'] = $this->getCurrentDateTime();
        $siswa['created_by'] = $this->userData['id'];
        $this->DAO_Siswa->insert($siswa);
        $this->showSuccessMessage('Data berhasil disimpan', 'sia_students/index');
      } else {
        // show error & form
        $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
        $this->setViewData(VDATAKEY_POST_DATA, $_POST);
        $this->loadView($this->viewEditor);
      }
    } else {
      $this->loadView($this->viewEditor);
    }
  }

  public function update()
  {
    $this->setViewData(VDATAKEY_FORM_ACTION_URL, uri_string());
    if ($this->isPostRequest()) {
      $validateResult = $this->validate();
      if ($validateResult[0] === true) {
        // update
        $siswa = $this->getFormData();
        $siswa['modified_by'] = $this->userData['id'];
        $this->DAO_Siswa->update($siswa, $this->recId);
        $this->showSuccessMessage('Data berhasil disimpan', 'sia_students/index');
      } else {
        // show error & form
        $this->setViewData(VDATAKEY_ERROR_MESSAGES, $validateResult[1]);
        $this->setViewData(VDATAKEY_POST_DATA, $_POST);
        $this->loadView($this->viewEditor);
      }
    } else {
      $student = $this->DAO_Siswa->getRow($this->recId);
      $this->setViewData(VDATAKEY_POST_DATA, $student);
      $this->loadView($this->viewEditor);
    }
  }

  public function delete()
  {
    if ($this->isPostRequest()) {
      // Show error
      show_error('Request method not supported!');
    } else {
      $data = array('active' => 'N', 'modified_by' => $this->userData['id']);
      $this->DAO_Siswa->update($data, $this->recId);
      $this->setViewData(VDATAKEY_ALERT_TYPE, ALERTTYPE_SUCCESS);
      $this->setViewData(VDATAKEY_ALERT_MESSAGE, 'Data berhasil dihapus');
      $this->setViewData(VDATAKEY_ALERT_PROCEED_URL, site_url('sia_students/index'));
      $this->loadView($this->viewAfterSaveMessage);
    }
  }

  private function getFormData()
  {
    $efectiveDate = $_POST["accepted_year"] . '-07-01"';
    $data = array(
      'nis' => $_POST["nis"],
      'name' => $_POST["name"],
      'nickname' => $_POST["nickname"],
      'gender' => $_POST["gender"],
      'birth_place' => $_POST["birth_place"],
      'birth_date' => $_POST["birth_date"],
      'weight' => $_POST["weight"],
      'height' => $_POST["height"],
      'blood_type' => $_POST["blood_type"],
      'illness_history' => $_POST["illness_history"],
      'nth_child' => $_POST["nth_child"],
      'n_siblings' => $_POST["n_siblings"],
      'n_step_siblings' => $_POST["n_step_siblings"],
      'n_foster_siblings' => $_POST["n_foster_siblings"],
      'religion_id' => $_POST["religion_id"],
      'classroom_id' => $_POST["classroom_id"],
      'nationality' => $_POST["nationality"],
      'address' => $_POST["address"],
      'phone_number' => $_POST["phone_number"],
      'daily_language' => $_POST["daily_language"],
      'stayed_at' => $_POST["stayed_at"],
      'father_name' => $_POST["father_name"],
      'mother_name' => $_POST["mother_name"],
      'father_last_education' => $_POST["father_last_education"],
      'mother_last_education' => $_POST["mother_last_education"],
      'parent_ocupation' => $_POST["parent_ocupation"],
      'guardian_name' => $_POST["guardian_name"],
      'guardian_last_education' => $_POST["guardian_last_education"],
      'guardian_relationship' => $_POST["guardian_relationship"],
      'guardian_ocupation' => $_POST["guardian_ocupation"],
      'entered_school_as' => $_POST["entered_school_as"],
      'student_origin' => $_POST["student_origin"],
      'play_group_name' => $_POST["play_group_name"],
      'play_group_location' => $_POST["play_group_location"],
      'tahun_dan_nomor_sttb' => $_POST["tahun_dan_nomor_sttb"],
      'years_of_learning' => $_POST["years_of_learning"],
      'origin_school_name' => $_POST["origin_school_name"],
      'transfer_from_grade' => $_POST["transfer_from_grade"],
      'accepted_date' => $_POST["accepted_date"],
      'accepted_year' => $_POST["accepted_year"],
      'efective_date' => $efectiveDate,
      'accepted_grade' => $_POST["accepted_grade"],
      'status' => $_POST['status']
    );
    if (!empty($_POST["transfer_date"])) {
      $data['transfer_date'] = $_POST["transfer_date"];
    }
    return $data;
  }

  private function validate()
  {
    $errMsg = array();
    $data = $this->getFormData();
    $editorState = $this->getControllerAction();

    // Nis
    if ((!empty($editorState) && $editorState == 'insert') && $this->DAO_Siswa->isNisExists($data['nis'])) {
      $errMsg[] = "Data siswa dengan NIS yang sama sudah terdaftar";
    }

    if (count($errMsg) > 0) {
      return array(false, $errMsg);
    } else {
      return array(true, $errMsg);
    }
  }

  private function generateNis()
  {
    $year = date('Y');
    $month = date('m');
    $seqName = sprintf("NIS-%s-%s", $year, $month);
    $seqRemark = sprintf("Sequence NIS tahun %s bulan %s", $year, $month);
    $seqNum = $this->DAO_Sequence->getSequence($seqName, $seqRemark);
    $nis = sprintf("S%s-%s-%04d", $year, $month, $seqNum);
    return $nis;
  }

  public function printview()
  {
    $student = $this->DAO_Siswa->getRowForPrintPreview($this->recId);
    $this->setViewData(VDATAKEY_PAGE_TITLE, $student['name']);
    $this->setViewData(VDATAKEY_POST_DATA, $student);
    $this->loadCompanyInfo();
    $this->loadPlainView($this->viewPrint);
  }

}
