<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * sia_transaction_type language file (EN)
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */

$lang["trxtype_type"] = "Transaction Type";
$lang["trxtype_name"] = "Transaction Type Name";
$lang["trxtype_code"] = "Transaction Type Code";
$lang["trxtype_parent"] = "Transaction Type Parent";
$lang["trxtype_doc_type"] = "Document Type";
$lang["trxtype_active_inactive"] = "Active / Inactive";
$lang["trxtype_debit_credit"] = "Debit / Credit";
$lang["trxtype_debit"] = "Debit";
$lang["trxtype_credit"] = "Credit";
$lang["trxtype_active"] = "Active";
$lang["trxtype_inactive"] = "Inactive";
$lang["trxtype_sub_type"] = "Sub Transaction Type";
