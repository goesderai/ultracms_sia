<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * sia_posting_profile_lang.php
 *
 * Posting profile language file (EN)
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */

$lang["pstngprfl_trx_type"] = "Transaction Type";
$lang["pstngprfl_account_debit"] = "Debit Account";
$lang["pstngprfl_account_credit"] = "Credit Account";
