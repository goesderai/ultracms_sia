<?php defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * sia_students language file (EN)
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */

$lang["student_title"] = "Student";
$lang["student_tab_person_info"] = "Student Information";
$lang["student_tab_parent_info"] = "Parent / Guardian";
$lang["student_tab_origin_info"] = "Student Origin";
$lang["student_nis"] = "Student Number";
$lang["student_name"] = "Name";
$lang["student_nickname"] = "Nickname";
$lang["student_gender"] = "Gender";
$lang["student_educational_level"] = "Educational Level";
$lang["student_grade"] = "Grade";
$lang["student_place_of_birth"] = "Place of Birth";
$lang["student_date_of_birth"] = "Date of Birth";
$lang["student_weight"] = "Weight (kg)";
$lang["student_height"] = "Height (cm)";
$lang["student_blood_type"] = "Blood Type";
$lang["student_illness_history"] = "Illness History";
$lang["student_nth_child"] = "Nth Child";
$lang["student_n_siblings"] = "Number of Siblings";
$lang["student_n_step_siblings"] = "Number of Step Brothers/Sisters";
$lang["student_n_foster_siblings"] = "Number of Foster Brothers/Sisters";
$lang["student_daily_language"] = "Daily Language";
$lang["student_citizenship"] = "Citizenship";
$lang["student_religion"] = "Religion";
$lang["student_address"] = "Address";
$lang["student_phone"] = "Phone Number";
$lang["student_status"] = "Status";
$lang["student_stayed_at"] = "Stayed at";
$lang["student_father_name"] = "Father's Name";
$lang["student_mother_name"] = "Mother's Name";
$lang["student_father_last_education"] = "Father's Last Education";
$lang["student_mother_last_education"] = "Mother's Last Education";
$lang["student_parent_ocupation"] = "Father's/Mother's Occupation";
$lang["student_parent_name"] = "Father's/Mother's Name";
$lang["student_guardian_name"] = "Guardian's Name (if any)";
$lang["student_guardian_last_education"] = "Guardian's Last Education";
$lang["student_guardian_relationship"] = "Guardian Relationship to Children";
$lang["student_guardian_ocupation"] = "Guardian Ocupation";
$lang["student_entered_school_as"] = "Entered This School as";
$lang["student_origin"] = "Student Origin";
$lang["student_play_group_name"] = "Play Group Name";
$lang["student_play_group_location"] = "Play Group Location";
$lang["student_tahun_dan_nomor_sttb"] = "Year & Graduation Certificate Number";
$lang["student_years_of_learning"] = "Years of Learning";
$lang["student_origin_school_name"] = "Origin School Name";
$lang["student_transfer_date"] = "Transferred From Origin School on";
$lang["student_transfer_from_grade"] = "Transferred From Grade";
$lang["student_accepted_date"] = "Accepted Date";
$lang["student_accepted_grade"] = "Accepted at Grade";
$lang["student_choose"] = "-- Choose --";
$lang["student_choose_educational_level"] = sprintf("-- Choose %s --", $lang['student_educational_level']);
