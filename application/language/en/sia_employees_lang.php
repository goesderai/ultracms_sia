<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * sia_employees language file (EN)
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
 
$lang["employee_title"] = "Employee";
$lang["employee_name"] = "Name";
$lang["employee_nickname"] = "Nickame";
$lang["employee_gender"] = "Gender";
$lang["employee_birth_place"] = "Birth Place";
$lang["employee_birth_date"] = "Birth Date";
$lang["employee_identity_number"] = "Identity Number";
$lang["employee_address"] = "Address";
$lang["employee_phone"] = "Phone Number";
$lang["employee_position"] = "Position";
$lang["employee_nett_salary"] = "Nett. Salary";
$lang["employee_total_salary"] = "Total Salary";
$lang["employee_benefit"] = "Benefit";
$lang["employee_others"] = "Others";
