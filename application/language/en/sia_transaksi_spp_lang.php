<?php defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * sia_transaksi_spp language file (EN)
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */

$lang["trxspp_page_title"] = "School Fees Income";
$lang["trxspp_nis"] = "NIS";
$lang["trxspp_student_name"] = "Student Name";
$lang["trxspp_student_grade_class"] = "Grade - Classroom";
$lang["trxspp_transaction_date"] = "Transaction Date";
$lang["trxspp_transaction_number"] = "Transaction Number";
$lang["trxspp_description"] = "Description";
$lang["trxspp_year"] = "Year";
$lang["trxspp_amount"] = "Amount";
$lang["trxspp_monthly_amount"] = "Monthly Payment Amount";
$lang["trxspp_monthly_discount"] = "Discount (Rp./month)";
$lang["trxspp_total_amount"] = "Total Payment Amount";
$lang["trxspp_choose_month"] = "Choose Month(s)";
$lang["trxspp_until"] = "until";
