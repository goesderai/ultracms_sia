<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Common labels language file (EN)
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
$lang["label_action"] = "Action";
$lang["label_search"] = "Search";
$lang["label_reset"] = "Reset";
$lang["label_add_new"] = "Add New";
$lang["label_add_new_hint"] = "Add new record";
$lang["label_delete"] = "Delete";
$lang["label_edit"] = "Edit";
$lang["label_pagenav"] = "Page navigation";
$lang["label_first_page"] = "First page";
$lang["label_prev_page"] = "Previous page";
$lang["label_next_page"] = "Next page";
$lang["label_last_page"] = "Last page";
$lang["label_page"] = "Page";
$lang["label_page_info"] = "Page information";
$lang["label_of"] = "of";
$lang["label_save"] = "Save";
$lang["label_save_hint"] = "Save changes";
$lang["label_close"] = "Close";
$lang["label_close_hint"] = "Close this page";
$lang["label_print_preview"] = "Print Preview";
$lang["label_change_password"] = "Change Password";
$lang["label_login"] = "Login";
$lang["label_relogin"] = "Relogin";
$lang["label_logout"] = "Logout";
$lang["label_username"] = "Username";
$lang["label_password"] = "Password";
$lang["label_applogin"] = "Application Login";
$lang["label_confirmation"] = "Confirmation";
$lang["label_attention"] = "Attention!";
$lang["label_yes"] = "Yes";
$lang["label_no"] = "No";
$lang["label_rownum"] = "#";
$lang["label_ok"] = "Ok";
$lang["label_all"] = "All";
