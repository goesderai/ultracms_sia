<?php defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * sia_user language file (EN)
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */

$lang["user_title"] = "Application Users";
$lang["user_username"] = "Username";
$lang["user_name"] = "Name";
$lang["user_status"] = "Status";
$lang["user_employee"] = "Employee";
$lang["user_password"] = "Password";
$lang["user_confirm_password"] = "Confirm Password";
$lang["user_active"] = "Active";
$lang["user_inactive"] = "Inactive";
$lang["user_roles"] = "Role(s)";
$lang["user_role_hint"] = "Press and hold the Ctrl key to select multiple roles";
$lang["user_choice_all"] = "-- All --";