<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * sia_edu_grade language file (EN)
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */

$lang["edugrd_title"]           = "Grade & Level";
$lang["edugrd_grade"]           = "Grade";
$lang["edugrd_grade_name"]      = "Grade Name";
$lang["edugrd_level"]           = "Level";
$lang["edugrd_level_name"]      = "Level Name";
$lang["edugrd_add_level"]       = "Add Level";
$lang["edugrd_add_level_hint"]  = "Add new level";
$lang["edugrd_level_count"]     = "Number of Levels";
$lang["edugrd_student_count"]   = "Number of Students";
$lang["edugrd_save_level_hint"] = "Save grade first to add level(s)";
