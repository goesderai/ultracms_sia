<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * sia_main_account language file (EN)
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */

$lang["main_account_parent"] = "Parent";
$lang["main_account_code"] = "Account Code";
$lang["main_account_name"] = "Account Name";
$lang["main_account_code_exists"] = "Account code already exist!";
$lang["main_account_has_children"] = "Account has child/children, delete the child/children first!";
$lang["main_account_has_posting_profile"] = "Account has posting profile, delete the posting profile first!";
