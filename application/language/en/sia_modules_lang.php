<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * sia_modules language file (EN)
 * 
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
 
$lang["module_title"] = "Application Modules";
$lang["module_parent"] = "Parent Module";
$lang["module_class_name"] = "Class Name";
$lang["module_description"] = "Description";
$lang["module_caption"] = "Caption";
$lang["module_icon_class"] = "Icon Class";
$lang["module_tree_level"] = "Tree Level";
$lang["module_display_order"] = "Display Order";
$lang["module_option_root"] = "-- No Parent (Root) --";
