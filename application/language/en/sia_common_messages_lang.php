<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Common messages language file (EN)
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */

// common messages
$lang["message_common_logout_successful"] = "You are now logged out.";
$lang["message_common_goto_login"] = "Go to login page";
$lang["message_common_save_success"] = "Data has been successfully saved.";
$lang["message_common_delete_success"] = "Data has been successfully deleted.";

// confirmation messages
$lang["message_confirm_close"] = "Are you sure you want to close this page?";
$lang["message_confirm_delete"] = "Are you sure you want to delete this data?";

// error messages
$lang["message_error_invalid_login_credentials"] = "Invalid Username and/or Password!";
$lang["message_error_invalid_recaptcha"] = "Please complete the reCaptcha challenge correctly!";
$lang["message_error_mandatory_fields"] = "Can't save data, the following errors must be fixed first!";
$lang["message_error_data_deletion"] = "Can't delete data, the following errors must be fixed first!";
$lang["message_error_invalid_request_method"] = "Request method not supported!";
