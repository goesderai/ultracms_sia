<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * sia_posting_profile_lang.php
 *
 * Posting profile language file (ID)
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */

$lang["pstngprfl_trx_type"] = "Tipe Transaksi";
$lang["pstngprfl_account_debit"] = "Akun Debit";
$lang["pstngprfl_account_credit"] = "Akun Kredit";
