<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * sia_edu_grade language file (ID)
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */

$lang["edugrd_title"]           = "Jenjang & Tingkatan";
$lang["edugrd_grade"]           = "Jenjang";
$lang["edugrd_grade_name"]      = "Nama Jenjang";
$lang["edugrd_level"]           = "Tingkatan";
$lang["edugrd_level_name"]      = "Nama Tingkatan";
$lang["edugrd_add_level"]       = "Tambah Tingkatan";
$lang["edugrd_add_level_hint"]  = "Tambah tingkatan baru";
$lang["edugrd_level_count"]     = "Jumlah Tingkatan";
$lang["edugrd_student_count"]   = "Jumlah Siswa";
$lang["edugrd_save_level_hint"] = "Simpan data jenjang terlebih dahulu untuk menambahkan data tingkatan";
