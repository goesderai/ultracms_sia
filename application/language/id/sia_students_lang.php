<?php defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * sia_students language file (ID)
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */

$lang["student_title"] = "Siswa";
$lang["student_tab_person_info"] = "Keterangan Siswa";
$lang["student_tab_parent_info"] = "Orang Tua / Wali";
$lang["student_tab_origin_info"] = "Asal Mula Siswa";
$lang["student_nis"] = "NIS";
$lang["student_name"] = "Nama";
$lang["student_nickname"] = "Nama Panggilan";
$lang["student_gender"] = "Jenis Kelamin";
$lang["student_educational_level"] = "Jenjang";
$lang["student_grade"] = "Tingkatan";
$lang["student_place_of_birth"] = "Tempat Lahir";
$lang["student_date_of_birth"] = "Tanggal Lahir";
$lang["student_weight"] = "Berat Badan (kg)";
$lang["student_height"] = "Tinggi Badan (cm)";
$lang["student_blood_type"] = "Golongan Darah";
$lang["student_illness_history"] = "Penyakit yang Pernah Diderita";
$lang["student_nth_child"] = "Anak Nomor ke...";
$lang["student_n_siblings"] = "Jumlah Saudara Kandung";
$lang["student_n_step_siblings"] = "Jumlah Saudara Tiri";
$lang["student_n_foster_siblings"] = "Jumlah Saudara Angkat";
$lang["student_daily_language"] = "Bahasa Sehari-Hari";
$lang["student_citizenship"] = "Kewarganegaraan";
$lang["student_religion"] = "Agama";
$lang["student_address"] = "Alamat";
$lang["student_phone"] = "No. Telephone";
$lang["student_status"] = "Status";
$lang["student_stayed_at"] = "Bertempat Tinggal di";
$lang["student_father_name"] = "Nama Ayah Kandung";
$lang["student_mother_name"] = "Nama Ibu Kandung";
$lang["student_father_last_education"] = "Pendidikan Terakhir Ayah Kandung";
$lang["student_mother_last_education"] = "Pendidikan Terakhir Ibu Kandung";
$lang["student_parent_ocupation"] = "Pekerjaan Orang Tua (Ayah/Ibu)";
$lang["student_parent_name"] = "Nama Orang Tua (Ayah/Ibu)";
$lang["student_guardian_name"] = "Nama Wali Siswa (Jika Ada)";
$lang["student_guardian_last_education"] = "Pendidikan Terakhir Wali Siswa";
$lang["student_guardian_relationship"] = "Hubungan Wali Terhadap Anak";
$lang["student_guardian_ocupation"] = "Pekerjaan Wali Siswa";
$lang["student_entered_school_as"] = "Masuk Sekolah ini Sebagai";
$lang["student_origin"] = "Asal Anak";
$lang["student_play_group_name"] = "Nama Kelompok Bermain";
$lang["student_play_group_location"] = "Lokasi Kelompok Bermain";
$lang["student_tahun_dan_nomor_sttb"] = "Tahun & Nomor STTB";
$lang["student_years_of_learning"] = "Lama Belajar";
$lang["student_origin_school_name"] = "Nama Sekolah Asal";
$lang["student_transfer_date"] = "Tanggal Pindah dari Sekolah Asal";
$lang["student_transfer_from_grade"] = "Pindah dari Tingkat";
$lang["student_accepted_date"] = "Tanggal Diterima";
$lang["student_accepted_grade"] = "Diterima di Tingkat";
$lang["student_choose"] = "-- Pilih --";
$lang["student_choose_educational_level"] = sprintf("-- Pilih %s --", $lang['student_educational_level']);
