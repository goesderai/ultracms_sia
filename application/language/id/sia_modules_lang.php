<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * sia_modules language file (ID)
 * 
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
 
$lang["module_title"] = "Modul Aplikasi";
$lang["module_parent"] = "Modul Induk";
$lang["module_class_name"] = "Nama Kelas";
$lang["module_description"] = "Deskripsi";
$lang["module_caption"] = "Judul";
$lang["module_icon_class"] = "Kelas Ikon";
$lang["module_tree_level"] = "Tingkat Pohon";
$lang["module_display_order"] = "Urutan Tampilan";
$lang["module_option_root"] = "-- Tanpa Induk (Root) --";
