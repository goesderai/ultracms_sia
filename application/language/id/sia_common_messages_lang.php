<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Common messages language file (ID)
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */

// common messages
$lang["message_common_logout_successful"] = "Anda telah keluar dari aplikasi.";
$lang["message_common_goto_login"] = "Pergi ke halaman masuk aplikasi";
$lang["message_common_save_success"] = "Data telah berhasil disimpan.";
$lang["message_common_delete_success"] = "Data telah berhasil dihapus.";

// confirmation messages
$lang["message_confirm_close"] = "Apakah anda yakin ingin menutup halaman ini?";
$lang["message_confirm_delete"] = "Apakah anda yakin ingin menghapus data ini?";

// error messages
$lang["message_error_invalid_login_credentials"] = "Nama Pengguna dan/atau Kata Sandi salah!";
$lang["message_error_invalid_recaptcha"] = "Tolong selesaikan tantangan reCaptcha dengan benar!";
$lang["message_error_mandatory_fields"] = "Tidak bisa menyimpan data, kesalahan-kesalahan berikut harus diperbaiki terlebih dahulu!";
$lang["message_error_data_deletion"] = "Tidak bisa menghapus data, kesalahan-kesalahan berikut harus diperbaiki terlebih dahulu!";
$lang["message_error_invalid_request_method"] = "Metode permintaan tidak didukung!";
