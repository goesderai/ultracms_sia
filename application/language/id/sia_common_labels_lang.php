<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Common labels language file (ID)
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
$lang["label_action"] = "Aksi";
$lang["label_search"] = "Cari";
$lang["label_reset"] = "Reset";
$lang["label_add_new"] = "Tambah";
$lang["label_add_new_hint"] = "Tambah data baru";
$lang["label_delete"] = "Hapus";
$lang["label_edit"] = "Sunting";
$lang["label_pagenav"] = "Navigasi halaman";
$lang["label_first_page"] = "Halaman pertama";
$lang["label_prev_page"] = "Halaman sebelumnya";
$lang["label_next_page"] = "Halaman berikutnya";
$lang["label_last_page"] = "Halaman terakhir";
$lang["label_page"] = "Halaman";
$lang["label_page_info"] = "Informasi halaman";
$lang["label_of"] = "dari";
$lang["label_save"] = "Simpan";
$lang["label_save_hint"] = "Simpan perubahan";
$lang["label_close"] = "Tutup";
$lang["label_close_hint"] = "Tutup halaman ini";
$lang["label_print_preview"] = "Pratinjau Cetakan";
$lang["label_change_password"] = "Ganti Kata Sandi";
$lang["label_login"] = "Masuk";
$lang["label_relogin"] = "Masuk Lagi";
$lang["label_logout"] = "Keluar";
$lang["label_username"] = "Nama Pengguna";
$lang["label_password"] = "Kata Sandi";
$lang["label_applogin"] = "Masuk Aplikasi";
$lang["label_confirmation"] = "Konfirmasi";
$lang["label_attention"] = "Perhatian!";
$lang["label_yes"] = "Ya";
$lang["label_no"] = "Tidak";
$lang["label_rownum"] = "#";
$lang["label_ok"] = "Ok";
$lang["label_all"] = "Semua";
