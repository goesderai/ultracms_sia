<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * sia_employees language file (ID)
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
 
$lang["employee_title"] = "Pegawai";
$lang["employee_name"] = "Nama";
$lang["employee_nickname"] = "Nama Panggilan";
$lang["employee_gender"] = "Jenis Kelamin";
$lang["employee_birth_place"] = "Tempat Lahir";
$lang["employee_birth_date"] = "Tanggal Lahir";
$lang["employee_identity_number"] = "No. Identitas";
$lang["employee_address"] = "Alamat";
$lang["employee_phone"] = "No. Telephone";
$lang["employee_position"] = "Jabatan";
$lang["employee_nett_salary"] = "Gaji Pokok";
$lang["employee_total_salary"] = "Total Gaji";
$lang["employee_benefit"] = "Tunjangan";
$lang["employee_others"] = "Lain-Lain";
