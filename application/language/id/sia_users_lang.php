<?php defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * sia_user language file (ID)
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */

$lang["user_title"] = "Pengguna Aplikasi";
$lang["user_username"] = "Nama Pengguna";
$lang["user_name"] = "Nama";
$lang["user_status"] = "Status";
$lang["user_employee"] = "Pegawai";
$lang["user_password"] = "Kata Sandi";
$lang["user_confirm_password"] = "Konfirmasi Kata Sandi";
$lang["user_active"] = "Aktif";
$lang["user_inactive"] = "Non-aktif";
$lang["user_roles"] = "Peran";
$lang["user_role_hint"] = "Tekan dan tahan tombol Ctrl untuk memilih banyak peran";
$lang["user_choice_all"] = "-- Semua --";