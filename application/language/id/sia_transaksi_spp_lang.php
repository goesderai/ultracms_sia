<?php defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * sia_transaksi_spp language file (ID)
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */

$lang["trxspp_page_title"] = "Pendapatan SPP";
$lang["trxspp_nis"] = "NIS";
$lang["trxspp_student_name"] = "Nama Siswa";
$lang["trxspp_student_grade_class"] = "Tingkat - Ruang Kelas";
$lang["trxspp_transaction_date"] = "Tanggal Transaksi";
$lang["trxspp_transaction_number"] = "Nomor Transaksi";
$lang["trxspp_description"] = "Keterangan";
$lang["trxspp_year"] = "Tahun";
$lang["trxspp_amount"] = "Jumlah";
$lang["trxspp_monthly_amount"] = "Jumlah Bayar per Bulan";
$lang["trxspp_monthly_discount"] = "Diskon (Rp./bulan)";
$lang["trxspp_total_amount"] = "Total Bayar";
$lang["trxspp_choose_month"] = "Pilih Bulan";
$lang["trxspp_until"] = "s/d";
