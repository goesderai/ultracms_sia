<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * sia_main_account language file (ID)
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */

$lang["main_account_parent"] = "Akun Induk";
$lang["main_account_code"] = "Kode Akun";
$lang["main_account_name"] = "Nama Akun";
$lang["main_account_code_exists"] = "Kode akun sudah terdaftar!";
$lang["main_account_has_children"] = "Kode akun memiliki anak, hapus anaknya terlebih dahulu!";
$lang["main_account_has_posting_profile"] = "Kode akun memiliki posting profile, hapus posting profilenya terlebih dahulu!";
