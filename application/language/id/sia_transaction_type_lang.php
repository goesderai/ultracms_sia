<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * sia_transaction_type language file (ID)
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */

$lang["trxtype_type"] = "Tipe Transaksi";
$lang["trxtype_name"] = "Nama Tipe Transaksi";
$lang["trxtype_code"] = "Kode Tipe Transaksi";
$lang["trxtype_parent"] = "Induk Tipe Transaksi";
$lang["trxtype_doc_type"] = "Tipe Dokumen";
$lang["trxtype_active_inactive"] = "Aktif / Non Aktif";
$lang["trxtype_debit_credit"] = "Debet / Kredit";
$lang["trxtype_debit"] = "Debet";
$lang["trxtype_credit"] = "Kredit";
$lang["trxtype_active"] = "Aktif";
$lang["trxtype_inactive"] = "Non Aktif";
$lang["trxtype_sub_type"] = "Sub Tipe Transaksi";
