<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*******************************************************************************************
 *
 * Class Name : phpReportGenerator
 * Version    : 1.0
 * Written By : Hasin Hayder
 * Start Date : 4th July, 2004
 * Copyright  : Systech Digital.
 *
 ********************************************************************************************
 *
 * Script to generate report from a valid mysql connection.
 * user have to supply which fields he want to display in table.
 * All properties are changable.
 *
 */
class PhpReportGenerator
{

  var $mysql_resource;
  var $header;
  var $footer;
  var $fieldsWidth = array(); // Lebar masing-masing kolom (termasuk kolom nomor) dalam satuan persen
  var $fields = array();
  var $dateFields = array(); // List of date fields with its format, eg: array('date1'=>'Y-m-d', ...)
  var $numberFields = array(); // List of number fields
  var $numberFieldsSummaries = array();
  var $row_number = 0;
  var $show_number_column = true;

  public function generateReport()
  {
    // any rows ?
    if (is_null($this->mysql_resource) || $this->mysql_resource->num_rows() <= 0) {
      $errMsg = '<p class="text-center">Maaf, tidak ada data untuk laporan ini.</p>';
      die ($errMsg);
    }

    /*
     * Lets calculate how many fields are there in supplied resource
     * and store their name in $this->fields[] array
     */
    $field_data = $this->mysql_resource->field_data();
    $field_count = $this->mysql_resource->num_fields();
    $i = 0;
    while ($i < $field_count) {
      $field = $field_data[$i];
      $this->fields[$i] = $field->name;
      $i++;
    }

    //Render report title
    echo "<b><i>{$this->header}</i></b>";
    echo "<p>&nbsp;</p>";

    //Render table header
    $this->renderTableHead($field_count);

    //BEGIN Render table body
    echo "<tbody>";
    for ($j = 0; $j < $this->mysql_resource->num_rows(); $j++) {
      $row = $this->mysql_resource->row_array($j);
      echo "<tr>";
      for ($i = 0; $i < $field_count; $i++) {
        //Render row number column
        if ($i == 0) {
          if ($this->row_number == 0) {
            $this->row_number = 1;
          } else {
            $this->row_number = $this->row_number + 1;
          }
          if ($this->show_number_column) echo "<td valign=\"top\">{$this->row_number}</td>";
        }

        //Prep data
        $column = $this->fields[$i];
        $columnData = $row[$column];
        $columnAlignment = "left";
        if (!empty($this->dateFields) && array_key_exists($column, $this->dateFields)) {
          $dateFormat = $this->dateFields[$column];
          $columnData = date($dateFormat, strtotime($columnData));
        }
        if (!empty($this->numberFields) && in_array($column, $this->numberFields)) {
          $this->updateSummaryFields($column, $columnData);
          $columnData = number_format((float)$columnData, 2, '.', ',');
          $columnAlignment = "right";
        }

        //Render column
        echo "<td valign=\"top\" style=\"text-align: {$columnAlignment}\">{$columnData}</td>";
      }
      echo "</tr>";
    }
    echo "</tbody>";
    //END Render table body

    // Render summary
    if (!empty($this->numberFieldsSummaries)) {
      $this->renderTableSummary($field_count);
    }

    // Closing table tag
    echo "</table>";
  }

  private function renderTableSummary($fieldCount)
  {
    // Find the first number column
    $colSpan = 0;
    for ($i = 0; $i < $fieldCount; $i++) {
      $colSpan++;
      $column = $this->fields[$i];
      if (in_array($column, $this->numberFields)) {
        break;
      }
    }

    // Render summary
    echo ($colSpan >= 2) ? "<tr><td valign=\"top\" style=\"text-align: right; padding-right: 10px\" colspan=\"{$colSpan}\"><b>Total:</b></td>" :
      "<tr><td valign=\"top\" style=\"text-align: right; padding-right: 10px\"><b>Total:</b></td>";
    for ($i = ($colSpan - 1); $i < $fieldCount; $i++) {
      $cell = $this->fields[$i];
      if (array_key_exists($cell, $this->numberFieldsSummaries)) {
        $sum = number_format((float)$this->numberFieldsSummaries[$cell], 2, '.', ',');
        echo "<td valign=\"top\" style=\"text-align: right\"><b>{$sum}</b></td>";
      } else {
        echo "<td valign=\"top\">&nbsp;</td>";
      }
    }
    echo "</tr>";
  }

  private function renderTableHead($fieldCount)
  {
    echo "<table id=\"reportTable\" class=\"table table-bordered table-condensed table-striped display\"><thead><tr>";
    if ($this->show_number_column) {
      echo !empty($this->fieldsWidth) ? "<th style=\"width: {$this->fieldsWidth[0]}%\" class=\"text-center\">#</th>" :
        "<th class=\"text-center\">#</th>";
    }
    for ($i = 0; $i < $fieldCount; $i++) {
      $fieldName = ucfirst($this->fields[$i]);
      $fieldWidth = !empty($this->fieldsWidth) ? $this->fieldsWidth[$i + 1] . '%' : '';
      echo "<th style=\"width: {$fieldWidth}\" class=\"text-center\">{$fieldName}</th>";
    }
    echo "</tr></thead>";
  }

  private function updateSummaryFields($column, $columnData)
  {
    if (!isset($this->numberFieldsSummaries[$column])) {
      $this->numberFieldsSummaries[$column] = $columnData;
    } else {
      $this->numberFieldsSummaries[$column] = $this->numberFieldsSummaries[$column] + $columnData;
    }
  }

}
