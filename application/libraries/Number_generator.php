<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Number generator
 * 
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
class Number_generator {
    
    protected $CI;
    
    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->model(array("DAO_Sequence"));
    }
    
    private function getYear(){
        return date('Y');
    }
    
    private function getMonth(){
        return date('m');
    }
    
    public function generateInvoiceNumber(){
        $seqName = sprintf("INVOICE-%s-%s", $this->getYear(), $this->getMonth());
        $seqRemark = sprintf("Sequence invoice tahun %s bulan %s", $this->getYear(), $this->getMonth());
        $seqNum = $this->CI->DAO_Sequence->getSequence($seqName, $seqRemark);
        $invoiceNum = sprintf("INV-%s-%s-%04d", $this->getYear(), $this->getMonth(), $seqNum);
        return $invoiceNum;
    }
    
}