<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Sidebar menu tree generator
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
class Sidebar_menu_generator {
	
	public function generateTree($menuData, $cssClass = 'nav navbar-nav side-nav', $listId=''){
		if(is_null($menuData) || empty($menuData)){
			return '';
		}
		
		// Generating tree
		$html = ($cssClass == 'collapse' && !empty($listId))? "<ul id=\"{$listId}\" class=\"{$cssClass}\">" : "<ul class=\"{$cssClass}\">";
		foreach ($menuData as $menu){
			$url = empty($menu->class_name)? "#" : site_url($menu->class_name);
			$menuCaption = htmlentities($menu->caption, ENT_QUOTES);
			if(empty($menu->children)){
				$html .= "<li><a href=\"{$url}\"><i class=\"fa fa-fw {$menu->icon}\"></i> {$menuCaption}</a></li>";
			}else{
				$ulId = uniqid('collapse_');
				$html .= "<li><a href=\"{$url}\" data-toggle=\"collapse\" data-target=\"#{$ulId}\"><i class=\"fa fa-fw {$menu->icon}\"></i> {$menuCaption}</a>"
						     . $this->generateTree($menu->children, 'collapse', $ulId)
						     . "</li>";
			}
		}
		$html .= "</ul>";
		return $html;
	}
	
}