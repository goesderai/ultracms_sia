<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseDAO.php';
require_once 'ICrud.php';

/**
 * DAO Tipe Transaksi
 *
 * @author <a href="mailto:agung3ka@gmail.com">Turah Eka</a>
 */
class DAO_TipeTransaksi extends BaseDAO implements ICrud
{

  var $table = 'gen_md_trx_type';
  var $vActiveTrxType ='v_trx_type_ids';

  public function __construct()
  {
    parent::__construct();
    $this->load->model(['DAO_System_Parameter']);
  }

  public function getList($filters = [], $sorts = [], $firstRowPos = 0, $pageSize = 10)
  {
    $sql = '';
    if (!empty($filters)) {
      $sql = 'select * from ' .
             $this->table . ' s where 1 = 1 ' .
             $this->buildWhereSql('s', $filters) . ' order by s.transaction_type' .
             ' limit ' . $firstRowPos . ', ' . $pageSize;
    }
    $qry = $this->db->query($sql);
    return $qry->result();
  }

  public function getListCount($filters)
  {
    if (is_null($filters) || !is_array($filters)) {
      return 0;
    } else {
      $sql = 'select count(*) as nrecs from ' .
             $this->table . ' s where 1 = 1 ' .
             $this->buildWhereSql('s', $filters);
      $qry = $this->db->query($sql);
      $row = $qry->row();
      return $row->nrecs;
    }
  }

  public function insert($data)
  {
    if (is_null($data) || !is_array($data)) {
      return false;
    } else {
      $this->db->insert($this->table, $data);
    }
  }

  public function update($data, $id)
  {
    if (is_null($data) || !is_array($data) || is_null($id) || empty($id)) {
      return false;
    } else {
      $this->db->where('id', $id);
      $this->db->update($this->table, $data);
    }
  }

  public function delete($id)
  {
    if (is_null($id) || empty($id)) {
      return false;
    } else {
      $this->db->where('id', $id);
      $this->db->update($this->table);
    }
  }

  public function getTransactionByDocTypeAndParentId($flow, $docType, $parent, $subType)
  {
    $sql = '';
    if ($flow != null) {
      if ($parent == null) {
        if ($flow == 'D') {
          $sql = 'select * from ' . $this->table . ' a where 1=1 AND a.id <> 1 AND a.id <> 2 AND a.active = "Y" and a.transaction_flow = "' . $flow . '" and a.document_type_id =' . $docType . ' and a.parent_type is not null and a.is_sub_type = ' . $subType . ' order by a.transaction_type';
        } else {
          $sql = 'select * from ' . $this->table . ' a where 1=1 AND a.id <> 1 AND a.id <> 2 AND a.active = "Y" and a.transaction_flow = "' . $flow . '" and a.document_type_id =' . $docType . ' and a.parent_type is null and a.is_sub_type = ' . $subType . ' order by a.transaction_type';
        }

      } else {
        $sql = 'select * from ' . $this->table . ' a where 1=1 AND a.id <> 1 AND a.id <> 2 AND a.active = "Y" and a.transaction_flow = "' . $flow . '" and a.document_type_id =' . $docType . ' and a.parent_type = ' . $parent . ' and a.is_sub_type = ' . $subType . ' order by a.transaction_type';
      }

    } else {
      $sql = 'select * from ' . $this->table . ' a where 1=1 AND a.id <> 1 AND a.id <> 2 AND a.active = "Y" and a.document_type_id =' . $docType . ' and a.is_sub_type = 1 order by a.transaction_type';
    }
    $qry = $this->db->query($sql);
    return $qry->result();
  }

  public function getTransactionByDocType($flow, $docType)
  {
    $sql = '';
    if ($flow != null) {
      $sql = 'select * from ' . $this->table . ' a where 1=1 AND a.id <> 1 AND a.id <> 2 AND a.active = "Y" and a.transaction_flow = "' . $flow . '" and a.document_type_id =' . $docType . ' and a.is_sub_type = 1 order by a.transaction_type';
    } else {
      $sql = 'select * from ' . $this->table . ' a where 1=1 AND a.id <> 1 AND a.id <> 2 AND a.active = "Y" and a.document_type_id =' . $docType . ' and a.is_sub_type = 1 order by a.transaction_type';
    }

    $qry = $this->db->query($sql);
    return $qry->result();
  }

  public function getAll($flow, $docType)
  {
    $sql = '';
    if (!empty($flow) && !empty($docType)) {
      $sql = 'select * from ' . $this->table . ' a where 1=1 AND a.id <> 1 AND a.id <> 2 AND a.active = "Y" and a.transaction_flow = "' . $flow . '" and a.document_type_id =' . $docType . ' order by a.transaction_type';
    }
    if (empty($flow) && !empty($docType)) {
      $sql = 'select * from ' . $this->table . ' a where 1=1 AND a.id <> 1 AND a.id <> 2 AND a.active = "Y" and a.document_type_id =' . $docType . ' order by a.transaction_type';
    }

    $qry = $this->db->query($sql);
    return $qry->result();
  }
  
public function getAllActiveTrxType($flow = null)
  {
    $sql = 'select * from ' . $this->vActiveTrxType . ' a where 1=1 ';
    if($flow != null){
    	$sql = $sql.' and a.flow = "'.$flow.'"';
    }
    $sql = $sql.' order by a.transaction_type';
    $qry = $this->db->query($sql);
    return $qry->result();
  }

  public function getAllType()
  {
    $sql = 'select * from ' . $this->table . ' a where 1=1  AND a.active = "Y" order by a.transaction_type';

    $qry = $this->db->query($sql);
    return $qry->result();
  }

  public function getAllParentType($dc, $docType)
  {

    $sql = "";
    if ($dc == null) {
      $sql = 'select * from ' . $this->table . ' a where 1=1  AND a.active = "Y" and a.is_sub_type = 0 order by a.transaction_type';
    } else {
      $sql = 'select * from ' . $this->table . ' a where 1=1  AND a.active = "Y" and a.transaction_flow = "' . $dc . '" and a.document_type_id=' . $docType . ' and a.parent_type is null order by a.transaction_type';
    }
    $qry = $this->db->query($sql);
    return $qry->result();
  }

  public function getAllSubType($parent, $docType)
  {
    $sql = 'select * from ' . $this->table . ' a where 1=1  AND a.active = "Y" and a.parent_type = ' . $parent . ' and a.document_type_id = ' . $docType . ' order by a.transaction_type';
    $qry = $this->db->query($sql);
    return $qry->result();
  }

  public function getAllAsArray($dc, $docType = 0, $emptyOption = null)
  {
    $tipeTransaksiRs = $this->getAll($dc, $docType);
    $tipeTransaksi   = [];
    foreach ($tipeTransaksiRs as $row) {
      $tipeTransaksi[$row->id] = $row->nama;
    }
    if (!is_null($emptyOption) && !empty($emptyOption) && is_array($emptyOption)) {
      return $emptyOption + $tipeTransaksi;
    } else {
      return $tipeTransaksi;
    }
  }
  
public function getAllActiveTrxTypeAsArray($emptyOption = null, $flow = null)
  {
    $tipeTransaksiRs = $this->getAllActiveTrxType($flow);
    $tipeTransaksi = array();
    foreach ($tipeTransaksiRs as $row) {
      $tipeTransaksi[$row->id] = $row->nama;
    }
    if (!is_null($emptyOption) && !empty($emptyOption) && is_array($emptyOption)) {
      return $emptyOption + $tipeTransaksi;
    } else {
      return $tipeTransaksi;
    }
  }

  public function getPendapatanTransaction($dc, $docType, $parent, $subType, $emptyOption = null)
  {
    $tipeTransaksiRs = $this->getTransactionByDocTypeAndParentId($dc, $docType, $parent, $subType);
    $tipeTransaksi   = [];
    foreach ($tipeTransaksiRs as $row) {
      $tipeTransaksi[$row->id] = $row->nama;
    }
    if (!is_null($emptyOption) && !empty($emptyOption) && is_array($emptyOption)) {
      return $emptyOption + $tipeTransaksi;
    } else {
      return $tipeTransaksi;
    }
  }

  public function getPembayaranTransaction($dc, $docType, $parent, $subType, $emptyOption = null)
  {
    $tipeTransaksiRs = $this->getTransactionByDocTypeAndParentId($dc, $docType, $parent, $subType);
    $tipeTransaksi   = [];
    foreach ($tipeTransaksiRs as $row) {
      $tipeTransaksi[$row->id] = $row->nama;
    }
    if (!is_null($emptyOption) && !empty($emptyOption) && is_array($emptyOption)) {
      return $emptyOption + $tipeTransaksi;
    } else {
      return $tipeTransaksi;
    }
  }

  public function getPembayaranGajiTransaction($dc, $docType, $emptyOption = null)
  {
    $tipeTransaksiRs = $this->getTransactionByDocType($dc, $docType);
    $tipeTransaksi   = [];
    foreach ($tipeTransaksiRs as $row) {
      $tipeTransaksi[$row->id] = $row->nama;
    }
    if (!is_null($emptyOption) && !empty($emptyOption) && is_array($emptyOption)) {
      return $emptyOption + $tipeTransaksi;
    } else {
      return $tipeTransaksi;
    }
  }

  public function getAllTrxTypeAsArray($emptyOption = null)
  {
    $tipeTransaksiRs = $this->getAllType();
    $tipeTransaksi   = [];
    foreach ($tipeTransaksiRs as $row) {
      $tipeTransaksi[$row->id] = $row->nama;
    }
    if (!is_null($emptyOption) && !empty($emptyOption) && is_array($emptyOption)) {
      return $emptyOption + $tipeTransaksi;
    } else {
      return $tipeTransaksi;
    }
  }

  public function getAllTrxParentTypeAsArray($emptyOption = null)
  {
    $tipeTransaksiRs = $this->getAllParentType(null, null);
    $tipeTransaksi   = [];
    foreach ($tipeTransaksiRs as $row) {
      $tipeTransaksi[$row->id] = $row->nama;
    }
    if (!is_null($emptyOption) && !empty($emptyOption) && is_array($emptyOption)) {
      return $emptyOption + $tipeTransaksi;
    } else {
      return $tipeTransaksi;
    }
  }

  public function getDCTrxParentTypeAsArray($dc, $docType, $emptyOption = null)
  {
    $tipeTransaksiRs = $this->getAllParentType($dc, $docType);
    $tipeTransaksi   = [];
    foreach ($tipeTransaksiRs as $row) {
      $tipeTransaksi[$row->id] = $row->nama;
    }
    if (!is_null($emptyOption) && !empty($emptyOption) && is_array($emptyOption)) {
      return $emptyOption + $tipeTransaksi;
    } else {
      return $tipeTransaksi;
    }
  }

  public function getDCTrxSubTypeAsArray($parent, $docType, $emptyOption = null)
  {
    $tipeTransaksiRs = $this->getAllSubType($parent, $docType);
    $tipeTransaksi   = [];
    $tipeTransaksi[] = "-- Pilih Tipe Transaksi --";
    foreach ($tipeTransaksiRs as $row) {
      $tipeTransaksi[$row->id] = $row->nama;
    }
    if (!is_null($emptyOption) && !empty($emptyOption) && is_array($emptyOption)) {
      return $emptyOption + $tipeTransaksi;
    } else {
      return $tipeTransaksi;
    }
  }

  public function getRow($id)
  {
    if (is_null($id) || empty($id) || !is_numeric($id)) {
      return null;
    } else {
      return $this->getRowByCriteria($this->table, ['id' => $id]);
    }
  }

  public function getTipeTransaksi($trxTyp)
  {
    if (is_null($trxTyp) || empty($trxTyp)) {
      return null;
    } else {
      $sql = 'select * from ' . $this->table . ' where transaction_type = ? and active = ?';
      $qry = $this->db->query($sql, [$trxTyp, "Y"]);
      return $qry->row_array();
    }
  }

  public function getTransactionTypePiutang($emptyOption = null)
  {
    // TRX_TYPE_PARENT_PIUTANG
    $parent          = $this->DAO_System_Parameter->getValue('TRX_TYPE_PARENT_PIUTANG');
    $sql             = 'select * from ' . $this->table . ' a where 1=1  AND a.active = ? and a.parent_type = ? order by a.transaction_type';
    $qry             = $this->db->query($sql, ['Y', $parent]);
    $tipeTransaksiRs = $qry->result();
    $tipeTransaksi   = [];
    foreach ($tipeTransaksiRs as $row) {
      $tipeTransaksi[$row->id] = $row->nama;
    }
    if (!is_null($emptyOption) && !empty($emptyOption) && is_array($emptyOption)) {
      return $emptyOption + $tipeTransaksi;
    } else {
      return $tipeTransaksi;
    }
  }

  //-------------------- BEGIN Function for getting IDs tree of a type --------------------//

  private $holder = [];

  public function getIdsTree($id)
  {
    $this->holder[] = $id;
    if ($this->hasChildren($id)) {
      $this->appendChildren($id, $this->holder);
    }
    return $this->holder;
  }

  private function appendChildren($parentId)
  {
    $qry = $this->db->query("select * from `{$this->table}` where `parent_type`=" .
                            $this->sanitizeArg($parentId) .
                            " order by `id`");

    if (!in_array($parentId, $this->holder)) $this->holder[] = $parentId;
    foreach ($qry->result() as $child) {
      if (!in_array($child->id, $this->holder)) $this->holder[] = $child->id;
      if ($this->hasChildren($child->id)) {
        $this->appendChildren($child->id, $this->holder);
      }
    }
  }

  private function hasChildren($id)
  {
    $qry = $this->db->query("select count(*) as n_recs from `{$this->table}` where `parent_type`=" . $this->sanitizeArg($id));
    $row = $qry->row();
    return ($row->n_recs > 0);
  }

  //-------------------- END Function for getting IDs tree of a type --------------------//

}
