<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseDAO.php';

/**
 * DAO_Edu
 * DAO untuk tabel-tabel yang berhubungan dengan data pendidikan/studi
 * @author Goesde Rai
 */
class DAO_Edu extends BaseDAO {
	
	var $tableGrade = 'm_edu_grade';
	var $tableGradeMutation = 'edu_student_grade_mutation';
	var $tableGradeMutationDetail = 'edu_student_grade_mutation_detail';
	var $tableClassroom = 'm_edu_classroom';
	
	public function __construct(){
		parent::__construct();
	}
	
	// BEGIN Grade functions -------------------------------------------------------------------------------------------------
	public function getGradeList(){
		$sql = "select g.id, g.name, g.active, " .
			"(select count(*) from {$this->tableClassroom} cr where cr.grade_id = g.id) classrooms " .
			"from {$this->tableGrade} g where g.active='Y' order by g.name";
		$qry = $this->db->query($sql);
		return $qry->result();
	}
	
	public function insertGrade($data){
		if(is_null($data) || !is_array($data)){
			return false;
		}else{
			$this->db->insert($this->tableGrade, $data);
		}
	}
	
	public function updateGrade($data, $id){
		if(is_null($data) || !is_array($data) || is_null($id) || empty($id)){
			return false;
		}else{
			$this->db->where('id', $id);
			$this->db->update($this->tableGrade, $data);
		}
	}
	
	public function getGradeSelectOptions(){
		$retval = array();
		$qry = $this->db->query("select * from {$this->tableGrade} g where g.active='Y' order by g.level");
		foreach($qry->result() as $row){
			$retval[$row->id] = $row->name;
		}
		return $retval;
	}
	
	public function getGradeById($id){
		if(is_null($id) || empty($id) || !is_numeric($id)){
			return null;
		}else{
			$sql = 'select * from ' . $this->tableGrade . ' where id = ?';
			$qry = $this->db->query($sql, array($id));
			return $qry->row_array();
		}
	}
	// END Grade functions ---------------------------------------------------------------------------------------------------
	
	// BEGIN Grade mutation functions ----------------------------------------------------------------------------------------
	public function insertGradeMutation($header, $studentIds){
	    if(is_null($header) || !is_array($header) || is_null($studentIds) || !is_array($studentIds)){
			return false;
		}else{
			// insert header
		    $this->db->insert($this->tableGradeMutation, $header);
		    
		    // insert detail
			$headerId = $this->db->insert_id();
			foreach ($studentIds as $studentId){
			    $siswa = $this->getRowByCriteria('m_student', array('id' => $studentId));
			    $detailData = array(
			        'mutation_id' => $headerId,
			        'student_id' => $studentId,
			        'previous_classroom_id' => $siswa['classroom_id'],
			        'created_date' => $header['created_date'],
			        'created_by' => $header['created_by']
			    );
			    $this->db->insert($this->tableGradeMutationDetail, $detailData);
			}
			
			// update students
			$studentIdsCsv = implode(",", $studentIds);
			$this->db->query("update `m_student` set `classroom_id`=? where `id` in ({$studentIdsCsv})",
			    array($header['dest_classroom_id']));
		}
	}
	
	public function getGradeMutationList($filters = array(), $sorts = array(), $firstRowPos = 0, $pageSize = 10){
		$sql = "SELECT m.id, m.mutation_date, cr.name as classroom_name, grd.name as grade_name 
            FROM `{$this->tableGradeMutation}` m inner join `m_edu_classroom` cr on cr.id = m.dest_classroom_id 
            inner join `m_edu_grade` grd on grd.id = cr.grade_id 
            where 1=1 ";
		if(!empty($filters)){
			$sql .= $this->buildWhereSql_2($filters) . " limit {$firstRowPos}, {$pageSize}";
		}else{
			$sql .= "limit {$firstRowPos}, {$pageSize}";
		}
		$qry = $this->db->query($sql);
		return $qry->result();
	}
	
	public function getGradeMutationListCount($filters){
		if(is_null($filters) || !is_array($filters)){
			return 0;
		}else{
		  $sql = "select count(m.id) as nrecs from `{$this->tableGradeMutation}` m 
              inner join `m_edu_classroom` cr on cr.id = m.dest_classroom_id 
              inner join `m_edu_grade` grd on grd.id = cr.grade_id 
              where 1=1 ";
		  if(!empty($filters)){
		  	$sql .= $this->buildWhereSql_2($filters);
		  }
			$qry = $this->db->query($sql);
			$row = $qry->row();
			return $row->nrecs;
		}
	}
	
	public function getGradeMutation($id){
	    if(is_null($id) || empty($id)){
	        return null;
	    }
	    $sql = "SELECT m.id, m.mutation_date, m.dest_classroom_id, cr.name AS `classroom_name`, grd.name AS `grade_name` 
                FROM {$this->tableGradeMutation} m 
                INNER JOIN m_edu_classroom cr ON cr.id = m.dest_classroom_id 
                INNER JOIN m_edu_grade grd ON grd.id = cr.grade_id 
                WHERE m.id = ?";
	    $qry = $this->db->query($sql, array($id));
	    return $qry->row();
	}
	
	public function getGradeMutationDetails($headerId){
	    if(is_null($headerId) || empty($headerId)){
	        return null;
	    }
	    $sql = "SELECT d.id, s.nis, d.student_id, s.name, grd.name AS gradeName, cr.name AS classroomName 
                FROM {$this->tableGradeMutationDetail} d 
                INNER JOIN m_student s ON s.id = d.student_id 
                INNER JOIN {$this->tableClassroom} cr ON cr.id = d.previous_classroom_id 
                INNER JOIN {$this->tableGrade} grd ON grd.id = cr.grade_id 
                WHERE d.mutation_id = ?";
	    return $this->db->query($sql, array($headerId));
	}
	// END Grade mutation functions ------------------------------------------------------------------------------------------
	
	// BEGIN Classroom functions ---------------------------------------------------------------------------------------------
	public function getClassroomSelectOptions($gradeId=null){
		$retval = array();
		$sql = "select * from {$this->tableClassroom} c ";
		$qry = null;
		if(is_null($gradeId) || empty($gradeId)){
			$sql .= "order by c.level";
			$qry = $this->db->query($sql);
		}else{
			$sql .= "where c.grade_id=? order by c.level";
			$qry = $this->db->query($sql, array($gradeId));			
		}
		foreach($qry->result() as $row){
		    $retval[$row->id] = $row->name;
		}
		return $retval;
	}
	
	public function getClassById($id){
		if(is_null($id) || empty($id) || !is_numeric($id)){
			return null;
		}else{
			$sql = 'select * from ' . $this->tableClassroom . ' where id = ?';
			$qry = $this->db->query($sql, array($id));
			return $qry->row_array();
		}
	}
	
	public function insertClassroom($data){
		if(empty($data) || !isset($data['grade_id'])){
			return false;
		}else{
			$this->db->insert($this->tableClassroom, $data);
		}
	}
	
	public function updateClassroom($data, $id){
		if(empty($id) || empty($data)){
			return false;
		}else{
			$this->db->where('id', $id);
			$this->db->update($this->tableClassroom, $data);
		}
	}
	
	public function deleteClassroom($id){
		if(empty($id)){
			return false;
		}else{
			$this->db->where('id', $id);
			$this->db->delete($this->tableClassroom);
		}
	}
	
	public function isClassroomNameExists($classroomName){
		if(empty($classroomName)){
			return false;
		}else{
			$qry = $this->db->query("select count(*) nrecs from {$this->tableClassroom} cr where cr.name=?", 
				array($classroomName));
			$row = $qry->row();
			return ($row->nrecs > 0);
		}
	}
	// END Classroom functions -----------------------------------------------------------------------------------------------
	
	// BEGIN Misc functions --------------------------------------------------------------------------------------------------
	public function getGradeAndClassroomLabel($studentId){
		if(is_null($studentId) || empty($studentId)){
			return "";
		}
		
		$siswa = null;
		if(!isset($this->DAO_Siswa)){
			$this->load->model('DAO_Siswa');
		}
		$siswa = $this->DAO_Siswa->getRow($studentId);
		
		if(!is_null($siswa)){
			$sql = "SELECT g.name grade_name, cr.name classroom_name " .
				"FROM {$this->tableClassroom} cr JOIN {$this->tableGrade} g " .
				"ON cr.grade_id = g.id AND cr.id=?";
			$qry = $this->db->query($sql, array($siswa['classroom_id']));
			$row = $qry->row();
			if(!is_null($row)){
				return sprintf("%s - %s", $row->grade_name, $row->classroom_name);
			}else{
				return "";
			}
		}else{
			return "";
		}
	}
	// END Misc functions ----------------------------------------------------------------------------------------------------
	
}