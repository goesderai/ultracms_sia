<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseDAO.php';
require_once 'ICrud.php';

class DAO_Employee extends BaseDAO implements ICrud{
	
	var $table = 'm_employee';
	var $vKasbonEmployee = 'v_kasbon_pegawai';
	
	public function __construct(){
		parent::__construct();
	}
	
	public function getRow($id){
		if(is_null($id) || empty($id) || !is_numeric($id)){
			return null;
		}else{
			return $this->getRowByCriteria($this->table, array('id' => $id));
		}
	}
	
	public function getRowKasbon($id){
		if(is_null($id) || empty($id) || !is_numeric($id)){
			return null;
		}else{
			$sql = 'select * from ' . $this->vKasbonEmployee . ' where employee_id = ?';
			$qry = $this->db->query($sql, array($id));
			return $qry->row_array();
		}
	}
	
	public function getList($filters = array(), $sorts = array(), $firstRowPos = 0, $pageSize = 10){
		$sql = '';
		if(!empty($filters)){
			$sql =	'select * from ' . 
							$this->table . ' s where 1 = 1 ' . 
							$this->buildWhereSql('s', $filters) . 
							' limit ' . $firstRowPos . ', ' . $pageSize;
		}
		$qry = $this->db->query($sql);
		return $qry->result();
	}
	
	public function getListCount($filters){
		if(is_null($filters) || !is_array($filters)){
			return 0;
		}else{
			$sql =	'select count(*) as nrecs from ' . 
							$this->table . ' s where 1 = 1 ' . 
							$this->buildWhereSql('s', $filters);
			$qry = $this->db->query($sql);
			$row = $qry->row();
			return $row->nrecs;
		}
	}
	
	public function insert($data){
		if(is_null($data) || !is_array($data)){
			return false;
		}else{
			$this->db->insert($this->table, $data);
		}
	}
	
	public function update($data, $id){
		if(is_null($data) || !is_array($data) || is_null($id) || empty($id)){
			return false;
		}else{
			$this->db->where('id', $id);
			$this->db->update($this->table, $data);
		}
	}
	
	public function delete($id){
		if(is_null($id) || empty($id)){
			return false;
		}else{
			$this->db->where('id', $id);
			$this->db->delete($this->table);
		}
	}
	
	public function getAll(){
		$sql = 'select * from ' . $this->table . ' a order by a.name';
		$qry = $this->db->query($sql);
		return $qry->result();
	}
	
	public function getAllAsArray(){
		$sql = 'select * from ' . $this->table . ' a order by a.name';
		$qry = $this->db->query($sql);
		return $qry->result_array();
	}
	
	public function isExists($data){
		if(is_null($data) || empty($data) || !is_array($data)){
			return false;
		}else{
			$sql = 'select count(*) as nrecs from ' . $this->table . ' where `name`=? and `id_number`=?';
			$qry = $this->db->query($sql, array($data['name'], $data['id_number']));
			$row = $qry->row();
			return ($row->nrecs > 0);
		}
	}
	
	public function getSelectOptionsForUserMapping(){
		$retval = array();
		$qry = $this->db->query("select `id`, `name` from {$this->table} where `active`='Y' order by `name`");
		foreach ($qry->result() as $row){
			$retval[$row->id] = $row->name;
		}
		return $retval;
	}
	
	public function getSelectOptions($emptyOption = null){
		$qry = $this->db->query("select * from {$this->table} where `active`='Y' order by `name`");
		$employeesRs = $qry->result();
		$employees = array();
		foreach ($employeesRs as $row){
			$employeesRs[$row->id] = $row->name." - ".$row->id_number;
		}
		if(!is_null($emptyOption) && !empty($emptyOption) && is_array($emptyOption)){
			return $emptyOption + $employeesRs;
		}else{
			return $employeesRs;
		}
	}
	
	public function getEmployeeCaptionById($id){
		$data = $this->getRow($id);
		return $data['name']." - ".$data['id_number'];
	}
	
	public function getRptEmployeeQuery($filters = array()){
		$sql =	'select s.id_number as `Nomor ID` , s.name as `Nama Karyawan`, s.nickname as `Nama Panggilan`, CASE s.gender WHEN "L" THEN "Laki-laki" WHEN "P" THEN "Perempuan" ELSE NULL END  as `Jenis Kelamin`, DATE_FORMAT(s.birth_date, "%l %M %Y") as `Tanggal Lahir`, s.address as `Alamat`, s.phone_number as `Nomor Telepon`, s.jabatan as `Jabatan` from `m_employee` s where 1 = 1 ' ;
		if(!empty($filters)){
			$sql = $sql.$this->buildWhereSql('s', $filters);
		}
		return $sql;
		
	}
	
}