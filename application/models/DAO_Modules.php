<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseDAO.php';
require_once 'ICrud.php';

/**
 * Application menu DAO
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
class DAO_Modules extends BaseDAO implements ICrud {
	
	var $table = 'm_modules';
	
	public function __construct(){
		parent::__construct();
	}
	
	public function getList($filters = array(), $sorts = array(), $firstRowPos = 0, $pageSize = 10){
	    $sql = '';
	    if(!empty($filters)){
	        $sql = "SELECT 	m.`id`, m.`parent_id`, p.`caption` AS parent_name, m.`class_name`, m.`description`, m.`caption`, 
                    m.`icon`, m.`tree_level`, m.`display_order` 
                    FROM {$this->table} m LEFT JOIN {$this->table} p ON p.id = m.parent_id 
                    WHERE 1=1 " . $this->buildWhereSql("m", $filters) . " LIMIT {$firstRowPos}, {$pageSize}";
	    }
	    $qry = $this->db->query($sql);
	    return $qry->result();
	}
	
	public function getListCount($filters){
	    if(is_null($filters) || !is_array($filters)){
	        return 0;
	    }else{
	        $sql = "SELECT COUNT(m.id) AS nrecs FROM `{$this->table}` m 
                    WHERE 1=1 " . $this->buildWhereSql("m", $filters);
	        $qry = $this->db->query($sql);
	        $row = $qry->row();
	        return $row->nrecs;
	    }
	}
	
	public function insert($data){
		if(is_null($data) || empty($data) || !is_array($data)){
			return false;
		}else{
			$this->db->insert($this->table, $data);
		}
	}
	
	public function update($data, $id){
		if(is_null($data) || empty($data) || !is_array($data) || 
			is_null($id) || empty($id) || !is_numeric($id)){
			return false;
		}else{
			$this->db->where('id', $id);
			$this->db->update($this->table, $data);
		}
	}
	
	public function delete($id){
		if(is_null($id) || empty($id) || !is_numeric($id)){
			return false;
		}else{
			$this->db->where('id', $id);
			$this->db->delete($this->table);
		}
	}
	
	public function getRow($id){
		if(is_null($id) || empty($id) || !is_numeric($id)){
			return null;
		}else{
			return $this->getRowByCriteria($this->table, array('id' => $id));
		}
	}
	
	public function getAllRows($sort='caption'){
	    if(is_null($sort) || empty($sort)){
	        return null;
	    }
	    $sort = trim($sort);
	    $qry = $this->db->query("select * from {$this->table} t order by t.{$sort}");
	    return $qry->result();
	}
	
	public function getRoots(){
	    return $this->db->query("select * from {$this->table} t where `parent_id` is null order by t.caption")->result();
	}
	
	public function isClassNameExists($className){
	    if(is_null($className) || empty($className)){
	        return false;
	    }
	    $qry = $this->db->query("select count(*) as nrecs from {$this->table} where `class_name`=?", array($className));
	    $row = $qry->row();
	    return ($row->nrecs > 0);
	}
	
}