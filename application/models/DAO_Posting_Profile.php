<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseDAO.php';
require_once 'ICrud.php';

/**
 * DAO Posting Profile
 *
 * @author <a href="mailto:agung3ka@gmail.com">Eka D.</a>
 */
class DAO_Posting_Profile extends BaseDAO  implements ICrud {
	var $table = 'gen_trx_posting_profile';
	var $vList = 'v_list_posting_profile';
	
	public function __construct(){
		parent::__construct();
	}
	
	public function insert($data){
		if(is_null($data) || !is_array($data)){
			return false;
		}else{
			$this->db->insert($this->table, $data);
		}
	}
	
	public function update($data, $id){
		if(is_null($data) || !is_array($data) || is_null($id) || empty($id)){
			return false;
		}else{
			$this->db->where('id', $id);
			$this->db->update($this->table, $data);
		}
	}
	
	public function delete($id){
		if(is_null($id) || empty($id)){
			return false;
		}else{
			$this->db->where('id', $id);
			$this->db->update($this->table);
		}
	}
	
	public function getList($filters = array(), $sorts = array(), $firstRowPos = 0, $pageSize = 10){
		$sql = '';
		if(!empty($filters)){
			$sql =	'select * from ' . 
							$this->vList . ' s where 1 = 1 ' . 
							$this->buildWhereSql('s', $filters) . ' order by s.transaction_type '.
							' limit ' . $firstRowPos . ', ' . $pageSize;
		}else{
			$sql =	'select * from ' . 
							$this->vList . ' s where 1 = 1 ' . ' order by s.transaction_type '.
							' limit ' . $firstRowPos . ', ' . $pageSize;
		}
		$qry = $this->db->query($sql);
		return $qry->result();
	}
	
	public function getListCount($filters){
		if(is_null($filters) || !is_array($filters)){
			return 0;
		}else{
			$sql =	'select count(*) as nrecs from ' . 
							$this->vList . ' s where 1 = 1 ' . 
							$this->buildWhereSql('s', $filters);
			$qry = $this->db->query($sql);
			$row = $qry->row();
			return $row->nrecs;
		}
	}
	
	
	/*public function getAllAsArray(){
		$mainAccountRs = $this->getAll();
		$mainAccount = array();
		$mainAccount[] = "-- Pilih Kode Akun --";
		foreach ($mainAccountRs as $row){
			$mainAccount[$row->id] = $row->account_code ." - ".$row->name;
		}
		return $mainAccount;
	}*/
	
	/*public function getMainAccountName($id){
		$mainAccount = $this->getRow($id);
		if($id!=null){
			return $mainAccount['account_code'];
		}else{
			return '-';
		}
		
	}*/
	
	public function getRow($id){
		if(is_null($id) || empty($id) || !is_numeric($id)){
			return null;
		}else{
			return $this->getRowByCriteria($this->table, array('id' => $id));
		}
	}
	
	public function isTypeExis($idType){
		if(is_null($idType) || empty($idType) || !is_numeric($idType)){
			return false;
		}else{
			$postingProfile = $this->getPostingProfileByType($idType);
			if($postingProfile != null){
				return true;
			}else {
				return false;
			}
		}
	}
	
	public function getPostingProfileByType($idType){
		if(is_null($idType) || empty($idType) || !is_numeric($idType)){
			return null;
		}else{
			$sql = 'select * from ' . $this->table . ' where trx_type = ?';
			$qry = $this->db->query($sql, array($idType));
			if($qry->num_rows() > 0){
				return $qry->row_array();
			}else {
				return null;
			}
		}
	}
	
}

	