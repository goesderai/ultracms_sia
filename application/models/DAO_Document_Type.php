<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseDAO.php';

/**
 * DAO Dokument Type
 *
 * @author <a href="mailto:agung3ka@gmail.com">Turah Eka</a>
 */
class DAO_Document_Type extends BaseDAO {
	
	var $table = 'm_document_type';
	
	public function __construct(){
		parent::__construct();
	}
	
	public function getAllSelectOptions($emptyOption = null){
		$sql = sprintf("select * from %s a order by a.document_type", $this->table);
		$qry = $this->db->query($sql);
		$docTypeRs = $qry->result();
		$docTypes = array();
		foreach ($docTypeRs as $row){
			$docTypes[$row->id] = $row->document_type.' - '.$row->desc_doc_type;
		}
		if(!is_null($emptyOption) && !empty($emptyOption) && is_array($emptyOption)){
			return $emptyOption + $docTypes;
		}else{
			return $docTypes;
		}
	}

	public function getSelectOptions($emptyOption = null){
		$sql = sprintf("select * from %s a order by a.document_type", $this->table);
		$qry = $this->db->query($sql);
		$docTypeRs = $qry->result();
		$docTypes = array();
		foreach ($docTypeRs as $row){
			$docTypes[$row->id] = $row->document_type.' - '.$row->desc_doc_type;
		}
		if(!is_null($emptyOption) && !empty($emptyOption) && is_array($emptyOption)){
			return $emptyOption + $docTypes;
		}else{
			return $docTypes;
		}
	}
	
}
