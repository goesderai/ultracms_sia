<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseDAO.php';
require_once 'ICrud.php';

/**
 * DAO Main Account
 *
 * @author <a href="mailto:agung3ka@gmail.com">Eka D.</a>
 */
class DAO_Main_Account extends BaseDAO implements ICrud
{
  var $table             = 'm_main_account';
  var $t_posting_profile = 'gen_trx_posting_profile';

  public function __construct()
  {
    parent::__construct();
  }

  public function getAll()
  {
    $sql = 'select * from ' . $this->table . ' a where a.active = "Y" order by a.account_code';
    $qry = $this->db->query($sql);
    return $qry->result();
  }

  public function insert($data)
  {
    if (is_null($data) || !is_array($data)) {
      return false;
    } else {
      $this->db->insert($this->table, $data);
    }
  }

  public function update($data, $id)
  {
    if (is_null($data) || !is_array($data) || is_null($id) || empty($id)) {
      return false;
    } else {
      $this->db->where('id', $id);
      $this->db->update($this->table, $data);
    }
  }

  public function delete($id)
  {
    if (is_null($id) || empty($id)) {
      return false;
    } else {
      $this->db->where('id', $id);
      $this->db->update($this->table);
    }
  }

  public function getList($filters = [], $sorts = [], $firstRowPos = 0, $pageSize = 10)
  {
    $sql = '';
    if (!empty($filters)) {
      $sql = 'select * from ' .
             $this->table . ' s where 1 = 1 ' .
             $this->buildWhereSql('s', $filters) . ' order by s.account_code' .
             ' limit ' . $firstRowPos . ', ' . $pageSize;
    }
    $qry = $this->db->query($sql);
    return $qry->result();
  }

  public function getListCount($filters)
  {
    if (is_null($filters) || !is_array($filters)) {
      return 0;
    } else {
      $sql = 'select count(*) as nrecs from ' .
             $this->table . ' s where 1 = 1 ' .
             $this->buildWhereSql('s', $filters);
      $qry = $this->db->query($sql);
      $row = $qry->row();
      return $row->nrecs;
    }
  }

  public function getListMainAccountExist($id)
  {
    if (is_null($id)) {
      return 0;
    } else {
      $sql = 'select count(*) as nrecs from ' . $this->t_posting_profile . ' s where 1 = 1 and (s.account_debet =? or s.account_credit =?)';
      $qry = $this->db->query($sql, [$id, $id]);
      $row = $qry->row();
      return $row->nrecs;
    }
  }

  public function getAllAsArray()
  {
    $mainAccountRs = $this->getAll();
    $mainAccount   = [];
    $mainAccount[] = "-- Pilih Kode Akun --";
    foreach ($mainAccountRs as $row) {
      $mainAccount[$row->id] = $row->account_code . " - " . $row->name;
    }
    return $mainAccount;
  }

  public function getMainAccountName($id)
  {
    $mainAccount = $this->getRow($id);
    if ($id != null) {
      return $mainAccount['account_code'] . ' - ' . $mainAccount['name'];
    } else {
      return '-';
    }

  }

  public function getRow($id)
  {
    if (is_null($id) || empty($id) || !is_numeric($id)) {
      return null;
    } else {
      return $this->getRowByCriteria($this->table, ['id' => $id]);
    }
  }

  public function getMainAccount($accountCode)
  {
    if (is_null($accountCode) || empty($accountCode)) {
      return null;
    } else {
      $sql = "select * from " . $this->table . " where account_code = ? and active='Y'";
      $qry = $this->db->query($sql, [$accountCode]);
      return $qry->row_array();
    }
  }

  public function getBankAsArray()
  {
    $mainAccountRs = $this->getBankAccount();
    $mainAccount   = [];
    $mainAccount[] = "-- Semua Bank --";
    foreach ($mainAccountRs as $row) {
      $mainAccount[$row->id] = $row->name;
    }
    return $mainAccount;
  }

  public function getBankAccount()
  {
    $sql = 'select * from ' . $this->table . ' a where a.active = "Y" and a.parent = 4 order by a.account_code';
    $qry = $this->db->query($sql);
    return $qry->result();
  }

  public function getMainAccountTransaksi($dateFrom, $dateTo)
  {
    $sql = 'SELECT al.transaction_date, a.account_code, a.name, SUM(COALESCE(al.amount_debet,0)) AS debet, SUM(COALESCE(al.amount_credit,0)) AS kredit FROM m_main_account a LEFT OUTER JOIN gen_trx_debet_credit al ON a.id=al.account_code and al.transaction_date BETWEEN "' . $dateFrom . '" and "' . $dateTo . '" GROUP BY a.id ORDER BY a.account_code';
    $qry = $this->db->query($sql);
    return $qry->result();
  }

  //-------------------- BEGIN Function for getting IDs tree of an account --------------------//

  private $holder = [];

  public function getIdsTree($id)
  {
    $this->holder[] = $id;
    if ($this->hasChildren($id)) {
      $this->appendChildren($id, $this->holder);
    }
    return $this->holder;
  }

  private function appendChildren($parentId)
  {
    $qry = $this->db->query("select * from `{$this->table}` where `parent`=" .
                            $this->sanitizeArg($parentId) .
                            " order by `id`");

    if (!in_array($parentId, $this->holder)) $this->holder[] = $parentId;
    foreach ($qry->result() as $child) {
      if (!in_array($child->id, $this->holder)) $this->holder[] = $child->id;
      if ($this->hasChildren($child->id)) {
        $this->appendChildren($child->id, $this->holder);
      }
    }
  }

  private function hasChildren($id)
  {
    $qry = $this->db->query("select count(*) as n_recs from `{$this->table}` where `parent`=" . $this->sanitizeArg($id));
    $row = $qry->row();
    return ($row->n_recs > 0);
  }

  //-------------------- END Function for getting IDs tree of an account --------------------//

}

