<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseDAO.php';
require_once 'ICrud.php';

/**
 * DAO Subjects
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
class DAO_Subjects extends BaseDAO implements ICrud {
	
	var $table = 'm_edu_subjects';
	
	public function __construct(){
		parent::__construct();
	}
	
	public function getRow($id){
		if(is_null($id) || empty($id) || !is_numeric($id)){
			return null;
		}else{
			return $this->getRowByCriteria($this->table, array('id' => $id));
		}
	}
	
	public function insert($data){
		if(is_null($data) || !is_array($data)){
			return false;
		}else{
			$this->db->insert($this->table, $data);
		}
	}
	
	public function update($data, $id){
		if(is_null($data) || !is_array($data) || is_null($id) || empty($id)){
			return false;
		}else{
			$this->db->where('id', $id);
			$this->db->update($this->table, $data);
		}
	}

	public function delete($id){
		if(is_null($id) || empty($id)){
			return false;
		}else{
			$this->db->where('id', $id);
			$this->db->delete($this->table);
		}
	}
	
	public function getList($filters = array(), $sorts = array(), $firstRowPos = 0, $pageSize = 10, $ascendingSort = true){
		if(!empty($filters)){
			$sql =	"select * from {$this->table} s where 1 = 1 " .
				$this->buildWhereSql('s', $filters) .
				$this->buildSortSql('s', $sorts, $ascendingSort) .
				" limit {$firstRowPos}, {$pageSize} ";
			$qry = $this->db->query($sql);
			return $qry->result();
		}else{
			return null;
		}
	}

	public function getListCount($filters){
		if(is_null($filters) || !is_array($filters)){
			return 0;
		}else{
			$sql =	'select count(*) as nrecs from ' .
				$this->table . ' s where 1 = 1 ' .
				$this->buildWhereSql('s', $filters);
				$qry = $this->db->query($sql);
				$row = $qry->row();
			return $row->nrecs;
		}
	}
	
	public function isExist($subjectName){
		if(is_null($subjectName) || empty($subjectName)){
			return false;
		}else{
			$qry = $this->db->query("select count(*) as nrecs from {$this->table} where `name`=?", array(trim($subjectName)));
			$row = $qry->row();
			return ($row->nrecs > 0);
		}
	}
	
}