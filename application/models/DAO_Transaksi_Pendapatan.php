<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseDAO.php';
require_once 'ICrud.php';

class DAO_Transaksi_Pendapatan extends BaseDAO implements ICrud
{

  var $table  = 'gen_trx_pendapatan';
  var $v_list = 'v_list_pendapatan_siswa';

  /*var $v_students_transaction = 'v_students_transaction';
    var $v_rpt_bayar_uang_pangkal = 'v_rpt_bayar_uang_pangkal';*/

  public function __construct()
  {
    parent::__construct();
    $this->load->model(['DAO_System_Parameter']);
    $this->load->model(['DAO_Transaksi', 'DAO_TipeTransaksi']);
  }

  public function getListTransaksi($filters = [], $sorts = [], $firstRowPos = 0, $pageSize = 10)
  {
    $sql = 'select * from ' . $this->v_list . ' s where 1 = 1 ';
    if (!empty($filters)) {
      $sql = $sql .
             $this->buildWhereSql('s', $filters) .
             ' order by s.created_date desc' .
             ' limit ' . $firstRowPos . ', ' . $pageSize;
    }
    $qry = $this->db->query($sql);
    return $qry->result();
  }

  public function getListCount($filters)
  {
    if (is_null($filters) || !is_array($filters)) {
      return 0;
    } else {
      $sql = 'select count(*) as nrecs from ' .
             $this->v_list . ' s where 1 = 1 ' .
             $this->buildWhereSql('s', $filters);
      $qry = $this->db->query($sql);
      $row = $qry->row();
      return $row->nrecs;
    }
  }

  public function insert($data)
  {
    if (is_null($data) || !is_array($data)) {
      return false;
    } else {
      $this->db->insert($this->table, $data);
      return $this->db->insert_id();
    }
  }

  public function insertKoreksi($id, $newTransactionNumber)
  {
    if (is_null($id) || is_null($newTransactionNumber)) {
      return false;
    } else {
      $pendapatan = $this->getRowByCriteria($this->table, ['id' => $id]);
      if ($pendapatan != null) {
        $koreksi                       = $pendapatan;
        $koreksi['id']                 = null;
        $koreksi['correction_trx_id']  = $id;
        $koreksi['transaction_number'] = $newTransactionNumber;
        $koreksi['jumlah_transaksi']   = -$pendapatan['jumlah_transaksi'];
        $koreksi['total_bayar']        = -$pendapatan['total_bayar'];
        $koreksi['dibayar']            = -$pendapatan['dibayar'];
        $koreksi['sisa_bayar']         = -$pendapatan['sisa_bayar'];
        $koreksi['tahun_ajaran']       = $pendapatan['tahun_ajaran'];
        $koreksi['remark']             = 'Koreksi Transaksi ' . $pendapatan['transaction_number'];
        $this->db->insert($this->table, $koreksi);
      }
    }
  }


  public function update($data, $id)
  {
    if (is_null($data) || !is_array($data) || is_null($id) || empty($id)) {
      return false;
    } else {
      /*$sql = 'select * from ' . $this->table;
            $qry = $this->db->query($sql, array($id));
            $dataLawan = $qry->row_array();
            $dataLawan['remark'] = $data['remark'];
            $this->db->where('id', $id);
            $this->db->update($this->table, $data);
            $this->db->where('id', $dataLawan['id']);
            $this->db->update($this->table, $dataLawan);*/
      $this->db->where('id', $id);
      $this->db->update($this->table, $data);
    }
  }

  public function delete($id)
  {
    if (is_null($id) || empty($id)) {
      return false;
    } else {
      $this->db->where('id', $id);
      $this->db->update($this->table);
    }
  }

  public function getRow($id)
  {
    if (is_null($id) || empty($id) || !is_numeric($id)) {
      return null;
    } else {
      return $this->getRowByCriteria($this->table, ['id' => $id]);
    }
  }

  /*public function getListStudentsTransaction($filters = array(), $sorts = array(), $firstRowPos = 0, $pageSize = 10){
        $sql = '';
        $sql =	'select * from '.$this->v_students_transaction . ' s where 1 = 1 ' ;
        if(!empty($filters)){
            $sql = $sql.
                            $this->buildWhereSql('s', $filters) .
                            ' limit ' . $firstRowPos . ', ' . $pageSize;
        }
        $qry = $this->db->query($sql);
        return $qry->result();
    }*/

  /*public function getBulanDibayarByStudentId($studentId, $fYear){
        $sql = '';
        $sql = 'SELECT financial_month FROM v_students_transaction WHERE transaction_type = 2 AND financial_year = '.$fYear.' AND student_id = '.$studentId.' AND ref_transaction_id IS NOT NULL';
        $qry = $this->db->query($sql);
        return $qry->result();
    }*/

  private function getPostingProfileByType($idType)
  {
    if (is_null($idType) || empty($idType) || !is_numeric($idType)) {
      return null;
    } else {
      $sql = 'select * from gen_trx_posting_profile pp where pp.trx_type = ?';
      $qry = $this->db->query($sql, [$idType]);
      if ($qry->num_rows() > 0) {
        return $qry->row_array();
      } else {
        return null;
      }
    }
  }

  public function getRptQuery($filters = [], $type, $docType)
  {
    $trxTable = '';
    if ($type == 'D') {
      if ($docType == 2) {
        $trxTable = "v_rpt_pendapatan_siswa";
      }
      if ($docType == 3) {
        $trxTable = "v_rpt_pendapatan";
      }
    } else {
      $trxTable = "v_rpt_pembayaran";
    }
    $sql = 'select s.transaction_date as `Tanggal Transaksi`, s.transaction_type as `Tipe Transaksi`, s.remark as `Keterangan`, s.amount as `Jumlah` from `' . $trxTable . '` s where 1 = 1 ';
    if (!empty($filters)) {
      $sql = $sql . $this->buildWhereSql('s', $filters) . ' order by s.transaction_date ASC';
    }
    return $sql;
  }
  
  public function getRptQueryGrouping($filters = [], $transactionType, $periodeGrouping)
  {
  	
  	
  	
    $trxTable = 'v_transaction_summary';
    $trxGrouping = '`s`.`parent_type_id`,';
    $orderByTrxDate = false;
    
    
    $trxTypeFilter = $transactionType['transaction_type'];

    $sql = 'select ';
  	if($periodeGrouping == '`s`.`transaction_date`'){
  		$sql = $sql.'s.transaction_date as `Tanggal Transaksi`, ';
  		$orderByTrxDate =true;
  	}
  	$sql = $sql.'s.grand_parent as `Tipe Transaksi`,s.parent_type as `Sub Tipe Transaksi` ';
    $ids = $this->DAO_TipeTransaksi->getIdsTree($transactionType['id']);
    if(!empty($ids)){
    	$trxTypeIds = implode(",", $ids);
    }else{
    	$trxTypeIds = $transactionType['id'];
    }
    
  	if($transactionType['is_sub_type'] == 1){
    	$trxGrouping = '`s`.`trx_id`,';
    	$sql = $sql.', s.type_trx as `Nama Transaksi`';
    }else{
    	$trxTypeChild = $this->DAO_TipeTransaksi->getRow($ids[1]);
    	if($trxTypeChild['is_sub_type'] == 1){
    		$trxGrouping = '`s`.`trx_id`,';
    		$sql = $sql.', s.type_trx as `Nama Transaksi`';
    	}
    }
    
    /*if($trxGrouping == ''){
    	$sql = $sql. ' ,s.remark as `Keterangan`';
    }*/
    $sql = $sql.' ,Sum(s.amount) as `Jumlah` from `' . $trxTable . '` s where 1 = 1 ';
    $sql = $sql.' and s.trx_id in ('.$trxTypeIds.') ';
    
    if (!empty($filters)) {
    	if($orderByTrxDate){
    		$sql = $sql . $this->buildWhereSql('s', $filters) . ' Group by '.$trxGrouping.$periodeGrouping.' order by s.transaction_date, s.transaction_type ASC';
    	}else{
    		$sql = $sql . $this->buildWhereSql('s', $filters) . ' Group by '.$trxGrouping.$periodeGrouping.' order by s.transaction_type ASC';
    	}	
    }
    return $sql;
  }
  

  /*public function getRptTunggakanUangPangkalQuery($year, $class, $grade){
        $sql =	'';
        if($class == 0 && $grade == 0){
            $sql = 'SELECT s.nis as `NIS`, s.name as `Nama Siswa`, g.name AS `Jenjang`, e.name as `Tingkatan`, s.phone_number as `No. Telepon` FROM m_student s INNER JOIN m_edu_classroom e ON s.classroom_id=e.id INNER JOIN m_edu_grade g ON e.grade_id = g.id WHERE s.active = "Y" AND s.id NOT IN (SELECT v.student_id FROM v_rpt_bayar_uang_pangkal v WHERE v.financial_year = '.$year.') order by e.name, s.nis';
        }if($class != 0 && $grade == 0){
            $sql = 'SELECT s.nis as `NIS`, s.name as `Nama Siswa`, g.name AS `Jenjang`, e.name as `Tingkatan`, s.phone_number as `No. Telepon` FROM m_student s INNER JOIN m_edu_classroom e ON s.classroom_id=e.id INNER JOIN m_edu_grade g ON e.grade_id = g.id WHERE s.active = "Y" AND s.id NOT IN (SELECT v.student_id FROM v_rpt_bayar_uang_pangkal v WHERE v.financial_year = '.$year.' and v.classroom_id = '.$class.') and e.id = '.$class.' order by e.name, s.nis';
        }if($class == 0 && $grade != 0){
            $sql = 'SELECT s.nis as `NIS`, s.name as `Nama Siswa`, g.name AS `Jenjang`, e.name as `Tingkatan`, s.phone_number as `No. Telepon` FROM m_student s INNER JOIN m_edu_classroom e ON s.classroom_id=e.id INNER JOIN m_edu_grade g ON e.grade_id = g.id WHERE s.active = "Y" AND s.id NOT IN (SELECT v.student_id FROM v_rpt_bayar_uang_pangkal v WHERE v.financial_year = '.$year.' and v.grade_id = '.$grade.') and g.id = '.$grade.' order by e.name, s.nis';
        }if($class != 0 && $grade != 0){
            $sql = 'SELECT s.nis as `NIS`, s.name as `Nama Siswa`, g.name AS `Jenjang`, e.name as `Tingkatan`, s.phone_number as `No. Telepon` FROM m_student s INNER JOIN m_edu_classroom e ON s.classroom_id=e.id INNER JOIN m_edu_grade g ON e.grade_id = g.id WHERE s.active = "Y" AND s.id NOT IN (SELECT v.student_id FROM v_rpt_bayar_uang_pangkal v WHERE v.financial_year = '.$year.' and v.grade_id = '.$grade.' and v.classroom_id = '.$class.') and e.id = '.$class.' and g.id = '.$grade.' order by e.name, s.nis';
        }
        if(!empty($filters)){
            $sql = $sql.$this->buildWhereSql('s', $filters);
        }
        return $sql;

    }*/

}
