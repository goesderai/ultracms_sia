<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseDAO.php';
require_once 'ICrud.php';

/**
 * DAO Gaji
 *
 * @author <a href="mailto:agung3ka@gmail.com">Eka D.</a>
 */
class DAO_Transaksi_Gaji extends BaseDAO  implements ICrud {
	var $table = 'gen_trx_gaji';
	
	public function __construct(){
		parent::__construct();
	}
	public function getAll(){
		$sql = 'select * from ' . $this->table . ' a order by a.transaction_date';
		$qry = $this->db->query($sql);
		return $qry->result();
	}
	
	public function insert($data){
		if(is_null($data) || !is_array($data)){
			return false;
		}else{
			$this->db->insert($this->table, $data);
		}
	}
	
	public function update($data, $id){
		if(is_null($data) || !is_array($data) || is_null($id) || empty($id)){
			return false;
		}else{
			$this->db->where('id', $id);
			$this->db->update($this->table, $data);
		}
	}
	
	public function delete($id){
		if(is_null($id) || empty($id)){
			return false;
		}else{
			$this->db->where('id', $id);
			$this->db->update($this->table);
		}
	}
	
	public function getList($filters = array(), $sorts = array(), $firstRowPos = 0, $pageSize = 10){
		$sql = '';
		if(!empty($filters)){
			$sql =	'select * from ' . 
							$this->table . ' s where 1 = 1 ' . 
							$this->buildWhereSql('s', $filters) . ' order by s.transaction_date'.
							' limit ' . $firstRowPos . ', ' . $pageSize;
		}
		$qry = $this->db->query($sql);
		return $qry->result();
	}
	
	public function getListCount($filters){
		if(is_null($filters) || !is_array($filters)){
			return 0;
		}else{
			$sql =	'select count(*) as nrecs from ' . 
							$this->table . ' s where 1 = 1 ' . 
							$this->buildWhereSql('s', $filters);
			$qry = $this->db->query($sql);
			$row = $qry->row();
			return $row->nrecs;
		}
	}
	
	public function getRow($id){
		if(is_null($id) || empty($id) || !is_numeric($id)){
			return null;
		}else{
			return $this->getRowByCriteria($this->table, array('id' => $id));
		}
	}
}

	