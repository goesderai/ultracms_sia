<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseDAO.php';

/**
 * DAO Agama
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
class DAO_Agama extends BaseDAO {
	
	var $table = 'm_religion';
	
	public function __construct(){
		parent::__construct();
	}
	
	public function getSelectOptions($emptyOption = null){
		$sql = sprintf("select * from %s a order by a.name", $this->table);
		$qry = $this->db->query($sql);
		$religionRs = $qry->result();
		$religions = array();
		foreach ($religionRs as $row){
			$religions[$row->id] = $row->name;
		}
		if(!is_null($emptyOption) && !empty($emptyOption) && is_array($emptyOption)){
			return $emptyOption + $religions;
		}else{
			return $religions;
		}
	}
	
}