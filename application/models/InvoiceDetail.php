<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Invoice detail data object
 * 
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
class InvoiceDetail {
    
    private $trxNumber;
    private $trxType;
    private $trxAmount;
    private $trxBalance;
    private $remark;
    
    public function getTrxNumber()
    {
        return $this->trxNumber;
    }

    public function setTrxNumber($trxNumber)
    {
        $this->trxNumber = $trxNumber;
    }

    public function getTrxType()
    {
        return $this->trxType;
    }

    public function setTrxType($trxType)
    {
        $this->trxType = $trxType;
    }

    public function getTrxAmount()
    {
        return $this->trxAmount;
    }

    public function setTrxAmount($trxAmount)
    {
        $this->trxAmount = $trxAmount;
    }

    public function getTrxBalance()
    {
        return $this->trxBalance;
    }

    public function setTrxBalance($trxBalance)
    {
        $this->trxBalance = $trxBalance;
    }

    public function getRemark()
    {
        return $this->remark;
    }

    public function setRemark($remark)
    {
        $this->remark = $remark;
    }
    
}
