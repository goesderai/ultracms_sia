<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseDAO.php';

/**
 * DAO Salary
 *
 * @author <a href="mailto:agung3ka@gmail.com">Eka D.</a>
 */
class DAO_Salary extends BaseDAO {
	
	var $table = 'm_salary';
	
	public function __construct(){
		parent::__construct();
	}
	
	public function getRow($employeeId){
		if(is_null($employeeId) || empty($employeeId) || !is_numeric($employeeId)){
			return null;
		}else{
			return $this->getRowByCriteria($this->table, array('employee_id' => $employeeId), "gaji_pokok, tunjangan, lain_lain");
		}
	}
	
    public function getRowByEmployeeId($id){
       if(is_null($id) || empty($id) || !is_numeric($id)){
              return null;
       }else{
             $sql = 'select * from ' . $this->table . ' where employee_id = ?';
             $qry = $this->db->query($sql, array($id));
             return $qry->row_array();
       }
    }
	
	
	public function getList($filters = array(), $sorts = array(), $firstRowPos = 0, $pageSize = 10){
		$sql = '';
		if(!empty($filters)){
			$sql =	'select * from ' . 
							$this->table . ' s where 1 = 1 ' . 
							$this->buildWhereSql('s', $filters) . 
							' limit ' . $firstRowPos . ', ' . $pageSize;
		}
		$qry = $this->db->query($sql);
		return $qry->result();
	}
	
	public function getListCount($filters){
		if(is_null($filters) || !is_array($filters)){
			return 0;
		}else{
			$sql =	'select count(*) as nrecs from ' . 
							$this->table . ' s where 1 = 1 ' . 
							$this->buildWhereSql('s', $filters);
			$qry = $this->db->query($sql);
			$row = $qry->row();
			return $row->nrecs;
		}
	}
	
	public function insert($data){
		if(is_null($data) || !is_array($data)){
			return false;
		}else{
			$this->db->insert($this->table, $data);
		}
	}
	
	public function update($data, $employeeId){
		if(is_null($data) || !is_array($data) || is_null($employeeId) || empty($employeeId)){
			return false;
		}else{
			$this->db->where('employee_id', $employeeId);
			$this->db->update($this->table, $data);
		}
	}
	
	public function delete($employeeId){
		if(is_null($employeeId) || empty($employeeId)){
			return false;
		}else{
			$this->db->where('employee_id', $employeeId);
			$this->db->delete($this->table);
		}
	}
	
	public function getAll(){
		$sql = 'select * from ' . $this->table . ' a order by a.employee_id';
		$qry = $this->db->query($sql);
		return $qry->result();
	}
	
	public function isExists($data){
		if(is_null($data) || empty($data) || !is_array($data)){
			return false;
		}else{
			$sql = 'select count(*) as nrecs from ' . $this->table . ' where `employee_id`=? and `active` = ?';
			$qry = $this->db->query($sql, array($data['employee_id'], 'Y'));
			$row = $qry->row();
			return ($row->nrecs > 0);
		}
	}
	
}