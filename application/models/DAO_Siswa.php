<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseDAO.php';
require_once 'ICrud.php';

class DAO_Siswa extends BaseDAO implements ICrud
{

  var $table = 'm_student';
  var $viewRptSingleStudent = 'v_rpt_single_student';

  public function __construct()
  {
    parent::__construct();
  }

  public function getRow($id)
  {
    if (is_null($id) || empty($id) || !is_numeric($id)) {
      return null;
    } else {
      $sql = "SELECT s.`id`, s.`nis`, s.`name`, s.`nickname`, s.`gender`, s.`birth_place`, 
				s.`birth_date`, s.`religion_id`, cr.`grade_id`, s.`classroom_id`, s.`nationality`, 
				s.`address`, s.`phone_number`, s.`weight`, s.`height`, s.`blood_type`, s.`illness_history`, 
				s.`nth_child`, s.`n_siblings`, s.`n_step_siblings`, s.`n_foster_siblings`, s.`daily_language`, 
				s.`stayed_at`, s.`father_name`, s.`mother_name`, s.`father_last_education`, s.`mother_last_education`, 
				s.`parent_ocupation`, s.`guardian_name`, s.`guardian_last_education`, s.`guardian_relationship`, 
				s.`guardian_ocupation`, s.`entered_school_as`, s.`student_origin`, s.`play_group_name`, 
				s.`play_group_location`, s.`tahun_dan_nomor_sttb`, s.`years_of_learning`, s.`origin_school_name`, 
				s.`transfer_date`, s.`transfer_from_grade`, s.`accepted_date`,s.`accepted_year`, s.`accepted_grade`, s.`status`
				FROM `{$this->table}` s INNER JOIN `m_edu_classroom` cr ON cr.`id` = s.`classroom_id` 
				WHERE s.`id`=?";
      $qry = $this->db->query($sql, array($id));
      return $qry->row_array();
    }
  }

  public function getRowForPrintPreview($id)
  {
    if (is_null($id) || empty($id) || !is_numeric($id)) {
      return null;
    } else {
      $qry = $this->db->query("select * from {$this->viewRptSingleStudent} s where s.id=?", array($id));
      return $qry->row_array();
    }
  }

  public function getList($filters = array(), $sorts = array(), $firstRowPos = 0, $pageSize = 10)
  {
    $sql = '';
    if (!empty($filters)) {
      $sql = "SELECT 	s.`id`, s.`nis`, s.`name`, s.`status`, cr.`name` as classroom_name, grd.`name` as grade_name 
              FROM `{$this->table}` s inner join `m_edu_classroom` cr on cr.id = s.classroom_id 
              inner join `m_edu_grade` grd on grd.id=cr.grade_id 
              where 1=1 " . $this->buildWhereSql_2($filters) . " limit {$firstRowPos}, {$pageSize}";
    }
    //var_dump($sql); die;
    $qry = $this->db->query($sql);
    return $qry->result();
  }

  public function getListCount($filters)
  {
    if (is_null($filters) || !is_array($filters)) {
      return 0;
    } else {
      $sql = "select count(s.id) as nrecs from `{$this->table}` s 
              inner join `m_edu_classroom` cr on cr.id = s.classroom_id 
              inner join `m_edu_grade` grd on grd.id=cr.grade_id 
              where 1=1 " . $this->buildWhereSql_2($filters);
      $qry = $this->db->query($sql);
      $row = $qry->row();
      return $row->nrecs;
    }
  }

  public function insert($data)
  {
    if (is_null($data) || !is_array($data)) {
      return false;
    } else {
      $this->db->insert($this->table, $data);
    }
  }

  public function update($data, $id)
  {
    if (is_null($data) || !is_array($data) || is_null($id) || empty($id)) {
      return false;
    } else {
      $this->db->where('id', $id);
      $this->db->update($this->table, $data);
    }
  }

  public function delete($id)
  {
    if (is_null($id) || empty($id)) {
      return false;
    } else {
      $this->db->where('id', $id);
      $this->db->delete($this->table);
    }
  }

  public function getSelectOptions($emptyOption = null)
  {
    $sql = sprintf("select * from %s a order by a.name", $this->table);
    $qry = $this->db->query($sql);
    $studentsRs = $qry->result();
    $students = array();
    foreach ($studentsRs as $row) {
      $students[$row->id] = $row->name . " - " . $row->nis;
    }
    if (!is_null($emptyOption) && !empty($emptyOption) && is_array($emptyOption)) {
      return $emptyOption + $students;
    } else {
      return $students;
    }
  }

  public function isNisExists($nis)
  {
    if (is_null($nis) || empty($nis)) {
      return false;
    }
    $qry = $this->db->query("select count(*) as nrecs from {$this->table} where `nis`=?", array(trim($nis)));
    $row = $qry->row();
    return ($row->nrecs > 0);
  }

  public function countByCriteria($criteria = '')
  {
    if (empty($criteria)) return 0;
    $qry = $this->db->query("select count(*) as nrecs from {$this->table} where {$criteria}");
    $row = $qry->row();
    return $row->nrecs;
  }

}
