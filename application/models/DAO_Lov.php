<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseDAO.php';

/**
 * DAO Lov
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
class DAO_Lov extends BaseDAO{

	var $headTable = 'm_lov';
	var $detailTable = 'm_lov_detail';

	public function __construct(){
		parent::__construct();
	}

	/**
	 * Get lovs by lov name
	 * @param string $name Lov name
	 * @param string $sortBy Sort by field
	 */
	public function getLovs($name, $sortBy = 'caption'){
		if(is_null($name) || empty($name)){
			return null;
		}else{
			$lovs = array();
			$sql = "select * from {$this->detailTable}
							where `lov_id` = (select `id` from {$this->headTable} where `name` = ?)
							order by `{$sortBy}`";
			$qry = $this->db->query($sql, array($name));
			foreach ($qry->result() as $row) {
				$lovs[$row->value] = $row->caption;
			}
			return $lovs;
		}
	}

}
