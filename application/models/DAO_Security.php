<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseDAO.php';

/**
 * DAO Security
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
class DAO_Security extends BaseDAO {
	
	var $tableUser = 'sec_user';
	var $tableUserRoles = 'sec_user_roles';
	var $tableRole = 'sec_role';
	var $tableRolePrivileges = 'sec_role_privileges';
	var $tableModules = 'm_modules';
	var $viewUserEmployee = 'v_user_employee';
	var $viewUserInquiry = 'v_user_inquiry';
	
	public function __construct(){
		parent::__construct();
	}
	
	public function isValidUser($username, $password){
		$retval = array('isValid' => false, 'userData' => null);
		if(is_null($username) || empty($username) || is_null($password) || empty($password)){
			return $retval;
		}else{
			$md5Pass = md5($password);
			$sql = "select count(*) as nrecs from {$this->tableUser} where `username`=? and `password`=? and `active`='Y'";
			$qry = $this->db->query($sql, array($username, $md5Pass));
			$row = $qry->row();
			$retval['isValid'] = ($row->nrecs > 0);
			$retval['userData'] = $this->getUserByUsernameAndPassword($username, $password);
			return $retval;
		}
	}
	
	public function getUserByUsernameAndPassword($username, $password){
		if(is_null($username) || empty($username) || is_null($password) || empty($password)){
			return null;
		}else{
			$md5Pass = md5($password);
			$sql = "select `id`, `username`, `password`, `name`, `id_number` 
                    from {$this->viewUserEmployee} 
                    where `username`=? and `password`=?";
			$qry = $this->db->query($sql, array(trim($username), trim($md5Pass)));
			$row = $qry->row_array();
			return $row;
		}
	}
	
	public function getUserById($id){
		if(is_null($id) || empty($id) || !is_numeric($id)){
			return null;
		}else{
			$sql = "select * from {$this->viewUserInquiry} where `id`=?";
			$qry = $this->db->query($sql, array($id));
			$row = $qry->row_array();
			return $row;
		}
	}
	
	public function getUserByFilter($filters = array(), $sorts = array(), $firstRowPos = 0, $pageSize = 10){
		$sql = '';
		if(!empty($filters)){
			$sql =	"select * from {$this->viewUserInquiry} s where 1 = 1 " . 
			         $this->buildWhereSql('s', $filters) . " limit {$firstRowPos}, {$pageSize}";
		}
		$qry = $this->db->query($sql);
		return $qry->result();
	}
	
	public function getUserCountByFilter($filters){
		if(is_null($filters) || !is_array($filters)){
			return 0;
		}else{
			$sql =	"select count(*) as nrecs from {$this->viewUserInquiry} s where 1 = 1 " . 
					$this->buildWhereSql('s', $filters);
			$qry = $this->db->query($sql);
			$row = $qry->row();
			return $row->nrecs;
		}
	}
	
	/**
	 * Cek apakah username sudah ada dan aktif
	 * @param String $username The username to check
	 */
	public function isUsernameExists($username){
		if(empty($username)){
			return false;
		}else{
			$sql = "select count(*) as nrecs from {$this->tableUser} where `active`='Y' and `username`=?";
			$qry = $this->db->query($sql, array($username));
			$row = $qry->row();
			return ($row->nrecs > 0);
		}
	}
	
	public function changePassword($userId, $newPass){
		if(is_null($userId) || empty($userId) || is_null($newPass) || empty($newPass)){
			return false;
		}else{
			$data = array('password' => md5($newPass));
			$this->db->where('id', $userId);
			$this->db->update($this->tableUser, $data);
		}
	}
	
	private function _findModuleParent($parentId, $parents){
		if(empty($parentId) || empty($parents) || !is_array($parents)){
			return null;
		}
		foreach ($parents as $parent){
			if($parent->id == $parentId){
				return $parent;
			}
		}
		return null;
	}
	
	/**
	 * Get granted modules by user id
	 * @param Integer $userId The user id
	 */
	public function getGrantedModules($userId){
		if(is_null($userId) || empty($userId)){
			return null;
		}else{
			// Get role privileges
			$sql = "SELECT DISTINCT(p.module_id) AS mod_id FROM `{$this->tableRolePrivileges}` p 
				WHERE p.role_id IN (SELECT role_id FROM `{$this->tableUserRoles}` WHERE user_id=?)";
			$qry = $this->db->query($sql, array($userId));
			$grantedModules = array();
			foreach ($qry->result() as $row){
				$grantedModules[] = $row->mod_id;
			}
			
			// Map menu - level
			$qry = $this->db->query("select max(tree_level) max_level from {$this->tableModules}");
			$row = $qry->row();
			$maxLevel = $row->max_level;
			$menuLevelMap = array();
			for($i = 0; $i <= $maxLevel; $i++){
				$qry2 = $this->db->query("select `id`, `parent_id`, `class_name`, `caption`, `icon` from {$this->tableModules} 
					where tree_level=? order by `display_order`, `caption`", array($i));
				$menuLevelMap[$i] = array();
				foreach ($qry2->result() as $row2){
					if(in_array($row2->id, $grantedModules)){
						$row2->children = array();
						$menuLevelMap[$i][] = $row2;
					}
				}
			}
			
			// Pair parent - children
			for($i = $maxLevel; $i > 0; $i--){
				$levelMenus = $menuLevelMap[$i];
				foreach ($levelMenus as $levelMenu){
					if($i > 0){
						$parentMenu = $this->_findModuleParent($levelMenu->parent_id, $menuLevelMap[$i - 1]);
						if(!is_null($parentMenu)){
							$parentMenu->children[] = $levelMenu;
						}
					}
				}
			}
			
			// Some cleaning
			for($i = $maxLevel; $i > 0; $i--){
				unset($menuLevelMap[$i]);
			}
			
			$menuStructure = $menuLevelMap[0];
			return $menuStructure;
		}
	}
	
	public function getUserRoles($userId){
		if(empty($userId) || !is_numeric($userId)){
			return array();
		}else{
			$retval = array();
			$qry = $this->db->query("select * from {$this->tableUserRoles} where `user_id`=?", array($userId));
			foreach ($qry->result() as $row){
				$retval[] = $row->role_id;
			}
			return $retval;
		}
	}
	
	public function getRoleSelectOptions(){
		$retval = array();
		$qry = $this->db->query("select * from {$this->tableRole} order by `name`");
		foreach ($qry->result() as $row){
			$retval[$row->id] = sprintf("%s (%s)", $row->name, $row->description);
		}
		return $retval;
	}
	
	/**
	 * Insert data user
	 * @param Array $user
	 */
	public function insertUser($user){
		if(empty($user) || !is_array($user)){
			return false;
		}else{
			$roles = $user['roles'];
			
			// insert user
			unset($user['roles']);
			$this->db->insert($this->tableUser, $user);
			$userId = $this->db->insert_id();
			
			// insert roles
			$userRoles = array();
			foreach($roles as $role){
				$userRoles[] = array('user_id' => $userId, 'role_id' => $role);
			}
			$this->db->insert_batch($this->tableUserRoles, $userRoles);
		}
	}
	
	/**
	 * Update data user
	 * @param Array $user The user data
	 * @param Integer $id The user id
	 */
	public function updateUser($user, $id){
		if(empty($user) || !is_array($user) || empty($id) || !is_numeric($id)){
			return false;
		}else{
			$roles = isset($user['roles'])? $user['roles'] : null;
			
			// update user
			unset($user['roles']);
			$this->db->where('id', $id);
			$this->db->update($this->tableUser, $user);
			
			if(!empty($roles)){
				// update roles (step 1)
				$this->db->query("delete from {$this->tableUserRoles} where `user_id`=?", array($id));
				
				// update roles (step 2)
				$userRoles = array();
				foreach($roles as $role){
					$userRoles[] = array('user_id' => $id, 'role_id' => $role);
				}
				$this->db->insert_batch($this->tableUserRoles, $userRoles);
			}
		}
	}
	
}