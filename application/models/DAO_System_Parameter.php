<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseDAO.php';

/**
 * DAO System Parameter
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
class DAO_System_Parameter extends BaseDAO {
	
	var $table = 'm_system_parameter';
	
	public function __construct(){
		parent::__construct();
	}
	
	/**
	 * Get system parameter value by key
	 * @param String $paramKey The parameter key
	 */
	public function getValue($paramKey){
		if(is_null($paramKey) || empty($paramKey)){
			return "";
		}else{
			$sql = 'select `value` from ' . $this->table . ' where `key` = ?';
			$qry = $this->db->query($sql, array($paramKey));
			$row = $qry->row();
			return $row->value;
		}
	}
	
	/**
	 * Get system parameter by group
	 * @param String $group The parameter group
	 */
	public function getParamGroup($group){
		if(is_null($group) || empty($group)){
			return null;
		}else{
			$sql = 'select * from ' . $this->table . ' where `group` = ?';
			$qry = $this->db->query($sql, array($group));
			return $qry->result();
		}
	}
	
}