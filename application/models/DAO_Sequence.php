<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseDAO.php';

/**
 * DAO Sequence
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */

class DAO_Sequence extends BaseDAO {
	
	var $table = 'm_sequence';
	
	public function __construct(){
		parent::__construct();
	}
	
	private function createSequence($seqName, $seqRemark){
		if(is_null($seqName) || empty($seqName) || is_null($seqRemark) || empty($seqRemark)){
			return false;
		}else{
			if(!$this->sequenceExists($seqName, $seqRemark)){
				$data = array(
					'seq_name' => trim($seqName),
					'seq_remark' => trim($seqRemark),
					'seq_value' => 0
				);
				$this->db->insert($this->table, $data);
				return $this->db->insert_id();
			}else{
				return false;
			}
		}
	}
	
	public function getSequence($seqName, $seqRemark){
		if(is_null($seqName) || empty($seqName) || is_null($seqRemark) || empty($seqRemark)){
			return false;
		}else{
			$seq = null;
			if($this->sequenceExists($seqName, $seqRemark)){
				$seq = $this->getSequenceByNameAndRemark($seqName, $seqRemark);
			}else{
				$newSeq = $this->createSequence($seqName, $seqRemark);
				if($newSeq !== false){
					$seq = $this->getSequenceById($newSeq);
				}
			}
			
			if(!is_null($seq)){
				$retval = $seq->seq_value + 1;
				$data = array('seq_value' => $retval);
				$this->db->where('id', $seq->id);
				$this->db->update($this->table, $data);
				return $retval;
			}else{
				return false;
			}
		}
	}
	
	private function getSequenceByNameAndRemark($seqName, $seqRemark){
		if(is_null($seqName) || empty($seqName) || is_null($seqRemark) || empty($seqRemark)){
			return false;
		}else{
			$sql = "select * from " . $this->table . " where `seq_name`=? and `seq_remark`=?";
			$qry = $this->db->query($sql, array(trim($seqName), trim($seqRemark)));
			$row = $qry->row();
			return $row;
		}
	}
	
	private function getSequenceById($id){
		if(is_null($id) || empty($id) || !is_numeric($id)){
			return false;
		}else{
			$sql = "select * from " . $this->table . " where `id`=?";
			$qry = $this->db->query($sql, array($id));
			$row = $qry->row();
			return $row;
		}
	}
	
	private function sequenceExists($seqName, $seqRemark){
		if(is_null($seqName) || empty($seqName) || is_null($seqRemark) || empty($seqRemark)){
			return false;
		}else{
			$sql = "select count(*) as nrecs from " . $this->table . " where `seq_name`=? and `seq_remark`=?";
			$qry = $this->db->query($sql, array(trim($seqName), trim($seqRemark)));
			$row = $qry->row();
			return ($row->nrecs > 0);
		}
	}
	
}