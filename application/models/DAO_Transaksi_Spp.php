<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseDAO.php';
require_once 'ICrud.php';

/**
 * DAO SPP
 *
 * @author <a href="mailto:agung3ka@gmail.com">Eka D.</a>
 */
class DAO_Transaksi_Spp extends BaseDAO  implements ICrud {
	var $table = 'gen_trx_spp';
	var $v_list = 'v_list_spp';
	
	public function __construct(){
		parent::__construct();
	}
	public function getAll(){
		$sql = 'select * from ' . $this->table . ' a order by a.transaction_number';
		$qry = $this->db->query($sql);
		return $qry->result();
	}
	
	public function insert($data){
		if(is_null($data) || !is_array($data)){
			return false;
		}else{
			$this->db->insert($this->table, $data);
			return $this->db->insert_id();
		}
	}
	
	public function update($data, $id){
		if(is_null($data) || !is_array($data) || is_null($id) || empty($id)){
			return false;
		}else{
			$this->db->where('id', $id);
			$this->db->update($this->table, $data);
		}
	}
	
	public function delete($id){
		if(is_null($id) || empty($id)){
			return false;
		}else{
			$this->db->where('id', $id);
			$this->db->update($this->table);
		}
	}
	
	public function getList($filters = array(), $sorts = array(), $firstRowPos = 0, $pageSize = 10){
		$sql = '';
		if(!empty($filters)){
			$sql =	'select * from ' . 
							$this->v_list . ' s where 1 = 1 ' . 
							$this->buildWhereSql('s', $filters) . ' order by s.created_date desc'.
							' limit ' . $firstRowPos . ', ' . $pageSize;
		}
		$qry = $this->db->query($sql);
		return $qry->result();
	}
	
	public function getListCount($filters){
		if(is_null($filters) || !is_array($filters)){
			return 0;
		}else{
			$sql =	'select count(*) as nrecs from ' . 
							$this->v_list . ' s where 1 = 1 ' . 
							$this->buildWhereSql('s', $filters);
			$qry = $this->db->query($sql);
			$row = $qry->row();
			return $row->nrecs;
		}
	}
	
	public function getRow($id){
		if(is_null($id) || empty($id) || !is_numeric($id)){
			return null;
		}else{
			return $this->getRowByCriteria($this->table, array('id' => $id));
		}
	}
	
	public function getRptSppQuery($filters = array()){
		$sql =	'select CONCAT(s.name, " - " ,s.nis) as `Nama Siswa`, s.transaction_date as `Tanggal Transaksi`, s.month_str as `Bulan`, s.financial_year as `Tahun`, s.remark as `Keterangan`, s.amount as `Jumlah` from `v_rpt_spp` s where 1 = 1 ' ;
		if(!empty($filters)){
			$sql = $sql.$this->buildWhereSql('s', $filters);
		}
		return $sql;
		
	}
	
	public function getRptTunggakanSppQuery($month, $year){
		$sql =	'SELECT s.nis as `NIS`, s.name as `Nama Siswa`, e.name as `Kelas`, s.phone_number as `No. Telepon` FROM m_student s INNER JOIN m_edu_classroom e ON s.classroom_id=e.id WHERE s.active = "Y" AND s.status = 2 AND s.id NOT IN (SELECT v.student_id FROM v_rpt_spp v WHERE v.financial_month='.$month.' AND v.financial_year='.$year.') and s.efective_date < Date("'.$year.'-'.$month.'-02") order by e.name, s.nis';
		if(!empty($filters)){
			$sql = $sql.$this->buildWhereSql('s', $filters);
		}
		return $sql;
		
	}
}

