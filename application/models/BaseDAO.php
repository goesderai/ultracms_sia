<?php defined('BASEPATH') OR exit('No direct script access allowed');

class BaseDAO extends CI_Model
{

  public function __construct()
  {
    parent::__construct();
  }

  private function isOrOperand($filter)
  {
    return (is_array($filter[QRYFILTER_OPERATOR]) &&
            sizeof($filter[QRYFILTER_OPERATOR]) > 1 &&
            is_array($filter[QRYFILTER_FIELD]) &&
            sizeof($filter[QRYFILTER_FIELD]) > 0 &&
            is_array($filter[QRYFILTER_VALUE]) &&
            sizeof($filter[QRYFILTER_VALUE]) > 0 &&
            $filter[QRYFILTER_OPERATOR][0] === QRYFILTER_OPERATOR_OR);
  }

  /**
   * Build sql query where arguments
   * @param string $tableAlias Primary table alias
   * @param string $filters Query filters
   * @return string Sql query with where arguments
   */
  public function buildWhereSql($tableAlias, $filters)
  {
    if (empty($tableAlias) || empty($filters) || !is_array($filters)) {
      return "";
    } else {
      $retval = '';
      foreach ($filters as $filter) {
        $filterValue = null;
        $isOrOperand = $this->isOrOperand($filter);

        // build 'and' statements
        if ($isOrOperand) {
          // OR
          $i       = 0;
          $tempSql = [];
          foreach ($filter[QRYFILTER_FIELD] as $field) {
            $filterValue = isset($filter[QRYFILTER_VALUE][$i]) ? $filter[QRYFILTER_VALUE][$i] : "";
            $tempSql[]   = sprintf("(%s.%s %s) ", $tableAlias, $field,
                                   $this->buildOperatorSql($filter[QRYFILTER_OPERATOR][$i + 1], $filterValue));
            $i++;
          }
          $retval .= "AND (" . implode(" OR ", $tempSql) . ") ";
        } elseif ($filter[QRYFILTER_OPERATOR] == QRYFILTER_OPERATOR_BETWEEN) {
          // BETWEEN
          $retval      .= "AND ({$tableAlias}." . $filter[QRYFILTER_FIELD] . " ";
          $filterValue = isset($filter[QRYFILTER_VALUE]) ? $filter[QRYFILTER_VALUE] : [];
        } else {
          // OTHERS
          $retval      .= "AND {$tableAlias}." . $filter[QRYFILTER_FIELD] . " ";
          $filterValue = isset($filter[QRYFILTER_VALUE]) ? $filter[QRYFILTER_VALUE] : "";
        }

        // build operator statement
        if (!$isOrOperand) {
          $retval .= $this->buildOperatorSql($filter[QRYFILTER_OPERATOR], $filterValue);
        }
      }
      return $retval;
    }
  }

  /**
   * Build sql query where arguments without table alias
   * @param string $filters Query filters
   * @return string Sql query with where arguments
   */
  public function buildWhereSql_2($filters)
  {
    if (empty($filters) || !is_array($filters)) {
      return "";
    } else {
      $retval = '';
      foreach ($filters as $filter) {
        $filterValue = null;
        $isOrOperand = $this->isOrOperand($filter);

        // build 'and' statements
        if ($isOrOperand) {
          // OR
          $i       = 0;
          $tempSql = [];
          foreach ($filter[QRYFILTER_FIELD] as $field) {
            $filterValue = isset($filter[QRYFILTER_VALUE][$i]) ? $filter[QRYFILTER_VALUE][$i] : "";
            $tempSql[]   = sprintf("(%s %s) ", $field,
                                   $this->buildOperatorSql($filter[QRYFILTER_OPERATOR][$i + 1], $filterValue));
            $i++;
          }
          $retval .= "AND (" . implode(" OR ", $tempSql) . ") ";
        } elseif ($filter[QRYFILTER_OPERATOR] == QRYFILTER_OPERATOR_BETWEEN) {
          $retval      .= "AND (" . $filter[QRYFILTER_FIELD] . " ";
          $filterValue = isset($filter[QRYFILTER_VALUE]) ? $filter[QRYFILTER_VALUE] : [];
        } else {
          $retval      .= "AND " . $filter[QRYFILTER_FIELD] . " ";
          $filterValue = isset($filter[QRYFILTER_VALUE]) ? $filter[QRYFILTER_VALUE] : "";
        }

        // build operator statement
        if (!$isOrOperand) {
          $retval .= $this->buildOperatorSql($filter[QRYFILTER_OPERATOR], $filterValue);
        }
      }
      return $retval;
    }
  }

  protected function buildOperatorSql($filterOperator, $filterValue)
  {
    if (is_null($filterOperator) || empty($filterOperator)) {
      return "";
    } else {
      $retval = "";
      if ($filterOperator == QRYFILTER_OPERATOR_LIKE) {
        // like
        $retval = $filterOperator . " '%" . $this->db->escape_like_str($filterValue) . "%' ESCAPE '!' ";
      } elseif ($filterOperator == QRYFILTER_OPERATOR_ISNULL) {
        // is null
        $retval = $filterOperator . " ";
      } elseif ($filterOperator == QRYFILTER_OPERATOR_BETWEEN) {
        // between
        $retval = $filterOperator . " " . $this->db->escape($filterValue[0]) .
                  " and " . $this->db->escape($filterValue[1]) . ") ";
      } elseif ($filterOperator == QRYFILTER_OPERATOR_IN || $filterOperator == QRYFILTER_OPERATOR_NOT_IN) {
        // IN or NOT IN
        $retval = $filterOperator . " (" . $this->stringifyInFilter($filterValue) . ") ";
      } else {
        // = or !=
        $retval = $filterOperator . " " . $this->db->escape($filterValue) . " ";
      }
      return $retval;
    }
  }

  protected function stringifyInFilter($inFilter)
  {
    if (is_null($inFilter) || empty($inFilter)) {
      return "";
    } else {
      $temp = [];
      if (is_array($inFilter)) {
        foreach ($inFilter as $f) {
          $temp[] = sprintf("%s", $this->db->escape(trim($f)));
        }
      } else {
        $filters = explode(",", trim($inFilter));
        foreach ($filters as $f) {
          $temp[] = sprintf("%s", $this->db->escape(trim($f)));
        }
      }
      return implode(",", $temp);
    }
  }

  public function buildSortSql($tableAlias, $sorts, $ascendingSort = true)
  {
    if (empty($tableAlias) || empty($sorts) || !is_array($sorts)) {
      return '';
    } else {
      $tempSorts = [];
      foreach ($sorts as $sort) {
        $tempSorts[] = "{$tableAlias}.{$sort}";
      }
      $sortDir  = $ascendingSort ? 'ASC' : 'DESC';
      $sortsCsv = implode(",", $tempSorts);
      return " order by {$sortsCsv} {$sortDir} ";
    }
  }

  /**
   * Get single row from a table by criteria
   * @param String $table <p>The table name</p>
   * @param Array  $criteria <p>The criteria. An associative array variable, eg:
   * array('name' => $name, 'title' => $title, 'status' => $status).<br>
   * To control the comparison, you can write it like this: array('name !=' => $name, 'id <' => $id, 'date >' => $date)</p>
   * @param String $fields <p>The fields to retrieve, default is *</p>
   * @return The row in associative array format.
   * @link https://codeigniter.com/user_guide/database/query_builder.html#selecting-data
   */
  public function getRowByCriteria($table, $criteria, $fields = "*")
  {
    if (is_null($table) || empty($table) || is_null($fields) || empty($fields) || empty($criteria) || !is_array($criteria)) {
      return null;
    } else {
      $qry = $this->db->select(trim($fields))->from(trim($table))->where($criteria)->limit(1)->get();
      return $qry->row_array();
    }
  }

  /**
   * Get rows from a table by criteria
   * @param String $table <p>The table name</p>
   * @param Array  $criteria <p>The criteria. An associative array variable, eg:
   * array('name' => $name, 'title' => $title, 'status' => $status).<br>
   * To control the comparison, you can write it like this: array('name !=' => $name, 'id <' => $id, 'date >' => $date)</p>
   * @param String $fields <p>The fields to retrieve, default is *</p>
   * @return The query resource.
   * @link https://codeigniter.com/user_guide/database/query_builder.html#selecting-data
   */
  public function getRowsByCriteria($table, $criteria, $fields = "*")
  {
    if (is_null($table) || empty($table) || is_null($fields) || empty($fields) || empty($criteria) || !is_array($criteria)) {
      return null;
    } else {
      return $this->db->select(trim($fields))->from(trim($table))->where($criteria)->get();
    }
  }

  /**
   * Sanitize argument to be used in a query
   *
   * @param mixed $arg The argument to be sanitized
   * @return mixed Sanitized argument
   */
  public function sanitizeArg($arg)
  {
    return $this->db->escape($arg);
  }

}
