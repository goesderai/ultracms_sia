<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CRUD Interface
 *
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
interface ICrud{
	function insert($data);
	function update($data, $id);
	function delete($id);
}