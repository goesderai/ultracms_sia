<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'BaseDAO.php';
require_once 'ICrud.php';

class DAO_Transaksi extends BaseDAO implements ICrud{

	var $table = 'gen_trx_debet_credit';
	var $v_list_pendapatan = 'v_list_pendapatan';
	var $v_list_pembayaran = 'v_list_pembayaran';
	var $v_students_transaction = 'v_students_transaction';
	var $v_rpt_bayar_uang_pangkal = 'v_rpt_bayar_uang_pangkal';
	var $v_rpt_kas = 'v_rpt_kas';
	var $v_kas_perhari = 'v_kas_perhari';
	var $v_rpt_kas_bulanan = 'v_rpt_kas_bulanan';
	var $v_rpt_kas_tahunan = 'v_rpt_kas_tahunan';
	var $v_rpt_bank = 'v_rpt_bank';
	var $v_rpt_bank_transactions = 'v_rpt_bank_transactions';
	var $v_rpt_piutang = 'v_piutang';
	var $v_rpt_piutang_with_year = 'v_piutang_with_year';
	var $v_nr_aktiva_lancar = 'v_nr_aktiva_lancar';
	var $v_nr_aktiva_tetap = 'v_nr_aktiva_tetap';
	var $v_nr_hutang = 'v_nr_hutang';
	var $v_nr_hut_pendek = 'v_nr_hut_pendek';
	var $v_nr_hut_panjang = 'v_nr_hut_panjang';
	var $v_nr_modal = 'v_nr_modal';
	var $v_lr_pendapatan = 'v_lr_pendapatan';
	var $v_lr_beban = 'v_lr_beban';



	public function __construct(){
		parent::__construct();
		$this->load->model(array('DAO_System_Parameter'));
	}

	public function getListTransaksiPendapatan($filters = array(), $sorts = array(), $firstRowPos = 0, $pageSize = 10){
		$sql = '';
		$sql =	'select * from '.$this->v_list_pendapatan . ' s where 1 = 1 ' ;
		if(!empty($filters)){
			$sql = $sql.
			$this->buildWhereSql('s', $filters) .
					' order by s.created_date desc'. 
					' limit ' . $firstRowPos . ', ' . $pageSize;
		}
		$qry = $this->db->query($sql);
		return $qry->result();
	}

	public function getListCountPendapatan($filters){
		if(is_null($filters) || !is_array($filters)){
			return 0;
		}else{
			$sql =	'select count(*) as nrecs from ' .
			$this->v_list_pendapatan . ' s where 1 = 1 ' .
			$this->buildWhereSql('s', $filters);
			$qry = $this->db->query($sql);
			$row = $qry->row();
			return $row->nrecs;
		}
	}

	public function getListTransaksiPembayaran($filters = array(), $sorts = array(), $firstRowPos = 0, $pageSize = 10){
		$sql = '';
		$sql =	'select * from '.$this->v_list_pembayaran . ' s where 1 = 1 ' ;
		if(!empty($filters)){
			$sql = $sql.
			$this->buildWhereSql('s', $filters).
					' order by s.created_date desc'. 
					' limit ' . $firstRowPos . ', ' . $pageSize;
		}
		$qry = $this->db->query($sql);
		return $qry->result();
	}

	public function getListCountPembayaran($filters){
		if(is_null($filters) || !is_array($filters)){
			return 0;
		}else{
			$sql =	'select count(*) as nrecs from ' .
			$this->v_list_pembayaran . ' s where 1 = 1 ' .
			$this->buildWhereSql('s', $filters);
			$qry = $this->db->query($sql);
			$row = $qry->row();
			return $row->nrecs;
		}
	}

	public function insert($data){
		if(is_null($data) || !is_array($data)){
			return false;
		}else{
			$this->db->insert($this->table, $data);
			$dataInsert = $this->getRow($this->db->insert_id());
			$postingProfile = $this->getPostingProfileByType($dataInsert['transaction_type']);

			$dataLawan = array(
						'ref_transaction_id'  => $dataInsert['id'],
						'transaction_number' => $dataInsert['transaction_number'],
						'transaction_date' => $dataInsert['transaction_date'],
						'remark' => $dataInsert['remark'],
						'transaction_type' => $dataInsert['transaction_type'],
						'financial_year' => $dataInsert['financial_year'],
						'financial_month' => $dataInsert['financial_month'],
						'created_date'=>$dataInsert['created_date'],
						'modified_date'=>$dataInsert['modified_date'],
						'created_by'=>$dataInsert['created_by'],
						'modified_by'=>$dataInsert['modified_by'],

			);
			if($dataInsert['amount_debet'] == 0 && $dataInsert['amount_credit'] != 0){
				$dataLawan['amount_debet'] = $dataInsert['amount_credit'];
				$dataLawan['account_code'] = $postingProfile['account_debet'];
			}else{
				$dataLawan['amount_credit'] = $dataInsert['amount_debet'];
				$dataLawan['account_code'] = $postingProfile['account_credit'];
			}
			if($dataInsert['student_id'] != null){
				$dataLawan['student_id'] = $dataInsert['student_id'];
			}
			if($dataInsert['employee_id'] != null){
				$dataLawan['employee_id'] = $dataInsert['employee_id'];
			}
			$this->db->insert($this->table, $dataLawan);
			return $dataInsert['id'];
		}
	}

	public function insertJurnalWithPiutang($data, $sisa){
		if(is_null($data) || !is_array($data)){
			return false;
		}else{
			$this->db->insert($this->table, $data);
			$dataInsert = $this->getRow($this->db->insert_id());
			$postingProfile = $this->getPostingProfileByType($dataInsert['transaction_type']);

			$dataLawan = array(
						'ref_transaction_id'  => $dataInsert['id'],
						'transaction_number' => $dataInsert['transaction_number'],
						'transaction_date' => $dataInsert['transaction_date'],
						'remark' => $dataInsert['remark'],
						'transaction_type' => $dataInsert['transaction_type'],
						'financial_year' => $dataInsert['financial_year'],
						'financial_month' => $dataInsert['financial_month'],
						'created_date'=>$dataInsert['created_date'],
						'modified_date'=>$dataInsert['modified_date'],
						'created_by'=>$dataInsert['created_by'],
						'modified_by'=>$dataInsert['modified_by'],

			);
			if($dataInsert['amount_debet'] == 0 && $dataInsert['amount_credit'] != 0){
				$dataLawan['amount_debet'] = $dataInsert['amount_credit']- $sisa;
				/* rubah agar hutang bisa di sattle
				 * $dataLawan['amount_debet'] = $dataInsert['amount_credit'] - $sisa;*/
				$dataLawan['account_code'] = $postingProfile['account_debet'];
			}else{
				$dataLawan['amount_credit'] = $dataInsert['amount_debet']- $sisa;
				/* rubah agar hutang bisa di sattle
				 * $dataLawan['amount_credit'] = $dataInsert['amount_debet'] - $sisa;*/
				$dataLawan['account_code'] = $postingProfile['account_credit'];
			}
			if($dataInsert['student_id'] != null){
				$dataLawan['student_id'] = $dataInsert['student_id'];
			}
			if($dataInsert['employee_id'] != null){
				$dataLawan['employee_id'] = $dataInsert['employee_id'];
			}
			/**
			 * insert data lawan dari pendapatan
			 */
			$this->db->insert($this->table, $dataLawan);

			if($sisa != 0){
				/**
				 * jika ada sisa buatkan jurnal piutang
				 * Enter description here ...
				 * @var unknown_type
				 */
				$type = $dataInsert['transaction_type'];
				if($type == 144 || $type == 145 || $type == 150){
					$type = $dataInsert['transaction_type'];
				}else{
					$type = 'LAIN';
				}
				$sysParamKey = 'PIUTANG-'.$type;
				$typTrx = $this->DAO_System_Parameter->getValue($sysParamKey);

				$dataPiutang = array(
						'ref_transaction_id'  => $dataInsert['id'],
						'transaction_number' => $dataInsert['transaction_number'],
						'transaction_date' => $dataInsert['transaction_date'],
						'transaction_type' => $dataInsert['transaction_type'],
						'financial_year' => $dataInsert['financial_year'],
						'financial_month' => $dataInsert['financial_month'],
						'created_date'=>$dataInsert['created_date'],
						'modified_date'=>$dataInsert['modified_date'],
						'created_by'=>$dataInsert['created_by'],
						'modified_by'=>$dataInsert['modified_by'],
					
				);
				if($dataInsert['amount_debet'] == 0 && $dataInsert['amount_credit'] != 0){
					$dataPiutang['amount_debet'] = $sisa;
				}else{
					$dataPiutang['amount_credit'] = $sisa;
				}
				if($dataInsert['student_id'] != null){
					$dataPiutang['student_id'] = $dataInsert['student_id'];
				}
				if($dataInsert['employee_id'] != null){
					$dataPiutang['employee_id'] = $dataInsert['employee_id'];
				}
				$dataPiutang['remark'] = $dataInsert['remark'] .'(Piutang)';
					
				$postingProfile = $this->DAO_Posting_Profile->getPostingProfileByType($typTrx);

				$accountCode = null;
				if($postingProfile != null){
					$dataPiutang['account_code'] = $postingProfile['account_debet'];
				}
				$this->db->insert($this->table, $dataPiutang);
			}
		}
	}

	public function insertSpp($data){
		if(is_null($data) || !is_array($data)){
			return false;
		}else{
			$this->db->insert($this->table, $data);
			$dataInsert = $this->getRow($this->db->insert_id());
			$postingProfile = $this->getPostingProfileByType($this->DAO_System_Parameter->getValue('TRX_TYPE_SPP'));

			$dataLawan = array(
						'ref_transaction_id'  => $dataInsert['id'],
						'transaction_number' => $data['transaction_number'],
						'student_id' => $data['student_id'],
						'amount_debet' => $data['amount_credit'],
						'transaction_date' => $data['transaction_date'],
						'remark' => $data['remark'],
						'transaction_type' => $this->DAO_System_Parameter->getValue('TRX_TYPE_SPP'),
						'financial_year' => $data['financial_year'],
						'financial_month' => $data['financial_month'],
						'account_code' => $postingProfile['account_debet'],
						'created_date'=>$data['created_date'],
						'modified_date'=>$data['modified_date']
			);
			$this->db->insert($this->table, $dataLawan);
		}
	}

	public function update($data, $id){
		if(is_null($data) || !is_array($data) || is_null($id) || empty($id)){
			return false;
		}else{
			$sql = 'select * from ' . $this->table . ' where ref_transaction_id = ?';
			$qry = $this->db->query($sql, array($id));
			$dataLawan = $qry->row_array();
			$dataLawan['remark'] = $data['remark'];
			$this->db->where('id', $id);
			$this->db->update($this->table, $data);
			$this->db->where('id', $dataLawan['id']);
			$this->db->update($this->table, $dataLawan);
		}
	}

	public function delete($id){
		if(is_null($id) || empty($id)){
			return false;
		}else{
			$this->db->where('id', $id);
			$this->db->update($this->table);
		}
	}

	public function getRow($id){
		if(is_null($id) || empty($id) || !is_numeric($id)){
			return null;
		}else{
			return $this->getRowByCriteria($this->table, array('id' => $id));
		}
	}

	public function getListStudentsTransaction($filters = array(), $sorts = array(), $firstRowPos = 0, $pageSize = 10){
		$sql = '';
		$sql =	'select * from '.$this->v_students_transaction . ' s where 1 = 1 ' ;
		if(!empty($filters)){
			$sql = $sql.
			$this->buildWhereSql('s', $filters) .
							' limit ' . $firstRowPos . ', ' . $pageSize;
		}
		$qry = $this->db->query($sql);
		return $qry->result();
	}

	public function getBulanDibayarByStudentId($studentId, $fYear){
		$sql = '';
		$sql = 'SELECT v.financial_month FROM v_students_transaction v WHERE v.transaction_type = (select s.value from m_system_parameter s where s.key = ?) AND v.financial_year = '.$fYear.' AND v.student_id = '.$studentId.' AND v.ref_transaction_id IS NOT NULL';
		$qry = $this->db->query($sql, array("TRX_TYPE_SPP"));
		return $qry->result();
	}

	private function getPostingProfileByType($idType){
		if(is_null($idType) || empty($idType) || !is_numeric($idType)){
			return null;
		}else{
			$sql = 'select * from gen_trx_posting_profile pp where pp.trx_type = ?';
			$qry = $this->db->query($sql, array($idType));
			if($qry->num_rows() > 0){
				return $qry->row_array();
			}else {
				return null;
			}
		}
	}

	public function getRptQuery($tipeTransaksi, $filters = array(), $type){
		$trxTable = '';
		$amount = '';
		if($type == 'D'){
			$trxTable = "v_list_pendapatan";
			$amount = 'amount_credit';
		}else{
			$trxTable = "v_list_pembayaran";
			$amount = 'amount_debet';
		}
		$sql = '';
		if($tipeTransaksi != ''){
			$sql =	'select s.transaction_date as `Tanggal Transaksi`, s.transaction_number as `Tipe Transaksi`, s.nama_tr as `Nama Transaksi`, s.remark as `Keterangan`, s.amount as `Jumlah` from `'.$trxTable. '` s where 1 = 1 and s.transaction_type ='.$tipeTransaksi.' or s.parent_transaction_type ='.$tipeTransaksi.' or s.grand_parent_transaction_type ='.$tipeTransaksi.' ';
		}else {
			$sql =	'select s.transaction_date as `Tanggal Transaksi`, s.transaction_number as `Tipe Transaksi`, s.nama_tr as `Nama Transaksi`, s.remark as `Keterangan`, s.amount as `Jumlah` from `'.$trxTable. '` s where 1 = 1 ' ;
		}
		
		if(!empty($filters)){
			$sql = $sql.$this->buildWhereSql('s', $filters);
		}
		return $sql;
	}

	/*public function getRptTunggakanUangPangkalQuery($year, $class, $grade){
		$sql =	'';
		if($class == 0 && $grade == 0){
		$sql = 'SELECT s.nis as `NIS`, s.name as `Nama Siswa`, g.name AS `Jenjang`, e.name as `Tingkatan`, s.phone_number as `No. Telepon` FROM m_student s INNER JOIN m_edu_classroom e ON s.classroom_id=e.id INNER JOIN m_edu_grade g ON e.grade_id = g.id WHERE s.active = "Y" AND s.id NOT IN (SELECT v.student_id FROM v_rpt_bayar_uang_pangkal v WHERE v.financial_year = '.$year.') order by e.name, s.nis';
		}if($class != 0 && $grade == 0){
		$sql = 'SELECT s.nis as `NIS`, s.name as `Nama Siswa`, g.name AS `Jenjang`, e.name as `Tingkatan`, s.phone_number as `No. Telepon` FROM m_student s INNER JOIN m_edu_classroom e ON s.classroom_id=e.id INNER JOIN m_edu_grade g ON e.grade_id = g.id WHERE s.active = "Y" AND s.id NOT IN (SELECT v.student_id FROM v_rpt_bayar_uang_pangkal v WHERE v.financial_year = '.$year.' and v.classroom_id = '.$class.') and e.id = '.$class.' order by e.name, s.nis';
		}if($class == 0 && $grade != 0){
		$sql = 'SELECT s.nis as `NIS`, s.name as `Nama Siswa`, g.name AS `Jenjang`, e.name as `Tingkatan`, s.phone_number as `No. Telepon` FROM m_student s INNER JOIN m_edu_classroom e ON s.classroom_id=e.id INNER JOIN m_edu_grade g ON e.grade_id = g.id WHERE s.active = "Y" AND s.id NOT IN (SELECT v.student_id FROM v_rpt_bayar_uang_pangkal v WHERE v.financial_year = '.$year.' and v.grade_id = '.$grade.') and g.id = '.$grade.' order by e.name, s.nis';
		}if($class != 0 && $grade != 0){
		$sql = 'SELECT s.nis as `NIS`, s.name as `Nama Siswa`, g.name AS `Jenjang`, e.name as `Tingkatan`, s.phone_number as `No. Telepon` FROM m_student s INNER JOIN m_edu_classroom e ON s.classroom_id=e.id INNER JOIN m_edu_grade g ON e.grade_id = g.id WHERE s.active = "Y" AND s.id NOT IN (SELECT v.student_id FROM v_rpt_bayar_uang_pangkal v WHERE v.financial_year = '.$year.' and v.grade_id = '.$grade.' and v.classroom_id = '.$class.') and e.id = '.$class.' and g.id = '.$grade.' order by e.name, s.nis';
		}
		return $sql;

		}*/

	/*public function getRptTunggakanUangAkademikQuery($year, $class, $grade){
		$sql =	'';
		if($class == 0 && $grade == 0){
		$sql = 'SELECT s.nis as `NIS`, s.name as `Nama Siswa`, g.name AS `Jenjang`, e.name as `Tingkatan`, s.phone_number as `No. Telepon` FROM m_student s INNER JOIN m_edu_classroom e ON s.classroom_id=e.id INNER JOIN m_edu_grade g ON e.grade_id = g.id WHERE s.active = "Y" AND s.id NOT IN (SELECT v.student_id FROM v_rpt_bayar_uang_akademik v WHERE v.tahun_ajaran = '.$year.') order by e.name, s.nis';
		}if($class != 0 && $grade == 0){
		$sql = 'SELECT s.nis as `NIS`, s.name as `Nama Siswa`, g.name AS `Jenjang`, e.name as `Tingkatan`, s.phone_number as `No. Telepon` FROM m_student s INNER JOIN m_edu_classroom e ON s.classroom_id=e.id INNER JOIN m_edu_grade g ON e.grade_id = g.id WHERE s.active = "Y" AND s.id NOT IN (SELECT v.student_id FROM v_rpt_bayar_uang_akademik v WHERE v.tahun_ajaran = '.$year.' and v.classroom_id = '.$class.') and e.id = '.$class.' order by e.name, s.nis';
		}if($class == 0 && $grade != 0){
		$sql = 'SELECT s.nis as `NIS`, s.name as `Nama Siswa`, g.name AS `Jenjang`, e.name as `Tingkatan`, s.phone_number as `No. Telepon` FROM m_student s INNER JOIN m_edu_classroom e ON s.classroom_id=e.id INNER JOIN m_edu_grade g ON e.grade_id = g.id WHERE s.active = "Y" AND s.id NOT IN (SELECT v.student_id FROM v_rpt_bayar_uang_akademik v WHERE v.tahun_ajaran = '.$year.' and v.grade_id = '.$grade.') and g.id = '.$grade.' order by e.name, s.nis';
		}if($class != 0 && $grade != 0){
		$sql = 'SELECT s.nis as `NIS`, s.name as `Nama Siswa`, g.name AS `Jenjang`, e.name as `Tingkatan`, s.phone_number as `No. Telepon` FROM m_student s INNER JOIN m_edu_classroom e ON s.classroom_id=e.id INNER JOIN m_edu_grade g ON e.grade_id = g.id WHERE s.active = "Y" AND s.id NOT IN (SELECT v.student_id FROM v_rpt_bayar_uang_akademik v WHERE v.tahun_ajaran = '.$year.' and v.grade_id = '.$grade.' and v.classroom_id = '.$class.') and e.id = '.$class.' and g.id = '.$grade.' order by e.name, s.nis';
		}
		return $sql;

		}*/

	public function getRptKasHarian($filters){
		$sql =	'select k.id,k.transaction_number, k.transaction_type, k.akun , k.debet , k.kredit, k.penarikan , k.setoran , k.sisa , k.keterangan From '. $this->v_rpt_kas. ' k where 1 = 1 ' ;
		if(!empty($filters)){
			$sql = $sql.$this->buildWhereSql('k', $filters). ' order by k.`id`';
		}
		return $sql;
	}

	public function getRptKasBulanan($filters){
		$sql =	'select k.transaction_date as tanggal, k.debet as `debet`, k.kredit as `kredit`, k.penarikan , k.setoran , k.sisa as `sisa` From '. $this->v_rpt_kas_bulanan. ' k where 1 = 1 ' ;
		if(!empty($filters)){
			$sql = $sql.$this->buildWhereSql('k', $filters).' order by k.transaction_date';
		}
		return $sql;
	}
	public function getRptBankTransactions($filters){
		$sql =	'select b.transaction_date as `Tanggal Transaksi`, b.remark as `Keterangan`, b.amount_debet as `Penyetoran`, b.amount_credit as `Penarikan`, (b.amount_debet-b.amount_credit) as Sisa From '. $this->v_rpt_bank_transactions. ' b where 1 = 1 ' ;
		if(!empty($filters)){
			$sql = $sql.$this->buildWhereSql('b', $filters).' order by b.transaction_date';
		}
		return $sql;
	}
	public function getRptKasTahunan($filters){
		$sql =	'select k.`financial_month` as bulan, k.debet as `debet`, k.kredit as `kredit`, k.penarikan , k.setoran , k.sisa as `sisa` From '. $this->v_rpt_kas_tahunan. ' k where 1 = 1 ' ;
		if(!empty($filters)){
			$sql = $sql.$this->buildWhereSql('k', $filters). ' order by k.`financial_month`';
		}
		return $sql;
	}

	public function getKas($filters){
		$sql =	'select SUM(k.debet) as debet, SUM(k.kredit) as kredit, (SUM(k.debet)-SUM(k.kredit)) as sisa FROM '. $this->v_kas_perhari. ' k where 1 = 1 ' ;
		if(!empty($filters)){
			$sql = $sql.$this->buildWhereSql('k', $filters);
		}
		return $sql;
	}


	public function getRptPiutang($filters){
		$sql =	'select COALESCE(k.employee_name, k.student_name) as Nama, k.class_name as Kelas, k.remark, k.piutang as Piutang, k.dibayar as Dibayar, k.sisa as Sisa FROM '. $this->v_rpt_piutang_with_year. ' k where 1 = 1 and k.sisa <> 0 ' ;
		if(!empty($filters)){
			$sql = $sql.$this->buildWhereSql('k', $filters);
		}
		$sql = $sql.' order by k.class_name';
		return $sql;
	}
	
	public function getRptPiutangTanpaTahun($filters){
		$sql =	'select COALESCE(k.employee_name, k.student_name) as Nama, k.class_name as Kelas, k.remark, k.piutang as Piutang, k.dibayar as Dibayar, k.sisa as Sisa FROM '. $this->v_rpt_piutang. ' k where 1 = 1 and k.sisa <> 0 ' ;
		if(!empty($filters)){
			$sql = $sql.$this->buildWhereSql('k', $filters);
		}
		$sql = $sql.' order by k.class_name';
		return $sql;
	}

	public function updatePendapatan($data, $transactionNumber){
		$sql = 'select * from '.$this->table.' t where 1=1 and t.transaction_number = ? and t.ref_transaction_id is null ';

		$qry = $this->db->query($sql, array($transactionNumber));
		$trDebetCredit = $qry->result();

		foreach ($trDebetCredit as $row){
			$row->remark = $data['remark'];
			$row->modified_date = new DateTime();
			$this->db->where('id', $row->id);
			$this->db->update($this->table, $row);
		}
	}

	public function insertJurnalKoreksi($transactionNumber, $newTransactionNumber){
		/*$data = $this->getRowByCriteria('gen_trx_pendapatan', array('id' => $id));*/

		$sql = 'select * from '.$this->table.' t where 1=1 and t.transaction_number = ? order by t.ref_transaction_id asc ';

		$qry = $this->db->query($sql, array($transactionNumber));
		$trDebetCredit = $qry->result();
		$mainTr = null;
		foreach ($trDebetCredit as $newData){
			$newData->id= null;
			$newData->transaction_number = $newTransactionNumber;
			if($newData->amount_debet != 0 && $newData->amount_credit == 0){
				$newData->amount_credit = $newData->amount_debet;
				$newData->amount_debet = 0;
			}else if($newData->amount_debet == 0 && $newData->amount_credit != 0){
				$newData->amount_debet = $newData->amount_credit;
				$newData->amount_credit = 0;
			}
			$remark = $newData->remark;
			$newData->remark = $remark.' (Koreksi '.$transactionNumber.')';
			$newData->ref_transaction_number = $transactionNumber;
			if($newData->ref_transaction_id == null){
				$this->db->insert($this->table, $newData);
				$dataInsert = $this->getRow($this->db->insert_id());
				$mainTr = $dataInsert['id'];
			}else{
				$newData->ref_transaction_id = $mainTr;
				$this->db->insert($this->table, $newData);
			}
		}
	}

	public function getAktivaLancar($filters){
		$sql =	'select al.`name` as nama , sum(al.`saldo`) as saldo From '. $this->v_nr_aktiva_lancar. ' al where 1 = 1 ' ;
		if(!empty($filters)){
			$sql = $sql.$this->buildWhereSql('al', $filters). ' group by al.`account_id` order by al.`account_code`';
		}
		return $sql;
	}

	public function getAktivaTetap($filters){
		$sql =	'select al.`name` as nama , sum(al.`saldo`) as saldo From '. $this->v_nr_aktiva_tetap. ' al where 1 = 1 ' ;
		if(!empty($filters)){
			$sql = $sql.$this->buildWhereSql('al', $filters). ' group by al.`account_id` order by al.`account_code`';
		}
		return $sql;
	}

	public function getHutang($filters){
		$sql =	'select al.`name` as nama , sum(al.`saldo`) as saldo From '. $this->v_nr_hutang. ' al where 1 = 1 ' ;
		if(!empty($filters)){
			$sql = $sql.$this->buildWhereSql('al', $filters). ' group by al.`account_id` order by al.`account_code`';
		}
		return $sql;
	}

	public function getHutangPendek($filters){
		$sql =	'select al.`name` as nama , sum(al.`saldo`) as saldo From '. $this->v_nr_hut_pendek. ' al where 1 = 1 ' ;
		if(!empty($filters)){
			$sql = $sql.$this->buildWhereSql('al', $filters). ' group by al.`account_id` order by al.`account_code`';
		}
		return $sql;
	}

	public function getHutangPanjang($filters){
		$sql =	'select al.`name` as nama , sum(al.`saldo`) as saldo From '. $this->v_nr_hut_panjang. ' al where 1 = 1 ' ;
		if(!empty($filters)){
			$sql = $sql.$this->buildWhereSql('al', $filters). ' group by al.`account_id` order by al.`account_code`';
		}
		return $sql;
	}
	public function getModal($filters){
		$sql =	'select al.`name` as nama , sum(al.`saldo`) as saldo From '. $this->v_nr_modal. ' al where 1 = 1 ' ;
		if(!empty($filters)){
			$sql = $sql.$this->buildWhereSql('al', $filters). ' group by al.`account_id` order by al.`account_code`';
		}
		return $sql;
	}

	public function getPendapatan($filters){
		$sql =	'select al.`name` as nama , sum(al.`saldo`) as saldo From '. $this->v_lr_pendapatan. ' al where 1 = 1 ' ;
		if(!empty($filters)){
			$sql = $sql.$this->buildWhereSql('al', $filters). ' group by al.`account_id` order by al.`account_code`';
		}
		return $sql;
	}

	public function getBeban($filters){
		$sql =	'select al.`name` as nama , sum(al.`saldo`) as saldo From '. $this->v_lr_beban. ' al where 1 = 1 ' ;
		if(!empty($filters)){
			$sql = $sql.$this->buildWhereSql('al', $filters). ' group by al.`account_id` order by al.`account_code`';
		}
		return $sql;
	}

	public function getRsRptPengeluaranBulanan($year){
    if (empty($year) || !is_numeric($year)) return null;
	  $retval = array('header' => array(), 'body' => array());

	  //Header data
	  $headerSql = "select ttyp.id, ttyp.nama 
                  from v_list_pembayaran as vlp 
                  join gen_md_trx_type ttyp on vlp.grand_parent_transaction_type = ttyp.id 
                  where vlp.financial_year=? 
                  group by ttyp.id, ttyp.nama 
                  order by ttyp.nama";
    $headerQry = $this->db->query($headerSql, array($year));

    //Body data
    $bodySql = "select 
                vlp.transaction_date 
                ,sum(vlp.amount) as amount 
                ,ttyp.id 
                ,ttyp.nama 
                from v_list_pembayaran as vlp 
                join gen_md_trx_type ttyp on vlp.grand_parent_transaction_type = ttyp.id 
                where vlp.financial_year=? 
                group by vlp.financial_month, ttyp.id, ttyp.nama 
                order by vlp.financial_month";
    $bodyQry = $this->db->query($bodySql, array($year));

    $retval['header'] = $headerQry->result();
    $retval['body'] = $bodyQry->result();
    return $retval;
  }

  public function getRsRptPengeluaranHarian($year, $month){
    if (empty($month) || !is_numeric($month) || empty($year) || !is_numeric($year)) return null;
    $retval = array('header' => array(), 'body' => array());

    //Header data
    $headerSql = "select ttyp.id, ttyp.nama 
                  from v_list_pembayaran as vlp 
                  join gen_md_trx_type ttyp on vlp.grand_parent_transaction_type = ttyp.id 
                  where vlp.financial_year=? 
                  and vlp.financial_month=? 
                  group by ttyp.id, ttyp.nama 
                  order by ttyp.nama";
    $headerQry = $this->db->query($headerSql, array($year, $month));

    //Body data
    $bodySql = "select 
                vlp.transaction_date 
                ,sum(vlp.amount) as amount 
                ,ttyp.id 
                ,ttyp.nama 
                from v_list_pembayaran as vlp 
                join gen_md_trx_type ttyp on vlp.grand_parent_transaction_type = ttyp.id 
                where vlp.financial_year=? 
                and vlp.financial_month=? 
                group by vlp.transaction_date, ttyp.id, ttyp.nama 
                order by vlp.transaction_date";
    $bodyQry = $this->db->query($bodySql, array($year, $month));

    $retval['header'] = $headerQry->result();
    $retval['body'] = $bodyQry->result();
    return $retval;
  }
  
  
  public function getSisaHutangByStudentIdAndTransactionType($studentId, $transactionType){
  	$sysParamKey = 'PEMBAYARAN-PIUTANG-';
  	if($transactionType == 352 || $transactionType == 353 || $transactionType == 358){
  		$sysParamKey = $sysParamKey.$transactionType;
  		
  		$typeTrxPiutang = $this->DAO_System_Parameter->getValue($sysParamKey);
  		
	  	$postingProfilePembayaranPiutang = $this->DAO_Posting_Profile->getPostingProfileByType($transactionType);
	  	$accountCodeFilter = null;
	  	if($postingProfilePembayaranPiutang != null){
	  		$accountCodeFilter = $postingProfilePembayaranPiutang['account_credit'];
	  	}
	  	
	  	$bodySql = "SELECT SUM(`amount_debet`) - SUM(`amount_credit`) AS sisa_hutang 
	  				FROM `gen_trx_debet_credit` 
	  				WHERE transaction_type IN (?,?) 
	  				AND `student_id`=? 
	  				AND `account_code`=? 
	  				GROUP BY `student_id`";
	  	/*var_dump($bodySql);
	  	var_dump($transactionType, $typeTrxPiutang, $studentId, $accountCodeFilter);
	  	die();*/
	  	$bodyQry = $this->db->query($bodySql, array($transactionType, $typeTrxPiutang,$studentId, $accountCodeFilter));
  		return $bodyQry->result();
  	}
  	return null;
  }
  
}
