<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Invoice header data object
 * 
 * @author <a href="mailto:goesderai@gmail.com">Goesde Rai</a>
 */
 
class InvoiceHeader {
    
    private $companyAddress;
    private $companyEmail;
    private $companyPhone;
    private $number;
    private $date;
    private $sub;
    private $trx;
    private $details = array();
    private $target; // S (student) or E (employee)
    private $targetName;
    private $targetIdNumber;
    private $copy = false;
    
    public function getCompanyAddress()
    {
        return $this->companyAddress;
    }

    public function setCompanyAddress($companyAddress)
    {
        $this->companyAddress = $companyAddress;
    }

    public function getCompanyEmail()
    {
        return $this->companyEmail;
    }

    public function setCompanyEmail($companyEmail)
    {
        $this->companyEmail = $companyEmail;
    }

    public function getCompanyPhone()
    {
        return $this->companyPhone;
    }

    public function setCompanyPhone($companyPhone)
    {
        $this->companyPhone = $companyPhone;
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function setNumber($number)
    {
        $this->number = $number;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date)
    {
        $this->date = $date;
    }

    public function getSub()
    {
        return $this->sub;
    }

    public function setSub($sub)
    {
        $this->sub = $sub;
    }

    public function getTrx()
    {
        return $this->trx;
    }

    public function setTrx($trx)
    {
        $this->trx = $trx;
    }

    public function getDetails()
    {
        return $this->details;
    }

    public function setDetails($details)
    {
        $this->details = $details;
    }

    public function getTarget()
    {
        return $this->target;
    }

    public function setTarget($target)
    {
        $this->target = $target;
    }

    public function getTargetName()
    {
        return $this->targetName;
    }

    public function setTargetName($targetName)
    {
        $this->targetName = $targetName;
    }

    public function getTargetIdNumber()
    {
        return $this->targetIdNumber;
    }

    public function setTargetIdNumber($targetIdNumber)
    {
        $this->targetIdNumber = $targetIdNumber;
    }

    public function isCopy()
    {
        return $this->copy;
    }

    public function setCopy($copy)
    {
        $this->copy = $copy;
    }

    
}