module.exports = function(grunt) {
	grunt.initConfig({
		babel: {
			options: {
				sourceMap: false
			},
			dist: {
				files: [{
					expand: true,
					cwd: 'assets/js/src',
					src: '*.js',
					dest: 'assets/js/compiled/',
					ext: '.compiled.js'
				}]
			}
		},
		uglify: {
			options : {
				sourceMap : false
			},
			dist: {
				files: [{
					expand: true,
					cwd: 'assets/js/compiled',
					src : '*.compiled.js',
					dest : 'assets/js',
					ext: '.min.js'
				}]
			}
		},
		less: {
			all: {
				files: [{
					expand: true,
					cwd: 'assets/themes/sb-admin/css/src',
					src: '*.less',
					dest: 'assets/themes/sb-admin/css/compiled',
					ext: '.css'
				}]
			}
		},
		cssmin: {
			target: {
				files: [{
				  expand: true,
				  cwd: 'assets/themes/sb-admin/css/compiled',
				  src: '*.css',
				  dest: 'assets/themes/sb-admin/css',
				  ext: '.min.css'
				}]
			}
		},
		watch: {
			files: ['assets/js/src/*.js'],
			tasks: ['babel', 'uglify']
		}
	});
	grunt.loadNpmTasks('grunt-babel');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.registerTask('default', ['babel', 'uglify', 'less', 'cssmin']);
	grunt.registerTask('compile-js', ['babel', 'uglify']);
	grunt.registerTask('compile-css', ['less', 'cssmin']);
};