-- modify table m_student
alter table `m_student` add column `status` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT 'Status siswa (calon, siswa, alumnus)';

-- modify table m_lov
alter table `m_lov` add column `is_predefined` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Predefined data is not editable and deletable';
alter table `m_lov` add column `created_by` int(11) DEFAULT NULL;
alter table `m_lov` add column `created_date` datetime DEFAULT NULL;
alter table `m_lov` add column `modified_by` int(11) DEFAULT NULL;
alter table `m_lov` add column `modified_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP;

-- insert new lov
insert into `m_lov` (`name`, `is_predefined`) values ('STATUS_SISWA', 1);